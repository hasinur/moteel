source 'https://rubygems.org'

gem 'rails', '3.2.13'
gem 'jquery-rails', '2.1.4'
gem 'mysql2'
gem 'sass-rails'
gem "prototype-rails"

gem 'remotipart', '~> 1.0'

gem 'execjs'
gem 'therubyracer', '~> 0.11.4'

gem 'i18n-timezones'
group :production do
  gem 'whenever', :require => false
end

group :assets do
  gem 'coffee-rails', '~> 3.2.1'
end
gem 'uglifier', '>= 1.0.3'

gem "google_visualr", "~> 2.1.0"
gem 'globalize3', "0.2.0"
gem "declarative_authorization"
gem "geokit"
gem 'css_parser'
gem 'json'
gem 'daemons'
gem "mongoid", "~> 3.0.0"
gem 'bson_ext'
gem 'delayed_job_active_record'

group :development, :test do
  gem 'pry', '~> 0.9.8'
  gem 'pry-remote', '~> 0.1.0'
  gem 'errship', '~> 2.2.0'
  gem 'capistrano'
  gem 'capistrano-ext'
  gem 'rb-readline', '~> 0.4.2'
  gem 'cucumber-rails', :require => false
  #gem 'cucumber-rails-training-wheels'
  gem "database_cleaner", "~> 1.0.1"
  gem 'selenium-webdriver'
  gem "factory_girl_rails"
  #gem "capybara-webkit"#, :git => 'git://github.com/thoughtbot/capybara-webkit.git'
  gem "rspec-rails", "~> 2.0"
  gem 'ffaker'
  gem 'simplecov'
  gem 'colored'
  gem 'shoulda'
  gem 'shoulda-matchers'
  gem 'mocha', require: false
  gem 'rvm-capistrano'
  gem 'rails_best_practices'
  gem 'bullet'
  #gem 'rack-mini-profiler'
end
group :test do
  gem 'guard-rspec'
  gem 'ruby-prof'
  gem 'test-unit'
  gem 'minitest-reporters'
  gem 'ruby-prof'
  gem 'test-unit'
  gem 'chronic'
end


gem 'awesome_print'

gem "letter_opener", :group => :development
gem 'quiet_assets'
gem "client_cache_update", :path => './lib/client_cache_update'
gem "dashboard", :path => './lib/dashboard'
gem "breadcrumbs_on_rails"
gem "awesome_nested_set"
gem "paperclip", "~> 3.0"
#gem "mini_magick"
gem "galetahub-simple_captcha", :require => "simple_captcha"

gem 'capistrano'
gem 'capistrano-ext'
gem 'numbers_and_words' #added to convert number to human readable format

#gem "prawn", '1.0.0.rc1'
gem 'prawn', git: 'git://github.com/prawnpdf/prawn', branch: 'master'
gem 'prawn_rails'
gem 'wicked'
gem "simple_form", ">= 2.0.4"
gem 'twitter-bootstrap-rails', " 2.1.4"
gem 'bootstrap-wysihtml5-rails'
gem 'savon', '=1.2.0'
gem 'kaminari'

gem "devise"
gem "cancan"
gem 'rails_admin'
gem 'ledermann-rails-settings', "~> 2.0.2", :require => 'rails-settings'
gem 'parsi-date' #needed for shomoos sync

gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'
gem "acts_as_paranoid", "~>0.4.0"
gem "grape"
gem 'grape-swagger'
gem 'angular-rails'

gem 'activeadmin'
gem 'activeadmin-translate'

gem 'state_machine'
gem 'ransack'
gem 'query_report', '0.1.2'
#gem 'query_report', :path => '/home/jitu/projects/query_report'
gem 'gruff'
gem 'rmagick', :require => false

gem "gmap_coordinates_picker"
gem "geocoder"
gem 'request_store'

gem 'mercury-rails'
gem 'newrelic_rpm'
gem "font-awesome-rails"
gem 'fog'
#gem 'capistrano-puma', require: false
