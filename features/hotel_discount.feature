Feature: Create a new discount
  In order to create a new hotel discount
  As a hotel owner
  I need to create a discount from hotel admin dashboard

  Background:
    Given I have hotel name "river crown"

  @javascript
  Scenario: Create a discount from hotel admin dashboard
    When I go to new discount page
    Then I fill in "hotel_discount title" with "Summer Offer" in en
    And I fill in "hotel_discount title" with "Summer Offer Ar" in ar
    And I fill "SUM-01"  in the "hotel_discount_code" textbox
    And I fill "50"  in the "hotel_discount_discount_value" textbox
    And I check "hotel_discount_fixed"
    Then I press "Create Discount"
    And I wait for 2 seconds
    And Page should have "Discount was successfully created." text
