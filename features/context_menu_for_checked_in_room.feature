Feature: Appearance of Context Menu
  In order to manage Checked In Room
  As a Front Desk Manager
  I should see context menu

  Background:
    Given  I have hotel name "river crown"
    And I have some countries cities and price unit
    And I have a cash counter
    And I have some service
    And I have 2 room types "standard", "delux"
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 1 rooms already checked-in "std1"


  @javascript
  Scenario: On Check In Room
    When I am in room view
    And  I wait for 5 seconds
    Then I right click on room "std1" in room view
    Then I click on "Add Charge" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Charge Room" pop up open
    And I wait for 2 seconds
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Payment" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Select cash counter" pop up open
    And I wait for 2 seconds
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Room Transfer" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Room Transfer" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Check Out" link with url in that context menu
    Then I should see A new "CheckIn-CheckIn ID" Tab open
    And I wait for 2 seconds
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Edit" link with url in that context menu
    Then I should see A new "CheckIn-CheckIn ID" Tab open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Uncheck In" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Cancel Check In" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Assign Employee" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Employee" pop up open
    And I wait for 2 seconds
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Assign Markers" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Markers" pop up open
    And I wait for 2 seconds
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Mark as 'Dirty'" link in that context menu
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I should see A new "Mark as 'Clean'" link appear this context menu
    Then I click on "Mark as 'Clean'" link in that context menu
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I should see A new "Mark as 'Dirty'" link appear this context menu



