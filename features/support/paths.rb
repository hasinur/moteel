module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition in web_steps.rb
  #
  def path_to(page_name)
    case page_name
      when /the home\s?page/
        root_path
      when /the hotel page/
        hotel_path(@hotel)
      when /the front desk/
        front_desk_path
      when /the new branch page/
        new_hotel_admin_hotel_path
      when /the new room type page/
        new_hotel_admin_room_type_path
      when /the new room page/
        new_hotel_admin_room_path
      when /the new employee wizard page/
        new_hotel_admin_employee_path
      when /new department page/
        new_hotel_admin_department_path
      when /new marker page/
        new_hotel_admin_marker_path
      when /new cash counter page/
        new_hotel_admin_cash_counter_path
      when /new discount page/
        new_hotel_admin_hotel_discount_path
      when /new service page/
        new_hotel_admin_service_path
      when /the hotel admin page/
        hotel_admin_dashboard_path
      else
        begin
          page_name =~ /the (.*) page/
          path_components = $1.split(/\s+/)
          self.send(path_components.push('path').join('_').to_sym)
        rescue Object => e
          raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                    "Now, go and add a mapping in #{__FILE__}"
        end
    end
  end
end

World(NavigationHelpers)
