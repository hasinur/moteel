Feature: Assign Template

  Background:
    Given I have hotel name "river crown"
    And I have a layout with 3 themes

  @javascript
  Scenario:  assign template
    When I go to the hotel admin page
    And I click on "Assign Template"
    Then I should see "Select Branch" within ".active-step"
    Then I select in "none_selected_branch_id" with "river crown" from drop down list
    And I click on "Next"
    Then I should see "Select Template" within ".active-step"
    Then I should see 1 ".inputs .layoutTheme" under "#main_content"
    Then I choose "Layout1"
    And I click on "Next"
    Then I should see "Select Theme" within ".active-step"
    Then I should see 3 ".inputs .layoutTheme" under "#main_content"
    And I click on "Cancel"
    Then I should see "Select Template" within ".active-step"
    Then I choose "Layout1"
    And I click on "Next"
    Then I should see "Select Theme" within ".active-step"
    Then I choose "Theme1"
    And I click on "Next"
    Then I should see the hotel page with selected layout
#
#
#
#
