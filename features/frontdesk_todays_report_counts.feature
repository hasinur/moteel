Feature: Room Transfer from a checkin
  In order to transfer a room from a checkin
  As a Front Desk Manager
  I need to transfer a checked in room to a available room

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 50 SAR
    And room price for "delux" room type is 100 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have 2 rooms already checked-in "std1, std2" from "today" to "tomorrow"
    And I have a cash counter "My cash counter"

  @javascript
  Scenario: Checkout list count
    Given I am in room view
    Then I should see "2" within "#todays_checkin_link_count"
    When I right click on room "std1" in room view
    Then I click on "Check Out" link in that context menu
    And I press "Payment" in the current tab
    And I select in "current cash counter" with "My cash counter" from drop down list
    And I press "Submit"
    And I fill in "hotel service payment amount" with "150"
    And I press "Save"
    And I close the popup
    And I press "Confirm Checkout"
    And I press "No"
    #And I wait for 12 seconds
    #And I am in room view
    #Then I should see "0" within "#todays_checkin_link_count"
    #Then I should see "2" within "#todays_checkout_link_count"
