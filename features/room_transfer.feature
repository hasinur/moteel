Feature: Room Transfer from a checkin
  In order to transfer a room from a checkin
  As a Front Desk Manager
  I need to transfer a checked in room to a available room

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 50 SAR
    And room price for "delux" room type is 100 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
      And I have a "fixed" discount "summer" is available 10 SAR
    And I have 2 rooms already checked-in "std1, std2" from "1 days ago" to "tomorrow" with discount "summer"
    And I have 3 existing "adult" guest "Mr. Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"
    And Logged in user's name "Md. Kamal Hossain"

  @javascript
  Scenario: Transfer a checked in room after one day
    Given I am in room view
    Then I right click on room "std2" in room view
    Then I click on "Room Transfer" link in that context menu
    And I select "delux" room type from room transfer popup
    And I select "dlx 2" room from room transfer popup
    Then I press "Continue"
    Then I should see "std 2 *old" within ".rt-old-room .rt-name"
    Then I should see "2" within ".rt-old-room .rt-night-count"
    Then I should see "1" within ".rt-old-room .rt-night-stay"
    Then I should see "90" within ".rt-old-room .rt-last-price"
    Then I should see the "allocated_room[price]" is set "50.0" within ".rt-old-room .rt-price"
    Then I should see "dlx 2 *new" within ".rt-new-room .rt-name"
    Then I should see "1" within ".rt-new-room .rt-night-count"
    Then I should see "-" within ".rt-new-room .rt-night-stay"
    Then I should see "-" within ".rt-new-room .rt-last-price"
    Then I should see the "change_room[price]" is set "90.0" within ".rt-old-room .rt-price"
    Then I press "Update"
    Then I should see notification "Operation completed successfully"
    Then I am in room view
    And I should see room "std2" as "available" in the room view
    And I should see room "dlx2" as "checked_in" in the room view
    And I wait for 3 seconds
    Then I right click on room "dlx2" in room view
    Then I click on "Edit" link in that context menu
    And I wait for 3 seconds
    Then I should see "Checked In- {{@checkin.id}}"
    Then I should see "dlx 2" within "room" from the 2nd row in check in


  @javascript
  Scenario: Transfer a checked in room
    Given I am in room view
    Then I right click on room "std2" in room view
    Then I click on "Room Transfer" link in that context menu
    And I select "delux" room type from room transfer popup
    And I wait for 3 seconds
    And I select "dlx 2" room from room transfer popup
    Then I click on "Continue" button
    And I wait for 3 seconds
    Then I click on "Update" button
    And I wait for 3 seconds
    Then I should see notification "Operation completed successfully"


