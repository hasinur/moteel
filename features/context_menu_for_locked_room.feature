Feature: Appearance of Context Menu for maintenance room
  In order to see available room information
  As a Hotel Owner
  I need to be manage available room

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And I have 2 rooms in "standard" room type "std1", "std2"
    And "std1" room already under locked

  @javascript
  Scenario: On maintenance Room
#    When I am in room view
#    And  I wait for 3 seconds
#    Then I should see "Room View"
#    Then I should see 1 room block "maintenance"
#    Then I should see 1 room block "available"
#    When I right click on room "std1" in room view
#    Then I click on "Remove from 'maintenance'" link with url in that context menu
#    And I wait for 4 seconds
#    Then I should see A new "Remove from 'maintenance'" pop up open
#    And I wait for 2 seconds
#
#    Given I am in room view
#    Then I right click on room "std1" in room view
#    Then I click on "Mark as Locked" link with url in that context menu
#    And I wait for 4 seconds
#    Then I should see A new "Mark as Locked" pop up open
#    And I wait for 2 seconds

    Given I am in room view
    Then I right click on available room block
    Then I click on "Mark as 'Maintenance'" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Mark as 'Maintenance'" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on available room block
    Then I click on "Unlocked" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Unlocked" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Assign Employee" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Employee" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Assign Markers" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Markers" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Mark as 'Dirty'" link in that context menu
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I should see A new "Mark as 'Clean'" link appear this context menu
    Then I click on "Mark as 'Clean'" link in that context menu
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I should see A new "Mark as 'Dirty'" link appear this context menu

