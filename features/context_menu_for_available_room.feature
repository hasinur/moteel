Feature: Appearance of Context Menu
  In order to see available room information
  As a Hotel Owner
  I need to be manage available room

  Background:
    Given  I am logged in as "hotel owner"
    And I have some countries cities and price unit
    And I have some rooms in my hotel

  @javascript
  Scenario: On available Room
    Given I am in room view
    Then I right click on available room block
    Then I click on "Check In" link with url in that context menu
    Then I should see A new "check In" Tab open
    Given I am in room view
    Then I right click on available room block
    Then I click on "Reservation" link with url in that context menu
    Then I should see A new "Reservation" Tab open
    Given I am in room view
    Then I right click on available room block
    Then I click on "Assign Employee" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Employee" pop up open
    Given I am in room view
    Then I right click on available room block
    Then I click on "Mark as 'Maintenance'" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Mark as Maintenance" pop up open
    Given I am in room view
    Then I right click on available room block
    Then I click on "Mark as Locked" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Mark as Locked" pop up open
    Given I am in room view
    Then I right click on available room block
    Then I click on "Assign Markers" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Markers" pop up open
    Given I am in room view
    Then I right click on available room block
    Then I click on "Mark as 'Dirty'" link in that context menu
    Given I am in room view
    Then I right click on available room block
    Then I should see A new "Mark as 'Clean'" link appear this context menu
    Then I click on "Mark as 'Clean'" link in that context menu
    Given I am in room view
    Then I right click on available room block
    Then I should see A new "Mark as 'Dirty'" link appear this context menu

