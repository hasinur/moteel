Feature: Appearance of Context Menu
  In order to manage Checked In Room
  As a Front Desk Manager
  I should see context menu

  Background:
    Given  I have hotel name "river crown"
    And I have some countries cities and price unit
    And I have a cash counter
    And I have some service
    And I have 2 room types "standard", "delux"
    And I have 2 rooms in "standard" room type "std1", "std2"


  @javascript
  Scenario: On Check In Room
    When I am in room view
    And I check  multiselect checkbox for room "std1"
    And I check  multiselect checkbox for room "std2"
    Then I right click on room "std1" in room view
    Then I click on "Check In" link with url in that multi select context menu
    And I wait for 2 seconds
    Then I should see A new "check In" Tab open
    And I wait for 5 seconds
    Given I am in room view
    And I check  multiselect checkbox for room "std1"
    And I check  multiselect checkbox for room "std2"
    Then I right click on room "std2" in room view
    Then I click on "Reservation" link with url in that multi select context menu
    And I wait for 2 seconds
    Then I should see A new "Reservation" Tab open
    And I wait for 5 seconds
  #



