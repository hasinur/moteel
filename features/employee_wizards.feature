Feature: Create a new employee
  In order to create a new employee
  As a hotel owner
  I need to create an employee from wizard

  Background:
    Given I have hotel name "river crown"
    And have permissions setup

  @javascript
  Scenario: Create an employee from employee wizard
    When I go to the new employee wizard page
    Then I fill in "user_profile_attributes first_name" with "Zakir" in en
    Then I fill in "user_profile_attributes first_name" with "Zakir Ar" in ar
    Then I fill in "user_profile_attributes last_name" with "Khan" in en
    Then I fill in "user_profile_attributes last_name" with "Khan Ar" in ar
    Then I fill "zakir@gmail.com"  in the "user_email" textbox
    Then I fill "123456"  in the "user_password" textbox
    Then I fill "123456"  in the "user_password_confirmation" textbox
    And I press "Next"
    When I select in "none selected branch id" with "river crown" from drop down list
    And I press "Next"
    And I press "Next"
    And I wait for 2 seconds
    And I should see "Employee"

