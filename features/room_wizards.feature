Feature: Create a new room type
  In order to create a room type
  As a hotel owner
  I need to create a hotel with some room type

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
  @javascript
  Scenario: Create Hotel with some Rooms from hotel wizard
    When I go to the new room page
    When I select in "none selected branch id" with "river crown" from drop down list
    Then I press "Next"
    And I wait for 3 seconds
    When I select in "none selected room type id" with "delux" from drop down list
    Then I press "Next"
    Then I fill "DLX"  in the "room_prefix" textbox
    Then I fill "101-105"  in the "room_room_number_comma" textbox
    Then I press "Next"
    And I wait for 3 seconds
    Then I attach a image "default_logo_hotel_1.jpg" to "room_images_attributes_index_to_replace_with_js_image"
    Then I press "Next"
    Then I should see "Room Additional Fields"
    Then I press "Skip All"
    And I wait for 3 seconds
    And I should see "Room"


#