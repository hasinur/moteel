Feature: Partial Check out
  In order to partial check out
  As a Hotel Owner
  I need to be manage CheckIn information

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 100 SAR
    And room price for "delux" room type is 200 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have 2 rooms already checked-in "std1, std2"
    And I have a "fixed" discount "summer" is available 10 SAR
    And I have 3 existing "adult" guest "Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"


  @javascript
  Scenario: Partial check out process from room view menu
    When I am in room view
    And I wait for 3 seconds
    Then I should see "Room View"
    When I right click on room "std2" in room view
    Then I click on "Partial checkout" link in that context menu
    Then I select room "std2" in room list of partial checkout popup
    Then I press "Continue"
    Then I press "Confirm"
    And I wait for 3 seconds
    And I should see "Partially checked out"

