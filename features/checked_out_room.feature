Feature: Checked out room context menu
  In order to see checked out room information
  As a Hotel Owner
  I need to manage checked out room

  Background:
    Given  I am logged in as "hotel owner"
    And I have some countries cities and price unit
    And I have some rooms in my hotel
    And I am in room view
    And I have 2 rooms checked in
    And I have the first rooms checked out of that 2 rooms
  @javascript
  Scenario: Check checked out room block context menu items from room view
    Given I am in room view
#    Then I open the context menu for that room
#    And I click on "..."


#    Then I right click on checked out room block
#    #Check in
#    Then I click on "Check In" link in context menu
#    Then I should see A new "check In" Tab open
#    #Reservation
#    Then I click on "Reservation" link in context menu
#    Then I should see A new "Reservation" Tab open
#    #Uncheck out
#    Then I click on "Uncheck out" link in context menu
#    Then I right click on checked in room block
#    Then I should see A new "Uncheck in" link appear this context menu
#    #Assign employee
#    Then I click on "Assign Employee" link in context menu
#    Then I should see A new "Assign Employee" pop up open
#    #Assign task
#    Then I click on "Assign Task" link in context menu
#    Then I should see A new "Assign Task'" pop up open
#    #Assign marker
#    Then I click on "Assign Markers" link in context menu
#    Then I should see A new "Assign Markers" pop up open
#    #Mark as dirty
#    Then I click on "Mark as 'Dirty'" link in context menu
#    Then I right click on checked out room block
#    Then I should see A new "Mark as 'Clean'" link appear this context menu
#
#    Given I am in room view
#    Then I right click on checked out room block
#    #Mark as clean
#    Then I click on "Mark as 'Clean'" link in context menu
#    Then I right click on checked out room block
#    Then I should see A new "Mark as 'Dirty'" link appear this context menu
#
#    Given I am in room view
#    Then I right click on checked out room block
#    #Mark as maintenance
#    Then I click on "Mark as 'Maintenance'" link in context menu
#    Then I should see A new "Mark as 'Maintenance'" pop up open
#    #Mark as locked
#    Then I click on "Mark as Locked" link in context menu
#    Then I should see A new "Mark as Locked" pop up open