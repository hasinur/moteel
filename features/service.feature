Feature: Create a new service
  In order to create a new service
  As a hotel owner
  I need to create a service from hotel admin dashboard

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And I have "House Keeping Department" department

  @javascript
  Scenario: Create a hotel service from hotel admin dashboard
    When I go to new service page
    And I select in "service department id" with "House Keeping Department" from drop down list
    And I select in "service room type id" with "standard" from drop down list
    Then I fill in "service name" with "Bed Tea" in en
    And I fill in "service name" with "Bed Tea Ar" in ar
    And I check "service_add_on"
    Then I fill in "service description" with "description about bed tea" in en
    And I fill in "service description" with "description about bed tea Ar" in ar
    And I fill "15"  in the "service_unit_price" textbox
    And I check "service_fixed_price"
    Then I select in "service field type" with "Check box" from drop down list
    And I press "Create Branch Service"
    And I wait for 2 seconds
    Then I should see "Branch Service was successfully created."

