Feature: Folio

  Background:
    Given I have hotel name "river crown"
    And I have 1 room types "standard"
    And room price for "standard" room type is 100 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 existing "adult" guest "Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"
    And I have 1 room already checked-in "std1" with guests "Abdul Rahim, Baby Rahim"
    And I have a "fixed" discount "summer" is available 10 SAR

  @javascript
  Scenario: Check In process from room view menu
    When I am in room view
    Then I click "Payment"
    When I type in "{{@checkin.id}}" into autocomplete field "payment_dashboard_check_in_search"
    And I wait for 2 seconds
    Then I choose "{{@checkin.id}}" from autocomplete list
    And I wait for 2 seconds
    Then I should see "Payment" within ".payment-result-table" in current tab
    Then I should see the button "Create Folio" enabled in current tab
    Then I should see the button "Delete Folio" disabled in current tab
    Then I should see the button "Create Invoice" disabled in current tab
    Then I should see "Abdul Rahim" within ".payment-result-table" in current tab
    Then I should see "0 SAR" within ".test_total_advance" in current tab
#    And I wait for 200 seconds
    Then I should see "Abdul Rahim" within ".test_folio_name" in the 1st row in "payment_search_result_table" table
    Then I should see "FOLIO1" within ".test_folio_name" in the 1st row in "payment_search_result_table" table
    Then I should see "GENERAL" within ".test_service_dept_name" in the 1st row in "payment_search_result_table" table
    Then I should see "100" within ".test_unit_price" in the 1st row in "payment_search_result_table" table
    Then I should see "1" within ".test_quantity" in the 1st row in "payment_search_result_table" table
    Then I should see "100" within ".test_service_total" in the 1st row in "payment_search_result_table" table
    Then I should see "100" within ".test_folio_bill" in the 1st row in "payment_search_result_table" table
    Then I should see "0" within ".test_folio_payment" in the 1st row in "payment_search_result_table" table
    Then I should see "100.0" within ".test_due" in current tab

    When I press "Create Folio"
    Then I should see the popup opened with title "Folio"
    Then I select "General" from "folio[department_id]"
    Then I select "Abdul Rahim" from "folio[billable_id]"
    When I press "Create Folio" within ".ui-dialog-content"
    Then I should see the message "Folio already exist according the department." as success notification

    Then I select "General" from "folio[department_id]"
    Then I select "Baby Rahim" from "folio[billable_id]"
    When I press "Create Folio" within ".ui-dialog-content"
    Then I should see the message "Folio created successful." as success notification
    And I close the popup

    Then I should see "Baby Rahim" within ".test_folio_name" in the 2nd row in "payment_search_result_table" table
    Then I should see "FOLIO2" within ".test_folio_name" in the 1st row in "payment_search_result_table" table
#    Then I should see "GENERAL" within ".test_service_dept_name" in the 1st row in "payment_search_result_table" table
#    Then I should see "100" within ".test_unit_price" in the 1st row in "payment_search_result_table" table
#    Then I should see "1" within ".test_quantity" in the 1st row in "payment_search_result_table" table
#    Then I should see "100" within ".test_service_total" in the 1st row in "payment_search_result_table" table
#    Then I should see "100" within ".test_folio_bill" in the 1st row in "payment_search_result_table" table
#    Then I should see "0" within ".test_folio_payment" in the 1st row in "payment_search_result_table" table
#    Then I should see "100.0" within ".test_due" in current tab


  #    Then I should see the default department is selected "General"
  #    Then I should see the default billing name "Mr. Abdul Rahim"
  #    Then I should see the payment type "cash"
  #    Then I set department to "select"
  #    When I click on create folio


    And I wait for 10 seconds