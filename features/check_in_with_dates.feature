Feature: Check in spec

  Background:
    Given I have hotel name "river crown"
    And I have 1 room type "standard"
    And I have 1 room in "standard" room type "std1", "std2", "std3", "std4", "std5"
    And room price for "standard" room type is 100 SAR

  Scenario:  cancel checkin
    And I have 1 room already checked-in "std2" from "yesterday" to "today"
    And I have 2 room already checked-in "std3, std4" from "7 days ago" to "2 days ago"
    And I have 2 rooms already checked-in "std1, std5"