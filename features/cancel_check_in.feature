Feature: Cancel Checkin

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have 1 rooms already checked-in "std1"

  @javascript
  Scenario:  cancel checkin
    When I am in room view
    And  I wait for 3 seconds
    Then I should see "Room View"
    Then I should see 1 room block "checked_in"
    Then I should see 4 room block "available"
    When I right click on room "std1" in room view
    Then I click on "Uncheck In" link in that context menu
    And I wait for 2 seconds
    Then I should see A new "Cancel Check In" pop up open
    Then I choose "cancel_option_single"
    When I press "Save" within "#cancel_check_in_container"
    And I wait for 2 seconds
    Then I should see "Checked In cancelled successfully"
    Then I wait for 3 seconds
    Then I should not see "Cancel Check In" pop up open
    Then I should see 5 room block "available"
    And That CheckIn should be "CANCELLED"

  @javascript
  Scenario:  cancel checkin after transfer room
    When I am in room view
    And  I wait for 3 seconds
    Then I should see "Room View"
    Then I should see 1 room block "checked_in"
    Then I should see 4 room block "available"
    When I transfer room "std1" to room "std2" in room view
    Then I am in room view
    And  I wait for 3 seconds
    When I right click on room "std2" in room view
    Then I click on "Uncheck In" link in that context menu
    And I wait for 2 seconds
    Then I should see A new "Cancel Check In" pop up open
    Then I choose "cancel_option_single"
    When I press "Save" within "#cancel_check_in_container"
    And I wait for 2 seconds
    Then I should see "Checked In cancelled successfully"
    Then I wait for 3 seconds
    Then I should not see "Cancel Check In" pop up open
    Then I should see 5 room block "available"
    And That CheckIn should be "CANCELLED"



