Feature: CheckIn
  In order to see Organize CheckIn information
  As a Hotel Owner
  I need to be manage CheckIn information

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 100 SAR
    And room price for "delux" room type is 200 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have a "fixed" discount "summer" is available 10 SAR

  @javascript
  Scenario: Check In process from room view menu
    Given I am in room view
    Then I right click on room "std2" in room view
    And I wait for 3 seconds
    Then I click on "Check In" link in that context menu
    And I wait for 10 seconds

    Then I should see the "billableType" is set "individual"
    Then I click on "a.bill-add-btn" custom link
    And I wait for 3 seconds
    And I should see "Guest Information"
    Then I fill "Hasinur"  in the "guest_first_name" textbox
    Then I fill "Rahman"  in the "guest_last_name" textbox
    Then I fill "Mohammadi Housing Ltd, Mohammadpur."  in the "guest_address" textbox
    And I press "Create"
    And I wait for 3 seconds
    Then I press "close"
    And I wait for 3 seconds
    When I empty the "billing_name" textbox
    When I empty the "main_guest_search" textbox
    And I wait for 10 seconds
    Then I click on "a.bill-add-btn" custom link
    And I wait for 3 seconds
    And I should see "Guest Information"
    Then I fill "Wasiqul"  in the "guest_first_name" textbox
    Then I fill "Hoque"  in the "guest_last_name" textbox
    Then I fill "Jigatola, Dhanmondi, Dhaka."  in the "guest_address" textbox
    And I press "Create"
    And I wait for 3 seconds
    Then I press "close"
    And I wait for 3 seconds
    When I empty the "billing_name" textbox

    And I wait for 5 seconds
    When I fill "Has"  in the "billing_name" textbox
    And I wait for 5 seconds
    Then I should see "Hasinur Rahman" within ".guest-search-result"
    Then I choose "Hasinur Rahman" from autocomplete list
    When I empty the "main_guest_search" textbox
    And I wait for 5 seconds
    When I fill "Was"  in the "main_guest_search" textbox
    And I wait for 5 seconds
    Then I should see "Wasiqul Hoque" within ".guest-search-result"
    Then I choose "Wasiqul Hoque" from autocomplete list
    And I wait for 5 seconds
    Then I press "Check In" in the current tab
    And I wait for 2 seconds
    And I should see "Check In Created Successfully"

    And I wait for 15 seconds

