Feature: Check out

  Background:
    Given I have hotel name "river crown"
    And I have 1 room type "standard"
    And I have 1 room in "standard" room type "std1"
    And room price for "standard" room type is 100 SAR
    And I have 1 room already checked-in "std1"
    And I have a cash counter "My cash counter"

  @javascript
  Scenario: check out
    When I am in room view
    And I wait for 3 seconds
    Then I should see "Room View"
    When I right click on room "std1" in room view
    Then I click on "Check Out" link in that context menu
    And I wait for 1 seconds
#    Then I should see the "Check Out" tab open in the Check In tab
    And I press "Payment" in the current tab
    And I wait for 1 seconds
    And I select in "current cash counter" with "My cash counter" from drop down list
    And I wait for 3 seconds
    And I press "Submit"
    And I wait for 1 seconds
    And I fill in "hotel service payment amount" with "100"
    And I press "Save"
    And I wait for 3 seconds
    And I close the popup
    And I press "Confirm Checkout"
    And I wait for 1 seconds
    And I press "No"
    And I wait for 1 seconds
    And I should see room "std1" as "available" in the room view
#
  Background: This will test the partial check-out and full check-out after the room trasnfer
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 100 SAR
    And room price for "delux" room type is 200 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have 2 rooms already checked-in "std1, dlx1"
    And I have a "fixed" discount "summer" is available 10 SAR
    And I have 3 existing "adult" guest "Mr. Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"
    And I have 1 existing company "Tasawr"

  @javascript
  Scenario: Partial room check-out after the room transfer
    When I am in room view
    When I right click on room "std1" in room view
    Then I click on "Room Transfer" link with url in that context menu
    And I wait for 1 seconds
    Then I should see A new "Room Transfer" pop up open
    Then I select "delux" from "changed_allocated_room[room_type_id]"
    And I wait for 2 seconds
    Then I select "dlx 2" from "changed_allocated_room[room_id]"
    When I click on "Continue" button
    Then I should see "std 1 *old" within ".old-room .title"
    Then I should see "dlx 2 *new" within ".new-room .title"
    Then I should see "1" within ".old-room .night-count"
    Then I should see "1" within ".new-room .night-count"
    Then I should see "0" within ".old-room .night-stay"
    Then I should see "1" within ".new-room .night-stay"
    Then I should see "100.0" within ".old-room .last-price"
    Then I should see "200.0" within ".new-room .last-price"
    Then I should see the "allocated_room[price]" is set "0"
    Then I should see the "change_room[price]" is set "200.0"
    When I click on "Update" button
    Then I should see notification "Operation completed successfully"

#
#    When I click on the partial check out
#    Then I should see the partial check-out form
#    Then I should see check-in number
#    Then I should see the start date
#    Then I should see the end date
#    Then I should see the rooms dlx2, dlx3
#    Then I should see the total spent 310
#    Then I should see the total paid 0
#    Then I should see the room number dlx2
#    Then I should see the guest name Mr. Abdul Rahim
#    Then I should see the room number dlx3
#    Then I should see the guest name Mr. Abdul Karim
#    Then I click on the checkboxes dlx2, dlx3
#    When I click on continue
#    Then I should see the error message "All guest can not be checked out"
#    Then I uncheck the checkbox dlx3
#    When I click on the continue
#    Then I should see the successful message
#
#    When I click on the print contract
#    Then I should see the contract paper
#    Then I should see the name Mr. Abdul Karim
#    Then I should see the reservation no.
#    Then I should see the date of birth 1/12/1977
#    Then I should see the occupation business
#    Then I should see the company name "Tasawr"
#    Then I should see the company postal code "1219"
#    Then I should see the company city Dhaka
#    Then I should see the company country Bangladesh
#    Then I should see the company email info@tasawr.com
#    Then I should see the company contact 01819444289
#    Then I should see the residential address "Rampura, Dhaka"
#    Then I should see the postal code 1219
#    Then I should see the city Dhaka
#    Then I should see the country Bangladesh
#    Then I should see the email abdul.karim@yahoo.com
#    Then I should see contact 01819444289
#    Then I should see the passport no. 0987654321
#    Then I should see the passport expiry date expiry date 1/12/2015
#    Then I should see the visa number 912345678
#    Then I should see the room number std2
#    Then I should see the room type standard
#    Then I should see the no. of adults 2
#    Then I should see the no. of children 1
#    Then I should see the room rate 50
#    Then I should see the room number dlx3
#    Then I should see the room type delux
#    Then I should see the no. of adults 1
#    Then I should see the no. of children 0
#    Then I should see the room rate 100
#    Then I should see the total charge 300 SAR
#    Then I should see the total discount 40 SAR
#    Then I should see the total 260 SAR
#
#    When I click on the Preview button
#    Then I should see the reservation number
#    Then I should see the check-in date
#    Then I should see the check-out date
#    Then I should see the nights 2
#    Then I should see the no. of rooms 2
#    Then I should see the Res. status "Checked in"
#    Then I should see the billing type individual
#    Then I should see the company name "Tasawr"
#    Then I should not see the company name
#    Then I should see the billing name Mr. Abdul Karim
#    Then I should see the guest name Mr. Abdul Karim
#    Then I should see the guest designation "visitor"
#    Then I should see the address is "Rampura, Dhaka"
#    Then I should see the email is "abdur.karim@yahoo.com"
#    Then I should see the ID type "passport"
#    Then I should see the ID number is "123456789"
#    Then I should see the job title "business"
#    Then I should see the company "individual"
#    Then I should see the passport number "0987654321"
#    Then I should see the visa number "912345678"
#    Then I should see the guest type "normal"
#    Then I should see the room type Standard
#    Then I should see the room number std2
#    Then I should see the adults/children 2/1
#    Then I should see the guest name "Mr. Abdul Rahim"
#    Then I should see the dependent "Ms. Rahim"
#    Then I should see the relationship "wife"
#    Then I should see the the price 50SAR
#    Then I should see the guest type "normal"
#    Then I should see the room type delux
#    Then I should see the room number dlx3
#    Then I should see the adults/children 1/0
#    Then I should see the guest name "Mr. Abdul Karim"
#    Then I should see the dependent ""
#    Then I should see the relationship ""
#    Then I should see the the price 100SAR
#    Then I should see the discount 40 SAR
#    Then I should see the total price 260 SAR
#    Then I should see the booked by "Md. Kamal Hossain"
#    Then I should see the source ""
#    Then I should see the notes ""
#
#
#    When I click on Print Check in Card
#    Then I should see the check-in card is open
#    Then I should see the billing name "Tasawr"
#    Then I should see the billing address "Mohakhali, DOHS"
#    Then I should see the billing Tel 01819444289
#    Then I should see the billing email "info@tasawr.com"
#    Then I should see the reservation name "Md. Abdul Karim"
#    Then I should see the reservation ID
#    Then I should see the reservation type checked in
#    Then I should see the reservation date
#    Then I should see the item name Standard
#    Then I should see the guest name Mr. Abdul Rahim
#    Then I should see the check-in date
#    Then I should see the check-out date
#    Then I should see the night stay 2
#    Then I should see the Pax 3
#    Then I should see the room rate 50
#    Then I should see the item name delux
#    Then I should see the guest name Mr. Abdul Karim
#    Then I should see the check-in date
#    Then I should see the check-out date
#    Then I should see the night stay 2
#    Then I should see the Pax 1
#    Then I should see the room rate 100 SAR
#    Then I should see the discount 40 SAR
#    Then I should see the total 260 SAR
#    When I click on the print button
#    Then I should see the print options
#    When I click on the close button
#  I should see the page disappears
#
#
#    When I click on the check-in list on the left panel
#  I should see the reservation number
#  I should see the guest name "Md. Abdul Karim"
#  I should see the room number dlx3
#  I should see the rate 90 SAR
#  I should see the arrival time "check-in time"
#  I should see the departure time "check-out time"
#  I should see the night count 2
#  I should see the pax 1
#
#    When I click on the check-out list on the left panel
#  I should see the reservation number
#  I should see the guest name "
#  I should see the stay duration "check in" and "check-out" time
#  I should see the departure time "check-out" time
#  I should see the room number std2
#  I should see the status "check out"
#  I should see the amount 80
#
#  I should see available 2 in the legend
#  I should see reserved 0
#  I should see confirmed 0
#  I should see checked-in 4
#  I should see locked 0
#  I should see maintenance 0
#  I should see due out 0
#
#
#    When I click on the room view tab
#  then I should see std1, dlx1, dlx3 are red
#  then I should see std2, dlx2 are green
#
#    When I mouse over on dlx3
#  then I should see the popup
#  then I should see reservation number
#  then I should see balance 260
#  then I should see guest name "Md. Abdul Karim"
#  then I should see check  in time
#  then I should see check out time
#  then I should see days left 1
#


