Feature: Hotel owner dashboard after login
  In order to see Dashboard after login
  As a hotel owner with role
  I should see dashboard

  Background:
    Given I am a user with email "test@moteel.com" and password "123456"
    And I have a hotel setup
    And have permissions setup

  @javascript
  Scenario: Hotel administration as dashboard
    Given I am logged in as "hotel owner"
    Then I should be on the "hotel_administration" page