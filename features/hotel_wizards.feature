Feature: Create a new hotel with some rooms
  In order to create a room
  As a hotel owner
  I need to create a hotel and some rooms of a room type

  Background:
    Given I have some countries cities and price unit
    And I am logged in as "hotel owner"
    And There are 10 "amenity_for_hotel"

  @javascript
  Scenario: Create Hotel with some Rooms from hotel wizard
    When I press "Next"
    Then I should see "can't be blank"
    Then I fill in "hotel title" with "Sheraton" in en
    And I fill in "hotel title" with "SheratonAr" in ar
    And I fill in "hotel address" with "Sheraton Address" in en
    And I fill in "hotel address" with "SheratonAr Address" in ar
    And I select in "hotel country" with "Bangladesh" from drop down list
    And I wait for 1 seconds
    And I select in "hotel city id" with "Dhaka" from drop down list
    And I wait for 1 seconds
    And I select in "hotel price unit id" with "BDT" from drop down list
    Then I press "Next"
    And I wait for 3 seconds
    Then I attach a image "default_logo_hotel_1.jpg" to "hotel_logo"
    Then I attach a image "default_logo_hotel_1.jpg" to "hotel_hotel_banner_attributes_image"
#    Then I attach the file at "/home/hasinur/Desktop/hotel_image/logo.png" to "hotel_logo"
#    Then I attach the file at "/home/hasinur/Desktop/hotel_image/hotel_banner.jpg" to "hotel_hotel_banner_attributes_image"

    Then I fill "sheraton"  in the "hotel_subdomain" textbox
    Then I fill "+982355245"  in the "hotel_phone" textbox
    Then I fill "info@sheraton.moteel.com"  in the "hotel_email" textbox
    And I fill in "hotel description" with "Sheraton hotel summery for english" in en
    And I fill in "hotel description" with "Sheraton hotel summery for english" in ar
    Then I press "Next"
    And I wait for 3 seconds

    And I fill in "hotel policies" with "Sheraton hotel policy for english" in en
    And I fill in "hotel policies" with "Sheraton hotel policy for english" in ar
    And I fill in "hotel payment_policy" with "Sheraton hotel payment policy for english" in en
    And I fill in "hotel payment_policy" with "Sheraton hotel payment policy for english" in ar
    And I fill in "hotel contract_terms_and_conditions" with "Sheraton hotel contract terms and conditions for english" in en
    And I fill in "hotel contract_terms_and_conditions" with "Sheraton hotel contract terms and conditions for english" in ar
    Then I press "Next"
    And I wait for 3 seconds
    Then I should see the "hotel[starting_hour]" is set "12"
    Then I should see the "hotel[ending_hour]" is set "11"
    Then I should see the "hotel[time_zone]" is set "Riyadh"
    Then the "Same day" radio_button should be checked
    Then I should see the "hotel[night_audit_settings][hours]" is set "22"
    Then I select "12" option from "hotel_starting_hour" deopdown list
    Then I select "0" option from "hotel_starting_minute" deopdown list
    Then I select "11" option from "hotel_ending_hour" deopdown list
    Then I select "59" option from "hotel_ending_minute" deopdown list
    Then I select "(GMT+06:00) Dhaka" option from "hotel_time_zone" deopdown list
    Then I choose "hotel_night_audit_settings_day_1"
    And I select "23" option from "hotel_night_audit_settings_hours" deopdown list
#    When I check "hotel_amenity_ids_"
#    And I wait for 60 seconds
    Then I press "Next"
    And I wait for 3 seconds
    Then I press "Add more"
    Then I press "Next"
    And I wait for 3 seconds
    Then I press "Next"
    Then I should see "Title can't be blank"
    Then I fill in "room_type title" with "Delux" in en
    Then I fill in "room_type title" with "Delux Ar" in ar
    Then I press "Next"
    And I wait for 3 seconds
    Then I select "2" option from "room_type_adult_nos" deopdown list
    Then I select "1" option from "room_type_child_nos" deopdown list
    Then I press "Next"
    Then I fill "DLX"  in the "room_prefix" textbox
    Then I fill "101-105"  in the "room_room_number_comma" textbox
    Then I press "Next"
    And I wait for 3 seconds
    Then I press "Skip All"
    And I wait for 3 seconds
    And I should see "Room"

