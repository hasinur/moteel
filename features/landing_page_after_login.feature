Feature: Landing page after login
  In order to see Landing page after login
  As a manager with role
  I should see front desk

  Background:
    Given I am a user with email "test@moteel.com" and password "123456"
    And I have a hotel setup
    And have permissions setup

  @javascript
  Scenario: Frontdesk as landing page
    Given I am a "front_desk_manager" under created hotel with "front_desk" as default dashboard
    And I am logged with email "test@moteel.com" and password "123456"
    Then I should be on the "front_desk" page

  @javascript
  Scenario: Housekeeping as landing page
    Given I am a "house_keeping_manager" under created hotel with "housekeeping" as default dashboard
    And I am logged with email "test@moteel.com" and password "123456"
    Then I should be on the "housekeeping" page

  @javascript
  Scenario: Roomservice as landing page
    Given I am a "room_service_home" under created hotel with "room_service" as default dashboard
    And I am logged with email "test@moteel.com" and password "123456"
    Then I should be on the "room_service" page

  @javascript
  Scenario: Reporting as landing page
    Given I am a "report_home" under created hotel with "reports_base" as default dashboard
    And I am logged with email "test@moteel.com" and password "123456"
    Then I should be on the "reports_base" page