Feature: Create a new cash counter
  In order to create a new cash counter
  As a hotel owner
  I need to create a cash counter from hotel admin dashboard

  Background:
    Given I have hotel name "river crown"

  @javascript
  Scenario: Create a cash counter from hotel admin dashboard
    When I go to new cash counter page
    Then I fill in "cash_counter name" with "Front Desk" in en
    And I fill in "cash_counter name" with "Front Desk Ar" in ar
    And I fill "6000"  in the "cash_counter_minimum_balance" textbox
    Then I fill in "cash_counter description" with "Front Desk" in en
    And I fill in "cash_counter description" with "Front Desk Ar" in ar
    And I press "Create Cash Counter"
    And I wait for 2 seconds
    And Page should have "Cash Counter was successfully created." text
