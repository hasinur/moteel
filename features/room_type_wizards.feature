Feature: Create a new room type
  In order to create a room type
  As a hotel owner
  I need to create a hotel with some room type

  Background:
    Given I have hotel name "river crown"

  @javascript
  Scenario: Create Hotel with some Rooms from hotel wizard
    When I go to the new room type page
    When I select in "none selected branch id" with "river crown" from drop down list
    Then I press "Next"
    And I wait for 3 seconds
    Then I press "Next"
    Then I should see "Title can't be blank"
    Then I fill in "room_type title" with "Delux" in en
    Then I fill in "room_type title" with "Delux Ar" in ar
    Then I press "Next"
    And I wait for 3 seconds
    Then I press "Skip"
    Then I fill "DLX"  in the "room_prefix" textbox
    Then I fill "101-105"  in the "room_room_number_comma" textbox
    Then I press "Next"
    And I wait for 3 seconds
    Then I press "Skip All"
    And I wait for 3 seconds
    And I should see "Room"


#