Feature: Create a new department
  In order to create a new department
  As a hotel owner
  I need to create a department

  Background:
    Given I have hotel name "river crown"

  @javascript
  Scenario: Create an department from hotel admin dashboard
    When I go to new department page
    Then I fill in "department name" with "HR & Admin" in en
    Then I fill in "department name" with "HR & Admin" in ar

    And I check "department_create_separate_folio"
    Then I fill in "department description" with "This is  description for HR & Admin" in en
    Then I fill in "department description" with "This is  description for HR & Admin" in ar
    And I press "Create Department"
    And I wait for 2 seconds
    And I should see "Department was successfully created."
