Feature: Create a new department
  In order to create a new department
  As a hotel owner
  I need to create a department

  Background:
    Given I have hotel name "river crown"

  @javascript
  Scenario: Create an department from hotel admin dashboard
    When I go to new marker page
    Then I fill "Air Condition"  in the "marker_name" textbox
    Then I fill in "marker title" with "Air Condition" in en
    Then I fill in "marker title" with "Air Condition Ar" in ar
    And I press "Create Marker"

    And I wait for 2 seconds
    And I should see "Marker was successfully created."
