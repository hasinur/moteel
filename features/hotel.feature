Feature: Hotel
  In order to see Organize hotel information
  As a Hotel Owner
  I need to be manage hotel information

  Background:
    Given I have some countries cities and price unit
    And I am logged in as "hotel owner"

  @javascript
  Scenario: Hotel and Room creation wizard
    Given I am on "New Branch" page and it has button "Cancel" and "Next"
    And  I fill in "hotel title" with "Sheraton" in en
    And I select in "hotel country" with "Bangladesh" from drop down list
    And I select in "hotel city" with "Dhaka" from drop down list
    When I press "Next" link it should go for next steps


    Then I am on "Add Additional Information" page and it has button "Skip" and "Continue"
    And  I fill in "hotel phone" with "+88017333333"
    And  I fill in "hotel email" with "contact@sheraton.com"
    When I press "Continue" link it should go for next steps

    Then I am on "Policies & Others" page and it has button "Skip" and "Continue"
    And  I fill in "hotel policies" with "test policies details." in en
    And  I fill in "hotel payment policy" with "test payment policies details." in en
    When I press "Continue" link it should go for next steps

    Then I am on "Photos and Videos" page and it has button "Skip" and "Continue"
    When I press "Continue" link it should go for next steps

    Then I am on "Create Room Type" page and it has button "Cancel" and "Next"
    And  I fill in "room_type title" with "president suite" in en
    And  I fill in "room type price" with "450"
    When I press "Next" link it should go for next steps

    Then I am on "Additional Information" page and it has button "Skip" and "Continue"
    And  I fill in "room_type description" with "President suit description" in en
    When I press "Continue" link it should go for next steps

    And I am on end of the wizard and should see "Congratulations !" message and a "Continue" link
    When I press "Continue" link it should go for next steps

    Then I am on "Create Room Type" page and it has button "Cancel" and "Next"
    And  I fill in "room prefix" with "PS"
    And  I fill in "room number comma" with "100, 102, 103"
    And  I fill in "price" with "450"
    When I press "Next" link it should go for next steps

    Then I am on "Add Room Additional Fields" page and it has button "Skip" and "Continue"
    When I press "Continue" link it should go for next steps
    Then I am on "Add Room Additional Fields" page and it has button "Skip" and "Continue"
    When I press "Continue" link it should go for next steps
    Then I am on "Add Room Additional Fields" page and it has button "Skip" and "Continue"
    When I press "Continue" link it should go for next steps

    And I am on end of the wizard and should see "Congratulations !" message and a "Finish" link
    When I press "Finish" link it should go for next steps




