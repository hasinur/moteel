Feature: Appearance of Context Menu
  In order to see reserve room information
  As a Hotel Owner
  I need to manage reserve room

  Background:
    Given  I have hotel name "river crown"
    And I have some countries cities and price unit
    And I have a cash counter
    And I have 2 room types "standard", "delux"
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have "std1" room is already due out


  @javascript
  Scenario: On Temporary Reservation Room
    When I am in room view
    And  I wait for 5 seconds
    Then I right click on room "std1" in room view
    Then I click on "Add Charge" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Charge Room" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Payment" link with url in that context menu
    And I wait for 4 seconds
    Then I should see A new "Select cash counter" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Check Out" link with url in that context menu
    Then I should see A new "CheckIn-CheckIn ID" Tab open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Edit" link with url in that context menu
    Then I should see A new "CheckIn-CheckIn ID" Tab open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Assign Employee" link in that context menu
    And I wait for 4 seconds
    Then I should see A new "Assign Employee" pop up open
    And I wait for 2 seconds

    Given I am in room view
    Then I right click on room "std1" in room view
    Then I click on "Mark as 'Dirty'" link in that context menu
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I should see A new "Mark as 'Clean'" link appear this context menu
    Then I click on "Mark as 'Clean'" link in that context menu
  #
    Given I am in room view
    Then I right click on room "std1" in room view
    Then I should see A new "Mark as 'Dirty'" link appear this context menu
