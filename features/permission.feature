Feature: Permission
  In order check the permissions
  As a Hotel Owner
  I need to be able to do what I am allowed to do

  Background:
    Given I am logged in as hotel owner with one hotel
    And have permissions setup

  @javascript
  Scenario: Hotel manage permission
    Given I have the "hotel create update" permission
    Then I can go to "new hotel" page