Feature: Partial Guest Check Out
  In order to partial guest check out
  As a Hotel Owner
  I need to be manage CheckIn information

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard"
    And room price for "standard" room type is 100 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 1 rooms already checked-in "std1"
    And I have a "fixed" discount "summer" is available 10 SAR
    And I have 3 existing "adult" guest "Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"


  @javascript
  Scenario: Partial guest check out process from room view menu
    When I am in room view
    And I wait for 3 seconds
    Then I should see "Room View"
    When I right click on room "std1" in room view
    Then I click on "Edit" link in that context menu
    And I wait for 5 seconds
    When I empty the "main_guest_search" textbox
    And I wait for 5 seconds
    When I fill "Ab"  in the "main_guest_search" textbox
    And I wait for 5 seconds
    Then I choose "Abdul Rahim" from autocomplete list
    And I wait for 5 seconds
    Then I click "addMoreDependent" link
    Then I fill "Ab"  in the "dependent_guest_name" textbox
    And I wait for 5 seconds
#    Then I should see "Abdul Karim" within ".guest-search-result"
    And I wait for 5 seconds
    Then I choose "Abdul Karim" from autocomplete list
    Then I select "Brother" option from "relation_type" deopdown list
    And I press "Done?"
    And I press "Update"
    And I wait for 10 seconds
    Then I am in room view
    And I wait for 3 seconds
    Then I should see "Room View"
    When I right click on room "std1" in room view
    Then I click on "Partial guest checkout" link in that context menu
    And I wait for 3 seconds
    Then I select "Abdul Karim" contain element id "partial_checkout_guest_allocation_id" from list of partial checkout popup
    And I wait for 3 seconds
    Then I press "Continue"
    And I wait for 60 seconds
    And I choose "Abdul Rahim" contain element id "partial_checkout_proposed_main_guest_id" from guest list popup
    And I wait for 3 seconds
    Then I press "Confirm"
    And I wait for 3 seconds
    And I should see "Partially checked out"

