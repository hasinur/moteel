Feature: CheckIn
  In order to see Organize CheckIn information
  As a Hotel Owner
  I need to be manage CheckIn information

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 100 SAR
    And room price for "delux" room type is 200 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have 2 rooms already checked-in "std1, dlx1"
    And I have a "fixed" discount "summer" is available 10 SAR
    And I have 3 existing "adult" guest "Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"

  @javascript
  Scenario: Check In process from room view menu
    When I am in room view
    Then I hover on "Front Desk"
    Then I click on "Walk in Check In" link with url in that top navigation
    Then I should see check-in time is set today
    Then I should see the check-out time is set tomorrow
    Then I should see the "check_in[nights_count]" is set "1"
    Then I should see the "check_in_rooms_count" is set "0"
    Then I should see the "check_in[status]" is set "CHECKED_IN"
    Then I should see the "billableType" is set "individual"
    And I should see 0 row in check in room allocation
    When I click on "a.add_new_room_allocation" custom link
    And I should see 1 "tr" under "table.checkin-table tbody"
    Then I should see the "check_in[visiting_reason_id]" is set "1"
    Then I select "2" from "check_in[nights_count]"
#    Then I select "2" from "check_in_rooms_count"
    And I should see 1 row in check in room allocation
    When I type in "Abd" into autocomplete field "billing_name"
    Then I should see "Abdul Karim" within ".guest-search-result"
    Then I should see "Abdul Rahim" within ".guest-search-result"
    Then I choose "Abdul Rahim" from autocomplete list
    Then I should see the "billing_name" is set "Abdul Rahim"
    Then I select "standard" from "Room type" from the 1st row in check in
    Then I should see "std 2" within "room" from the 1st row in check in
    Then I select "std 2" from "room" from the 1st row in check in
    Then I should see the "price" is set to "200" in the 1st row in check in
    Then I should see "200" within ".total-room-price"
    When I type in "abdul" into autocomplete field "main_guest_search" in the 1st row in "allocated_rooms" table
    Then I should see "Abdul Rahim" within ".guest-search-result"
    Then I should see "Abdul Karim" within ".guest-search-result"
    Then I choose "Abdul Rahim" from autocomplete list
    Then I click on "a.more-dependent" in the 1st row in check in
    Then I should see the dependant guest popup open for the 1st row in check in

    When I type in "abdul" into autocomplete field "dependent_guest_name" in the 1st row in "allocated_rooms" table
    Then I choose "Abdul Karim" from autocomplete list
    Then I select "Brother" from "relation_type" from the 1st row in check in
    Then I click on "Done?" in the 1st row in check in
    #Then I press "Check In" in the current tab
    Then I should see "Abdul Karim" within ".room-dependents" for the 1st row in check in
    #Then I should see room "std2" as "Checked In" in the room view
    And I wait for 3 seconds

    Then I click on "a.more-dependent" in the 1st row in check in
    Then I should see the dependant guest popup open for the 1st row in check in
    When I type in "Rahim" into autocomplete field "dependent_guest_name" in the 1st row in "allocated_rooms" table
    Then I should see "Abdul Rahim" within ".dependent-guest-search"
    Then I should see "Ms. Rahim" within ".dependent-guest-search"
    Then I should see "Bby Rahim" within ".dependent-guest-search"
    Then I choose "Ms. Rahim" from autocomplete list
    Then I select "Wife" from "relation_type" from the 1st row in check in
    Then I click on "Done?" in the 1st row in check in
    Then I should see "Abdul Karim x 1 more" within ".room-dependents" for the 1st row in check in
    And I wait for 3 seconds

    Then I click on "a.more-dependent" in the 1st row in check in
    Then I should see the dependant guest popup open for the 1st row in check in
    When I type in "Rahim" into autocomplete field "dependent_guest_name" in the 1st row in "allocated_rooms" table
    Then I should see "Abdul Rahim" within ".dependent-guest-search"
    Then I should see "Ms. Rahim" within ".dependent-guest-search"
    Then I should see "Bby Rahim" within ".dependent-guest-search"
    Then I choose "Bby Rahim" from autocomplete list
    Then I select "Son" from "relation_type" from the 1st row in check in
    Then I click on "Done?" in the 1st row in check in
    Then I should see "Abdul Karim x 2 more" within ".room-dependents" for the 1st row in check in
    And I wait for 3 seconds

#    Then I should see the "check_in[allocated_rooms_attributes][0][adults]" is set "3"
#    Then I should see the "check_in[allocated_rooms_attributes][0][children]" is set "1"

    When I click on "a.add_new_room_allocation" custom link
    And I wait for 3 seconds

    Then I select "delux" from "Room type" from the 2nd row in check in
    Then I should see "dlx 2" within "room" from the 2nd row in check in
    Then I should see "dlx 3" within "room" from the 2nd row in check in
    Then I select "dlx 3" from "room" from the 2nd row in check in
    Then I should see the "price" is set to "400" in the 2nd row in check in
    Then I should see "600" within ".total-room-price"
    When I type in "abdul" into autocomplete field "main_guest_search" in the 2nd row in "allocated_rooms" table
    Then I should see "Abdul Rahim" within ".guest-search-result"
    Then I should see "Abdul Karim" within ".guest-search-result"
    Then I choose "Abdul Karim" from autocomplete list
    Then I click on "a.more-dependent" in the 2nd row in check in
    Then I should see the dependant guest popup open for the 2nd row in check in

    Then I should see the "check_in[allocated_rooms_attributes][0][adults]" is set "1"
#    When I select the discount summer
#    Then I should see total price is set 560
#    When I click on "a.add_new_room_allocation" custom link
#    Then I should see no. of rooms is set to 3
#    Then I should see the x sign to the newly added row
#    When I click on the x sign
#    Then I should see the newly added row is removed
#    Then I select the visiting reason not available
#    Then I select the date time now
#    Then I type source name "Tasawr"
#    Then I type valid until now
#    Then I type note this is a demo check-in so please ignore this
#    Then I press "Check In" in the current tab
#    Then I should see the loading image until check-in process completes
#    Then I should see the message "Check-in created successfully"
#    Then I should see the the same form loaded with the posted info
#    Then I should see the check-in number in the current tab
#    Then I should see the delete check-box next to the saved room info
#    Then I should see the buttons update, print contract, preview, print check-in card, check out, room transfer, payment
#
