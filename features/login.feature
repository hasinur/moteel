Feature: Login
  In order to see the home page
  As a website user
  I need to be able to visit home page

  Background:
    Given System has a hotel owner with email "test@moteel.com" and password "123456"

  @javascript
  Scenario: User Sign In Box
    Given I am logged with email "test@moteel.com" and password "123456"
    Then It should redirect to Branch Create page if I have no Hotel