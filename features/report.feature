Feature: Report
  In order to see Organize Report information
  As a Hotel Owner
  I need to be show report information

  Background:
    Given I have hotel name "river crown"
    And I have 2 room types "standard", "delux"
    And room price for "standard" room type is 100 SAR
    And room price for "delux" room type is 200 SAR
    And I have 2 rooms in "standard" room type "std1", "std2"
    And I have 3 rooms in "delux" room type "dlx1", "dlx2", "dlx3"
    And I have 2 rooms already checked-in "std1", "dlx1"
    And I have a "fixed" discount "summer" is available 10 SAR
    And I have 3 existing "adult" guest "Mr. Abdul Rahim", "Ms. Rahim", "Abdul Karim"
    And I have 1 existing "children" guest "Baby Rahim"
    And I have a cash counter


  @javascript
  Scenario: Check In process from room view menu
    And  I paid amount 1000 by "cheque"
    When I am in reporting dashboard
    Then I click on "Pending Cheque List" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Pending Cheque List" Tab open
    Then I wait for 5 seconds

    Then I click "Report" tab link
    Then I click on "Bounce Cheque List" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Bounce Cheque List" Tab open

    Then I click "Report" tab link
    Then I click on "Cash closeout" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Cash closeout" Tab open

    Then I click "Report" tab link
    Then I click on "Payment Type Wise Transactions" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Payment Type Wise Transactions" Tab open

    Then I click "Report" tab link
    Then I click on "Service Wise Revenue" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Service Wise Revenue" Tab open

    Then I click "Report" tab link
    Then I click on "Deposit Listing" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Deposit Listing" Tab open

    Then I click "Report" tab link
    Then I click on "Financial Statement" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Financial Statement" Tab open

    Then I click "Report" tab link
    Then I click on "Billing Information" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Billing Information" Tab open

    Then I click "Report" tab link
    Then I click on "Group Revenue Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Group Revenue Report" Tab open

    Then I click "Report" tab link
    Then I click on "Room Types Revenue Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room Types Revenue Report" Tab open

    Then I click "Report" tab link
    Then I click on "Cashier Ledger" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Cashier Ledger" Tab open

    Then I click "Report" tab link
    Then I click on "Revenue Report By Counter" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Revenue Report By Counter" Tab open

    Then I click "Report" tab link
    Then I click on "Room Rates Variation" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room Rates Variation" Tab open

    Then I click "Report" tab link
    Then I click on "Invoice list" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Invoice list" Tab open

    Then I click "Report" tab link
    Then I click on "Group Stay Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Group Stay Report" Tab open

    Then I click "Report" tab link
    Then I click on "Reservation Activity Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Reservation Activity Report" Tab open

    Then I click "Report" tab link
    Then I click on "Reservation Activity by Cash Counter" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Reservation Activity by Cash Counter" Tab open

    Then I click "Report" tab link
    Then I click on "Room History Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room History Report" Tab open

    Then I click "Report" tab link
    Then I click on "Room Wise Estimated Revenue Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room Wise Estimated Revenue Report" Tab open

    Then I click "Report" tab link
    Then I click on "Room Wise Actual Revenue Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room Wise Actual Revenue Report" Tab open

    Then I click "Report" tab link
    Then I click on "Room Type Wise Occupancy Report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room Type Wise Occupancy Report" Tab open

    Then I click "Report" tab link
    Then I click on "Room Rate" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room Rate" Tab open

    Then I click "Report" tab link
    Then I click on "Room net Revenue" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room net Revenue" Tab open

    Then I click "Report" tab link
    Then I click on "Night Audit Report By Date" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Night Audit Report By Date" Tab open

    Then I click "Report" tab link
    Then I click on "Room occupancy report" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Room occupancy report" Tab open

    Then I click "Report" tab link
    Then I click on "CheckIn Bill Summary" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "CheckIn Bill Summary" Tab open

    Then I click "Report" tab link
    Then I click on "Credit invoices" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Credit invoices" Tab open

    Then I click "Report" tab link
    Then I click on "Guest Revenue" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Guest Revenue" Tab open

    Then I click "Report" tab link
    Then I click on "Cash Counter Transactions" link in that reporting dashboard
    Then I wait for 3 seconds
    Then I should see A new "Cash Counter Transactions" Tab open
#
#

