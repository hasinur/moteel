Feature: To do Night Audit Operation
  In order to do the night audit operation
  As a hotel owner
  I need to go the night audit wizard page

  Background:
    Given I have hotel name "river crown"

  @javascript
  Scenario: Create Hotel with some Rooms from hotel wizard
    When I go to the hotel admin page
    And I press "Night Audit"
    And I press "Continue"
#    When I select in "none selected branch id" with "river crown" from drop down list
#    Then I click on "Next"
#    And I wait for 3 seconds
#    Then I click on "Next"
#    Then Page should have "Title can't be blank" text
#    Then I fill in "room_type title" with "Delux" in en
#    Then I fill in "room_type title" with "Delux Ar" in ar
#    Then I click on "Next"
#    And I wait for 3 seconds
#    Then I click on "Skip"
#    Then I fill "DLX"  in the "room_prefix" textbox
#    Then I fill "101-105"  in the "room_room_number_comma" textbox
#    Then I click on "Next"
#    And I wait for 3 seconds
#    Then I click on "Skip All"
#    And I wait for 3 seconds
#    And Page should have "Room" text


