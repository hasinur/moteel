require 'rspec/expectations'
require 'chronic'
#Background process for CheckIn a room

def with_in_current_tab
  within('#v_tab_content .v_tab_content_item', visible: true) do
    yield
  end
end

def nth_allocated_room(n)
  within("#allocated_rooms tbody tr.qa-allocated-room:nth-child(#{n})", visible: true) do
    yield
  end
end

Given /^I have hotel name "(.*?)"$/ do |title|
  create_and_sign_in(:hotel_owner)
  @hotel = FactoryGirl.create(:hotel, title: title, user: @user)
end

Given /I have a cash counter(?: "(.+)")?$/ do |counter_name|
  @cash_counter = FactoryGirl.build(:cash_counter, branch: @hotel)
  @cash_counter.name = counter_name if counter_name
  @cash_counter.save!
  @user.cash_counters << @cash_counter
end

Given /I have some service$/ do
  @service = FactoryGirl.create(:service, hotel: @hotel)
end

And /^I have (\d+) room types? (".+")$/ do |number, room_types|
  room_types.scan(/"([^"]+?)"/).flatten.each do |room_type|
    object = FactoryGirl.create(:room_type, :title => room_type, :price => 450, :hotel => @hotel)
    instance_variable_set "@#{room_type}".to_sym, object
  end
end

And /^I have (\d+) rooms? in "(.*?)" room type (".+")$/ do |number, room_type_title, room_titles|
  room_type = RoomType.find_by_title(room_type_title)
  room_titles.scan(/"([^"]+?)"/).flatten.each do |room_title|
    prefix = room_title.split(/(\w+)(\d+)/)[1]
    room_number = room_title.split(/(\w+)(\d+)/)[2]
    object = FactoryGirl.create(:room, :prefix => prefix, :room_number => room_number, :price => 450, :user => @user, :hotel => @hotel, :room_type => room_type)
    instance_variable_set "@#{room_title}".to_sym, object
  end
end

And /^I have \d+ rooms? already checked\-in "([\w, ]+)"(?: for (\d+) days?)?$/ do |rooms_input, days_count|
  room_numbers = rooms_input.split(/\W+/).delete_if { |r| r.empty? }
  rooms = []
  room_numbers.collect do |room_title|
    room = instance_variable_get("@#{room_title}".to_sym)
    if room
      rooms << room
    else
      raise "Can not find room #{room_title}"
    end
  end
  days_count ||= 1
  start_date = Time.zone.now.start_of(@hotel)
  end_date = days_count.day.from_now.end_of(@hotel)
  @checkin = FactoryGirl.create(:check_in, :hotel => @hotel, rooms: rooms, start_date: start_date, end_date: end_date)
end

And /^I have \d+ rooms? already checked\-in "([\w, ]+)" with guests? "([^\"]*)"(?: for (\d+) days?)?$/ do |rooms_input, guest_names, days_count|
  guests = []
  guest_names.split(',').each do |guest_name|
    last_name = guest_name.split.last.strip
    first_name = guest_name.split.first.strip
    name = "#{first_name}_#{last_name}".downcase.split(/\W*/).join
    guest = instance_variable_get "@#{name}".to_sym
    guests << guest
  end

  room_numbers = rooms_input.split(/\W+/).delete_if { |r| r.empty? }
  rooms = []
  room_numbers.collect do |room_title|
    room = instance_variable_get("@#{room_title}".to_sym)
    if room
      rooms << room
    else
      raise "Can not find room #{room_title}"
    end
  end
  days_count ||= 1
  start_date = Time.zone.now.start_of(@hotel)
  end_date = days_count.day.from_now.end_of(@hotel)
  billing_information = FactoryGirl.create(:billing_information, billable: guests.first)
  @checkin = FactoryGirl.create(:check_in, :hotel => @hotel, rooms: rooms, start_date: start_date, end_date: end_date, guests: guests, billing_information: billing_information)
end

#For detailed time format support please visit: https://github.com/mojombo/chronic
#I have 1 room already checked-in "sda1" from "yesterday" to "today"
And(/^I have \d+ rooms? already checked\-in "(.*?)" from "(.*?)" to "(.*?)"(?: with discount "(.*?)")?$/) do |rooms_input, from, to, discount|
  room_numbers = rooms_input.split(/\W+/).delete_if { |r| r.empty? }.delete_if { |r| r.empty? }
  rooms = []
  room_numbers.collect do |room_title|
    room = instance_variable_get("@#{room_title}".to_sym)
    if room
      rooms << room
    else
      raise "Can not find room #{room_title}"
    end
  end
  start_date = Chronic.parse(from).start_of(@hotel)
  end_date = Chronic.parse(to).end_of(@hotel)
  date_diff = (end_date.to_date - start_date.to_date).to_i

  discount = instance_variable_get "@#{discount}".to_sym
  @checkin = FactoryGirl.create(:check_in, :hotel => @hotel, rooms: rooms, hotel_discount: discount,
                                start_date: Time.zone.now.start_of(@hotel),
                                end_date: date_diff.day.from_now.end_of(@hotel))
    @checkin.update_attributes(start_date: start_date, end_date: end_date)
    @checkin.allocated_rooms.each do |ar|
      ar.update_attributes(start_date: start_date, end_date: end_date)
    end
  @checkin = @checkin.reload
end

And(/^I have (\d+) rooms already temporary reserved (".+")$/) do |number, rooms|
  room = []
  rooms.scan(/"([^"]+?)"/).flatten.each do |room_title|
    prefix = room_title.split(/(\w+)(\d+)/)[1]
    room_number = room_title.split(/(\w+)(\d+)/)[2]
    room << Room.where(prefix: prefix, room_number: room_number).first
  end
  @checkin = FactoryGirl.create(:reserved_check_in, :hotel => @hotel, rooms: room)

end

And /^I have (".+") room is already due out$/ do |room_title|
  room = []
  prefix = room_title.split(/(\w+)(\d+)/)[1]
  room_number = room_title.split(/(\w+)(\d+)/)[2]
  room << Room.where(prefix: prefix, room_number: room_number).first
  @checkin = FactoryGirl.create(:check_in, :hotel => @hotel, rooms: room)
  @checkin.update_column(:start_date, 4.days.ago)
  @checkin.update_column(:end_date, 3.days.ago)
  @checkin.save
end

And /^(".+") room already under maintenance$/ do |room_title|
  prefix = room_title.split(/(\w+)(\d+)/)[1]
  room_number = room_title.split(/(\w+)(\d+)/)[2]
  room = Room.where(prefix: prefix, room_number: room_number).first
  @task = FactoryGirl.create(:damaged_room_task, creator: @user, hotel: @hotel, room: room)
  @damaged_room = FactoryGirl.create(:damaged_room, hotel: @hotel, room: room, task: @task, entry_date: Time.now, released_date: 2.days.from_now)
end

And /^(".+") room already under locked$/ do |room_title|
  prefix = room_title.split(/(\w+)(\d+)/)[1]
  room_number = room_title.split(/(\w+)(\d+)/)[2]
  room = Room.where(prefix: prefix, room_number: room_number).first
  @task = FactoryGirl.create(:locked_room_task, creator: @user, hotel: @hotel, room: room)
  @damaged_room = FactoryGirl.create(:locked_room, hotel: @hotel, room: room, task: @task, entry_date: Time.now, released_date: 2.days.from_now)
end


And /^I have some rooms in my hotel$/ do
  @hotel = FactoryGirl.create(:hotel, :title => "Sheraton", :country => @country, :city => @city, :user => @user)
  @room_type = FactoryGirl.create(:room_type, :title => "President suite", :price => 450, :hotel => @hotel, :price_unit => @price_unit)
  @room = FactoryGirl.create(:room, :prefix => "PS", :room_number => "100", :price => 450, :user => @user, :hotel => @hotel, :room_type => @room_type)
  @guest = FactoryGirl.create(:guest, :agent_user_id => @user.id, :city => @city.title, :country => @country, :email => "guest@moteel.com", :first_name => "guest", :last_name => "name", :date_of_birth => "1991-12-19")
end

And /^I have (\d+) rooms checked in$/ do |number_of_room|
  @checkin = FactoryGirl.create(:checked_in_check_in, :hotel => @hotel, number_of_rooms: number_of_room)
  @checkin.allocated_rooms.first
end

And /^I have (\d+) rooms in reservation$/ do |number_of_room|
  @tem_resrvation = FactoryGirl.create(:reserved_check_in, :hotel => @hotel, number_of_rooms: number_of_room)
end

And /^I have the first rooms checked out of that 2 rooms$/ do
  @allocated_room = @checkin.allocated_rooms.first
  @allocated_room.update_attributes(status: 'CHECKED_OUT')
end
#@checkin =

#Scenario: Check In process from room view menu

Given /^I am on room view menu$/ do
  visit "/front_desk?lan=en"
  page.execute_script %Q{ $('.room_view_box').first().trigger({ type: 'contextmenu'}) }
  page.should have_link('Check In', :href => 'javascript:void(0)')
  click_link_or_button "Check In"
end

When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end

And /^I select last element from auto-complete$/ do
  page.execute_script %Q{ $('.auto_complete_results').last().trigger('click') }
end

And /^room price for "(\w+)" room type is (\d+) \w+$/ do |room_type, price|
  room_type = instance_variable_get "@#{room_type}".to_sym

  rate = FactoryGirl.build(:effective_rate, rate_code: "Standard rate for #{room_type.title}", hotel: @hotel)

  rate.available_on_days = []
  Date.today.upto(6.days.from_now.to_date) do |date|
    rate.available_on_days << date.strftime('%A').downcase
  end

  rate.save!
  room_type.ratables.create!(rate_id: rate.id, price: price, overcharge_per_hour: 0)
end


And /^I have a "(\w+)" discount "(\w+)" is available (\d+) SAR$/ do |type, code, amount|
  type = type == "fixed"
  object = FactoryGirl.create(:hotel_discount, hotel: @hotel, fixed: type, code: code, discount_value: amount)
  instance_variable_set "@#{code}".to_sym, object
end


And /^I have (\d+) existing "(.+)" guest (".+")$/ do |n, guest_type, guests|
  child = guest_type.to_s == "children"
  guests.scan(/"([^"]+?)"/).flatten.each do |guest_name|
    last_name = guest_name.split.last.strip
    first_name = guest_name.split.first.strip
    guest = FactoryGirl.create(:guest, first_name: first_name, last_name: last_name, agent_user_id: @user.id, child: child)
    name = "#{first_name}_#{last_name}".downcase.split(/\W*/).join
    #ap name
    instance_variable_set "@#{name}".to_sym, guest
  end
end

And /^I have \d+ existing company "(.+)"$/ do |company_name|
  FactoryGirl.create(:group, name: company_name)
end

When /^I hover on "([^\"]*)"$/ do |link_text|
  page.execute_script %Q{ $('.jq-menu li.pull-down a:contains("#{link_text}")').trigger("mouseenter"); }
end

Then /^I click on "(.+?)" link with url in that top navigation$/ do |link_text|
  link_href = "javascript:void(0)"
  page.execute_script %Q{ $('.nav-sub-menu li a:contains("#{link_text}")').trigger("mouseenter").click(); }
end


Then /^That CheckIn should be "(\w+)"$/ do |status|
  assert(@checkin.reload.cancelled? == true)
end

Then /^I should see check-in time is set today$/ do
  expected_time = I18n.l @checkin.start_date.start_of(@checkin.hotel)
  find_field('check_in[start_date]').value.should eq expected_time
end

Then /^I should see the check-out time is set tomorrow$/ do
  expected_time = I18n.l @checkin.end_date.end_of(@checkin.hotel)
  find_field('check_in[end_date]').value.should eq expected_time
end

And /^I check  multiselect checkbox for room "(\w+)"$/ do |room|
  room = instance_variable_get "@#{room}".to_sym
  step "I check \"multi_select_room\" within \"#room_view_#{room.id}\""
end

When /^(?:|I )select "([^\"]*)" from "([^\"]*)" from the (\d+)\w* row in check in$/ do |value, selector, row_number|
  selector_split = selector.downcase.split
  formatted_selector = selector_split.join('_')
  with_in_current_tab do
    nth_allocated_room(row_number) do
      select(value, :from => formatted_selector)
    end
  end
end

Then /^I should see "([\w ]+)" within "(\w+)" from the (\d+)\w* row in check in$/ do |text, selector, row_number|
  formatted_selector = selector.downcase.split.join('_')
  formatted_selector = formatted_selector['.'] ? formatted_selector : "##{formatted_selector}"
  with_in_current_tab do
    nth_allocated_room(row_number) do
      with_scope(formatted_selector) do
        page.should have_content(text)
      end
    end
  end
end

Then /^I should see the "(.+)" is set to "(.+)" in the (\d+)\w* row in check in$/ do |selector, text, row_number|
  formatted_selector = selector.downcase.split.join('_')
  with_in_current_tab do
    nth_allocated_room(row_number) do
      find_field(formatted_selector, visible: true).value.should eq text
    end
  end
end