Given /^I am logged in as "(.+)"$/ do |user_type|
  create_and_sign_in(user_type.split.join('_').downcase.to_sym)
  @hotel_owner ||= @user
  @me ||= @user
end

Given /^I am logged in as hotel owner with one hotel$/ do
  create_and_sign_in(:hotel_owner)
  @hotel = FactoryGirl.create(:hotel, user: @user)
end

Given /^I am logged with email "(.*?)" and password "(.*?)"$/ do |email, password|
  sign_in(email, password)
end

Given /^Logged in user's name "(.*?)"$/ do |name|
  profile = @user.profile
  first_name = name.split(' ').first
  last_name = name.gsub(first_name, '').strip
  profile.update_attributes(first_name: first_name, last_name: last_name)
end

class String
  def underscorize
    split.join('_').downcase
  end
end

def assign_permission_to_current_hotel(permission)
  pg = FactoryGirl.create(:permission_group, hotel: @hotel)
  @user.assignments.create(permission_group: pg, hotel_id: @hotel.id)
  pg.roles << Role.where(name: permission.underscorize).first
  @user.hotel_assignments.create(hotel: @hotel)
  @user.current_branch_id = @hotel.id
  @user.update_assigned_roles
  #p "role_symbols :: #{@user.role_symbols}"
end

private
def sign_in(email, password)
  visit root_path
  fill_in 'user_email', :with => email
  fill_in 'user_password', :with => password
  click_button 'Sign In'
  #p page.driver.status_code
end

def create_and_sign_in(user_type)
  password = '123456'
  @user = FactoryGirl.create(user_type.to_sym, {password: password, password_confirmation: password})
  sign_in(@user.email, password)
end
