require 'rspec/expectations'

Given /^System has a hotel owner with email "(.+)" and password "(.+)"/ do |email, password|
  FactoryGirl.create(:hotel_owner, {email: email, password: password, password_confirmation: password})
end

Given /^I am a user with email "(.+)" and password "(.+)"/ do |email, password|
  @user = FactoryGirl.create(:user, {email: email, password: password, password_confirmation: password})
end

Then /^It should redirect to Branch Create page if I have no Hotel$/ do
  visit new_hotel_path()
end

Given /^I am a "(.*?)" under created hotel with "(.*?)" as default dashboard$/ do |role, dashboard|
   @user.assigned_hotels << @hotel
   assign_permission_to_current_hotel(role)
   @user.settings.default_dashboard = dashboard
end       z