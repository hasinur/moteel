require 'rspec/expectations'
Then /^I fill "(.+?)"  in the "(.+?)" textbox$/ do |value, input_name|
  fill_in(input_name, :with => value)
end

Then /^I empty the "(.+?)" textbox$/ do |input_name|
  fill_in(input_name, :with => '')
end


When /^I attach the file at "([^\"]*)" to "([^\"]*)"$/ do |path, field|
  attach_file(field, path)
  end

When /^I attach a image "([^\"]*)" to "([^\"]*)"$/ do |image_name, field|
  attach_file(field, Rails.root.join('app', 'assets', 'images', image_name))
end

When /^I should see the hotel page with selected layout$/ do
  @hotel = @hotel.reload
  page.should have_css('head title', :text => @hotel.title)
  page.should have_xpath("//link[contains(@href, '/assets/#{@hotel.layout.path}/#{@hotel.layout.name_space}_application.css')]")
end

