require 'rspec/expectations'


Then /^I clicked on add charge link on checked in room in room view$/ do
  visit "/front_desk?lan=en"
  context_menu_id =  page.evaluate_script %Q{ $j('.room_view_box').first().attr('context-menu-id') }
  page.execute_script %Q{ $j('.room_view_box').first().trigger({ type: 'contextmenu'}) }
  context_menu_visibility = page.evaluate_script %Q{ $j("##{context_menu_id}").is(':visible')  }
  message = "expectation failed for add charge context menu in checked in room."
  assert( context_menu_visibility == true, message )

  if context_menu_visibility == true
    textvalue =  page.evaluate_script %Q{ $j("##{context_menu_id}").html()}
    page.should have_link('Add Charge', :href => 'javascript:void(0)')

    find('.cm_add_charge', :text => 'Add Charge').click
    sleep 2
    pop_up_visible =  page.evaluate_script %Q{ $j("form#loading-effect").find('#supplied_type').val()}

    assert(pop_up_visible == 'CheckedInRoom', "Add Charge Pop up window open failed")
  else
    pust "Context menu not found!"
  end

end


Then /^I click on "(.+?)" link from context menu$/ do |link_text|
  #page.evaluate_script %Q{ $j('a.check_in').click()}
  click_link_within "//*[@id=\"room_view_context_2\"]/div[1]/a/div", link_text
  #find("a.check_in", :text => link_text).click
end
