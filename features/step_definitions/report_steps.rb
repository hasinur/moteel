require 'rspec/expectations'

#Background process for CheckIn a room

Given /^I am in reporting dashboard$/ do
  visit "/reports_base?lan=en"
end


Then /^I click on "(.+?)" link in that reporting dashboard$/ do |link_text|
  @tab_count = page.evaluate_script %Q{ $j("#v_tab_content").find('.v_tab_content_item').length  }
  step %(I follow "#{link_text}" within "#reports_base_dashboard_content")
  end
Then /^I click "(.+?)" tab link$/ do |link_text|
  step %(I follow "#{link_text}" within "#v_tabs")
end

And /^I paid amount (\d+) by "(.+?)"$/ do |amount, payment_method|

  payment = FactoryGirl.create(:hotel_service_payment,  :hotel => @hotel, :payment_method => payment_method, :amount => amount )

end
