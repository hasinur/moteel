require 'rspec/expectations'

#Background process for Hotel features

Given /^I have some countries cities and price unit$/ do
  @country = FactoryGirl.create(:country, :title => "Bangladesh")
  @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
  @price_unit = FactoryGirl.create(:price_unit, :title => "BDT")
end

#Scenario: Hotel and Room creation wizard

Given /^I am on "(.+)" page and it has button "(.+)" and "(.+)"$/ do |page_name, btn1, btn2|
  page.should have_link(btn1)
  page.should have_link(btn2)
end

When /^I press "(.+)" link it should go for next steps$/ do |button_text|
  click_link_or_button button_text
end

And /^I am on end of the wizard and should see "(.+)" message and a "(.+)" link$/ do |message, button_text|
  page.should have_content(message)
  page.should have_link(button_text)
end

And /^I have a hotel setup$/ do
  country = FactoryGirl.create(:country, :title => "Bangladesh")
  city = FactoryGirl.create(:city, :title => "Dhaka", :country => country)
  price_unit = FactoryGirl.create(:price_unit, :title => "BDT")
  @hotel_owner ||= FactoryGirl.create(:hotel_owner)
  @hotel = FactoryGirl.create(:hotel, :title => "Sheraton", :time_zone => "Dhaka", :country => country, :city => city, :user => @hotel_owner)
  @room_type = FactoryGirl.create(:room_type, :title => "President suite", :price => 450, :hotel => @hotel, :price_unit => price_unit)
  @room = FactoryGirl.create(:room, :prefix => "PS", :room_number => "100", :price => 450, :user => @hotel_owner, :hotel => @hotel, :room_type => @room_type)
  @guest = FactoryGirl.create(:guest, :agent_user_id => @hotel_owner.id, :city => city.title, :country => country, :email => "guest@moteel.com", :first_name => "guest", :last_name => "name", :date_of_birth => "2000-12-19")
end





