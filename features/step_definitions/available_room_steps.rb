require 'rspec/expectations'


#Scenario: Check In tab open from room view menu on available room
Given /^I am in room view$/ do
  visit "/front_desk?lan=en"
end

Then /^I right click on available room block$/ do
  @context_menu_id =  page.evaluate_script %Q{ $j('.room_view_box').first().attr('context-menu-id') }
  page.execute_script %Q{ $j('.room_view_box').first().trigger({ type: 'contextmenu'}) }
  context_menu_visibility = page.evaluate_script %Q{ $j("##{@context_menu_id}").is(':visible')  }
  message = "expectation failed for available room context menu"
  assert( context_menu_visibility == true, message )
end


Then /^I click on "(.+?)" link in that context menu$/ do |link_text|
  #step %(I follow "#{link_text}" within "##{@context_menu_id}")
  page.execute_script %Q{ $j(".context-menu-item:visible a:contains('#{link_text}')").click() }
end


Then /^I click on "(.+?)" link with url in that context menu$/ do |link_text|
    @tab_count = page.evaluate_script %Q{ $j("#v_tab_content").find('.v_tab_content_item').length  }
    link_href = "javascript:void(0)"
    find("##{@context_menu_id} .context-menu-item .avl_room .context-menu-item-inner", :text => link_text).click
end

Then /^I click on "(.+?)" link with url in that multi select context menu$/ do |link_text|
  @tab_count = page.evaluate_script %Q{ $j("#v_tab_content").find('.v_tab_content_item').length  }
  room_id = @context_menu_id.split("_").last
  link_href = "javascript:void(0)"
  find("#room_view_multi_context_#{room_id} .context-menu-item .avl_room .context-menu-item-inner", :text => link_text).click
end

Then /^I should see A new "(.+?)" Tab open$/ do |tab_name|
    tab_count_after = page.evaluate_script %Q{ $j("#v_tab_content").find('.v_tab_content_item').length  }
    message = "#{tab_name} Tab open failed"
    assert( tab_count_after == (@tab_count+1), message )
end

Then /^I should see A new "(.+?)" pop up open$/ do |pop_up_name|
  pop_up_visibility = page.evaluate_script %Q{$j(".ui-dialog-title:contains('#{pop_up_name}')").parents('.ui-dialog').is(':visible') }
  assert(pop_up_visibility == true)
end

Then /^I should not see "(.+?)" pop up open$/ do |pop_up_name|
  pop_up_visibility = page.evaluate_script %Q{$j(".ui-dialog-title:contains('#{pop_up_name}')").parents('.ui-dialog').is(':visible') }
  assert(pop_up_visibility == false)
end


Then /^I should see A new "(.+?)" link appear this context menu$/ do |link_text|
  room_id = @context_menu_id.split("_").last
  if link_text == "Mark as 'Clean'"
  link_href = "/rooms/mark_as_clean?room_id=#{room_id}"
  find("##{@context_menu_id} .context-menu-item .avl_room #manage_cleaning_#{room_id}", :text => "Mark as 'Clean'")
  elsif  link_text == "Mark as 'Dirty'"
    link_href = "/rooms/mark_as_dirty?room_id=#{room_id}"
    find("##{@context_menu_id} .context-menu-item .avl_room #manage_cleaning_#{room_id}", :text => "Mark as 'Dirty'")
  end
  page.should have_link("#{link_text}", :href =>"#{link_href}")
end
