
Then /^I should be on the "(.+?)" page$/ do |page_name|
  current_path.should == send("#{page_name.downcase.gsub(' ','_')}_path")
end
