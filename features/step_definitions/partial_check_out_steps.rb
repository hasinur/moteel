require 'rspec/expectations'

Then /^I select room "(.*?)" in room list of partial checkout popup$/ do  |room_name|
  room_object = instance_variable_get  "@#{room_name}".to_sym
  chk_box_id = "partial_checkout_allocated_room_id_#{room_object.id}"
  check(chk_box_id)
end


Then /^I select "(.*?)" contain element id "(.*?)" from list of partial checkout popup$/ do  |guest_name, element_id|
  guest_name = guest_name.downcase.sub!(' ', '_')
  guest  = instance_variable_get  "@#{guest_name}".to_sym
  chk_box_id = element_id + "_"+guest.id.to_s
  check(chk_box_id)
end

Then /^I choose "(.*?)" contain element id "(.*?)" from guest list popup$/ do  |guest_name, element_id|
  guest_name = guest_name.downcase.sub!(' ', '_')
  guest  = instance_variable_get  "@#{guest_name}".to_sym
  rdo_btn_id = element_id + "_"+guest.id.to_s
  choose(rdo_btn_id)
end
Then /^I choose "(.*?)" contain element id "(.*?)" from guest list$/ do  |guest_name, element_id|
  guest_object = instance_variable_get  "@#{guest_name}".to_sym
  chk_box_id = element_id + "_"+guest_object.id
  choose(chk_box_id)
end

Then /^I click "(.*?)" link/ do  |selector|
  find("a.#{selector}").click
end
