require 'rspec/expectations'

Given /^I have "(.*?)" department$/ do |name|
  @department = FactoryGirl.create(:department, name: name, branch_id: @hotel.id)
end