require 'rspec/expectations'

#Background process for CheckIn a room

Then /^I should see the "(.+)" tab open in the Check In tab$/ do |tab_text|
  actual_tab_text = page.evaluate_script %Q{ $j('.v_tab_content_item:visible .check_in_tab_wrapper li.ui-tabs-active a').text() }
  assert( tab_text == actual_tab_text )
end