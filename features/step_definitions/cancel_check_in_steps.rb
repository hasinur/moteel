And(/^I have another room_type as "(.*?)"$/) do |var|
  object = FactoryGirl.create(:room_type, hotel: @hotel)
  instance_variable_set("@#{var}".to_sym, object)
end


And(/^I have (\d+) rooms under "(.*?)"$/) do |count, room_type|
  room_type = instance_variable_get "@#{room_type}".to_sym
  count.to_i.times do
    FactoryGirl.create(:room, room_type: room_type)
  end
end

Then /^I right click on room "(.*?)" in room view$/ do  |room_name|
  room_object = instance_variable_get  "@#{room_name}".to_sym
  @context_menu_id =  "room_view_context_#{room_object.id}"
  room_block_id =  "room_view_#{room_object.id}"
  page.execute_script %Q{ $j('##{room_block_id} .room_view_box').trigger({ type: 'contextmenu'}) }
end