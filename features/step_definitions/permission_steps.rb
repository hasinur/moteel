require 'rspec/expectations'

Given /^I have the "(.+)" permission$/ do |permission|
  assign_permission_to_current_hotel(permission.underscorize)
end

Given /^I can go to "(.+)" page$/ do |page|
  page_path = "#{page.underscorize}_path".to_sym
  p page_path
  visit self.send(page_path)
  #p page.driver.status_code
  #page.driver.status_code.should == 200
end

Given /^have permissions setup$/ do
  setup_permissions
end

private
def setup_permissions
  general_roles = (Constants::DEFAULTS_GROUPED_ROLES.values << Constants::GROUP_ROLES).flatten.sort
  general_roles.each do |role_name|
    Role.create(:name => role_name.to_s, :title => role_name.to_s.humanize, :role_type_id => Constants::AGENT_PANEL_ROLE_ID)
  end

  [Constants::HOTEL_OWNER_ROLE_NAME].each do |role_name|
    Role.create(:name => role_name.to_s, :title => role_name.to_s.humanize, :role_type_id => Constants::GENERAL_ROLE_ID)
  end

  Constants::DEFAULT_GROUP_PERMISSIONS_WITH_ROLES.each_pair do |key, value|
    permission_group = PermissionGroup.create(:name => key, :compulsory => true)

    value.each do |role_name|
      role = Role.find_by_name(role_name)
      permission_group.roles << role if role.present?
    end
    permission_group.save
  end
end