require 'rspec/expectations'

Then /^I select "(.+?)" option from "(.+?)" deopdown list/ do |sel_text, ddl|
  select sel_text, :from => ddl
end

Then /^I select "(.+?)" room type from room transfer popup/ do |sel_text|
  select sel_text, :from => 'changed_allocated_room_room_type_id'
end


Then /^I select "(.+?)" room from room transfer popup$/ do |sel_text|
  select sel_text, :from => 'changed_allocated_room_room_id'
end

Then  /^I click on "(.+?)" button$/ do |btn_text |
  find_button(btn_text).click
end

Then /^I should see a successful room transfer message$/ do
  page.should have_content "Operation completed successfully"
end

Then /^I transfer room "(.+?)" to room "(.+?)" in room view$/ do |from_room, to_room|
  room_object = instance_variable_get  "@#{from_room}".to_sym
  context_menu_id =  "room_view_context_#{room_object.id}"
  page.execute_script %Q{ $('.room_view_box').first().trigger({ type: 'contextmenu'}) }

  click_link('Room Transfer')
  transfer_room_object = instance_variable_get  "@#{to_room}".to_sym
  transfer_room_type = transfer_room_object.room_type.title
  transfer_room_title = transfer_room_object.prefix + ' ' + transfer_room_object.room_number

  select transfer_room_type, :from => 'changed_allocated_room_room_type_id'
  sleep 2
  select transfer_room_title, :from => 'changed_allocated_room_room_id'
  find_button('Continue').click
  sleep 2
  find_button('Update').click
  sleep 2
  #page.should have_content "Operation completed successfully"
end

Then /^I should see notification "(.+?)"$/ do |notification|
  page.should have_selector(".toast", :text => notification)
end

Then /^I click on "(.+?)" from context menu$/ do |link_text|
  #click_link(link_text)

    find('.context-menu-item-inner', :text => 'link_text').click

end
