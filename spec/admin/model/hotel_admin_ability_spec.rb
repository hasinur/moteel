require 'colored'
require 'spec_helper'
require 'cancan'
require 'cancan/matchers'

describe HotelAdminAbility do

  # Don't edit hotel admin and branch manager roles, this is for Rspec automated test for active admin

  HOTEL_ADMIN_GROUPED_ROLES = {
      :asset_categories => [:asset_categories_read, :asset_categories_create_update, :asset_categories_delete, :asset_categories_manage_all],
      :cash_counters => [:cash_counters_read, :cash_counters_create_update, :cash_counters_delete, :cash_counters_manage_all],
      :departments => [:departments_read, :departments_create_update, :departments_delete, :departments_manage_all],
      :employee => [:employee_read, :employee_create_update, :employee_delete, :employee_assign_branch, :employee_assign_departments, :employee_assign_cash_counters, :employee_assign_role, :employee_manage_all],
      :fixed_assets => [:fixed_assets_read, :fixed_assets_create_update, :fixed_assets_delete, :fixed_assets_asset_allocation, :fixed_assets_manage_all],
      :groups => [:groups_read, :groups_create_update, :groups_delete, :groups_manage_all],
      :guests => [:guests_read, :guests_create_update, :guests_delete, :guests_manage_all],
      :hotel => [:hotel_read, :branch_admin_home, :hotel_create_update, :hotel_delete, :hotel_manage_layout_theme, :hotel_manage_all],
      :hotel_discount => [:hotel_discount_read, :hotel_discount_create_update, :hotel_discount_delete, :hotel_discount_manage_all],
      :inventory_items => [:inventory_items_read, :inventory_items_create_update, :inventory_items_delete, :inventory_items_manage_all],
      :item_transactions => [:item_transactions_read, :item_transactions_create_update, :item_transactions_delete, :item_transactions_manage_all],
      :markers => [:markers_read, :markers_create_update, :markers_delete, :markers_manage_all],
      :permission_group => [:permission_group_read, :permission_group_create_update, :permission_group_delete, :permission_group_manage_all],
      :rate => [:rate_read, :rate_create_update, :rate_delete, :rate_manage_all],
      :room_type => [:room_type_read, :room_type_create_update, :room_type_delete, :room_type_manage_all],
      :room => [:room_read, :room_create_update, :room_delete, :room_assign_markers, :room_manage_all, :room_housekeeping_status_update],
      :service => [:service_read, :service_create_update, :service_delete, :service_manage_all],
      :task => [:task_read, :task_create_update, :task_delete, :task_manage_all],
      :audit_trails => [:versions_read],
      :wallets => [:wallets_read, :wallets_create_update, :wallets_delete, :wallets_manage_all]
  }

  BRANCH_MANAGER_ROLES = {
      :asset_categories => [:asset_categories_manage_all],
      :cash_counters => [:cash_counters_manage_all],
      :departments => [:departments_manage_all],
      :user => [:employee_manage_all],
      :fixed_assets => [:fixed_assets_manage_all],
      :hotel => [:hotel_manage_all],
      :inventory_items => [:inventory_items_manage_all],
      :item_transactions => [:item_transactions_manage_all],
      :room_type => [:room_type_manage_all],
      :room => [:room_manage_all],
      :service => [:service_manage_all],
      :task => [:task_manage_all]
  }

  BRANCH_MANAGER_ROLES_EXCLUDES = {

      :hotel_discount => [:discount_read, :discount_create_update, :discount_delete, :discount_manage_all],
      :groups => [:groups_read, :groups_create_update, :groups_delete, :groups_manage_all],
      :guests => [:guests_read, :guests_create_update, :guests_delete, :guests_manage_all],
      :markers => [:markers_read, :markers_create_update, :markers_delete, :markers_manage_all],
      :permission_group => [:permission_group_read, :permission_group_create_update, :permission_group_delete, :permission_group_manage_all],
      :rate => [:rate_read, :rate_create_update, :rate_delete, :rate_manage_all],
      :audit_trails => [:versions_read],
      :wallets => [:wallets_read, :wallets_create_update, :wallets_delete, :wallets_manage_all]
  }


  context "check role definition" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @ability = HotelAdminAbility.new(@user)
    end

    it "should check all role are define under hotel admin" do
      HOTEL_ADMIN_GROUPED_ROLES.each do |key, value|
        value.each do |role|
          begin
            @ability.send(role)
            puts "Implemented     >>>> #{role}".blue
          rescue Exception => ex
            puts "Not implemented >>>> #{role}".red.underline
          end
        end
      end
    end
  end


  context "check crud operation for object for Normal User" do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @ability = HotelAdminAbility.new(@user)
    end
    HOTEL_ADMIN_GROUPED_ROLES.each do |key, value|
      value.each do |role|

        it "should check all roles are permitted for Normal User" do
          begin
            @ability.send(role)
          rescue Exception => ex
            puts "Normal User has no privilege/not implemented >>>> #{role}".red.underline
          end

          @ability.instance_eval { @rules }.each do |rule|
            @ability.should_not be_able_to(:read, rule.instance_eval { @subjects })
            @ability.should_not be_able_to(:create, rule.instance_eval { @subjects })
            @ability.should_not be_able_to(:update, rule.instance_eval { @subjects })
            @ability.should_not be_able_to(:destroy, rule.instance_eval { @subjects })
          end

        end
      end
    end
  end

  context "check crud operation for object for Hotel Owner" do
    before(:each) do
      @user = FactoryGirl.create(:user, role: "hotel_owner")
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @ability = HotelAdminAbility.new(@user)
    end
    HOTEL_ADMIN_GROUPED_ROLES.each do |key, value|
      value.each do |role|

        it "should check all roles are permitted for hotel owner" do

          begin
            @ability.send(role)
          rescue Exception => ex
            puts "Hotel Owner has no privilege/not implemented >>>> #{role}".red.underline
          end

          @ability.instance_eval { @rules }.each do |rule|
            @ability.should be_able_to(:read, rule.instance_eval { @subjects })
            @ability.should be_able_to(:create, rule.instance_eval { @subjects })
            @ability.should be_able_to(:update, rule.instance_eval { @subjects })
            @ability.should be_able_to(:destroy, rule.instance_eval { @subjects })
          end

        end
      end
    end
  end

  context "check crud operation for object for Branch Manager" do

    before(:each) do
      @user = FactoryGirl.create(:user, role: "branch_manager")
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @ability = HotelAdminAbility.new(@user)
    end

    BRANCH_MANAGER_ROLES.each do |key, value|
      value.each do |role|
        it "should check all roles are permitted for Branch Manager" do
          begin
            @ability.send(role)
          rescue Exception => ex
            puts "Branch Manager has no privilege/not implemented >>>> #{role}".red.underline
          end

          subject = key.to_s.classify.constantize
          @ability.should be_able_to(:read, subject)
          @ability.should be_able_to(:create, subject)
          @ability.should be_able_to(:update, subject)
          @ability.should be_able_to(:destroy, subject)
        end
      end
    end

    #TODO::Enhance following example block to check which role are not permitted for for Branch Manager

    #BRANCH_MANAGER_ROLES_EXCLUDES.each do |key, value|
    #  key = key.to_s.classify.constantize
    #  value.each do |role|
    #    it "should check all roles not permitted for Branch Manager" do
    #      begin
    #        @ability.send(role)
    #      rescue Exception => ex
    #        puts "Branch Manager has no privilege/not implemented >>>> #{role}".red.underline
    #      end
    #      @ability.instance_eval { @rules }.each do |rule|
    #        @ability.should_not be_able_to(:read, key)
    #        @ability.should_not be_able_to(:create, key)
    #        @ability.should_not be_able_to(:update, key)
    #        @ability.should_not be_able_to(:destroy, key)
    #      end
    #    end
    #  end
    #end

  end
end
