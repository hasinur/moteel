require 'spec_helper'

describe HotelServicePaymentDetail do
  context 'Status and transitions' do
    before(:each) do
      @hspd = FactoryGirl.create(:hotel_service_payment_detail)
    end

    it "should have the initial status as pending" do
      @hspd.status.should == 'pending'
    end

    it "should be able to paid from pending" do
      @hspd.paid!
      @hspd.status.should == 'paid'
    end

    it "should be able to bounce from pending" do
      @hspd.bounce!
      @hspd.status.should == 'bounced'
    end

    it "should be able to pending from bounced" do
      @hspd.status = 'bounced'
      @hspd.pending!
      @hspd.status.should == 'pending'
    end

    it "should be able to cancel from pending" do
      @hspd.cancel!
      @hspd.status.should == 'canceled'
    end

    it "should be able to cancel from bounced" do
      @hspd.status = 'bounced'
      @hspd.cancel!
      @hspd.status.should == 'canceled'
    end
  end
end

