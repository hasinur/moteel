require 'spec_helper'

describe Guest do
  context 'validate the attributes for shomoos' do
    before(:each) do
      @guest = FactoryGirl.build(:guest)
    end

    context 'customer_type' do
      it 'should allow valid data' do
        guest_should_be_valid_with(:customer_type, Constants::CUSTOMER_TYPES.values)
      end

      it 'should not allow invalid data' do
        guest_should_not_be_valid_with(:customer_type, 100)
      end
    end

    context 'id_type' do
      it 'should allow valid data' do
        guest_should_be_valid_with(:id_type, Constants::ID_TYPES.values)
      end

      it 'should not allow invalid data' do
        guest_should_not_be_valid_with(:id_type, 100)
      end
    end

    context 'ids' do
      context 'citizen' do
        before(:each) do
          @guest.customer_type = Constants::CUSTOMER_TYPES[:citizen]
        end
        it 'should not allow with out passport number or moi number' do
          @guest.valid?.should be false
        end
        it 'should allow with moi number' do
          @guest.moi_number = '123'
          @guest.valid?.should be true
        end
        it 'should allow with passport number' do
          @guest.passport_number = '123'
          @guest.valid?.should be true
        end
      end

      context 'foreigner' do
        before(:each) do
          @guest.customer_type = Constants::CUSTOMER_TYPES[:foreigner]
        end
        it 'should not allow with out passport or iqama number' do
          @guest.valid?.should be false
        end
        it 'should allow with moi number' do
          @guest.iqama_number = '123'
          @guest.valid?.should be true
        end
        it 'should allow with passport number' do
          @guest.passport_number = '123'
          @guest.valid?.should be true
        end
      end
    end

  end
end

private
def guest_should_be_valid_with(attr, values)
  values.each do |val|
    @guest.send("#{attr.to_s}=", val)
    @guest.valid?
    @guest.errors.has_key?(attr).should be nil
  end
end

def guest_should_not_be_valid_with(attr, values)
  values = [values] unless values.kind_of?(Array)
  values.each do |val|
    @guest.send("#{attr.to_s}=", val)
    @guest.valid?
    @guest.errors.has_key?(attr).should_not be nil
  end
end