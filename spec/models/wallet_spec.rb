require 'spec_helper'

describe Wallet do
  context 'check wallet balance' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
    end

    it "should create wallet with correct balance." do
      guest = FactoryGirl.create(:guest)
      wallet = guest.create_wallet(hotel_id: @hotel.id, credit_limit: 1000)
      wallet.allowable_amount.should == 1000
    end

    it "should create wallet transaction through reservation line item." do
      guest = FactoryGirl.create(:guest)
      guest.create_wallet(hotel_id: @hotel.id, credit_limit: 1000)

      folio = FactoryGirl.create(:folio, billable: guest, price: 100, payment_type: 'credit')
      rli = FactoryGirl.create(:reservation_line_item, payment: false, sub_total: 1000, folio_id: folio.id)
      rli
    end
  end
end