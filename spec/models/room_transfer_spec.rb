require 'spec_helper'

describe AllocatedRoom do
  context 'transfer current room to selected room.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
    end

    it "should checkout current room and checkin selected room." do
      allocated_room = @check_in.allocated_rooms.first
      end_date = allocated_room.end_date
      transferred_at = Time.zone.now
      change_room = FactoryGirl.create(:room, hotel: @hotel)
      change_room_price = 10.0
      allocated_room_price = 12.0

      @check_in.transfer_room(transferred_at, allocated_room, change_room, change_room_price, allocated_room_price)

      @check_in.reload
      allocated_room.reload

      allocated_room.status.should == "CHECKED_OUT"
      allocated_room.transferred_at.to_s.should == transferred_at.to_s
      allocated_room.end_date.to_s.should == transferred_at.to_s

      @check_in.allocated_rooms.checked_in.collect(&:room_id).should == [change_room.id]
      @check_in.allocated_rooms.checked_in.collect { |ar| ar.start_date.to_s }.should == [transferred_at.to_s]
      @check_in.allocated_rooms.checked_in.collect { |ar| ar.end_date.to_s }.should == [end_date.to_s]
    end

    it "should checkout adjust room price and check in price." do
      allocated_room = @check_in.allocated_rooms.first
      transferred_at = Time.zone.now
      change_room = FactoryGirl.create(:room, price: 250.00, hotel: @hotel)

      @check_in.transfer_room(transferred_at, allocated_room, change_room, change_room.price, allocated_room.price)
      @check_in.reload

      @check_in.total_room_price == change_room.price
    end


    it "should transfer all check in add ons from current room to selected room." do
      check_in_add_on = FactoryGirl.create(:check_in_add_on, check_in: @check_in)
      allocated_room = @check_in.allocated_rooms.first
      transferred_at = Time.zone.now
      change_room = FactoryGirl.create(:room, hotel: @hotel)
      change_room_price = 10.0
      allocated_room_price = 12.0

      @check_in.transfer_room(transferred_at, allocated_room, change_room, change_room_price, allocated_room_price)

      @check_in.reload
      check_in_add_on.reload

      check_in_add_on.room_id.should == change_room.id
      check_in_add_on.task.room.id.should == change_room.id
    end

    it "should change old room status available in ReportItemDailyActivity." do
      allocated_room = @check_in.allocated_rooms.first
      transferred_at = Time.zone.now
      change_room = FactoryGirl.create(:room, hotel: @hotel)
      change_room_price = 10.0
      allocated_room_price = 12.0

      @check_in.transfer_room(transferred_at, allocated_room, change_room, change_room_price, allocated_room_price)

      items = ReportItemDailyActivity.generate_availability(@check_in.hotel_id, allocated_room.room_type_id, allocated_room.start_date, allocated_room.end_date)

      items.collect { |i| i[:status] }.should == ['available']
    end

  end

end

