require 'spec_helper'

describe CheckIn do
  context 'handle room services after checked out' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
    end

    it 'should cancel all incomplete tasks after checked out' do
      FactoryGirl.create(:task, hotel: @hotel, room: @check_in.allocated_rooms.first.room)
      FactoryGirl.create(:task, hotel: @hotel, room: @check_in.allocated_rooms.first.room)
      @check_in.check_out!

      @check_in.allocated_rooms.first.room.tasks.collect { |t| t.status }.should == ["CANCELED", "CANCELED"]
    end

    it 'should not cancel completed tasks after checked out' do
      FactoryGirl.create(:task, hotel: @hotel, room: @check_in.allocated_rooms.first.room, status: "COMPLETED")
      FactoryGirl.create(:task, hotel: @hotel, room: @check_in.allocated_rooms.first.room)
      @check_in.check_out!

      @check_in.allocated_rooms.first.room.tasks.collect { |t| t.status }.should == ["COMPLETED", "CANCELED"]
    end

    it 'should not cancel tasks if checked out being room transfer.' do
      allocated_room = @check_in.allocated_rooms.first
      FactoryGirl.create(:task, hotel: @hotel, room: allocated_room.room)
      FactoryGirl.create(:task, hotel: @hotel, room: allocated_room.room)

      transferred_at = Time.zone.now
      change_room = FactoryGirl.create(:room, hotel: @hotel)
      change_room_price = 10.0
      allocated_room_price = 12.0

      @check_in.transfer_room(transferred_at, allocated_room, change_room, change_room_price, allocated_room_price)

      @check_in.reload

      allocated_room.status.should == "CHECKED_OUT"
      allocated_room.room.tasks.collect { |t| t.status }.should == ["ASSIGNED", "ASSIGNED"]
    end
  end

  context 'handle partial checked out' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @start_date, @end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: @start_date, end_date: @end_date, number_of_rooms: 2)
    end

    it 'should should update associated item ids for current checked in rooms after partial checked out' do
      allocated_room = @check_in.allocated_rooms.last
      @check_in.allocated_rooms.first.check_out!

      items = ReportItemDailyActivity.generate_availability(@check_in.hotel_id, allocated_room.room_type_id, allocated_room.start_date, allocated_room.end_date)

      @check_in.allocated_rooms.first.status.should == "CHECKED_OUT"
      allocated_room.status.should == "CHECKED_IN"
      items.collect { |i| i[:associated_item_ids].size }.sum.should == 0
    end
  end
end
