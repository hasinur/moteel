require 'spec_helper'

describe Rate do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @room = FactoryGirl.create(:room, hotel: @hotel)
  end

  context 'available on days' do
    it 'should only allow valid dates' do
      rate = FactoryGirl.build(:rate)
      rate.available_on_days = ['sunday', 'monday', 'tuesday']
      rate.should be_valid
    end

    it 'should not allow invalid date' do
      rate = FactoryGirl.build(:rate)
      rate.available_on_days = ['Bla bla', 'monday', 'tuesday']
      rate.should be_invalid
    end
  end

  context 'min max days' do
    it 'should only allow valid data' do
      rate = FactoryGirl.build(:rate, min_duration: 1, max_duration: 1)
      rate.should be_valid
    end

    it 'should not allow invalid data' do
      [{min_duration: 0, max_duration: 0},
       {min_duration: 0, max_duration: 1},
       {min_duration: 1, max_duration: 0},
       {min_duration: 2, max_duration: 1}].each do |data|
        rate = FactoryGirl.build(:rate, min_duration: data[:min_duration], max_duration: data[:max_duration])
        rate.should be_invalid
      end
    end
  end

  context 'duration type' do
    it 'should only allow valid data' do
      Constants::DURATION_TYPES.each do |duration_type|
        rate = FactoryGirl.build(:rate, duration_type: duration_type)
        rate.should be_valid
      end
    end

    it 'should not allow invalid data' do
      rate = FactoryGirl.build(:rate, duration_type: 'BlaBla')
      rate.should be_invalid
    end
  end

  context 'effective for' do
    before(:each) do
      create_rate(effective_from: 7.days.ago, effective_until: 7.days.from_now)
      create_rate(effective_from: 2.days.ago, effective_until: 1.days.ago)
      create_rate(active: false, effective_from: 2.days.ago, effective_until: 1.days.from_now)
      create_rate(effective_from: 1.days.from_now, effective_until: 10.days.from_now)
      create_rate(effective_from: 1.days.from_now, effective_until: 10.days.from_now, hotel: FactoryGirl.create(:hotel))

      #dates |-|-|-|-|-|-|-|-#-|-|-|-|-|-|-|-|-|-|
      #r1    |- - - - - - - -#- - - - - - - - - -|
      #r2                |- -#-|
      #r4                  |-#- - - - - - - - - -|
      #r3(inactive)      |- -#-|
      #r5(another hotel)   |-#- - - - - - - - - -|
    end

    it 'should see only effective rates' do
      Rate.effective_with_in(Time.zone.now, 2.days.from_now).count.should == 3
      Rate.effective_with_in(2.days.from_now, 3.days.from_now).count.should == 3
      Rate.effective_with_in(7.days.from_now, 9.days.from_now).count.should == 2
      Rate.effective_with_in(6.days.ago, 9.days.from_now).count.should == 4

      Rate.active.count.should == 4
      Rate.count.should == 5
    end
  end

  context 'applicable_for' do
    it 'should see only applicable rate' do
      create_rate(effective_from: 1.days.ago, effective_until: 14.days.from_now,
                  min_duration: 3, max_duration: 3, available_on_days: [Date.current, 3])
      Rate::ReservationRateSearch.new(@room.id, nil, Time.zone.now, 3.days.from_now).search.size.should == 1
      Rate::ReservationRateSearch.new(@room.id, nil, 1.days.from_now, 4.days.from_now).search.size.should == 1
      Rate::ReservationRateSearch.new(@room.id, nil, Time.zone.now, 2.days.from_now).search.size.should == 0
    end
  end

  context 'applicable on' do
    before(:each) do
      @r1 = create_rate(priority: 1, price: 9, effective_from: 7.days.ago, effective_until: 7.days.from_now, rate_code: 'R1')
      @r2 = create_rate(priority: 2, price: 10, effective_from: 2.days.ago, effective_until: 1.days.from_now, rate_code: 'R2')
      @r3 = create_rate(priority: 2, price: 8, effective_from: Time.zone.now, effective_until: 10.days.from_now, rate_code: 'R3')
      @rates = [@r1.reload, @r2.reload, @r3.reload]

      #dates |-|-|-|-|-|-|-|-#-|-|-|-|-|-|-|-|-|-|
      #r1    |- - - - - - - -#- - - - - - - - - -|
      #r2                |- -#-|
      #r3                    #- - - - - - - - - -|
    end

    it 'should pick the rate with priority and min rate sorted' do
      #ap @rates.collect { |r| {rate_code: r.rate_code, priority: r.priority, price: r.ratables.first.price} }
      Rate.applicable_on(@rates, @room, nil, 1.day.ago).id.should == @r2.id
      Rate.applicable_on(@rates, @room, nil, Time.zone.now).id.should == @r3.id
    end
  end

  context 'available_on_days?' do
    before(:each) do
      @rate = FactoryGirl.build(:effective_rate, effective_from: 7.days.ago, effective_until: 7.days.from_now)
      set_available_on_days(@rate, 1.day.from_now, 3)
    end

    it 'should be available for all days if set' do
      @rate.available_on_days?(1.days.from_now, 3.day.from_now).should == true
    end

    it 'should not be available_on_days which are not set' do
      @rate.available_on_days?(Time.zone.now, 3.day.from_now).should == true
      @rate.available_on_days?(3.days.from_now, 5.day.from_now).should == false
    end
  end

  context 'valid_min_max_duration_for?' do
    before(:each) do
      @rate = FactoryGirl.build(:effective_rate, effective_from: 7.days.ago, effective_until: 7.days.from_now, min_duration: 2, max_duration: 3)
    end

    it 'should be valid_min_max_duration' do
      @rate.valid_min_max_duration_for?(Time.zone.now, 3.day.from_now).should == true
    end

    it 'should not be valid for invalid dates' do
      @rate.valid_min_max_duration_for?(Time.zone.now, 1.day.from_now).should == false
      @rate.valid_min_max_duration_for?(Time.zone.now, 3.day.from_now).should == true
    end
  end

  it 'should calculate the daily applicable_till' do
    effective_until = 3.days.from_now

    @rate = FactoryGirl.build(:effective_rate, effective_from: 7.days.ago, effective_until: effective_until,
                              min_duration: 2, max_duration: 3)
    hotel = @rate.hotel
    applicable_till = effective_until.change(:hour => hotel.ending_hour, :min => hotel.ending_minute, :sec => 0)
    @rate.applicable_till(Time.zone.now, 5.days.from_now).should == applicable_till
  end

  #it 'should calculate the hourly applicable_till' do
  #  effective_until = 3.days.from_now
  #
  #  @rate = FactoryGirl.build(:effective_hourly_rate, effective_from: 7.days.ago, effective_until: effective_until,
  #                            min_duration: 2, max_duration: 3)
  #  hotel = @rate.hotel
  #  applicable_till = effective_until.change(:hour => hotel.ending_hour, :min => hotel.ending_minute, :sec => 0)
  #  @rate.applicable_till(Time.zone.now, 5.days.from_now).should == applicable_till
  #end

  context 'calculation of duration' do
    before(:each) do
      @rate = FactoryGirl.build(:effective_rate, min_duration: 2, max_duration: 3)
    end

    it 'should calculate the hourly duration' do
      @rate.duration_type = Constants::HOURLY_DURATION_TYPE
      t1 = Time.zone.now
      t2 = t1 + 3.days + 5.hours
      total_hours = (3.days + 5.hours) / 3600
      @rate.calculate_duration(t1, t2).should == total_hours
    end

    it 'should calculate the daily duration' do
      @rate.duration_type = Constants::DAILY_DURATION_TYPE
      t1 = Time.zone.now
      t2 = t1 + 3.days + 5.hours
      @rate.calculate_duration(t1, t2).should == 3
    end
  end

  context 'calculation of price' do
    context 'without over charge' do
      it 'should calculate the daily price' do
        rate = create_rate(min_duration: 2, max_duration: 3, price: 100, overcharge_per_hour: 20)
        t1 = Time.zone.now
        t2 = t1.change(:hour => @hotel.ending_hour, :min => @hotel.ending_minute, :sec => 0) + 2.days + 3.hours
        rate.calculate_price_for(@room, t1, t2).should == 200
      end

      it 'should calculate the hourly price' do
        rate = create_rate(min_duration: 200, max_duration: 300, price: 10, overcharge_per_hour: 20, duration_type: Constants::HOURLY_DURATION_TYPE)
        t1 = Time.zone.now
        t2 = t1 + 2.days + 5.hours
        rate.calculate_price_for(@room, t1, t2).should == (24 * 2 + 5) * 10
      end
    end

    context 'with over charge' do
      it 'should calculate the daily price with overcharge' do
        rate = create_rate(min_duration: 2, max_duration: 3, price: 100, overcharge_per_hour: 20)
        t1 = Time.zone.now
        t2 = t1.change(:hour => @hotel.ending_hour, :min => @hotel.ending_minute, :sec => 0) + 3.days + 5.hours
        rate.calculate_price_for(@room, t1, t2).should == 300
      end

      it 'should calculate the hourly price with overcharge' do
        rate = create_rate(min_duration: (24 * 3), max_duration: (24 * 10), price: 10, overcharge_per_hour: 20, duration_type: Constants::HOURLY_DURATION_TYPE)
        t1 = Time.zone.now
        t2 = t1 + 3.days + 5.hours
        rate.calculate_price_for(@room, t1, t2).should == ((24 * 3 + 5) * 10)
      end
    end
  end
end

private
def set_available_on_days(rate, from_date, number_of_days)
  rate.available_on_days = []
  from_date.to_date.upto((number_of_days-1).days.from_now.to_date) do |date|
    rate.available_on_days << date.strftime('%A').downcase
  end
end

def create_rate(options)
  effective_from = options[:effective_from] || 1.month.ago
  effective_until = options[:effective_until] || 2.years.from_now
  options[:available_on_days] ||= [Date.current, 7]

  price = options[:price] || 10
  overcharge_per_hour = options[:overcharge_per_hour] || 1
  duration_type = options.has_key?(:duration_type) ? options[:duration_type] : Constants::DAILY_DURATION_TYPE
  active = options.has_key?(:active) ? options[:active] : true
  hotel = options.has_key?(:hotel) ? options[:hotel] : @hotel
  rate = FactoryGirl.build(:rate, active: active,
                           rate_code: options[:rate_code] || 'Rate',
                           effective_from: effective_from, effective_until: effective_until,
                           hotel: hotel,
                           duration_type: duration_type,
                           min_duration: options[:min_duration],
                           max_duration: options[:max_duration],
                           priority: options[:priority] || 0)
  set_available_on_days(rate, options[:available_on_days][0], options[:available_on_days][1]) if options[:available_on_days]
  rate.save!
  @room.ratables.create!(rate_id: rate.id, price: price, overcharge_per_hour: overcharge_per_hour)
  rate
end