require 'spec_helper'

describe ServiceCharge do
  context 'create charges against default folios or newly created folios, based on allow create different folios, using default wallet credit limit 1000 unit.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @billing_info = FactoryGirl.create(:billing_information)
      @wallet = FactoryGirl.create(:wallet, receivable: @billing_info.billable, hotel_id: @hotel.id, credit_limit: 1000)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id,
                                     billing_information: @billing_info, default_payment_type: "credit")
    end

    it "should assign reservation line item to default folio, for credit type folio, department does not allow different folios." do
      room_charge = @check_in.reservation_line_items.last.sub_total

      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)
      amount = 1000 - room_charge
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: amount)

      @check_in.reload

      @check_in.folios.size.should == 1
      @check_in.folios.first.payment_type.should == 'credit'
      @check_in.reservation_line_items.size.should == 2
      @check_in.reservation_line_items.first.folio.should == @check_in.reservation_line_items.last.folio
    end

    it "should assign reservation line item to newly created folio, for credit type folio, department allow different folios." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 250)

      @check_in.reload

      @check_in.folios.size.should == 2
      @check_in.reservation_line_items.size.should == 2
      @check_in.reservation_line_items.first.folio.should_not == @check_in.reservation_line_items.last.folio
    end
  end

  context 'create service charges against default folios or newly created folios, based on wallet limit, using default wallet credit limit 1000 unit.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @billing_info = FactoryGirl.create(:billing_information)
      @wallet = FactoryGirl.create(:wallet, receivable: @billing_info.billable, hotel_id: @hotel.id, credit_limit: 1000)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id,
                                     billing_information: @billing_info, default_payment_type: "credit")
    end

    it "should create cash type folio if allowable amount in wallet exceeds." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)

      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 750)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 850)

      @check_in.reload

      #ap @check_in.reservation_line_items.collect { |rl| ["rsl.id#{rl.id}, rsl.sub_total::#{rl.sub_total}, folio.id::#{rl.folio.id}, folio.payment_type:: #{rl.folio.payment_type}"] }
      #
      #@check_in.reservation_line_items.each do |rsli|
      #  ap rsli.folio.billable.wallet
      #  ap rsli.folio.billable.wallet.wallet_transactions
      #end

      @check_in.folios.size.should == 2
      @check_in.reservation_line_items.size.should == 3
      @check_in.reservation_line_items.first.folio.should_not == @check_in.reservation_line_items.last.folio
      @check_in.reservation_line_items.last.folio.payment_type.should == "cash"
    end

    it "should associate to credit folio if allowable amount in wallet permits." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 850)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 145)

      @check_in.reload
      ap @check_in.reservation_line_items.collect { |rl| ["rsl.id#{rl.id}, rsl.sub_total::#{rl.sub_total}, folio.id::#{rl.folio.id}, folio.payment_type:: #{rl.folio.payment_type}"] }

      @check_in.folios.size.should == 1
      @check_in.reservation_line_items.last.folio.payment_type.should == "credit"
    end

    it "should create new folio for all service charge, department allow different folios." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 850)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: 450)

      @check_in.reload

      @check_in.folios.size.should == 3
      @check_in.reservation_line_items.size.should == 3
      @check_in.reservation_line_items.last.folio.payment_type.should == "cash"
    end
  end

  context 'observe wallet transaction, using default wallet credit limit 1000 unit.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @billing_info = FactoryGirl.create(:billing_information)
      @wallet = FactoryGirl.create(:wallet, credit_limit: 1000, receivable: @billing_info.billable, hotel_id: @hotel.id)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id,
                                     billing_information: @billing_info, default_payment_type: "credit")
    end

    it "should reflect wallet and wallet transaction, department does not allow different folios." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)
      amount = 850
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: amount)

      @check_in.reload

      rsli = @check_in.reservation_line_items.last
      walt = rsli.folio.billable.wallet
      walt_trx = walt.wallet_transactions.last

      @check_in.folios.size.should == 1
      @check_in.reservation_line_items.size.should == 2
      @check_in.reservation_line_items.first.folio.should == @check_in.reservation_line_items.last.folio

      walt.credit_limit.to_f.should == @wallet.credit_limit.to_f
      walt.balance.to_f.should == walt_trx.current_balance

      walt_trx.current_balance.to_f.should == @wallet.balance.to_f - amount.to_f
      walt_trx.debit.to_f.should == 0
      walt_trx.credit.to_f.should == amount.to_f

    end

    it "should associate to existing credit folio if wallet permits." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)
      amount = 850
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: amount)

      @check_in.reload

      rsli = @check_in.reservation_line_items.last
      walt = rsli.folio.billable.wallet
      walt_trx = walt.wallet_transactions.last

      @check_in.folios.size.should == 1
      @check_in.reservation_line_items.size.should == 2
      @check_in.reservation_line_items.first.folio.should == @check_in.reservation_line_items.last.folio

      walt.credit_limit.to_f.should == @wallet.credit_limit.to_f
      walt.balance.to_f.should == walt_trx.current_balance

      walt_trx.current_balance.to_f.should == @wallet.balance.to_f - amount.to_f
      walt_trx.debit.to_f.should == 0
      walt_trx.credit.to_f.should == amount.to_f

    end

  end

  context 'service charges for cash payment type checkin and wallet credit limit 1000.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @billing_info = FactoryGirl.create(:billing_information)
      @wallet = FactoryGirl.create(:wallet, receivable: @billing_info.billable, hotel_id: @hotel.id, credit_limit: 1000)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id,
                                     billing_information: @billing_info, default_payment_type: "cash")
    end

    it "should not reflect wallet, for cash type reservation." do
      room_charge = @check_in.reservation_line_items.last.sub_total

      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)
      amount = 1000 - room_charge
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: amount)

      @check_in.reload

      @check_in.reservation_line_items.last.wallet_transactions.size.should == 0
    end

  end

end
private
def create_charge_with(options)
  sv = FactoryGirl.create(:service, hotel: @hotel, department: options[:department])
  FactoryGirl.create(:service_charge, hotel: @hotel, allocated_room: options[:allocated_room], amount: options[:amount], service: sv)
end