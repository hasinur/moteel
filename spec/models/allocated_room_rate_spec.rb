require 'spec_helper'

describe AllocatedRoomRate do
  context 'relationships' do
    it { should belong_to(:rate) }
    it { should belong_to(:allocated_room) }
  end

  context 'validations' do
    it { should validate_presence_of(:rate_id) }
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:end_date) }
    it { should validate_presence_of(:price) }

    #overlap validation is not working because of the nested resource deletion
    #it 'should not allow dates to be overlapped with same allocated room id' do
    #  start_date = Time.now
    #  end_date = 3.days.from_now
    #  allocated_room = FactoryGirl.create(:allocated_room)
    #  existing_ar_rate = FactoryGirl.create(:allocated_room_rate, allocated_room: allocated_room, start_date: start_date, end_date: end_date)
    #
    #  invalid_ar_rate = FactoryGirl.build(:allocated_room_rate, start_date: 1.day.from_now, end_date: 4.days.from_now)
    #  invalid_ar_rate.should_not be_valid
    #  invalid_ar_rate = FactoryGirl.build(:allocated_room_rate, start_date: 1.day.ago, end_date: 1.day.from_now)
    #  invalid_ar_rate.should_not be_valid
    #  invalid_ar_rate = FactoryGirl.build(:allocated_room_rate, start_date: 1.day.ago, end_date: 5.days.from_now)
    #  invalid_ar_rate.should_not be_valid
    #  invalid_ar_rate = FactoryGirl.build(:allocated_room_rate, start_date: 1.day.from_now, end_date: 2.days.from_now)
    #  invalid_ar_rate.should_not be_valid
    #end
  end

  context 'split' do
    it 'should split into allocated room rates' do
      @hotel = FactoryGirl.create(:hotel)
      @room = FactoryGirl.create(:room, hotel: @hotel)

      @r1 = create_rate(priority: 0, price: 10, effective_from: 7.days.ago, effective_until: 7.days.from_now, rate_code: 'Regular', min_duration: 1)
      @r2 = create_rate(priority: 5, price: 15, effective_from: 2.days.ago, effective_until: 10.days.from_now,
                        rate_code: 'Week end rate', min_duration: 1, available_on_days: [Date.current, 2])
      @r3 = create_rate(priority: 5, price: 9, effective_from: 1.days.from_now, effective_until: 10.days.from_now, rate_code: 'Hajj rate')

      #dates |-|-|-|-|-|-|-|-#-|-|-|-|-|-|-|-|-|-|
      #r1    |- - - - - - - -#- - - - - - - - - -|
      #r2                    #- -|
      #r3                    #-|- - - - - - - - -|

      start_date = Time.zone.now.start_of(@hotel)
      end_date = 3.days.from_now.end_of(@hotel)
      allocated_room_rates = AllocatedRoomRate.split(@room, nil, start_date, end_date)
      #ap allocated_room_rates.collect { |arRate| {rate_code: arRate.rate_code, price: arRate.price, start_date: arRate.start_date, end_date: arRate.end_date} }

      allocated_room_rates[0].price.should == 15
      allocated_room_rates[1].price.should == 18

      allocated_room_rates[0].rate_id.should == @r2.id
      allocated_room_rates[1].rate_id.should == @r3.id
    end

    it 'should split into allocated room rates for online entity' do
      @hotel = FactoryGirl.create(:hotel)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel)

      @r1 = create_rate(priority: 0, price: 10, effective_from: 7.days.ago, effective_until: 7.days.from_now, rate_code: 'Regular', min_duration: 1,
                        publish_online: true, ratable_entity: @room_type)
      @r2 = create_rate(priority: 5, price: 15, effective_from: 2.days.ago, effective_until: 10.days.from_now,
                        publish_online: true, rate_code: 'Week end rate', min_duration: 1, available_on_days: [Date.current, 2], ratable_entity: @room_type)
      @r3 = create_rate(priority: 5, price: 9, effective_from: 1.days.from_now, effective_until: 10.days.from_now,
                        publish_online: true, rate_code: 'Hajj rate', ratable_entity: @room_type)

      @r4 = create_rate(priority: 6, price: 9, effective_from: 1.days.from_now, effective_until: 10.days.from_now,
                        publish_online: false, rate_code: 'Hajj rate(Offline)', ratable_entity: @room_type)

      #dates |-|-|-|-|-|-|-|-#-|-|-|-|-|-|-|-|-|-|
      #r1    |- - - - - - - -#- - - - - - - - - -|
      #r2                    #- -|
      #r3                    #-|- - - - - - - - -|

      end_date = 3.days.from_now.change(:hour => @hotel.ending_hour, :min => @hotel.ending_minute, :sec => 0)
      allocated_room_rates = AllocatedRoomRate.split(@room_type, nil, Time.zone.now, end_date)
      #ap allocated_room_rates.collect { |arRate| {rate_code: arRate.rate_code, price: arRate.price, start_date: arRate.start_date, end_date: arRate.end_date} }

      allocated_room_rates[0].price.should == 15
      allocated_room_rates[1].price.should == 18

      allocated_room_rates[0].rate_id.should == @r2.id
      allocated_room_rates[1].rate_id.should == @r3.id
    end
  end
end

private
def set_available_on_days(rate, from_date, number_of_days)
  rate.available_on_days = []
  from_date.to_date.upto((number_of_days-1).days.from_now.to_date) do |date|
    rate.available_on_days << date.strftime('%A').downcase
  end
end

def create_rate(options)
  effective_from = options[:effective_from] || 1.month.ago
  effective_until = options[:effective_until] || 2.years.from_now
  options[:available_on_days] ||= [Date.current, 7]

  price = options[:price] || 10
  overcharge_per_hour = options[:overcharge_per_hour] || 1
  duration_type = options.has_key?(:duration_type) ? options[:duration_type] : Constants::DAILY_DURATION_TYPE
  active = options.has_key?(:active) ? options[:active] : true
  hotel = options.has_key?(:hotel) ? options[:hotel] : @hotel
  rate = FactoryGirl.build(:rate, active: active,
                           rate_code: options[:rate_code],
                           effective_from: effective_from.beginning_of_day, effective_until: effective_until.end_of_day,
                           hotel: hotel,
                           duration_type: duration_type,
                           min_duration: options[:min_duration],
                           max_duration: options[:max_duration],
                           priority: options[:priority] || 0,
                           publish_online: options[:publish_online])
  set_available_on_days(rate, options[:available_on_days][0], options[:available_on_days][1]) if options[:available_on_days]
  rate.save!
  ratable_entity = options[:ratable_entity] || @room
  ratable_entity.ratables.create!(rate_id: rate.id, price: price, overcharge_per_hour: overcharge_per_hour)
  rate
end