require 'spec_helper'

describe Invoice do
  before(:each) do

  end
  context 'create invoice' do
    it 'should take folio_ids update those folios under created invoice' do
      folio = FactoryGirl.create(:folio)

      invoice = FactoryGirl.build(:invoice)
      invoice.folio_ids = [folio.id]
      invoice.save
      folio.reload

      folio.invoice_id.should == invoice.id
    end

    it 'payment type of all folios should be same' do
      folio1 = FactoryGirl.create(:folio, :payment_type => Constants::PAYMENT_TYPE_CASH )
      folio2 = FactoryGirl.create(:folio, :payment_type => Constants::PAYMENT_TYPE_CREDIT )

      if folio1.payment_type == folio2.payment_type
        invoice = FactoryGirl.build(:invoice)
        invoice.folio_ids = [folio1.id, folio2.id]
        invoice.save
      end
      folio1.invoice_id.should == folio2.id
    end

    #it 'should not allow folio to remove from invoice' do
    #  folio = FactoryGirl.create(:folio)
    #  invoice = FactoryGirl.build(:invoice)
    #  invoice.folio_ids = [folio.id]
    #  invoice.save
    #
    #  invoice.folio_ids = []
    #  invoice.save
    #  folio.valid?.should == false
    #end



  end

end
