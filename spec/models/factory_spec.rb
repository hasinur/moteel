require 'spec_helper'

describe CheckIn do
  it "should be able to create factory" do

    @hotel = FactoryGirl.create(:hotel)
    #room = FactoryGirl.create(:room, hotel: hotel)
    #room_type = room.room_type
    #rate = FactoryGirl.build(:effective_rate, rate_code: "Standard rate for #{room_type.title}", hotel: hotel)
    #
    #rate.available_on_days = []
    #Date.today.upto(6.days.from_now.to_date) do |date|
    #  rate.available_on_days << date.strftime('%A').downcase
    #end
    #
    #rate.save!
    #room_type.ratables.create!(rate_id: rate.id, price: 200, overcharge_per_hour: 0)
    #
    #check_in = FactoryGirl.create(:check_in, hotel: hotel, rooms: [room])
    #
    ##hsp = FactoryGirl.create(:hotel_service_payment)
    #ap check_in.allocated_rooms
    #ap check_in.allocated_rooms.first.allocated_room_rates

    #cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
    #room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
    #room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
    #check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
    #FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 325,allow_edit:true, cash_counter: cash_counter, payment_method:'cheque')
    #ap CashCounterTransaction.all
    #hsp1 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 250,allow_edit:true,payment_method:"cheque")
    #hsp2 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2, hotel: @hotel, amount: 350,allow_edit:true,payment_method:"cheque")

    room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Standard')
    room_type2 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Delux')
    room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type1, room_number: 'std1' )
    room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type2, room_number: 'std2' )
    room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type1, room_number: 'std3' )
    room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type2, room_number: 'dlx1' )
    room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type1, room_number: 'dlx2' )
    room6 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type2, room_number: 'dlx3' )
    @guest1 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Rahim')
    @guest2 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Karim')
    @guest3 = FactoryGirl.create(:guest, first_name:'Mrs', last_name: 'Rahim')
    @guest4 = FactoryGirl.create(:guest, first_name:'Baby', last_name: 'Rahim')

    @discount1 = FactoryGirl.create(:hotel_discount, hotel: @hotel, fixed: 'fixed', code: 'winter', discount_value: 20)

    @check_in1 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest1), hotel: @hotel,
                                    hotel_discount: @discount1, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room2], guests: [@guest1])
    @check_in2 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest2), hotel: @hotel,
                                    hotel_discount: @discount1, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room6])
    ap @check_in1.allocated_rooms.first.allocated_room_guests.collect(&:name)
    @check_in1.allocated_rooms.first.allocated_room_guests.create(guest: @guest3, main_guest_id:@guest1.id)
    @check_in1.allocated_rooms.first.allocated_room_guests.create(guest: @guest4, main_guest_id:@guest2.id)
    ap @check_in1.allocated_rooms.first.allocated_room_guests.collect(&:name)

    #puts "Dependent Guest of Checkin 1 ======== #{@check_in1.allocated_rooms.first.allocated_room_guests.inspect}"

  end
end