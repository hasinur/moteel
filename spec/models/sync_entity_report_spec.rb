require 'spec_helper'

describe SyncEntityReport do
  context 'sync items creation' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
    end

    it 'should create one item' do
      SyncEntityReport.count.should == 1
    end
  end
end
