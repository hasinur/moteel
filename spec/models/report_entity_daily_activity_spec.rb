require 'spec_helper'

describe ReportEntityDailyActivity do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @start_date, @end_date = Time.zone.now, 2.days.from_now
    room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100)
    room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500)
    room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 50)
    @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: @start_date, end_date: @end_date, rooms: [room1, room2, room3])
    ReportEntityDailyActivity.sync_activity(@hotel.id, @start_date, @end_date)
  end

  context 'todays activity count' do
    it 'should produce correct activity counts' do
      todays_activity_count = ReportEntityDailyActivity.where(:date => Date.current, :entity_id => @hotel.id).first[:todays_activity_count]
      todays_activity_count['todays_checkin'].should == 1
      todays_activity_count['todays_checkin_rooms'].should == 3
    end
    it 'should produce correct activity counts after checkout' do
      @check_in.check_out!
      ReportEntityDailyActivity.sync_activity(@hotel.id, @start_date, @end_date)
      todays_activity_count = ReportEntityDailyActivity.where(:date => Date.current, :entity_id => @hotel.id).first[:todays_activity_count]
      todays_activity_count['todays_checkin'].should == 0
      todays_activity_count['todays_checkin_rooms'].should == 0
      todays_activity_count['todays_checkout_rooms'].should == 3
    end
  end

end
