require 'spec_helper'

describe Folio do

  context 'create folio based on given attributes.' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)
    end

    it "should create a new folio, using checkin attributes if not define." do
      folio = FactoryGirl.create(:folio, manual_folio_creation: true)
      dept = folio.department

      folio.name.should == "#{dept.name}1".upcase
      folio.is_default.should == false
      folio.payment_type.should == "cash"
      folio.billable.should == folio.check_in.billing_information.billable
    end

    it "should create a new folio, using define attributes" do
      dept = FactoryGirl.create(:department, branch: @hotel)
      billable = FactoryGirl.create(:guest)
      FactoryGirl.create(:wallet, receivable: billable, hotel_id: @hotel.id, credit_limit: 1000)
      folio = FactoryGirl.create(:folio, department: dept, billable: billable, payment_type: "credit", manual_folio_creation: true)

      folio.name.should == "#{dept.name}1".upcase
      folio.payment_type.should == "credit"
      folio.department.should == dept
      folio.billable.should == billable
    end

    it "should create credit type folio if user has wallet" do
      cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      billing_info = FactoryGirl.create(:billing_information)
      wallet = FactoryGirl.create(:wallet, receivable: billing_info.billable, hotel_id: @hotel.id)
      check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: cash_counter.id,
                                    billing_information: billing_info, default_payment_type: "cash")

      f2 = FactoryGirl.create(:folio, payment_type: Constants::PAYMENT_TYPE_CREDIT, check_in: check_in, department: dept)
      check_in.folios.size.should == 2
    end

    it "should not create credit type folio if user has no wallet" do
      cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      billing_info = FactoryGirl.create(:billing_information)

      check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: cash_counter.id,
                                    billing_information: billing_info, default_payment_type: "cash")

      f2 = FactoryGirl.build(:folio, payment_type: Constants::PAYMENT_TYPE_CREDIT, check_in: check_in, department: dept)

      f2.should_not be_valid
    end

  end

  context 'edit folios based on invoiced.' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)
    end
    it "should edit folio, invoice not present." do
      folio = FactoryGirl.create(:folio, manual_folio_creation: true)
      FactoryGirl.create(:wallet, receivable: folio.billable, hotel_id: @hotel.id, credit_limit: 1000)
      folio = folio.reload
      folio.update_attributes!(payment_type: "credit")
      folio.update_attributes(payment_type: "credit").should == true
    end

    it "should not edit folio, if already invoiced." do
      invoice = FactoryGirl.create(:invoice)
      invoice.folios.first.update_attributes(payment_type: "credit").should == false
    end
  end

  context 'add service to separate credit folio for group checkin.' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:group_type_billing_information)
      @wallet = FactoryGirl.create(:wallet, balance: 0, credit_limit: 1000, receivable: @billing_information.billable)

      @room = FactoryGirl.create(:room, rate_price: 700, hotel: @hotel)
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, rooms: [@room], default_payment_type: Constants::PAYMENT_TYPE_CREDIT, cash_counter_id: @cash_counter.id, billing_information: @billing_information)
    end

    it "should  create a credit type folio for a group which has credit limit " do
      @check_in.folios.count.should == 1
      @check_in.folios.first.payment_type.should == Constants::PAYMENT_TYPE_CREDIT
      allocated_room = @check_in.allocated_rooms.first
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      sv = FactoryGirl.create(:service, hotel: @hotel, department: dept)
      service_charge = FactoryGirl.create(:service_charge, hotel: @hotel, allocated_room: allocated_room, amount: 100, service: sv, bill_to_id: @billing_information.billable_id, bill_to_type: @billing_information.billable_type)
      @check_in.folios.count.should == 2
      service_charge.reservation_line_item.folio.payment_type.should == Constants::PAYMENT_TYPE_CREDIT
    end
  end

  context 'credit type checkin exced credit limit' do
    before(:each) do

      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:group_type_billing_information)
      @wallet = FactoryGirl.create(:wallet, balance: 0, credit_limit: 60, receivable: @billing_information.billable)
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)

      @room = FactoryGirl.create(:room, rate_price: 100, hotel: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, rooms: [@room], default_payment_type: Constants::PAYMENT_TYPE_CREDIT, cash_counter_id: @cash_counter.id, billing_information: @billing_information)
    end

    it "should  create a credit type folio when service charge in credit limit" do
      @check_in.folios.count.should == 1
      @check_in.folios.first.payment_type.should == Constants::PAYMENT_TYPE_CASH
      allocated_room = @check_in.allocated_rooms.first
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      sv = FactoryGirl.create(:service, hotel: @hotel, department: dept)
      service_charge = FactoryGirl.create(:service_charge, hotel: @hotel, allocated_room: allocated_room, amount: 50, service: sv, bill_to_id: @billing_information.billable_id, bill_to_type: @billing_information.billable_type)
      @check_in.folios.count.should == 2
      service_charge.reservation_line_item.folio.payment_type.should == Constants::PAYMENT_TYPE_CREDIT
    end
  end


  context 'create folio based on presence of department and billing person.' do
    before(:each) do

      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)

      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id, billing_information: @billing_information)
    end

    it "should create a new folio, with different department (create_separate_folio: true) and default billing person." do
      @check_in.reload
      @check_in.folios.count.should == 1
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      Folio.find_or_create_folio(@check_in, dept, 100, nil, nil, nil)
      @check_in.reload.folios.count.should == 2
    end

    it "should not create a new folio, with same department(create_separate_folio: nil) and same billing person." do
      @check_in.reload
      @check_in.folios.count.should == 1
      dept = FactoryGirl.create(:department, branch: @hotel)
      Folio.find_or_create_folio(@check_in, dept, 100, nil, nil, nil)
      @check_in.reload.folios.count.should == 1
    end

    it "should create a new folio, with same department and different billing person." do
      @check_in.reload
      bill_to = FactoryGirl.create(:billing_information)
      bill_to_2 = FactoryGirl.create(:billing_information)
      dept = FactoryGirl.create(:department, branch: @hotel)
      FactoryGirl.create(:folio, check_in: @check_in, billable: bill_to.billable, department: dept)
      return_folios = Folio.find_or_create_folio(@check_in, dept, 100, nil, bill_to_2.billable_id, bill_to_2.billable_type)
      @check_in.folios.count.should == 3
    end

    it "should create a new folio, with different department and same billing person." do
      @check_in.reload
      bill_to = FactoryGirl.create(:billing_information)
      dept = FactoryGirl.create(:department, branch: @hotel)
      dept_2 = FactoryGirl.create(:department, branch: @hotel)
      FactoryGirl.create(:folio, check_in: @check_in, billable: bill_to.billable, department: dept)

      return_folios = Folio.find_or_create_folio(@check_in, dept_2, 100, nil, bill_to.billable_id, bill_to.billable_type)

      @check_in.folios.count.should == 3
    end

    it "should create a new folio, with different department and different billing person." do
      @check_in.reload
      bill_to = FactoryGirl.create(:billing_information)
      bill_to_2 = FactoryGirl.create(:billing_information)
      dept = FactoryGirl.create(:department, branch: @hotel)
      dept_2 = FactoryGirl.create(:department, branch: @hotel)
      FactoryGirl.create(:folio, check_in: @check_in, billable: bill_to.billable, department: dept)

      Folio.find_or_create_folio(@check_in, dept_2, 100, nil, bill_to_2.billable_id, bill_to_2.billable_type)
      @check_in.folios.count.should == 3
    end
  end


  context 'transfer folios.' do
    before(:each) do

      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)

      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id, billing_information: @billing_information)
    end

    it "should move folio, means assign a reservation line item to another folio under same department." do
      @check_in.reload
      dept_1 = FactoryGirl.create(:department, branch: @hotel)

      f1 = FactoryGirl.create(:folio, check_in: @check_in, department: dept_1)
      f2 = FactoryGirl.create(:folio, check_in: @check_in, department: dept_1)

      line_item = FactoryGirl.create(:reservation_line_item, folio_id: f1.id)
      FactoryGirl.create(:reservation_line_item, folio_id: f1.id)
      FactoryGirl.create(:reservation_line_item, folio_id: f2.id)

      f1.move_rsl_item_to_another_folio(line_item, f2)
      f2.reload.reservation_line_items.size.should == 2
    end

    it "should reflect to wallet ledger on moving items from credit type folio to cash type folio and vice versa." do
      wallet = FactoryGirl.create(:wallet, balance: 0, credit_limit: 10000, receivable: @billing_information.billable)
      @check_in.reload
      dept_1 = FactoryGirl.create(:department, branch: @hotel)
      f1 = FactoryGirl.create(:folio, payment_type: Constants::PAYMENT_TYPE_CREDIT, check_in: @check_in, department: dept_1)
      f2 = FactoryGirl.create(:folio, check_in: @check_in, department: dept_1)
      line_item = FactoryGirl.create(:reservation_line_item, sub_total: 100, folio_id: f1.id)
      f1.reload.bill.should == 100
      wallet.reload.balance.should == -100
      f1.move_rsl_item_to_another_folio(line_item, f2)
      f1.reload.bill.should == 0
      f2.reload.bill.should == 100
      wallet.reload.balance.should == 0
      f2.move_rsl_item_to_another_folio(line_item, f1)
      f2.reload.bill.should == 0
      f1.reload.bill.should == 100
      wallet.reload.balance.should == 100
    end

    it "should reflect to wallet ledger on deleting credit folio." do
      wallet = FactoryGirl.create(:wallet, balance: 0, credit_limit: 10000, receivable: @billing_information.billable)
      @check_in.reload
      dept_1 = FactoryGirl.create(:department, branch: @hotel)
      f1 = FactoryGirl.create(:folio, payment_type: Constants::PAYMENT_TYPE_CREDIT, check_in: @check_in, department: dept_1)
      line_item = FactoryGirl.create(:reservation_line_item, sub_total: 100, folio_id: f1.id)
      f1.reload.bill.should == 100
      wallet.reload.balance.should == -100
      f1.destroy
      wallet.reload.balance.should == 0
    end

    it "should not move folio, means can't assign a reservation line item to another folio under different department with create_separate_folio." do
      @check_in.reload
      dept_1 = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)

      f1 = FactoryGirl.create(:folio, check_in: @check_in)
      f2 = FactoryGirl.create(:folio, check_in: @check_in, department: dept_1)

      line_item = FactoryGirl.create(:reservation_line_item, folio_id: f1.id)
      FactoryGirl.create(:reservation_line_item, folio_id: f1.id)
      FactoryGirl.create(:reservation_line_item, folio_id: f2.id)

      f1.move_rsl_item_to_another_folio(line_item, f2)
      f2.reload.reservation_line_items.size.should == 1
    end

    it "should not move folio, means can't assign a reservation line item to another folio with different payment type." do
      @check_in.reload
      wallet = FactoryGirl.create(:wallet, balance: 0, credit_limit: 10000, receivable: @billing_information.billable)
      f1 = FactoryGirl.create(:folio, check_in: @check_in, payment_type: Constants::PAYMENT_TYPE_CREDIT)
      f2 = FactoryGirl.create(:folio, check_in: @check_in, payment_type: Constants::PAYMENT_TYPE_CASH)

      line_item = FactoryGirl.create(:reservation_line_item, folio_id: f1.id)
      FactoryGirl.create(:reservation_line_item, folio_id: f1.id)
      FactoryGirl.create(:reservation_line_item, folio_id: f2.id)
      f2.reload.reservation_line_items.count.should == 1
      f1.move_rsl_item_to_another_folio(line_item, f2)
      f2.reload.reservation_line_items.count.should == 1
    end
  end

  context 'edit folios.' do
    before(:each) do

      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)

      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id, billing_information: @billing_information)
    end

    it "should should check if the folio is editable." do
      @check_in.reload
      f1 = FactoryGirl.create(:folio, check_in: @check_in)
      f1.editable?.should == true
    end

  end
end

private
def create_charge_with(options)
  sv = FactoryGirl.create(:service, hotel: @hotel, department: options[:department])
  FactoryGirl.create(:service_charge, hotel: @hotel, allocated_room: options[:allocated_room], amount: options[:amount], service: sv)
end
