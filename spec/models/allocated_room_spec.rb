require 'spec_helper'

describe AllocatedRoom do
  context 'available on days' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @standard_rate = FactoryGirl.build(:effective_daily_rate, hotel: @hotel,
                                         price: 120, overcharge_per_hour: 20, rate_code: 'Standard rate')
      set_available_on_days(@standard_rate, Date.current, 7)
      @standard_rate.save!

      @three_days_rate = FactoryGirl.build(:effective_daily_rate, min_duration: 3, max_duration: 3, hotel: @hotel,
                                           price: 100, overcharge_per_hour: 20, rate_code: 'Three days rate')
      set_available_on_days(@three_days_rate, Date.current, 7)
      @three_days_rate.save!

      @hourly_rate = FactoryGirl.build(:effective_hourly_rate, hotel: @hotel,
                                       price: 15, overcharge_per_hour: 20, rate_code: 'Hourly rate')
      set_available_on_days(@hourly_rate, Date.current, 7)
      @hourly_rate.save!
    end

    it 'should destroy line items and update folio bills' do
      ar = FactoryGirl.create(:allocated_room, price: 100, start_date: Time.zone.now, end_date: 4.days.from_now, hotel: @hotel)
      ar2 = FactoryGirl.create(:allocated_room, check_in: ar.check_in, price: 100, start_date: Time.zone.now, end_date: 4.days.from_now, hotel: @hotel)
      ar.check_in.reload.total_bill.should == 600
      ar2.destroy
      ar.check_in.reload.folios.sum(:bill).should == 300
    end


    #  it 'should build to allocated room rate with standard rate' do
    #    ar = FactoryGirl.create(:allocated_room, start_date: Time.zone.now, end_date: 5.days.from_now, hotel: @hotel)
    #
    #    #rate = Rate.applicable_for(ar.hotel_id, ar.start_date, ar.end_date).first
    #    #ap "rate :: #{rate.rate_code} [#{ar.start_date} - #{ar.end_date}]"
    #    #applicable_till = rate.applicable_till(ar.start_date, ar.end_date)
    #    #ap "applicable_till :: #{applicable_till}"
    #    #
    #    #applicable_till = rate.applicable_till(applicable_till, ar.end_date)
    #    #ap "applicable_till :: #{applicable_till}"
    #    #
    #    #applicable_till = rate.applicable_till(applicable_till, ar.end_date)
    #    #ap "applicable_till :: #{applicable_till}"
    #
    #    allocated_room_rate = ar.build_allocated_room_rates
    #    ap allocated_room_rate.rate_code
    #    allocated_room_rate.should_not == nil
    #    allocated_room_rate.rate_id.should == @standard_rate.id
    #  end
    #
    #  it 'should build to allocated room rate with hourly rate' do
    #    ar = FactoryGirl.create(:allocated_room, start_date: Time.zone.now, end_date: 5.hours.from_now, hotel: @hotel)
    #
    #    allocated_room_rate = ar.build_allocated_room_rates
    #    ap allocated_room_rate.rate_code
    #    allocated_room_rate.should_not == nil
    #    allocated_room_rate.rate_id.should == @hourly_rate.id
    #  end
    #
    #  it 'should build to allocated room rate with three days package rate' do
    #    ar = FactoryGirl.create(:allocated_room, start_date: Time.zone.now, end_date: 3.days.from_now, hotel: @hotel)
    #
    #    allocated_room_rate = ar.build_allocated_room_rates
    #    ap allocated_room_rate.rate_code
    #    allocated_room_rate.should_not == nil
    #    allocated_room_rate.rate_id.should == @three_days_rate.id
    #  end
    #
    #
  end

  context "Existing allocated room update/destroy" do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
    end
    it 'should unset room->guest_in_room on removing active allocated room' do
      ar = FactoryGirl.create(:allocated_room, start_date: Time.zone.now, end_date: 3.days.from_now, hotel: @hotel)
      room = ar.room
      room.guests_in_room.should == true
      ar.destroy
      room.reload.guests_in_room.should == false
    end

    it 'should not unset room->guest_in_room on removing non(checkin) allocated room' do
      ar = FactoryGirl.create(:allocated_room, start_date: Time.zone.now, end_date: 3.days.from_now, status: Constants::CHECK_IN_STATUSES[:reserved], hotel: @hotel)
      room = ar.room
      room.guests_in_room.should == false
      ar.destroy
      room.reload.guests_in_room.should == false
    end


  end
end

private
def set_available_on_days(rate, from_date, number_of_days)
  rate.available_on_days = []
  from_date.to_date.upto(number_of_days.days.from_now.to_date) do |date|
    rate.available_on_days << date.strftime('%A').downcase
  end
end