require 'spec_helper'

describe SocialLink do
  context 'relationships' do
    it { should belong_to(:hotel) }
  end

  context 'validations' do
    it { should validate_presence_of(:provider) }
    it { should validate_presence_of(:url) }
    it { should validate_uniqueness_of(:provider).scoped_to(:hotel_id) }

    it 'should ensure url have http(s)' do
      link = FactoryGirl.create(:social_link, url: 'some_url')
      expect(link.url).to eq 'http://some_url'
    end

    it 'should retain url have http(s)' do
      given_url = 'https://some_url'
      link = FactoryGirl.create(:social_link, url: given_url)
      expect(link.url).to eq given_url
    end

  end
end
