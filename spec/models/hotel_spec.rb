require 'spec_helper'

describe Hotel do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @country = FactoryGirl.create(:country, :title => "Bangladesh")
    @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
    @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
  end


  it "is invalid without a country" do
    FactoryGirl.build(:hotel, country: nil, :city => @city).should_not be_valid
  end

  it "is invalid without a city" do
    FactoryGirl.build(:hotel, country: @country, :city => nil).should_not be_valid
  end

  it "is invalid without a country and city" do
    FactoryGirl.build(:hotel, country: nil, :city => nil).should_not be_valid
  end

  it "should require a shomoos username" do
    Hotel.new(:shomoos_username => "", country: @country, :city => @city).should_not be_valid
  end

  it "should require a shomoos password" do
    Hotel.new(:shomoos_password => "", country: @country, :city => @city).should_not be_valid
  end

  it "should require a shomoos spsc id" do
    Hotel.new(:shomoos_spsc_id => "", country: @country, :city => @city).should_not be_valid
  end

  it "is valid with a country and city" do
    FactoryGirl.build(:hotel, country: @country, :city => @city).should be_valid
  end

  it "returns and create default account data" do
    flatten_asset_hash(Constants::DEFAULT_ACCOUNT_HEADS, :name).flatten.collect { |value| value.humanize }.sort_by { |w| w }.should == Account.where(entity_id: @hotel).collect { |a| a.send(:name) }.sort_by { |w| w }
  end

  it "returns and create default markers" do
    should_be_equal(@hotel.markers, Marker.default_markers, :title)
  end

  it "returns and create departments" do
    @hotel.departments.collect { |a| a.send(:default_type) }.should == Constants::DEFAULT_DEPARTMENTS.collect { |a| a[:name] }
  end


  it "returns and create services" do
    @hotel.services.collect { |a| a.send(:title) }.should == Constants::DEFAULT_HOTEL_SERVICES.first.map { |key, value| value }.first.to_a
  end

  describe "Destroying hotel" do
    it "should destroy hotel with out check in" do
      expect { @hotel.destroy }.not_to raise_error
    end

    it "should not destroy hotel with check in" do
      room = FactoryGirl.create(:room, rate_price: 100, hotel: @hotel)
      @check_in = FactoryGirl.create(:check_in, rooms: [room], hotel: @hotel)
      expect { @hotel.destroy }.to raise_error
    end
  end

  describe "associations" do
    [:hotel_images, :hotel_discounts, :accounts, :hotel_assignments, :assigned_users,
     :services, :allocated_rooms, :hotel_reviews, :check_ins, :cash_counters, :departments,
     :markers, :reservation_line_items, :rooms, :room_types, :tasks, :rates, :social_links].each do |hm|
      it { should have_many hm }
    end

    [:user, :country, :city, :price_unit, :layout].each do |bt|
      it { should belongs_to bt }
    end
  end

  describe "nested attributes" do
    [:hotel_images, :hotel_banner, :social_links].each do |nested_model|
      it { should accept_nested_attributes_for(nested_model) }
    end

    it "should accept nested attributes for social_links" do
      expect {
        @hotel.update_attributes(:social_links_attributes=>{'0'=>{'provider'=>'facebook', 'url'=>"facebook url"}})
      }.to change { @hotel.social_links.count }.by(1)
    end
  end

  private
  def should_be_equal(ar1, ar2, prop)
    ar1.collect { |a| a.send(prop) }.sort.should == ar2.collect { |a| a.send(prop) }.sort
  end

  def flatten_asset_hash(arr, key)
    arr.collect do |a|
      temp_a = []
      temp_a << flatten_asset_hash(a[:children], key) if a.kind_of?(Hash) and a.has_key? :children
      temp_a << a[key]
    end
  end

end