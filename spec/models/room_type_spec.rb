require 'spec_helper'

describe RoomType do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @room = FactoryGirl.create(:room, hotel: @hotel)
    @room_type = @room.room_type
  end

  context 'deletion' do
    it 'should be allowed to delete new room type' do
      @room_type.destroyable?.should be true
      expect { @room_type.destroy }.not_to raise_error
    end

    it 'should not be allowed to delete room type which has check in' do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, rooms: [@room])

      @room_type.destroyable?.should be false
      expect { @room_type.destroy }.to raise_error
    end
  end
end