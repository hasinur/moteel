require 'spec_helper'

describe CheckIn do
  context 'auto invoice after checked out' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
    end

    it 'should create/attach invoice for each of the folios after checked out' do
      @check_in.check_out!
      folio_without_invoice = @check_in.reload.folios.find_all { |folio| folio.invoice_id.nil? }
      folio_without_invoice.should be_blank
    end

    it 'should create different invoices for folios with \'create_separate_folio\' department' do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      amount = 0
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: amount)
      @check_in.check_out!
      folio_without_invoice = @check_in.reload.folios.find_all { |folio| folio.invoice_id.nil? }
      folio_without_invoice.should be_blank
      @check_in.invoices.count.should == 2
    end

    it 'should create different invoices for different billables' do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: false)
      amount = 0
      another_guest = FactoryGirl.create(:guest)
      create_charge_with(department: dept, allocated_room: @check_in.allocated_rooms.first, amount: amount, :billable => {:bill_to_id => another_guest.id, :bill_to_type => 'Guest'})
      @check_in.check_out!
      folio_without_invoice = @check_in.reload.folios.find_all { |folio| folio.invoice_id.nil? }
      folio_without_invoice.should be_blank
      @check_in.invoices.count.should == 2
    end
  end

  context 'cancel check in', focus: true do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 2)
    end

    it 'should be able to cancel a whole check in' do
      @check_in.cancel!
      @check_in.reload.status.should == Constants::CHECK_IN_STATUSES[:cancelled]
    end

    it 'should be able to cancel a particular check in' do
      @check_in.cancel_room!(@check_in.allocated_rooms.first.room_id)
      @check_in.allocated_rooms.first.reload.status.should == Constants::CHECK_IN_STATUSES[:cancelled]
      @check_in.allocated_rooms.last.reload.status.should == Constants::CHECK_IN_STATUSES[:checked_in]
    end

    it 'should be able to cancel a particular check in' do
      @check_in.cancel_room!(@check_in.allocated_rooms.first.room_id)
      @check_in.cancel_room!(@check_in.allocated_rooms.last.room_id)
      @check_in.allocated_rooms.first.reload.status.should == Constants::CHECK_IN_STATUSES[:cancelled]
      @check_in.allocated_rooms.last.reload.status.should == Constants::CHECK_IN_STATUSES[:cancelled]
      @check_in.reload.status.should == Constants::CHECK_IN_STATUSES[:cancelled]
    end
  end
end

private
def create_charge_with(options)
  sv = FactoryGirl.create(:service, hotel: @hotel, department: options[:department])
  service_charge_attrs = {hotel: @hotel, allocated_room: options[:allocated_room], amount: options[:amount], service: sv}
  service_charge_attrs.merge!(options[:billable]) if options[:billable].present?
  FactoryGirl.create(:service_charge, service_charge_attrs)
end