require 'spec_helper'

describe HotelServicePayment do
  context 'transfer folios.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @billing_information = FactoryGirl.create(:billing_information)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id, billing_information: @billing_information)
    end

    it "should create HotelServicePayment." do
      @check_in.reload
      hsp = FactoryGirl.create(:hotel_service_payment, check_in: @check_in, hotel: @hotel, amount: 250)
      resp = HotelServicePayment.add_payment_distribute_to_folios(@check_in.id, @hotel.id, 100)
      ap resp.serviceable
    end

    it "should distribute payment among folios." do
      @check_in.reload
      dept_1 = FactoryGirl.create(:department, branch: @hotel)

      f1 = FactoryGirl.create(:folio, check_in: @check_in, department: dept_1, bill: 100)
      f2 = FactoryGirl.create(:folio, check_in: @check_in, department: dept_1, bill: 120)

      hsp = FactoryGirl.create(:hotel_service_payment, check_in: @check_in, hotel: @hotel, amount: 250)


      hsp.distribute_payment_among_folios([f1.id, f2.id])
      f1.reload.payment.should == 100
      f2.reload.payment.should == 120
    end

  end

  context 'create folio based on presence of department and billing person.' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      @hotel = FactoryGirl.create(:hotel)
      @user.assigned_hotels << @hotel
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
    end

    it "should assign reservation line item to default folio, for credit type folio, department does not allow different folios." do
      dept = FactoryGirl.create(:department, branch: @hotel, create_separate_folio: true)
      billing_info = FactoryGirl.create(:billing_information)
      wallet = FactoryGirl.create(:wallet, receivable: billing_info.billable, hotel_id: @hotel.id)
      check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id,
                                    billing_information: billing_info, default_payment_type: "credit")
      service = FactoryGirl.create(:service, hotel: @hotel, department: dept)
      hsp = FactoryGirl.create(:hotel_service_payment, check_in: check_in, payment_method: "credit", hotel: @hotel, amount: 250)

      check_in.reload

      puts "reservation_line_items:::#{check_in.reservation_line_items.collect { |f| [f.id, f.folio_id, f.payment] }}"
      puts "folios:::#{check_in.folios.collect { |f| [f.id, f.name] }}"
      puts "wallet_transactions:::#{check_in.reservation_line_items.collect { |rsl| rsl.folio.billable.wallet.wallet_transactions.inspect if rsl.folio.present? }}"

      check_in.folios.size.should == 1
      check_in.reservation_line_items.size.should == 2
      check_in.reservation_line_items.first.folio.should == check_in.reservation_line_items.last.folio
    end

  end

end

