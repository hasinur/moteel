def create_hotel(sym=:hotel)
  city = Factory.create(:city)
  Factory.create(sym, {city: city, country: city.country})
end