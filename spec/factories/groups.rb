FactoryGirl.define do
  factory :group do |f|
    #association :country
    f.sequence(:email) { |n| "group#{n}@moteel.com" }
    f.sequence(:name) { |n| "group#{n}" }
    f.sequence(:code) { |n| "code#{n}" }
    f.sequence(:address) { |n| "address#{n}" }
    f.sequence(:group_type) { |n| "group" }
  end
end