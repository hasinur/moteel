# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :activity_log do
    name "MyString"
    audit_trails_count "MyString"
    user_id 1
    event "MyString"
  end
end
