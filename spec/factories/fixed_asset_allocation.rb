FactoryGirl.define do
  factory :fixed_asset_allocation do
    association :fixed_asset_detail
    association :room
    start_date Time.zone.now
  end
end