FactoryGirl.define do
  factory :cash_counter_transaction do |f|
    association :hotel
    association :cash_counter
    association :processed_by, factory: :user
    transaction_date Time.zone.now
    dr_amount 0
    cr_amount 0
    factory :dr_cash_counter_transaction do
      dr_amount 500
      cr_amount 200
    end

    factory :cr_cash_counter_transaction do
      dr_amount 0.0
      cr_amount 300
    end

  end
end