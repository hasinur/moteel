# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice do |i|
    invoice_date Time.now
    i.sequence(:description) { |n| "description#{n}" }

    before(:create) do |inv|
      inv.folio_ids = FactoryGirl.create(:folio).id
    end

  end
end
