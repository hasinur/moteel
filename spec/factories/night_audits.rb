# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :night_audit do
    hotel_id 1
    user_id 1
    date "2012-09-22"
  end
end
