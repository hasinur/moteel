# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :online_reservation_detail do
    association :online_reservation
    association :room_type
    rooms_count 1
  end
end