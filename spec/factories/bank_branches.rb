FactoryGirl.define do
  factory :bank_branch do |f|
    association :bank
    f.sequence(:name) { |n| "Motijheel Branch.#{n}" }
  end
end