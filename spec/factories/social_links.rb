# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :social_link, :class => 'SocialLink' do
    hotel
    provider "facebook"
    url "some_url"
  end
end
