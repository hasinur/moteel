FactoryGirl.define do
  factory :bank do |f|
    #association :hotel, :foreign_key => 'branch_id'
    f.sequence(:name) { |n| "Standard Bank Ltd.#{n}" }
  end
end