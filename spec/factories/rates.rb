FactoryGirl.define do
  factory :rate do
    rate_code "RateCode"
    active true
    association :hotel
    duration_type Constants::DAILY_DURATION_TYPE
    effective_from 1.months.ago
    effective_until 2.years.from_now

    factory :effective_rate do
      active true
      effective_from 1.months.ago
      effective_until 2.months.from_now

      factory :effective_hourly_rate do
        duration_type Constants::HOURLY_DURATION_TYPE
      end

      factory :effective_daily_rate do
        duration_type Constants::DAILY_DURATION_TYPE
      end
    end

    ignore do
      price 10
      overcharge_per_hour 1
      associate_all_days false
    end

    before(:create) do |rate, evaluator|
      if evaluator.associate_all_days
        Date.current.upto(6.days.from_now.to_date) do |date|
          rate.available_on_days << date.strftime('%A').downcase
        end
      end
    end

    after(:create) do |rate, evaluator|
      rate.hotel.room_types.each do |room_type|
        ratable = room_type.ratables.build(rate_id: rate.id, price: evaluator.price, overcharge_per_hour: evaluator.overcharge_per_hour)
      end
    end
  end
end