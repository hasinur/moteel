FactoryGirl.define do
  factory :country do |f|
    f.sequence(:title) { |n| "Country name#{n}" }
  end
end