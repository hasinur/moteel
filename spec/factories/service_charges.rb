FactoryGirl.define do
  factory :service_charge do |f|
    association :service
    association :hotel
    association :allocated_room

    after(:build) do |svc|
      svc.room_type ||= svc.allocated_room.room_type
    end
  end
end