FactoryGirl.define do
  factory :fixed_asset do |f|
    f.sequence(:name) { |n| "Fixed Asset Name-#{n}" }
  end
end