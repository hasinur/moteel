FactoryGirl.define do
  factory :service do |f|
    association :hotel
    association :department
    f.sequence(:name) { |n| "Test service #{n}" }
  end
end
