FactoryGirl.define do
  factory :room do |f|
    association :hotel
    room_type { FactoryGirl.create(:room_type, hotel: hotel) }
    f.sequence(:room_number) {|n| "PS_#{n}"}
    f.sequence(:price) {|n| 100*n}

    ignore do
      rate_price nil
    end

    after(:create) do |room, evaluator|
      if evaluator.rate_price.present?
        rate = FactoryGirl.build(:effective_rate, rate_code: "Standard rate for #{room.title}", hotel: room.hotel)
        rate.available_on_days = []
        Date.today.upto(6.days.from_now.to_date) do |date|
          rate.available_on_days << date.strftime('%A').downcase
        end
        rate.save!
        room.ratables.create(rate_id: rate.id, price: evaluator.rate_price, overcharge_per_hour: 0)
      end
    end
  end
end