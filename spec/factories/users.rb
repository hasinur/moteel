FactoryGirl.define do
  factory :user do |f|
    f.sequence(:email) { |n| "user#{n}@moteel.com" }
    password "123456"
    password_confirmation "123456"
    is_active 1

    factory :hotel_owner, class: User do
      role "hotel_owner"
    end

    after(:create) do |user|
      FactoryGirl.create(:profile, user: user)
      user.access_grants.create()
    end
  end
end
