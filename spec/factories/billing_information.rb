FactoryGirl.define do
  factory :billing_information do |f|
    association :billable, factory: :guest
    factory :group_type_billing_information do
      association :billable, factory: :group
    end
    factory :guest_type_billing_information do
      association :billable, factory: :guest
    end
  end
end