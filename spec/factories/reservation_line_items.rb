FactoryGirl.define do
  factory :reservation_line_item do |f|
    association :folio
    association :check_in
    association :guest
    association :hotel
    association :created_by, factory: :user
    after(:build) do |line_item|
      line_item.room = FactoryGirl.create(:room, hotel: line_item.hotel)
      #line_item.allocated_room = FactoryGirl.create(:allocated_room, check_in: line_item.check_in)
    end
  end
end
