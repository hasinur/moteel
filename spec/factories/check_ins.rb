FactoryGirl.define do
  factory :check_in do |f|
    association :hotel
    association :billing_information
    association :cash_counter
    start_date Time.zone.now
    end_date 1.day.from_now
    status 'CHECKED_IN'
    default_payment_type Constants::PAYMENT_TYPE_CASH
    factory :checked_in_check_in do
      status 'CHECKED_IN'
    end

    factory :checked_out_check_in do
      status 'CHECKED_OUT'
    end

    factory :reserved_check_in do
      status 'RESERVED'
    end

    factory :confirmed_check_in do
      status 'CONFIRMED'
    end

    ignore do
      number_of_rooms 1
    end

    ignore do
      rooms []
    end
    ignore do
      guests []
    end

    after(:build) do |check_in, evaluator|
      if evaluator.rooms.present?
        evaluator.rooms.each do |room|
          ar = FactoryGirl.build(:allocated_room, room: room, room_type: room.room_type, check_in: check_in, hotel: check_in.hotel, guests: evaluator.guests, number_of_allocated_room_guest: 1)
          allocated_room_rates = AllocatedRoomRate.split(room, check_in.billing_information.billable, check_in.start_date, check_in.end_date)
          ar.price = check_in.hotel_discount.present? ? check_in.hotel_discount.apply(allocated_room_rates.sum(&:price)) : allocated_room_rates.sum(&:price)
          ar.allocated_room_rates << allocated_room_rates
          check_in.allocated_rooms << ar
        end
      else
        if evaluator.number_of_rooms.to_i > 0
          1.upto(evaluator.number_of_rooms.to_i) do
            room = FactoryGirl.create(:room, hotel: check_in.hotel, rate_price: 100)
            ar = FactoryGirl.build(:allocated_room, room: room, room_type: room.room_type, check_in: check_in, hotel: check_in.hotel, guests: evaluator.guests, number_of_allocated_room_guest: 1)
            allocated_room_rates = AllocatedRoomRate.split(room, check_in.billing_information.billable, check_in.start_date, check_in.end_date)
            ar.price = check_in.hotel_discount.present? ? check_in.hotel_discount.apply(allocated_room_rates.sum(&:price)) : allocated_room_rates.sum(&:price)
            ar.allocated_room_rates << allocated_room_rates
            check_in.allocated_rooms << ar
          end
          #create some rates as well
          FactoryGirl.create(:effective_rate, hotel: check_in.hotel, associate_all_days: true)
        end
      end
      check_in.total_charge = check_in.allocated_rooms.sum(&:price)
      check_in.total_room_price = check_in.total_charge
      #check_in.total_room_price = check_in.allocated_rooms.sum(&:price)
    end
  end


end