FactoryGirl.define do
  factory :marker do |f|
    association :hotel, :foreign_key => 'entity_id'
    f.sequence(:name) { |n| "non_smoking#{n}" }
    f.sequence(:title) { |n| "Non Smoking#{n}" }
  end
end