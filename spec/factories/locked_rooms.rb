FactoryGirl.define do
  factory :locked_room do
    room
    room_type
    hotel
    association :task
  end
end