FactoryGirl.define do
  factory :layout do |f|
    sequence(:name) { |n| "Layout#{n}" }
    sequence(:path) { |n| "custom_layouts/layout#{n}" }
    sequence(:name_space) { |n| "layout#{n}" }
  end
end