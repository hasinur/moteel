FactoryGirl.define do
  factory :city do |f|
    association :country
    f.sequence(:title) { |n| "city name#{n}" }
  end
end
