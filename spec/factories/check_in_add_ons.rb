FactoryGirl.define do
  factory :check_in_add_on do |f|
    association :service
    association :reservation_line_item

    ignore do
      check_in nil
    end

    after(:build) do |add_on, evaluator|
      check_in = evaluator.check_in || FactoryGirl.create(:check_in, number_of_rooms: 1)
      add_on.check_in = check_in
      add_on.allocated_room = check_in.allocated_rooms.first
      add_on.room = check_in.allocated_rooms.first.room

      add_on.task = FactoryGirl.create(:check_in_add_on_task, hotel: check_in.hotel, room: check_in.allocated_rooms.first.room)
    end
  end
end