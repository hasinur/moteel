FactoryGirl.define do
  factory :department do |f|
    association :branch, factory: :hotel
    f.sequence(:name) { |n| "House Keeping#{n}" }
  end
end