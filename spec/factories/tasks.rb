FactoryGirl.define do
  factory :task do |f|
    association :assigned_user, factory: :user
    association :creator, factory: :user
    association :hotel
    f.sequence(:title) { |n| "Test task #{n}" }

    factory :locked_room_task, class: LockedRoomTask do
      type 'LockedRoomTask'
    end

    factory :damaged_room_task, class: DamagedRoomTask do
      type 'DamagedRoomTask'
    end

    ignore do
      room nil
    end

    after(:build) do |task, evaluator|
      task.department = FactoryGirl.create(:department, branch: task.hotel)
      task.room = evaluator.room || FactoryGirl.create(:room, hotel: task.hotel)
    end
  end
end
