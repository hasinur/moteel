FactoryGirl.define do
  factory :cash_counter do |f|
    association :branch, factory: :hotel
    f.sequence(:name) { |n| "Cash Counter#{n}" }
  end
end