FactoryGirl.define do
  factory :fixed_asset_detail do |f|
    association :fixed_asset
    f.sequence(:code) { |n| "Asset Code - #{n}" }
    f.sequence(:actual_value) { |n| 5000 }
  end
end