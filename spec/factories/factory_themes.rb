FactoryGirl.define do
  factory :layout_theme do |f|
    layout
    sequence(:name) { |n| "Theme#{n}" }
    path 'path'

    after(:create) do |theme|
      theme.update_attributes(path: "custom_layouts/#{theme.layout.name_space}/themes/#{theme.name}" )
    end
  end
end