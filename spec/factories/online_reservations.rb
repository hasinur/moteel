# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :online_reservation do
    start_date Time.zone.now
    end_date 2.days.from_now
    email "online_reservation@moteel.com"
    first_name "First name"
    last_name "Last name"
    association :hotel

    ignore do
      number_of_rooms 1
    end

    after(:build) do |online_reservation, evaluator|
      1.upto(evaluator.number_of_rooms.to_i) do
        online_reservation.details.build(FactoryGirl.attributes_for(:online_reservation_detail).merge({room_type: FactoryGirl.create(:room_type)}))
      end
    end
  end
end