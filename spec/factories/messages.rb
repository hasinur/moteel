# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    association :sender, factory: :user
    title "MyString"
    content "MyString"
    notify_with_email false

    ignore do
      number_of_recipients 0
      receiver nil
    end

    after(:create) do |message, evaluator|
      if evaluator.number_of_recipients.to_i > 0
        receiver = evaluator.receiver || Factory.create(:user)
        1.upto(evaluator.number_of_recipients.to_i) do
          message.message_recipients << FactoryGirl.create(:message_recipient, message: message, user: receiver)
        end
      end
    end

  end
end
