FactoryGirl.define do
  factory :guest do |f|
    f.sequence(:email) { |n| "guest#{n}@moteel.com" }
    f.sequence(:first_name) { |n| "guest#{n}" }
    f.sequence(:last_name) { |n| "guest#{n}" }
    f.sequence(:address) { |n| "address#{n}" }
    customer_type Constants::CUSTOMER_TYPES[:citizen]
    id_type Constants::ID_TYPES[:id_card]
    id_serial_number '123'
  end
end