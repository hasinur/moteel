FactoryGirl.define do
  factory :hotel do |f|
    city
    country
    user
    f.sequence(:title) { |n| "Hotel #{n}" }
    f.sequence(:address) {|n| "Shabag#{n}"}
    time_zone "Dhaka"
    starting_hour 12
    starting_minute 00
    ending_hour 12
    ending_minute 00

    ignore do
      number_of_rooms 0
    end
    after(:create) do |hotel, evaluator|
      1.upto(evaluator.number_of_rooms) do
        FactoryGirl.create(:room, hotel: hotel)
      end
    end
  end
end
