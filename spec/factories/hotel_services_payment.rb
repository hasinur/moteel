FactoryGirl.define do
  factory :hotel_service_payment do |f|
    association :check_in
    association :hotel
    #association :
    transaction_type "deposit"
    payment_method "cash"
    allow_edit true
    amount 100

    after(:create) do |hsp|
      FactoryGirl.create(:hotel_service_payment_detail, hotel_service_payment: hsp) if hsp.payment_method == 'cheque'
    end
  end
end

