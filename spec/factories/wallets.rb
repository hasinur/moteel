FactoryGirl.define do
  factory :wallet do
    association :receivable, factory: :guest
    credit_limit 1000
    balance 0
  end
end
