FactoryGirl.define do
  factory :account do |f|
    #association :entity, :class_name => 'Hotel', :foreign_key => 'entity_id'
    f.sequence(:name) { |n| "Revenue Account#{n}" }
    f.sequence(:current_balance) { |n| "#{0}" }
    f.sequence(:code) { |n| "100021#{n}" }
    f.sequence(:account_type) { |n| "R" }
  end
end