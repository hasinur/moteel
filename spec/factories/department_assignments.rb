FactoryGirl.define do
  factory :department_assignment do |f|
    association :department
    association :user
  end
end