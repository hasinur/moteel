FactoryGirl.define do
  factory :hotel_discount do |f|
    f.sequence(:title) { |n| "Discount Offer#{n}" }
    f.sequence(:code) { |n| "code#{n}" }
  end
end