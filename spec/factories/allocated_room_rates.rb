# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :allocated_room_rate do
    association :rate
    association :allocated_room
    start_date Time.now
    end_date 2.days.from_now
    price 200
  end
end
