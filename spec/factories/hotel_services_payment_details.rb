FactoryGirl.define do
  factory :hotel_service_payment_detail do |f|
    association :hotel_service_payment
    status 'pending'
  end
end

