# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sync_entity_report do
    entity_id 1
    start_date "2012-12-31"
    end_date "2012-12-31"
    execution_time "2012-12-31 14:25:18"
  end
end
