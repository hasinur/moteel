FactoryGirl.define do
  factory :amenity do |f|
    f.sequence(:title) { |n| "Amenity#{n}" }
    factory :amenity_for_hotel do
      for_hotel true
    end
  end
end