FactoryGirl.define do
  factory :damaged_room do
    room
    room_type
    hotel
    association :task
  end
end