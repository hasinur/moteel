FactoryGirl.define do
  factory :room_marker do |f|
    association :room
    association :marker
  end
end