FactoryGirl.define do
  factory :check_in_add_on_task do |f|
    association :hotel
    department { FactoryGirl.create(:department, branch: hotel) }
    association :room
    association :creator, factory: :user
    title "News paper"
    type "CheckInAddOnTask"
  end
end