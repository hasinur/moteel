FactoryGirl.define do
  factory :asset_category do |f|
    f.sequence(:name) { |n| "Asset Category Name-#{n}" }
  end
end