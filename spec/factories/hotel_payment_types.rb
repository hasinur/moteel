FactoryGirl.define do
  factory :hotel_payment_type do |f|
    f.sequence(:title) { |n| "Discount Offer#{n}" }
    association :hotel
  end
end