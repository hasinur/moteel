# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cash_counter_access_log do
    cash_counter_id 1
  end
end
