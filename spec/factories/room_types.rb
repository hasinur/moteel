FactoryGirl.define do
  factory :room_type do |f|
    association :hotel
    association :price_unit
    f.sequence(:title) {|n| "RoomType#{n}"}
    f.sequence(:price) {|n| 100*n}
  end
end