FactoryGirl.define do
  factory :allocated_room do |f|
    start_date Time.now
    end_date 2.days.from_now
    association :room
    association :room_type
    association :hotel
    #association :main_guest, factory: :guest
    association :check_in
    status Constants::CHECK_IN_STATUSES[:checked_in]
    ignore do
      number_of_allocated_room_guest 1
      guests []
    end

    after(:build) do |allocated_room, evaluator|
      if evaluator.guests.size > 0
        evaluator.guests.each do |guest|
          allocated_room.allocated_room_guests << FactoryGirl.build(:allocated_room_guest, allocated_room: allocated_room, guest: guest)
        end
      else
        if evaluator.number_of_allocated_room_guest.to_i > 0
          1.upto(evaluator.number_of_allocated_room_guest.to_i) do
            allocated_room.allocated_room_guests << FactoryGirl.build(:allocated_room_guest, allocated_room: allocated_room)
          end
        end
      end
    end

    after(:build) do |ar|
      ar.price = ar.room.price
    end
  end
end

