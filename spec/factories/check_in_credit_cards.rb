# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :check_in_credit_card, :class => CheckInCreditCard do
    card_type "MyString"
    card_number "MyString"
    secret_code "MyString"
    expires_at "2012-09-04"
    check_in_id 1
  end
end
