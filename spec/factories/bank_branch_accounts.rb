FactoryGirl.define do
  factory :bank_branch_account do |f|
    association :bank_branch
    f.sequence(:name) { |n| "Hotel Seagull International#{n}" }
  end
end