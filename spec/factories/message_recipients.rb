# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message_recipient do
    association :message
    association :user
    read_at nil
  end
end
