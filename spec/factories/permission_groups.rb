FactoryGirl.define do
  factory :permission_group do |f|
    f.sequence(:name) { |n| "Permission Group #{n}" }
  end
end
