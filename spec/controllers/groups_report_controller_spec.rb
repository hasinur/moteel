require 'spec_helper'
require_relative '../supports/controller_spec_support'

describe GroupsReportController do
  before(:each) do
    login_with_hotel_owner
  end


  describe "GET All Group Stay List" do
    before(:each) do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Standard')
      room_type2 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Delux')
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 80, room_type: room_type1, room_number: 'std2' )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 80, room_type: room_type2, room_number: 'dlx3' )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type1, room_number: 'std3' )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type2, room_number: 'dlx4' )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type1, room_number: 'std4' )
      room6 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type2, room_number: 'dlx5' )
      @guest1 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Rahim')
      @guest2 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Karim')
      @check_in1 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest1), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room1])
      @check_in2 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest2), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room2])
      @check_in3 = FactoryGirl.create(:checked_out_check_in,billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest1), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room3])
      @check_in4 = FactoryGirl.create(:checked_out_check_in,billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest2), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room4])
    end


    it "should see all checked in Group Stay Report a given date range" do
      service_charge1 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in1.allocated_rooms.first, amount:5)
      get :group_stay, {start_date: 1.day.ago, end_date: 3.days.from_now, checkstatus: Constants::CHECK_IN_STATUSES[:checked_in]}, {}
      response.should be_success
      assigns(:checkInList).collect{|rec| rec["Total"]}.should eq([(80*2)+5,(80*2)])
    end

    it "should see all checked out Group Stay Report a given date range" do
      service_charge1 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in3.allocated_rooms.first, amount:5)
      get :group_stay, {start_date: 1.day.ago, end_date: 3.days.from_now, checkstatus: Constants::CHECK_IN_STATUSES[:checked_out]}, {}
      response.should be_success
      assigns(:checkInList).collect{|rec| rec["Total"]}.should eq([(90*2)+5,(90*2)])
    end

    it "shoudl see all checked in and checked out Group Stay Report a given date range" do
      service_charge1 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in1.allocated_rooms.first, amount:5)
      service_charge2 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in3.allocated_rooms.first, amount:5)
      get :group_stay, {start_date: 1.day.ago, end_date: 3.days.from_now}, {}
      response.should be_success
      assigns(:checkInList).collect{|rec| rec["Total"]}.should eq([(80*2)+5,(80*2),(90*2)+5,(90*2)])
    end
  end

  describe "GET groups revenue List" do
    it "Get all groups_revenue Report in a given date range and group type" do
      check_in1 = create_group_check_in_with(Time.zone.now, 5.days.from_now, :confirmed_check_in)
      check_in2 = create_group_check_in_with(Time.zone.now, 5.days.from_now, :reserved_check_in)
      check_in3 = create_group_check_in_with(Time.zone.now, 5.days.from_now, :checked_in_check_in)
      check_in4 = create_group_check_in_with(Time.zone.now, 7.days.from_now, :confirmed_check_in)
      check_in5 = create_group_check_in_with(Time.zone.now, 7.days.from_now, :checked_in_check_in)
      check_in6 = create_group_check_in_with(Time.zone.now, 7.days.from_now, :reserved_check_in)

      get :groups_revenue, {start_date: 1.day.ago, end_date: 8.days.from_now}, {}
      response.should be_success
      assigns(:groups_revenue).collect(&:id).should eq(check_in3.allocated_room_ids+check_in5.allocated_room_ids)
      assigns(:groups_revenue).size.should eq(2)
    end
  end

  describe "GET room type wise revenue List" do
    it "Get room type wise revenue Report in a  given date range and a Checkin Status" do

      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, :title => 'Super')
      room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel, :title => 'Delux')
      room1 = FactoryGirl.create(:room, hotel: @hotel,room_number:'SUPER_101', rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel,room_number:'SUPER_102', rate_price: 400, room_type: room_type2 )
      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date:4.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room2])

      room_result = [
          {"Room Type" => "Super","Occupancy (%)" => 25.00,"Total Charge" =>1200},
          {"Room Type" => "Delux","Occupancy (%)" => 12.50,"Total Charge" =>800}

      ]

      get :room_types_revenue, {start_date: 1.day.ago, end_date: 7.days.from_now}, {}

      response.should be_success
      assigns(:roomTypeList).should eq(room_result)
    end
  end

  describe "GET room wise revenue List" do
    it "Get room  wise revenue Report in a  given date range and a Checkin Status" do

      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel,room_number:'SUPER_101', rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel,room_number:'SUPER_102', rate_price: 400, room_type: room_type1 )

      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date:4.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room2])

      room_result = [
          {"Room" => "SUPER_101","Occupancy (%)" => 50.00,"Total Charge" =>1200},
          {"Room" => "SUPER_102","Occupancy (%)" => 25.00,"Total Charge" =>800}

      ]

      get :room_wise_revenue, {start_date: 1.day.ago, end_date: 7.days.from_now}, {}
      response.should be_success
      assigns(:room_list).should eq(room_result)
    end
  end

  describe "GET reservation activity report List" do

    before(:each) do
      cash_counter1 = FactoryGirl.create(:cash_counter, branch: @hotel, name: 'Default Counter')
      cash_counter2 = FactoryGirl.create(:cash_counter, branch: @hotel, name: 'Main Counter')
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Standard')
      room_type2 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Delux')
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 80, room_type: room_type1, room_number: 'std1' )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type1, room_number: 'std2' )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type1, room_number: 'std3' )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type2, room_number: 'dlx1' )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type2, room_number: 'dlx2' )
      room6 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 200, room_type: room_type2, room_number: 'dlx3' )
      @guest1 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Rahim')
      @guest2 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Karim')
      @guest3 = FactoryGirl.create(:guest, first_name:'Mrs', last_name: 'Rahim')
      @guest4 = FactoryGirl.create(:guest, first_name:'Baby', last_name: 'Rahim')

      @discount1 = FactoryGirl.create(:hotel_discount, hotel: @hotel, fixed: true, code: 'winter', discount_value: 20)

      @check_in1 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest1), hotel: @hotel,
                                      hotel_discount: @discount1, cash_counter:cash_counter1, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room2], guests: [@guest1])
      @check_in2 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest2), hotel: @hotel,
                                      hotel_discount: @discount1,cash_counter:cash_counter2, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room6],guests: [@guest2])

      @check_in1.allocated_rooms.first.allocated_room_guests.create(guest: @guest3, main_guest_id:@guest1.id)
      @check_in1.allocated_rooms.first.allocated_room_guests.create(guest: @guest4, main_guest_id:@guest2.id)
    end



    it "should see reservation activity Report in a  given date range" do
      get :reservation_activity_report, {start_date: 1.day.ago, end_date: 3.days.from_now}, {}
      response.should be_success
      assigns(:reservation_list).collect{|rec| rec["Main Guest"]}.should eq([@guest1.name,@guest2.name])
      assigns(:reservation_list).collect{|rec| rec["Total"]}.should eq([(100*2)-20,(200*2)-20])

    end
    it "should see Reservation activity report by cash counter in a given date range" do
      get :reservation_cash_counter, {start_date: 1.day.ago, end_date: 3.days.from_now}, {}
      response.should be_success
      assigns(:reservation_list).collect { |rec| rec["Cash Counters"] }.should eq(['Default Counter', 'Main Counter'])

    end

    it "should see room history report in a given date range" do
      get :room_history_report, {format:'js'}, {}
      response.should be_success
      assigns(:room_history_result).collect(&:total_charge).should eq([380, 180])

    end
  end

  #describe "GET reservation cash counter report List" do
  #  it "Get all reservation cash counter Report in a  given date range and a Checkin Status" do
  #    cash_counter1 = FactoryGirl.create(:cash_counter,branch:@hotel)
  #    cash_counter2 = FactoryGirl.create(:cash_counter,branch:@hotel)
  #    check_in1 = create_cash_counter_check_in_with(Time.zone.now, 4.days.from_now, :checked_in_check_in,cash_counter1)
  #    check_in2 = create_cash_counter_check_in_with(Time.zone.now, 2.days.from_now, :checked_in_check_in,cash_counter2)
  #
  #    get :reservation_cash_counter, {start_date: 1.day.ago, end_date: 7.days.from_now}, {}
  #    response.should be_success
  #    assigns(:reservation_activity).collect(&:cash_counter_id).should eq([cash_counter1.id,cash_counter2.id])
  #  end
  #end
  #
  describe "GET Cheque list report " do
    it "Get all Cheque list Reports" do
      cash_counter1 = FactoryGirl.create(:cash_counter,branch:@hotel)
      cash_counter2 = FactoryGirl.create(:cash_counter,branch:@hotel)
      check_in1 = create_cash_counter_check_in_with(Time.zone.now, 4.days.from_now, :checked_in_check_in,cash_counter1)
      check_in2 = create_cash_counter_check_in_with(Time.zone.now, 2.days.from_now, :checked_in_check_in,cash_counter2)
      hsp1 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 250,allow_edit:true, cash_counter: cash_counter1,payment_method:"cheque",transaction_type:Constants::PAYMENT_DEPOSIT)
      hsp2 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2, hotel: @hotel, amount: 350,allow_edit:true, cash_counter: cash_counter2,payment_method:"cheque",transaction_type:Constants::PAYMENT_DEPOSIT)
      get :cheque_list_report, {status:"pending"}, {}
      response.should be_success
      assigns(:hsp_cheque_list).collect(&:id).should eq([hsp1.id,hsp2.id])
    end
  end

  describe "GET Room rate report" do
    it "Get all Room Rate Reports" do
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300)
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400)
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500)
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 600)
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 700)
      get :room_rate_report, {format:'json'}, {}
      assigns(:room_rate_report).collect{|room_rate| room_rate["Rate"]}.should eq([300, 400, 500, 600, 700])
    end
  end

  describe "GET Room net revenue report" do
    it "Get all Room net revenue Reports" do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500, room_type: room_type2 )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 600, room_type: room_type2 )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 700)

      check_in1 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 4.days.from_now, rooms: [room2])
      check_in3 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 6.days.from_now, rooms: [room3])
      check_in4 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 3.days.from_now, rooms: [room4])
      check_in5 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room5])

      get :room_net_revenue_report, {format:'json'}, {}

      assigns(:room_rate_report_result).collect{|room_rate| room_rate["Revenue"]}.should eq([300*5, 400*4, 500*6, 600*3, 700*2])
    end
  end

  #describe "GET room history report" do
  #  it "Get all Room history Reports" do
  #    room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
  #    room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel)
  #    room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
  #    room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
  #    room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500, room_type: room_type2 )
  #    room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 600, room_type: room_type2 )
  #    room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 700)
  #
  #    check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
  #    check_in2 = FactoryGirl.create(:reserved_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 4.days.from_now, rooms: [room2])
  #    check_in3 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 6.days.from_now, rooms: [room3])
  #    check_in4 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 3.days.from_now, rooms: [room4])
  #    check_in5 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room5])
  #
  #    get :room_history_report, {format:'json'}, {}
  #
  #    assigns(:room_history_result).collect(&:price).should eq([700*2, 500*6, 300*5])
  #  end
  #end

  describe "GET room wise revenue report" do
    it "Get all Room wise revenue report" do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500, room_type: room_type2 )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 600, room_type: room_type2 )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 700)
      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:reserved_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 4.days.from_now, rooms: [room2])
      check_in3 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 6.days.from_now, rooms: [room3])
      check_in4 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 3.days.from_now, rooms: [room4])
      check_in5 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room5])
      get :room_wise_revenue_report, {start_date: 1.day.ago, end_date: 3.days.from_now,format:'json'}, {}
      assigns(:room_type_wise_revenue).values.collect{|e| e.values.collect{|e1| e1["total_room_charge"]}}.flatten.should eq([300*3, 400*3, 500*3,600*3,700*2])
    end
  end

  describe "GET Room Type Occupancy Revenue report" do
    it "Get all Room Type Occupancy Revenue report" do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500, room_type: room_type2 )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 600, room_type: room_type2 )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 700)

      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:reserved_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 4.days.from_now, rooms: [room2])
      check_in3 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 6.days.from_now, rooms: [room3])
      check_in4 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 3.days.from_now, rooms: [room4])
      check_in5 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room5])


      get :room_type_occupancy_revenue, {start_date: 1.day.ago, end_date: 3.days.from_now,format:'json'}, {}
      assigns(:room_type_list).collect{|e| e["Total Charge"]}.should eq([(300+400)*3,(500+600)*3,700*2])
    end
  end

  describe "GET Room Rate Variation report" do
    it "Get all Room Rate Variation report" do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room_type2 = FactoryGirl.create(:room_type, :price => 550, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 500, room_type: room_type2 )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 600, room_type: room_type2 )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 700)

      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 4.days.from_now, rooms: [room2])
      check_in3 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 6.days.from_now, rooms: [room3])
      check_in4 = FactoryGirl.create(:confirmed_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 3.days.from_now, rooms: [room4])
      check_in5 = FactoryGirl.create(:checked_out_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room5])

      get :room_rates_revenue, {start_date: 1.day.ago, end_date: 3.days.from_now,format:'json'}, {}

      assigns(:records).collect{|e| e["Rate Offered"]}.should eq([300*5,400*4,500*6])
    end
    end

  describe "GET cash counter transaction report" do
    it "Get all cash counter transaction report" do
      cash_counter1 = FactoryGirl.create(:cash_counter, branch: @hotel)
      cash_counter2 = FactoryGirl.create(:cash_counter, branch: @hotel)
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room2])
      hotel_service_payment1 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 325,allow_edit:true, cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment2 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 425,allow_edit:true, cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment3 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 200,allow_edit:true, cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_WITHDRAW)
      hotel_service_payment4 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, hotel: @hotel, amount: 100,allow_edit:true, cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_WITHDRAW)

      hotel_service_payment5 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2, hotel: @hotel, amount: 800,allow_edit:true, cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment6 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2, hotel: @hotel, amount: 400,allow_edit:true, cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment7 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2, hotel: @hotel, amount: 300,allow_edit:true, cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_WITHDRAW)
      hotel_service_payment8 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2, hotel: @hotel, amount: 200,allow_edit:true, cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_WITHDRAW)

      get :cash_counter_transactions_report, {}, {}
      response.should be_success
      assigns(:report).records.collect{|rec| rec["Balance"]}.should eq([((325+425)-(200+100)),((800+400)-(300+200))])
    end
  end

  describe "GET guest revenue report" do
    it "Get guest revenue report" do
      cash_counter1 = FactoryGirl.create(:cash_counter, branch: @hotel)
      cash_counter2 = FactoryGirl.create(:cash_counter, branch: @hotel)
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )

      check_in1 = FactoryGirl.create(:checked_in_check_in,billing_information: FactoryGirl.build(:guest_type_billing_information), hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:checked_in_check_in,billing_information: FactoryGirl.build(:guest_type_billing_information), hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room2])

      get :guest_revenue_report, {}, {}
      response.should be_success
      assigns(:report).records.collect{|rec| rec["Revenue"]}.should eq([300*5, 400*5])
    end
  end

  describe "GET Room Asset Allocation report" do
    it "Get room asset allocation report" do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      fixed_asset_detail1 = FactoryGirl.create(:fixed_asset_detail, fixed_asset:FactoryGirl.build(:fixed_asset))
      fixed_asset_detail2 = FactoryGirl.create(:fixed_asset_detail, fixed_asset:FactoryGirl.build(:fixed_asset))
      fixed_asset_allocation1 = FactoryGirl.create(:fixed_asset_allocation,fixed_asset_detail:fixed_asset_detail1, room:room1)
      fixed_asset_allocation2 = FactoryGirl.create(:fixed_asset_allocation,fixed_asset_detail:fixed_asset_detail2, room:room2)

      get :room_asset_allocation_report, {}, {}
      response.should be_success
      #assigns(:report).records.collect{|rec| rec["Revenue"]}.should eq([300*5, 400*5])
    end
  end


end



private
def create_check_in_with(start_date, end_date, check_in_type = :reserved_check_in)
  FactoryGirl.create(check_in_type.to_sym, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
end

def create_group_check_in_with(start_date, end_date, check_in_type = :reserved_check_in)
  FactoryGirl.create(check_in_type.to_sym, billing_information: FactoryGirl.build(:group_type_billing_information), hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
end

def create_guest_check_in_with(start_date, end_date, check_in_type = :reserved_check_in)
  FactoryGirl.create(check_in_type.to_sym, billing_information: FactoryGirl.build(:guest_type_billing_information), hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
end

def create_cash_counter_check_in_with(start_date, end_date, check_in_type = :reserved_check_in, cash_counter)
  FactoryGirl.create(check_in_type.to_sym, hotel: @hotel,cash_counter: cash_counter, start_date: start_date, end_date: end_date, number_of_rooms: 1)
  #FactoryGirl.create(check_in_type.to_sym, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
end



def report_should_be_equal_to(array)
  assigns(:report).query.collect(&:id).sort.should eq(array.collect(&:id).sort)
end