require 'spec_helper'
require 'controller_spec_support'

describe FrontDesk::ReportsController do

  before(:each) do
    login_with_hotel_owner
    @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, number_of_rooms: 1)
  end

  context 'should show left panel report for,' do

    it "arrival list for today" do
      xhr :get, :arrival_list
    end

    it "no show guests for today" do
      xhr :get, :no_show_guests
    end

    it "checkin list for today" do
      xhr :get, :checkin_list
    end

    it "checkin guest for today" do
      xhr :get, :departure_guests
    end

    it "reservation list for today" do
      xhr :get, :reservation_list
    end

  end

end
