require 'spec_helper'
require_relative '../supports/controller_spec_support'

describe OnlineReservationsController do
  describe "GET /online_reservations" do
    it "works! (now write some real specs)" do
      login_with_hotel_owner
      FactoryGirl.create(:online_reservation, hotel: @hotel)
      get :index, {format: 'csv'}, {}
      response.status.should be(200)
      assigns(:online_reservations).size.should eq(1)
    end
  end
end