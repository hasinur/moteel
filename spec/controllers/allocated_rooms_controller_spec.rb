require 'spec_helper'
require 'supports/controller_spec_support'

describe AllocatedRoomsController do

  before(:each) do
    login_with_hotel_owner
    @allocated_room = FactoryGirl.create(:allocated_room, start_date: Time.zone.now, end_date: 5.days.from_now, hotel: @hotel, number_of_allocated_room_guest: 2)
  end

  context "should add charge" do

    it "show add charge popup" do
      get :add_charge_from_room_view, :reference_id => @allocated_room.id
      assigns(:allocated_room).should eq(@allocated_room)
      assigns(:guests).should eq(@allocated_room.check_in.all_guests)
    end

  end

  context "should update allocated room" do

    it "updates the requested allocated room" do
      service = FactoryGirl.create(:service, hotel: @hotel)
      service_charges = FactoryGirl.build(:service_charge, hotel: @hotel, allocated_room: @allocated_room, amount: 122, service: service).attributes

      xhr :put, :update, :id => @allocated_room.id, :service_charges => service_charges
      assigns(:allocated_room).should eq(@allocated_room)
      flash[:notice].should == I18n.t('notifications.operation_successful')
      response.should be_success
    end

  end

end
