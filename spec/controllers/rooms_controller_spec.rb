require 'spec_helper'
require 'controller_spec_support'

describe RoomsController do

  before(:each) do
    login_with_hotel_owner
    @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, number_of_rooms: 1)
    @room = @check_in.allocated_rooms.first.room
  end

  context "should assign marker" do

    before(:each) do
      @marker = FactoryGirl.create(:marker, hotel: @hotel)
      FactoryGirl.create(:room_marker, room: @room, marker: @marker)
    end


    it "show assign marker popup" do

      xhr :get, :edit_markers, :id => @room.id
      assigns(:room).should eq(@room)
      assigns(:available_markers).should eq(@room.markers)
    end

    it "show update marker for selected room" do
      new_marker = FactoryGirl.create(:marker, hotel: @hotel)
      xhr :put, :update_markers, :room_id => @room.id, :marker_ids => [new_marker.id]
      assigns(:room).should eq(@room)
      flash.now[:notice].should == I18n.t('notifications.operation_successful')
      response.should be_success
    end


  end

  context "should change the rooms house keeping status" do

    it "make the house keeping status as dirty for this room" do
      xhr :post, :mark_as_dirty, :room_id => @room.id
      assigns(:room).should eq(@room)
      flash.now[:notice].should == I18n.t('notifications.housekepping_status_updated')
      response.should be_success
    end


    it "make the house keeping status as clean for this room" do
      xhr :post, :mark_as_clean, :room_id => @room.id
      assigns(:room).should eq(@room)
      flash.now[:notice].should == I18n.t('notifications.housekepping_status_updated')
      response.should be_success
    end


  end


end
