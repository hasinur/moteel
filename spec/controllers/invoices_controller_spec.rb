require 'spec_helper'

describe BranchPanel::InvoicesController do

  describe 'should visit new invoices path' do
    before(:each) do
      @check_in = FactoryGirl.create(:check_in)
      @folio = FactoryGirl.create(:folio)

    end

    it 'create new invoices' do
      expect{
        post :create , {:folio_ids=> @folio.id}
        assigns(:check_in).should be_kind_of(CheckIn)
        assigns(:invoice).should be_kind_of(Invoice)
        controller.params[:folio_ids].should_not be_nil
      }.to change(Invoice,:count).by(1)



    end

    it 'destroy invoice' do
      @invoice = FactoryGirl.create(:invoice)

      expect{
        xhr :delete, :destroy, id: @invoice
        response.should be_success
      }.to change(Invoice,:count).by(-1)



    end
    it 'visit credit invoices report' do
      get :credit_invoices
    end


  end



end