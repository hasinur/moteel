require 'spec_helper'
require 'controller_spec_support'

describe TasksController do

  before(:each) do
    login_with_hotel_owner
    @room = FactoryGirl.create(:room, hotel: @hotel)
    @department = FactoryGirl.create(:department, branch: @hotel)
  end

  def valid_attributes_for_damaged_room
    {title: "Task Note", room_id: @room.id, department_id: @department.id, assigned_user_id: "", due_date: Time.zone.now, done: "0", note: "Test Notes",
     damaged_room_attributes: {entry_date: Time.zone.now, released_date: 2.days.from_now, hotel_id: @room.hotel_id, room_id: @room.id}}
  end

  def valid_attributes_for_locked_room
    {title: "Task Note", room_id: @room.id, department_id: @department.id, assigned_user_id: "", due_date: Time.zone.now, done: "0", note: "Test Notes",
     locked_room_attributes: {entry_date: Time.zone.now, released_date: 2.days.from_now, hotel_id: @room.hotel_id, room_id: @room.id}}
  end


  context "Add Task as DamagedRoomTask/LockedRoomTask" do

    it "show Add Task popup for mark as maintenance" do
      xhr :get, :new_lb, {supplied_type: "DamagedRoomTask", task: {room_id: @room.id}, title: "Mark as Maintenance"}
      assigns(:title).should eq("Mark as Maintenance")
      assigns(:task).should be_kind_of(DamagedRoomTask)
      assigns(:rooms).should eq(@hotel.rooms)
    end

    it "show Add Task popup for mark as locked" do
      xhr :get, :new_lb, {supplied_type: "LockedRoomTask", task: {room_id: @room.id}, title: "Mark as Locked"}
      assigns(:title).should eq("Mark as Locked")
      assigns(:task).should be_kind_of(LockedRoomTask)
      assigns(:rooms).should eq(@hotel.rooms)
    end
  end

  context 'should create Task,' do

    it "as DamagedRoomTask and Mark as Maintenance with valid params" do
      expect {
        xhr :post, :create, task: valid_attributes_for_damaged_room, supplied_type: "DamagedRoomTask"
      }.to change(DamagedRoomTask, :count).by(1)

      assigns(:task).should be_kind_of(DamagedRoomTask)
      flash.now[:notice].should == I18n.t('notifications.task_created')
      response.should be_success
    end

    it "as LockedRoomTask and Mark as locked with valid params" do
      expect {
        xhr :post, :create, task: valid_attributes_for_locked_room, supplied_type: "LockedRoomTask"
      }.to change(LockedRoomTask, :count).by(1)

      assigns(:task).should be_kind_of(LockedRoomTask)
      flash.now[:notice].should == I18n.t('notifications.task_created')
      response.should be_success
    end

  end

  context "Remove Task" do

    it "show Remove Task popup for Remove from maintenance" do
      @task = FactoryGirl.create(:damaged_room_task, type: "DamagedRoomTask", hotel: @hotel, room: @room, assigned_user: @user, creator: @user)
      xhr :get, :edit_lb, id: @task.id

      assigns(:task).should eq(@task)
      assigns(:rooms).should eq(@hotel.rooms)
    end

    it "updates the damaged room task as done" do
      @task = FactoryGirl.create(:damaged_room_task, type: "DamagedRoomTask", hotel: @hotel, room: @room, assigned_user: @user, creator: @user)
      xhr :put, :update, :id => @task.id, :task => {done: "1"}
      assigns(:task).should eq(@task)
      flash.now[:notice].should == I18n.t('notifications.task_updated')
      response.should be_success
    end

    it "show Remove Task popup for Remove from locked" do
      @task = FactoryGirl.create(:locked_room_task, hotel: @hotel, room: @room, assigned_user: @user, creator: @user)
      xhr :get, :edit_lb, id: @task.id

      assigns(:task).should eq(@task)
      assigns(:rooms).should eq(@hotel.rooms)
    end

    it "updates the damaged room task as done/unlock" do
      @task = FactoryGirl.create(:locked_room_task, type: "LockedRoomTask", hotel: @hotel, room: @room, assigned_user: @user, creator: @user)
      xhr :put, :update, :id => @task.id, :task => {done: "1"}
      assigns(:task).should eq(@task)
      flash.now[:notice].should == I18n.t('notifications.task_updated')
      response.should be_success
    end


  end

end
