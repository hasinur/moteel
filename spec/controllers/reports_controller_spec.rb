require 'spec_helper'
require_relative '../supports/controller_spec_support'

describe FrontDesk::ReportsController do
  before(:each) do
    login_with_hotel_owner
  end

  describe "GET Arrival list" do
    it "Get all Arrival list in a given date range" do
      check_in1 = create_check_in_with(8.days.ago, 5.days.ago, :confirmed_check_in)
      check_in2 = create_check_in_with(Time.zone.now, 7.days.from_now, :confirmed_check_in)
      check_in3 = create_check_in_with(3.days.from_now, 7.days.from_now, :confirmed_check_in)
      check_in4 = create_check_in_with(3.days.from_now, 7.days.from_now, :reserved_check_in)

      get :arrival_list, {custom_search: {date_gteq: 1.day.ago, date_lteq: 8.days.from_now}}, {}
      response.should be_success

      report_should_be_equal_to(check_in2.allocated_rooms + check_in3.allocated_rooms)
    end
  end

  describe "GET No show  list" do
    it "Get all No Show  list in Today" do
      check_in = create_check_in_with(Time.zone.now-5.minutes, 2.days.from_now, :confirmed_check_in)
      check_in1 = create_check_in_with(Time.zone.now + 10.minutes, 7.days.from_now, :confirmed_check_in)

      get :no_show_guests, {}, {}
      response.should be_success
      assigns(:report).query.collect(&:id).should eq(check_in.allocated_room_ids)
      assigns(:report).query.size.should eq(1)
    end
  end

  describe "GET Check In list" do
    it "Get all Check In list in a given date range" do
      check_in1 = create_check_in_with(Time.zone.now, 5.days.from_now, :confirmed_check_in)
      check_in2 = create_check_in_with(Time.zone.now, 5.days.from_now, :reserved_check_in)
      check_in3 = create_check_in_with(Time.zone.now, 5.days.from_now, :checked_in_check_in)
      check_in4 = create_check_in_with(Time.zone.now, 7.days.from_now, :confirmed_check_in)
      check_in5 = create_check_in_with(Time.zone.now, 7.days.from_now, :checked_in_check_in)
      check_in6 = create_check_in_with(Time.zone.now, 7.days.from_now, :reserved_check_in)

      get :checkin_list,  {start_date: 1.day.ago, end_date: 8.days.from_now}, {}
      response.should be_success
      assigns(:checkin_list).collect(&:id).should eq(check_in3.allocated_room_ids + check_in5.allocated_room_ids)
      assigns(:checkin_list).size.should eq(2)
    end
  end

  describe "#check_out_list" do
    it "should reflect actual checkout count " do
      check_in1 = create_check_in_with(Time.zone.now, 5.days.from_now, :confirmed_check_in)
      check_in2 = create_check_in_with(Time.zone.now, 5.days.from_now, :checked_out_check_in)
      check_in3 = create_check_in_with(Time.zone.now, 5.days.from_now, :checked_in_check_in)
      check_in4 = create_check_in_with(Time.zone.now, 6.days.from_now, :confirmed_check_in)
      check_in5 = create_check_in_with(Time.zone.now, 6.days.from_now, :checked_out_check_in)
      check_in6 = create_check_in_with(Time.zone.now, 6.days.from_now, :checked_in_check_in)
      check_in7 = create_check_in_with(Time.zone.now, 8.days.from_now, :checked_out_check_in)
      check_in8 = create_check_in_with(Time.zone.now, 8.days.from_now, :checked_in_check_in)
      check_in9 = create_check_in_with(Time.zone.now, 8.days.from_now, :reserved_check_in)

      get :checkout_list,  {format:'js', date:[1.day.ago.to_s, 7.days.from_now.to_s]}, {}
      response.should be_success
      assigns(:checkout_list).collect(&:id).should eq(check_in2.allocated_room_ids + check_in5.allocated_room_ids + check_in7.allocated_room_ids )
      assigns(:checkout_list).size.should eq(3)
    end
  end


  describe "GET Departure  list" do
    it "Get all Departure list in a given date range" do
      check_in1 = create_check_in_with(Time.zone.now, 10.hours.from_now, :checked_in_check_in)
      check_in2 = create_check_in_with(Time.zone.now, 8.hours.from_now, :checked_in_check_in)
      check_in3 = create_check_in_with(Time.zone.now,9.hours.from_now, :checked_in_check_in)
      check_in4 = create_check_in_with(Time.zone.now, 2.days.from_now, :confirmed_check_in)
      check_in5 = create_check_in_with(Time.zone.now, 3.days.from_now, :checked_in_check_in)
      check_in6 = create_check_in_with(Time.zone.now, 4.days.from_now, :reserved_check_in)

      get :departure_guests, {format:'js', date:[Time.zone.now.to_s, 1.day.from_now.to_s]}, {}
      response.should be_success
      assigns(:departure_guests).collect(&:id).should eq(check_in1.allocated_room_ids+check_in2.allocated_room_ids + check_in3.allocated_room_ids)
      assigns(:departure_guests).size.should eq(3)
    end
  end

  describe "GET Temporary Reservation list" do
    it "Get all Temporary Reservation list in a given date range" do
      check_in1 = create_check_in_with(Time.zone.now, 1.day.from_now, :reserved_check_in)
      check_in2 = create_check_in_with(Time.zone.now, 3.days.from_now, :reserved_check_in)
      check_in3 = create_check_in_with(Time.zone.now, 5.days.from_now, :checked_in_check_in)
      check_in4 = create_check_in_with(Time.zone.now, 2.days.from_now, :reserved_check_in)
      check_in5 = create_check_in_with(Time.zone.now, 3.days.from_now, :checked_in_check_in)
      check_in6 = create_check_in_with(5.days.from_now, 7.days.from_now, :reserved_check_in)

      get :reservation_list, {format:'js', date:[Time.zone.now.to_s, 3.days.from_now.to_s]}, {}
      response.should be_success
      assigns(:reservation_list).collect(&:id).should eq(check_in1.allocated_room_ids+check_in2.allocated_room_ids + check_in4.allocated_room_ids)
      assigns(:reservation_list).size.should eq(3)
    end
  end

  describe "GET CheckIn Bill Summary  list" do
    it "Get all CheckIn Bill Summary list in a given date range" do
      check_in1 = create_check_in_with(Time.zone.now, 1.day.from_now, :reserved_check_in)
      check_in2 = create_check_in_with(Time.zone.now, 3.days.from_now, :reserved_check_in)
      check_in3 = create_check_in_with(Time.zone.now, 5.days.from_now, :checked_in_check_in)
      check_in4 = create_check_in_with(Time.zone.now, 2.days.from_now, :reserved_check_in)
      check_in5 = create_check_in_with(Time.zone.now, 3.days.from_now, :checked_in_check_in)
      check_in6 = create_check_in_with(5.days.from_now, 7.days.from_now, :reserved_check_in)

      get :checkin_bill_summary, {custom_search: {date_gteq: 1.day.ago, date_lteq: 8.days.from_now}}, {}
      response.should be_success
      report_should_be_equal_to(check_in3.allocated_rooms + check_in5.allocated_rooms)
      assigns(:report).query.size.should eq(2)
    end
  end

end


private
def create_check_in_with(start_date, end_date, check_in_type = :reserved_check_in)
  FactoryGirl.create(check_in_type.to_sym, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
end

def report_should_be_equal_to(array)
  assigns(:report).query.collect(&:id).sort.should eq(array.collect(&:id).sort)
end