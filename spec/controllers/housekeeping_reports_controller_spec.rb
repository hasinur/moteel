require 'spec_helper'
require_relative '../supports/controller_spec_support'

describe Housekeeping::HousekeepingReportsController do
  before(:each) do
    login_with_hotel_owner
  end

  describe "Payment Type Wise Transactions" do
    before(:each) do
      cash_counter1 = FactoryGirl.create(:cash_counter, branch: @hotel)
      cash_counter2 = FactoryGirl.create(:cash_counter, branch: @hotel)
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel)
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 300, room_type: room_type1 )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 400, room_type: room_type1 )
      check_in1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room1])
      check_in2 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, rooms: [room2])

      service_charge1 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:check_in1.allocated_rooms.first, amount:5)
      service_charge2 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:check_in2.allocated_rooms.first, amount:15)

      hotel_service_payment1 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, payment_method: 'cash',
                                                  hotel: @hotel, amount: 325,allow_edit:true, payment_date:Time.zone.now,
                                                  cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment2 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1, payment_method: 'cheque',
                                                  hotel: @hotel, amount: 425,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment3 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1,
                                                  hotel: @hotel, amount: 200,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_WITHDRAW)
      hotel_service_payment4 = FactoryGirl.create(:hotel_service_payment, check_in: check_in1,
                                                  hotel: @hotel, amount: 100,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter1,transaction_type:Constants::PAYMENT_WITHDRAW)

      hotel_service_payment5 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2,payment_method: 'cheque',
                                                  hotel: @hotel, amount: 800,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment6 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2,
                                                  hotel: @hotel, amount: 400,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_DEPOSIT)
      hotel_service_payment7 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2,
                                                  hotel: @hotel, amount: 300,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_WITHDRAW)
      hotel_service_payment8 = FactoryGirl.create(:hotel_service_payment, check_in: check_in2,
                                                  hotel: @hotel, amount: 200,allow_edit:true,payment_date:Time.zone.now,
                                                  cash_counter: cash_counter2,transaction_type:Constants::PAYMENT_WITHDRAW)

    end
    it "should show all transaction according to payment type" do
      get :payment_type_wise_transactions, {}, {}
      response.should be_success
      assigns(:report).records.collect{|rec| rec["Amount"]}.should eq([325,425,-200,-100,800,400,-300,-200])

    end

    it "should show all deposit information" do
      get :deposit_listing, {}, {}
      response.should be_success
      assigns(:transaction_list).collect{|rec| rec["Total Deposit"]}.should eq([(325+425-200-100),(800+400-300-200)])

    end

    it "should show all financial information" do
      get :financial_statement, {}, {}
      response.should be_success
      assigns(:transaction_list).collect{|rec| rec["Service Charges"]}.should eq([300*5+5, 400 *5+15])
      assigns(:transaction_list).collect{|rec| rec["Total paid"]}.should eq([(325+425-200-100),(800+400-300-200)])

    end
  end

  describe "Service wise revenue report" do
    it "should show all service wise revenue Report in a given date range" do
      room_type1 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Standard')
      room_type2 = FactoryGirl.create(:room_type, :price => 450, :hotel => @hotel, title: 'Delux')
      room1 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 80, room_type: room_type1, room_number: 'std2' )
      room2 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 80, room_type: room_type2, room_number: 'dlx3' )
      room3 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type1, room_number: 'std3' )
      room4 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 90, room_type: room_type2, room_number: 'dlx4' )
      room5 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type1, room_number: 'std4' )
      room6 = FactoryGirl.create(:room, hotel: @hotel, rate_price: 100, room_type: room_type2, room_number: 'dlx5' )
      @guest1 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Rahim')
      @guest2 = FactoryGirl.create(:guest, first_name:'Abdul', last_name: 'Karim')
      @check_in1 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest1), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room1])
      @check_in2 = FactoryGirl.create(:checked_in_check_in, billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest2), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room2])
      @check_in3 = FactoryGirl.create(:checked_out_check_in,billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest1), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room3])
      @check_in4 = FactoryGirl.create(:checked_out_check_in,billing_information: FactoryGirl.build(:guest_type_billing_information, billable: @guest2), hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, rooms: [room4])

      service_charge1 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in1.allocated_rooms.first, amount:5)
      service_charge2 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in2.allocated_rooms.first, amount:15)
      service_charge3 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in3.allocated_rooms.first, amount:25)
      service_charge4 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in4.allocated_rooms.first, amount:35)
      service_charge5 = FactoryGirl.create(:service_charge, hotel:@hotel, allocated_room:@check_in1.allocated_rooms.first, amount:45)
      get :service_wise_revenue, {start_date: 1.day.ago, end_date: 3.days.from_now}, {}
      response.should be_success
      assigns(:results).collect{|rec| rec["amount"]}.should eq([(80*2+80*2+90*2+90*2),5,15,25,35,45])
    end
  end


end


private
def create_check_in_with(start_date, end_date, check_in_type = :reserved_check_in)
  FactoryGirl.create(check_in_type.to_sym, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
end

def report_should_be_equal_to(array)
  assigns(:report).query.collect(&:id).sort.should eq(array.collect(&:id).sort)
end