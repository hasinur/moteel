require 'spec_helper'
require 'controller_spec_support'

describe RoomServiceController do

  before(:each) do
    login_with_hotel_owner
    @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, number_of_rooms: 1)
  end

  context "should open room service tab" do

    it "show room service tab" do
      get :home
      assigns(:task).should be_kind_of(Task)
    end

  end

end
