require 'spec_helper'
require 'controller_spec_support'

describe HotelServicesPaymentController do

  before(:each) do
    login_with_hotel_owner
    @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, number_of_rooms: 1)
  end

  context "should manage payment" do

    it "show manage payment popup" do
      post :add_payment, :check_in_id => @check_in.id
      assigns(:hotel_service_payment).should be_kind_of(HotelServicePayment)
      assigns(:check_in).should eq(@check_in)
      assigns(:folios).should eq(@check_in.folios.un_paid.not_invoiced)
    end

    it "add the payment" do
      dept = FactoryGirl.create(:department, branch: @hotel)
      f1 = FactoryGirl.create(:folio, check_in: @check_in, department: dept, bill: 100)
      f2 = FactoryGirl.create(:folio, check_in: @check_in, department: dept, bill: 120)

      hotel_service_payment = FactoryGirl.create(:hotel_service_payment, check_in: @check_in, hotel: @hotel, amount: 250, allow_edit: true, cash: true, folio_ids: [f1.id, f2.id]).attributes

      xhr :post, :create_payment, :hotel_service_payment => hotel_service_payment
      assigns(:hotel_service_payment).should be_kind_of(HotelServicePayment)
      assigns(:check_in).should eq(@check_in)
      assigns(:folios).should eq(@check_in.folios.un_paid.not_invoiced)
    end


  end

end
