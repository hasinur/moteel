require 'spec_helper'
require_relative '../supports/controller_spec_support'

describe CheckInsController do
  describe "POST /check_ins" do
    it "creating check in" do
      login_with_hotel_owner
      @guest = FactoryGirl.create(:guest)
      @room = FactoryGirl.create(:room)
      start_date, end_date = Time.zone.now, 1.days.from_now

      check_in_params = {check_in: {start_date: start_date.to_s,
                                    end_date: end_date.to_s,
                                    status: Constants::CHECK_IN_STATUSES[:reserved],
                                    billing_information_attributes: {billable_id: @guest.id,
                                                                     billable_type: "Guest",
                                                                     reference_guest_id: @guest.id},
                                    allocated_rooms_attributes: {"0" => {hotel_id: @hotel.id, room_type_id: @room.room_type_id, room_id: @room.id,
                                                                         allocated_room_guests_attributes: {"0" => {guest_id: @guest.id}},
                                                                         price: 600}},
                                    booked_by: "A.K.M. Ashrafuzzaman",
                                    booking_date: "2013-02-18 07:39 AM",
                                    note: ""},
                         whodoneit: @user.id, format: 'js'}

      post :create, check_in_params, {}
      response.status.should be(200)
      assigns(:check_in).should_not be nil
      CheckIn.count.should be 1
    end
  end
end