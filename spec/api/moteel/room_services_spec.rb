require 'spec_helper'
describe Moteel::API do

  before(:each) do
    initialize_api_data #initializes @user, @token, @hotel
  end

  describe "GET /v1/room_services" do
    it "Returns all rooms and rooms status under the hotel id provided." do
      start_date, end_date, task_status = Time.zone.today, 1.days.from_now, Constants::TASK_STATUSES[:completed]
      @assigned_user = FactoryGirl.create(:user)
      @task = FactoryGirl.create(:task, hotel: @hotel, assigned_user: @assigned_user, creator: @user, type: "CheckInAddOnTask", status: task_status)
      room_services = CheckInAddOnTask.with_departments(@task.department_id).by_due_date(start_date, end_date)
      room_services = room_services.with_status(task_status)

      @test_response = {:hotel_id => @hotel.id,
                        :title => @hotel.available_locales_hash(:title),
                        :room_services => room_services.collect { |c| {id: c.id, :title => c.available_locales_hash(:title)} },
      }

      moc_params = {hotel_id: @hotel.id, department_id: @task.department_id, start_date: start_date, end_date: end_date, status: task_status}

      get "/api/v1/room_services.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", moc_params
      response.body.should == @test_response.to_json
    end
  end

  describe "PUT /v1/room_services/checkin/:id/add_payment" do
    it "Update payment for a certain checkin under the  hotel id provided." do
      @main_guest = FactoryGirl.create(:user)
      @cash_counter = FactoryGirl.create(:cash_counter, branch: @hotel)
      @hotel_payment_type = FactoryGirl.create(:hotel_payment_type, hotel: @hotel)
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, number_of_rooms: 1, cash_counter_id: @cash_counter.id)

      moc_params = {check_in: {hotel_service_payments_attributes: {:"0" => {hotel_payment_type_id: @hotel_payment_type.id, allocated_room_id: @check_in.allocated_rooms.first.id, amount: 1999,
                                                                            note: "test", hotel_id: @hotel.id, cash_counter_id: @cash_counter.id}}},
                    allocated_room_id: @check_in.allocated_rooms.first.id, allow_print_money_receipt: true}


      put "/api/v1/room_services/checkin/#{@check_in.id}/add_payment.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

  describe "DELETE /v1/room_services/tasks/destroy" do
    it "Delete task under the hotel id provided." do
      @task = FactoryGirl.create(:task, hotel: @hotel)
      moc_params = {task_ids: [@task.id]}
      expect { delete "/api/v1/room_services/tasks/destroy.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", moc_params }.to change(Task, :count).by(-1)
      response.body.should == {:success => true}.to_json
    end
  end

  describe "PUT /v1/room_services/tasks/cancel" do
    it "Cancel task under the hotel id provided." do
      @task = FactoryGirl.create(:task, hotel: @hotel)
      moc_params = {task_ids: [@task.id]}
      moc_params[:task_ids].each do |task_id|
        Task.find(task_id).canceled!
      end
      put "/api/v1/room_services/tasks/cancel.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

end
