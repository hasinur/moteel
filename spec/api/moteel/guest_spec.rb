require 'spec_helper'
describe Moteel::API do
  describe "POST /v1/guest/create" do
    it "Add guest." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @guest = FactoryGirl.create(:guest, agent_user_id: @user.id)
      token = @user.access_grants.first.access_token
      guest_hash = {guest:{
                       :first_name => @guest.first_name,
                       :middle_name => @guest.middle_name,
                       :last_name =>@guest.last_name,
                       :address => @guest.address
        }
      }
      guest = FactoryGirl.create(:guest, agent_user_id: @user.id)
      #ap guest
      post "/api/v1/guests/create?user_id=#{@user.id}&access_token=#{token}",guest_hash
      response.body.should.to_json == {:success => true}.to_json
    end
  end
end

