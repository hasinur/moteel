require 'spec_helper'
describe Moteel::API do
  describe "POST /v1/hotel_services_payment/invoice/create" do
    it "Add invoice." do
      @user = FactoryGirl.create(:user)
      @hotel_services_payment = FactoryGirl.create(:hotel_service_payment)

      token = @user.access_grants.first.access_token
      @test_response = {id: @hotel_services_payment.id,
                                       :hotel_payment_type_id => @hotel_services_payment.hotel_payment_type_id,
                                       :guest_name => @hotel_services_payment.check_in.allocated_rooms.first.main_guest.name,
                                       :room_no => @hotel_services_payment.check_in.allocated_rooms.first.room.title,
                                       :amount => @hotel_services_payment.amount,
                                       :unit_of_currency => @hotel_services_payment.hotel.price_unit.title,
                                       :note => @hotel_services_payment.note,
                                       :reservation_line_item_id => @hotel_services_payment.reservation_line_item_id
      }

      get "/api/v1/hotel_services_payment/invoice/create?payment_ids=#{@hotel_services_payment.id}&access_token=#{token}"
      response.body.should.to_json == [@test_response].to_json
    end
  end


  describe "POST /v1/hotel_services_payment/folio/create" do
    it "Generate Folio from hotel service payment." do
      #@check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, number_of_rooms: 1)
      @folio = FactoryGirl.create(:folio)
      moc_params = {reservation_line_item_ids: [@folio.reservation_line_item.id]}

      post "/api/v1/hotel_services_payment/folio/create.json?&access_token=#{@token}&check_in_id=#{@folio.check_in.id}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

end

