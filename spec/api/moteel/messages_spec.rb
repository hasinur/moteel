require 'spec_helper'
describe Moteel::API do

  before(:each) do
    @user = FactoryGirl.create_list(:user, 2)
    @messages = FactoryGirl.create_list(:message, 2, :title => 'API test messages', :sender => @user.last, :receiver => @user.first, :number_of_recipients => 1)
  end


  describe "GET /v1/messages/inbox" do
    it "Fetch all messages." do
      token = @user.last.access_grants.first.access_token
      @test_response = {:user => @user.first.as_json,
                        :messages => @messages.as_json(:methods => [:sender_name],
                                                       :include => [:sender, :message_recipients, :recipients])
      }
      get "/api/v1/messages/inbox.json?user_id=#{@user.first.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end


  describe "GET /v1/messages/:id" do
    it "Fetch a message under the message id provided." do
      token = @user.first.access_grants.first.access_token
      @test_response = {:messages => @messages.first.as_json(:methods => [:sender_name],
                                                       :include => [:sender, :message_recipients, :recipients])
      }
      get "/api/v1/messages/#{@messages.first.id}.json?access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "PUT /v1/messages.json" do
    it "Make a message status Read" do
      token = @user.first.access_grants.first.access_token
      moc_params = {user_id: @user.first.id, message_id: @messages.first.id}

      @unread_message = Message.find(@messages.first.id)

      @test_response = { :message_id => @messages.first.id,
                         :user_id => @user.first.id,
                         :mark_read =>true
      }
      put "/api/v1/messages.json?access_token=#{token}", moc_params
      response.body.should == @test_response.to_json
      @unread_message = @unread_message.reload
      @unread_message.message_recipients.first.read_at.should_not be_nil
    end
  end



  describe "PUT /v1/messages/create.json" do
    it "Create a News Message" do
      token = @user.first.access_grants.first.access_token
      @multi_user = FactoryGirl.create_list(:user, 2)
      moc_params = {sender_id: @user.last.id,
                    message: {title:"API test Message",
                              content:"API TEST Message Body",
                              recipient_ids:[@user.first.id,@multi_user.first.id, @multi_user.last.id]
                    }
      }

      @test_response = { :message => message,
                         :success => true
      }
      put "/api/v1/messages/create.json?access_token=#{token}", moc_params
      all_message = Message.all
      @test_response = { :message => all_message.last.as_json,
                         :success => true
      }
      response.body.should == @test_response.to_json
    end
  end
end

