require 'spec_helper'
describe Moteel::API do

  before :each do
    initialize_api_data #initializes @user, @token, @hotel
  end

  it "should check authorized user for given hotel" do
    unless @hotel && !@user.accessible_hotel_ids.include?(@hotel.id)
      stub!(:authorize!).and_return(true)
    end
    authorize!.should == true
  end

  it "should add 401 error for an unauthorized user" do
    hotel = FactoryGirl.create(:hotel, time_zone: Time.zone.name)
    if hotel && !@user.accessible_hotel_ids.include?(hotel.id)
      stub!(:authorize!).and_return('401 Unauthorized')
    end
    authorize!.should == '401 Unauthorized'
  end

end