require 'spec_helper'
require 'api_helper'

describe Moteel::API do

  before(:each) do
    initialize_api_data #initializes @user, @token, @hotel
    @assigned_user = FactoryGirl.create(:user)
    @task = FactoryGirl.create(:task, hotel: @hotel, assigned_user: @assigned_user, creator: @user)
  end

  describe "GET /v1/front_desk_report/departure_list.json" do
    it "Fetch all Departure list in a given date." do
      @token = @user.access_grants.first.access_token
      @checkin1 = FactoryGirl.create(:checked_in_check_in, hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)
      @checkin2 = FactoryGirl.create(:checked_in_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:6.days.from_now,number_of_rooms:1)
      @checkin3 = FactoryGirl.create(:checked_in_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:7.days.from_now,number_of_rooms:1)
      departure_list = AllocatedRoom.where(check_in_id: @checkin1.id)
      from_date = "'#{Date.current}'"
      to_date = "'#{5.days.from_now.to_date}'"
      @test_response = {:hotel_id => @hotel.id,
       :title => "Departure list",
       :start_date =>from_date,
       :end_date => to_date,
       :departure_list => departure_list.as_json(:methods => [:check_in_guest_str], :include => [:check_in, :guests, :room])

      }
      get "/api/v1/front_desk_report/departure_list.json?hotel_id=#{@hotel.id}&start_date=#{from_date}&end_date=#{to_date}&access_token=#{@token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk_report/temp_reservation_list.json" do
    it "Fetch all Temporary Reservation list in a given date" do
      @checkin4 = FactoryGirl.create(:reserved_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)
      @checkin5 = FactoryGirl.create(:reserved_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:6.days.from_now,number_of_rooms:1)
      @checkin6 = FactoryGirl.create(:reserved_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:7.days.from_now,number_of_rooms:1)
      reservation_list = AllocatedRoom.where("check_in_id IN (?)", [@checkin4.id,@checkin5.id,@checkin6.id])
      from_date = "'#{Date.current}'"
      to_date = "'#{5.days.from_now.to_date}'"
      @test_response = {:hotel_id => @hotel.id,
                        :title => "Temporary Reservation list",
                        :start_date =>from_date,
                        :end_date => to_date,
                        :reservation_list => reservation_list.as_json(:methods => [:check_in_guest_str], :include => [:check_in, :guests, :room])

      }
      get "/api/v1/front_desk_report/temp_reservation_list.json?hotel_id=#{@hotel.id}&start_date=#{from_date}&end_date=#{to_date}&access_token=#{@token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk_report/no_show_reservation.json" do
    it "Fetch all no show Reservation list in a given hotel" do
      @checkin7 = FactoryGirl.create(:reserved_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)
      @checkin8 = FactoryGirl.create(:confirmed_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)

      current_time = Time.zone.now
      condition =[]
      no_show_list = CheckIn.where("id IN (?)", [@checkin7.id,@checkin8.id]).collect(&:allocated_rooms).flatten

      @test_response = {:hotel_id => @hotel.id,
                        :title => "No Show Reservation list",
                        :no_show_list => no_show_list.as_json(:methods => [:check_in_guest_str])
      }

      get "/api/v1/front_desk_report/no_show_reservation.json?hotel_id=#{@hotel.id}&access_token=#{@token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk_report/arrival_list.json" do
    it "Fetch all arrival list in a given hotel" do
      @checkin9 = FactoryGirl.create(:checked_in_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)
      @checkin10 = FactoryGirl.create(:confirmed_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)

      current_time = Time.zone.now
      condition =[]
      arrival_list = CheckIn.where("id IN (?)", [@checkin9.id,@checkin10.id]).collect(&:allocated_rooms).flatten
      from_date = "'#{Date.current}'"
      to_date = "'#{5.days.from_now.to_date}'"
      @test_response = {:hotel_id =>@hotel.id,
                        :title => "Arrival list",
                        :arrival_list => arrival_list.as_json(:methods => [:check_in_guest_str], :include => [:guests, :room])
      }

      get "/api/v1/front_desk_report/arrival_list.json?hotel_id=#{@hotel.id}&start_date=#{from_date}&end_date=#{to_date}&access_token=#{@token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk_report/checkin_list.json" do
    it "Fetch all check in list in a given hotel" do
      @checkin11 = FactoryGirl.create(:checked_in_check_in,hotel:@hotel,start_date:Time.zone.now,end_date:5.days.from_now,number_of_rooms:1)
      checkin_list = AllocatedRoom.where(check_in_id: @checkin11.id)
      from_date = "'#{Date.current}'"
      to_date = "'#{5.days.from_now.to_date}'"
      @test_response = {:hotel_id => @hotel.id,
                        :title => "Check In list",
                        :checkin_list => checkin_list.as_json(:methods => [:check_in_guest_str], :include => [:guests, :room])
      }
      get "/api/v1/front_desk_report/checkin_list.json?hotel_id=#{@hotel.id}&start_date=#{from_date}&end_date=#{to_date}&access_token=#{@token}"
      response.body.should == @test_response.to_json
    end
  end
  describe "GET /v1/front_desk_report/tasks/assigned" do
    it "Returns all Assigned room tasks under the hotel id and user id provided." do
      hotel = Hotel.find(@hotel.id)
      tasks = Task.with_branch(@hotel.id).assigned.assigned_to(@assigned_user.id).includes(:department => :translations, :creator => {:profile => :translations})
      @test_response = {:hotel_id => @hotel.id,
                        :title => hotel.available_locales_hash(:title),
                        :tasks => tasks.collect { |task| {id: task.id, title: task.available_locales_hash(:title), status: task.status, creator: task.creator.full_name,
                                                          created_at: task.created_at.calendarize, department: task.department.name, room: task.room.title, due_date: task.due_date.calendarize} }
      }

      get "/api/v1/front_desk_report/tasks/assigned.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&user_id=#{@assigned_user.id}"

      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk_report/tasks/created" do
    it "Returns all Created room tasks under the hotel id and user id provided." do
      hotel = Hotel.find(@hotel.id)
      tasks = Task.with_branch(@hotel.id).with_creator(@user.id).includes(:department => :translations, :assigned_user => :profile, :creator => :profile)
      @test_response = {:hotel_id => @hotel.id,
                        :title => hotel.available_locales_hash(:title),
                        :tasks => tasks.collect { |task| {id: task.id, title: task.available_locales_hash(:title), status: task.status, creator: task.creator.full_name,
                                                          created_at: task.created_at.calendarize, department: task.department.name, room: task.room.title, due_date: task.due_date.calendarize} }
      }

      get "/api/v1/front_desk_report/tasks/created.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&user_id=#{@user.id}"

      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk_report/tasks/over_due" do
    it "Returns all Over Due room tasks under the hotel id and user id provided." do
      hotel = Hotel.find(@hotel.id)
      tasks = Task.with_branch(@hotel.id).assigned.assigned_to(@assigned_user.id).over_due.includes(:department => :translations, :assigned_user => :profile, :creator => :profile)
      @test_response = {:hotel_id => @hotel.id,
                        :title => hotel.available_locales_hash(:title),
                        :tasks => tasks.collect { |task| {id: task.id, title: task.available_locales_hash(:title), status: task.status, creator: task.creator.full_name,
                                                          created_at: task.created_at.calendarize, department: task.department.name, room: task.room.title, due_date: task.due_date.calendarize} }
      }

      get "/api/v1/front_desk_report/tasks/over_due.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&user_id=#{@assigned_user.id}"

      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/front_desk/room_view_data" do
    it "Returns all Data for room view under the hotel id provided." do
      filter_start_date, filter_end_date = Time.zone.today, Time.zone.today + 1.day
      @room = FactoryGirl.create(:room, hotel: @hotel)
      @report_item_daily_activity = FactoryGirl.create(:report_item_daily_activity, entity_id: @hotel.id, item_type_id: @room.room_type_id, start_date: filter_start_date, end_date: filter_end_date)
      @report_item = FactoryGirl.create(:report_item, entity_id: @hotel.id, item_type_id: @room.room_type_id)

      @test_response = ReportItemDailyActivity.generate_availability(@hotel.id, @room.room_type_id, filter_start_date, filter_end_date)

      get "/api/v1/front_desk/room_view_data.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&filter_start_date=#{filter_start_date}&filter_end_date=#{filter_end_date}&room_type_id=#{@room.room_type_id}"

      response.body.should == @test_response.to_json
    end
  end

end

