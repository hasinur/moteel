require 'spec_helper'
describe Moteel::API do

  before(:each) do
    initialize_api_data #initializes @user, @token, @hotel
    @room = FactoryGirl.create(:room, hotel: @hotel)
    @marker = FactoryGirl.create(:marker, hotel: @hotel)
    @department = FactoryGirl.create(:department, branch: @hotel)
  end


  describe "PUT /v1/front_desk_operation/rooms/:id/mark_as_dirty" do
    it "Room mark as Dirty." do
      @room.dirty!
      put "/api/v1/front_desk_operation/rooms/#{@room.id}/mark_as_dirty.json?&access_token=#{@token}&hotel_id=#{@hotel.id}"
      response.body.should == {:success => true}.to_json
    end
  end

  describe "PUT /v1/front_desk_operation/rooms/:id/mark_as_clean" do
    it "Room mark as Clean." do
      @room.clean!
      put "/api/v1/front_desk_operation/rooms/#{@room.id}/mark_as_clean.json?&access_token=#{@token}&hotel_id=#{@hotel.id}"
      response.body.should == {:success => true}.to_json
    end
  end

  describe "POST /v1/front_desk_operation/tasks/create" do
    it "Add Task to Room. You can Specify Task as mark as Locked or Maintenance." do
      moc_params = {task: {title: "Test task", room_id: @room.id, department_id: @department.id, assigned_user_id: @user.id, due_date: Time.zone.now, done: 0, note: "test note",
                           locked_room_attributes: {entry_date: Time.zone.now, released_date: (Time.zone.now+1.day), hotel_id: @hotel.id, room_id: @room.id}}, supplied_type: "LockedRoomTask"}

      post "/api/v1/front_desk_operation/tasks/create.json?&access_token=#{@token}&user_id=#{@user.id}&hotel_id=#{@hotel.id}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

  describe "GET /api/v1/front_desk_operation/markers" do
    it "Fetch all Markers list." do
      @test_response = {:hotel_id => @hotel.id,
                        :title => @hotel.available_locales_hash(:title),
                        :available_markers => @room.markers.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} },
                        :permanent_markers => Marker.filtered(@hotel.id).permanent.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} },
                        :temporary_markers => Marker.filtered(@hotel.id).temporary.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} }
      }
      get "/api/v1/front_desk_operation/markers.json?&access_token=#{@token}&room_id=#{@room.id}&hotel_id=#{@hotel.id}"

      response.body.should == @test_response.to_json
    end
  end

  describe "PUT v1/front_desk_operation/markers/:id/update_markers" do
    it "Assigns Marker to Room." do
      moc_params = {room: {marker_ids: ["#{@marker.id}"]}}

      @room.update_attributes(moc_params[:room])
      put "/api/v1/front_desk_operation/markers/#{@room.id}/update_markers.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

  describe "GET /api/v1/front_desk_operation/availability_list" do
    it "Fetch all Markers list." do
      reporting_date, filter_start_date, filter_end_date = Time.zone.today, Time.zone.today + 1.day, Time.zone.today
      @report_item_daily_activity = FactoryGirl.create(:report_item_daily_activity, entity_id: @hotel.id, item_type_id: @room.room_type_id, start_date: filter_start_date, end_date: filter_end_date)
      @report_task = FactoryGirl.create(:report_task, entity_id: @hotel.id, item_type_id: @room.room_type_id, item_id: @room.id)

      todays_activity_count = ReportEntityDailyActivity.where(:date => reporting_date, :entity_id => @hotel.id).first[:todays_activity_count] rescue {}
      items = ReportItemDailyActivity.generate_availability(@hotel.id, @room.room_type_id, filter_start_date, filter_end_date)
      item_types = @hotel.room_types
      item_with_task_count = ReportTask.tasks_by_room_within_dates(@hotel.id, @user.id, filter_start_date, filter_end_date)

      custom_markers = @hotel.markers
      compulsory_markers = custom_markers.compulsory.includes(:translations)

      @test_response = {:hotel_id => @hotel.id,
                        :title => Hotel.find(@hotel.id).available_locales_hash(:title),
                        :todays_activity_count => todays_activity_count,
                        :items => items.collect { |c| {item_id: c[:item_id], name: c[:name], current_balance: c[:current_balance], status: c[:status], reference_id: c[:reference_id], check_in_id: c[:check_in_id]} },
                        :item_types => item_types.collect { |c| {id: c.id, :title => c.available_locales_hash(:title)} },
                        :item_with_task_count => item_with_task_count.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} },
                        :custom_markers => custom_markers.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} },
                        :compulsory_markers => compulsory_markers.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} }
      }
      get "/api/v1/front_desk_operation/availability_list.json?&access_token=#{@token}&user_id=#{@user.id}&hotel_id=#{@hotel.id}&filter_start_date=#{filter_start_date}&filter_end_date=#{filter_end_date}"

      response.body.should == @test_response.to_json
    end
  end

end
