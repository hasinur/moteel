require 'spec_helper'
describe Moteel::API do

  #before(:each) do
  #  @user = FactoryGirl.create(:user)
  #  @country = FactoryGirl.create(:country, :title => 'Bangladesh')
  #  @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
  #  @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
  #  @room_type = FactoryGirl.create(:room_type, hotel: @hotel, price_unit_id: 2, price: 334.0)
  #  @room = FactoryGirl.create(:room, hotel: @hotel, room_type: @room_type)
  #  @marker = FactoryGirl.create(:marker, hotel: @hotel)
  #  @department = FactoryGirl.create(:department, branch: @hotel)
  #end


  describe "GET /api/v1/look_up/countries" do
    it "Fetch all countries." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      token = @user.access_grants.first.access_token
      @test_response = {:id => @country.id, :title => @country.available_locales_hash(:title)}
          get "/api/v1/look_up/countries.json?access_token=#{token}"
          response.body.should == [@test_response].to_json
    end
  end

  describe "GET /api/v1/look_up/cities" do
    it "Fetch all cities." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      token = @user.access_grants.first.access_token
      @test_response = {:country_id => @country.id,
                        :country_title => @country.available_locales_hash(:title),
                        :cities => @country.active_cities.all.collect { |c| {id: c.id, :name => c.available_locales_hash(:title)}}}
      get "/api/v1/look_up/cities.json?country_id=#{@country.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/banks" do
    it "Fetch all banks." do
        @user = FactoryGirl.create(:user)
        @country = FactoryGirl.create(:country, :title => 'Bangladesh')
        @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
        @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
        @bank = FactoryGirl.create(:bank, branch_id: @hotel.id)
        token = @user.access_grants.first.access_token
      @test_response = {:id => @bank.id, :name => @bank.available_locales_hash(:name)}
      get "/api/v1/look_up/banks.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == [@test_response].to_json
    end
  end

  describe "GET /api/v1/look_up/bank_branches" do
    it "Fetch all bank branches." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @bank = FactoryGirl.create(:bank, branch_id: @hotel.id)
      @bank_branch = FactoryGirl.create(:bank_branch, bank_id: @bank.id, branch_id: @hotel.id)
      @test_response = @bank.bank_branches.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)}}
      token = @user.access_grants.first.access_token
      #@test_response = {:id => @bank_branch.id, :name => @bank_branch.available_locales_hash(:name)}
      get "/api/v1/look_up/bank_branches.json?bank_id=#{@bank.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/bank_branch_accounts" do
    it "Fetch all bank branch accounts." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @bank = FactoryGirl.create(:bank, branch_id: @hotel.id)
      @bank_branch = FactoryGirl.create(:bank_branch, bank_id: @bank.id, branch_id: @hotel.id)
      @bank_branch_account = FactoryGirl.create(:bank_branch_account, bank_branch_id: @bank_branch.id)
      token = @user.access_grants.first.access_token
      @test_response = @bank_branch.bank_branch_accounts.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)}}

      #@test_response = {:id => @bank_branch_account.id, :name => @bank_branch_account.available_locales_hash(:name)}
      get "/api/v1/look_up/bank_branch_accounts.json?bank_branch_id=#{@bank_branch.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/departments" do
    it "Fetch all departments." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @department = FactoryGirl.create(:department, branch: @hotel)
      token = @user.access_grants.first.access_token
      @test_response = @hotel.departments.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)}}
      get "/api/v1/look_up/departments.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/guests" do
    it "Fetch all guests." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @guest = FactoryGirl.create(:guest, agent_user_id: @user.id)
      token = @user.access_grants.first.access_token
      @test_response = {id: @guest.id,
                              :name => @guest.name,
                              :id_number => @guest.id_number,
                              :id_type => @guest.id_type,
                              :email => @guest.email,
                              :phone => @guest.phone,
                              :mobile => @guest.mobile,
                              :city => @guest.city,
                              :country_id => @guest.country_id,
                              :post_code => @guest.post_code,
                              :customer_type => @guest.customer_type,
                              :passport_number => @guest.passport_number,
                              :visa_number => @guest.visa_number,
                              :nationality_id => @guest.nationality_id,
                              :address => @guest.address,
                              :language => @guest.language,
                              :job_title => @guest.job_title,
                              :guest_type => @guest.guest_type,
                              :company => @guest.company,
                              :currency => @guest.currency,
                              :date_of_birth => @guest.date_of_birth,
                              :nationality => @guest.nationality
      }
      get "/api/v1/look_up/guests.json?agent_user_id=#{@user.id}&access_token=#{token}"
      response.body.should == [@test_response].to_json
    end
  end

  describe "GET /api/v1/look_up/groups" do
    it "Fetch all groups." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @group = FactoryGirl.create(:group, agent_user_id: @user.id)
      token = @user.access_grants.first.access_token
      @test_response = {id: @group.id,
                        :name => @group.available_locales_hash(:name),
                        :address => @group.available_locales_hash(:address),
                        :billing_address => @group.available_locales_hash(:billing_address),
                        :business_type => @group.available_locales_hash(:business_type),
                        :email => @group.email,
                        :phone => @group.phone,
                        :group_type => @group.group_type,
                        :city => @group.city,
                        :country_id => @group.country_id,
                        :postal_code => @group.postal_code,
                        :contact_person => @group.contact_person,
                        :language => @group.language,
                        :telephone => @group.telephone,
                        :fax => @group.fax
      }
      get "/api/v1/look_up/groups.json?agent_user_id=#{@user.id}&access_token=#{token}"
      response.body.should == [@test_response].to_json
    end
  end

  describe "GET /api/v1/look_up/markers" do
    it "Fetch all markers." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel, price_unit_id: 2, price: 334.0)
      @room = FactoryGirl.create(:room, hotel: @hotel, room_type: @room_type)
      @marker = FactoryGirl.create(:marker, hotel: @hotel)
      token = @user.access_grants.first.access_token
      @test_response = @hotel.markers.collect { |c| {id: c.id,
                        :title => c.available_locales_hash(:title),
                        :permanent => c.permanent,
                        :compulsory => c.compulsory
      }}
      get "/api/v1/look_up/markers.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/hotel_discounts" do
    it "Fetch all hotel_discounts." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @hotel_discount = FactoryGirl.create(:hotel_discount, hotel_id: @hotel.id)
      token = @user.access_grants.first.access_token
      @test_response =  {id: @hotel_discount.id,
                        :title => @hotel_discount.available_locales_hash(:title),
                        :code => @hotel_discount.code,
                        :fixed => @hotel_discount.fixed,
                        :discount_value => @hotel_discount.discount_value
      }
      get "/api/v1/look_up/hotel_discounts.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == [@test_response].to_json
    end
  end

  describe "GET /api/v1/look_up/hotel_payment_types" do
    it "Fetch all payment types" do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @hotel_payment_type = FactoryGirl.create(:hotel_payment_type, hotel_id: @hotel.id)
      token = @user.access_grants.first.access_token
      @test_response = @hotel.hotel_payment_types.collect { |c| {id: c.id,
                                                                 :title => c.available_locales_hash(:title),
                                                                 :account_id => c.account_id
      } }
      get "/api/v1/look_up/hotel_payment_types.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/hotels" do
    it "Fetch all hotels." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, user_id: @user.id,  country: @country, :city => @city)
      token = @user.access_grants.first.access_token
      @test_response = {id: @hotel.id,
                       :title => @hotel.available_locales_hash(:title),
                       :time_zone => @hotel.time_zone
      }
      get "/api/v1/look_up/hotels.json?user_id=#{@user.id}&access_token=#{token}"
      response.body.should == [@test_response].to_json
    end
  end

  describe "GET /api/v1/look_up/room_type" do
    it "Fetch all room types." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel, price_unit_id: 2, price: 334.0)
      token = @user.access_grants.first.access_token
      @test_response = @hotel.room_types.collect { |c| {id: c.id,
                                                             :title => c.available_locales_hash(:title),
                                                             :is_active => c.is_active,
                                                             :price => c.price,
                                                             :adult_nos => c.adult_nos,
                                                             :child_nos => c.child_nos
      }}
      get "/api/v1/look_up/room_types.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/rooms" do
    it "Fetch all rooms." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel, price_unit_id: 2, price: 334.0)
      @room = FactoryGirl.create(:room, hotel_id: @hotel.id, room_type: @room_type)
      token = @user.access_grants.first.access_token
      @test_response = @hotel.rooms.collect { |c| {id: c.id,
                                                             :title => c.available_locales_hash(:title),
                                                             :price => c.price,
                                                             :house_keeping_status => c.house_keeping_status
      }}
      get "/api/v1/look_up/rooms.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /api/v1/look_up/account_heads" do
    it "Fetch all account heads." do
      @user = FactoryGirl.create(:user)
      @country = FactoryGirl.create(:country, :title => 'Bangladesh')
      @city = FactoryGirl.create(:city, :title => "Dhaka", :country => @country)
      @hotel = FactoryGirl.create(:hotel, country: @country, :city => @city)
      @account_head = FactoryGirl.create(:account, entity_id: @hotel.id)
      token = @user.access_grants.first.access_token
      @test_response = @hotel.accounts.where(leaf_node: true).collect  { |c| {id: c.id,
                                                        :name => c.available_locales_hash(:name),
                                                        :current_balance => c.current_balance,
                                                        :code => c.code,
                                                        :account_type => c.account_type
      }}
      get "/api/v1/look_up/account_heads.json?hotel_id=#{@hotel.id}&access_token=#{token}"
      response.body.should == @test_response.to_json
    end
  end

  #



  #describe "POST /v1/front_desk_operation/tasks/task" do
  #  it "Add Task to Room. You can Specify Task as mark as Locked or Maintenance." do
  #    moc_params = {task: {title: "test", room_id: @room.id, department_id: @department.id, assigned_user_id: @user.id, due_date: "2013-02-11 11:59 PM", done: 0, note: "test note",
  #                         locked_room_attributes: {entry_date: "2013-02-11 09:00 AM", released_date: "2013-02-12 12:01 AM", hotel_id: @hotel.id, room_id: @room.id}}, supplied_type: "LockedRoomTask"}
  #
  #    task = FactoryGirl.create(:locked_room_task, moc_params[:task]) rescue FactoryGirl.create(:task, moc_params[:task])
  #    task.creator = @user
  #    task.hotel_id = @hotel.id
  #    post "/api/v1/front_desk_operation/tasks/task?task=#{moc_params}"
  #    response.body.should == {:success => true}.to_json
  #  end
  #end

  #describe "GET /api/v1/look_up/account_heads" do
  #  it "Fetch all Markers list." do
  #    @test_response = {:hotel_id => @hotel.id,
  #                      :title => @hotel.available_locales_hash(:title),
  #                      :available_markers => @room.markers.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} },
  #                      :permanent_markers => Marker.filtered(@hotel.id).permanent.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} },
  #                      :temporary_markers => Marker.filtered(@hotel.id).temporary.collect { |c| {id: c.id, :name => c.available_locales_hash(:name)} }
  #    }
  #    get "/api/v1/front_desk_operation/markers?room_id=#{@room.id}&hotel_id=#{@hotel.id}"
  #
  #    response.body.should == @test_response.to_json
  #  end
  #end
end

