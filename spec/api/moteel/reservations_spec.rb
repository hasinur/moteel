require 'spec_helper'

describe Moteel::API do

  before(:each) do
    initialize_api_data #initializes @user, @token, @hotel
    @hotel = FactoryGirl.create(:hotel)
    @room = FactoryGirl.create(:room, hotel: @hotel)
    @guest = FactoryGirl.create(:guest)

    @user.assigned_hotels << @hotel

  end

  describe "POST /v1/reservations" do
    it "should create check in with given params" do
      start_date, end_date = Time.zone.now, 1.days.from_now

      check_in_params = {check_in: {start_date: start_date,
                                    end_date: end_date,
                                    status: Constants::CHECK_IN_STATUSES[:reserved],
                                    billing_information_attributes: {billable_id: @guest.id,
                                                                     billable_type: "Guest",
                                                                     reference_guest_id: @guest.id},
                                    allocated_rooms_attributes: {"0" => {hotel_id: @hotel.id, room_type_id: @room.room_type_id, room_id: @room.id,
                                                                         allocated_room_guests_attributes: {"0" => {guest_id: @guest.id}},
                                                                         price: 600}},
                                    booked_by: "A.K.M. Ashrafuzzaman",
                                    booking_date: "2013-02-18 07:39 AM",
                                    note: ""},
                         whodoneit: @user.id}


      post "/api/v1/reservations.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", check_in_params
      check_in = CheckIn.last
      response.body.should == {:check_in_id => check_in.id, status: true}.to_json
      check_in.start_date.to_date.should == start_date.to_date
      check_in.end_date.to_date.should == end_date.to_date
      check_in.status.should == Constants::CHECK_IN_STATUSES[:reserved]

      check_in.billing_information.billable_id.should == @guest.id
      check_in.billing_information.billable_type.should == "Guest"
      check_in.billing_information.reference_guest_id.should == @guest.id


      check_in.allocated_rooms.count.should == 1
      ar = check_in.allocated_rooms.first
      ar.hotel_id.should == @hotel.id
      ar.hotel_id.should == @hotel.id
      ar.room_type_id.should == @room.room_type_id
      ar.room_id.should == @room.id
      ar.price.should == 600
      ar.allocated_room_guests.count.should == 1

      arg = ar.allocated_room_guests.first
      arg.guest_id.should == @guest.id
    end
  end


  describe "PUT /v1/reservations/:id" do
    it "should update check in with given params" do
      @checkin1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 5.days.from_now, number_of_rooms: 1)
      start_date, end_date = Time.zone.now, 2.days.from_now
      check_in_params = {check_in: {start_date: start_date,
                                    end_date: end_date,
                                    status: Constants::CHECK_IN_STATUSES[:reserved],
                                    billing_information_attributes: {billable_id: @guest.id,
                                                                     billable_type: "Guest",
                                                                     reference_guest_id: @guest.id},
                                    allocated_rooms_attributes: {"0" => {hotel_id: @hotel.id,
                                                                         id: @checkin1.allocated_rooms.first.id,
                                                                         room_type_id: @room.room_type_id,
                                                                         room_id: @room.id,
                                                                         allocated_room_guests_attributes: {"0" => {guest_id: @guest.id}},
                                                                         price: 700}},
                                    booked_by: "A.K.M. Ashrafuzzaman",
                                    booking_date: "2013-02-18 07:39 AM",
                                    note: ""},
                         whodoneit: @user.id}
      put "/api/v1/reservations/#{@checkin1.id}.json?&access_token=#{@token}", check_in_params
      response.body.should == {:check_in_id => @checkin1.id, status: true}.to_json
      check_in = CheckIn.find(@checkin1.id)
      check_in.start_date.to_date.should == start_date.to_date
      check_in.end_date.to_date.should == end_date.to_date
      check_in.allocated_rooms.count.should == 1
      ar = check_in.allocated_rooms.first
      ar.price.should == 700
    end
  end

  describe "GET /v1/reservations/search" do
    it "should search check in with given params" do
      c1 = FactoryGirl.create(:reserved_check_in, hotel: @hotel, start_date: Time.zone.now, end_date: 2.days.from_now, number_of_rooms: 1)
      c2 = FactoryGirl.create(:reserved_check_in, hotel: @hotel, start_date: 1.days.from_now, end_date: 2.days.from_now, number_of_rooms: 1)
      FactoryGirl.create(:reserved_check_in, start_date: 1.days.from_now, end_date: 2.days.from_now, number_of_rooms: 1)
      get "/api/v1/reservations/search.json?whodoneit=#{@user.id}&hotel_id=#{@hotel.id}&reservation_search['c_id']=&reservation_search['guest_name']&reservation_search['status']='RESERVED'&reservation_search['booking_source']=''&access_token=#{@token}&per_page=10"
      check_ins = CheckIn.where(hotel_id: @hotel.id)

      check_ins.count.should == 2
      check_ins.collect(&:id).should == [c1.id, c2.id]
      response.body.should == {:total_records => 2,
                               :per_page => 10,
                               :total_pages => 1,
                               :current_page => 1,
                               :data => check_ins.as_json}.to_json
    end
  end

  describe "PUT /v1/reservations/:id/cancel_checkin" do
    it "Cancel check_in or Reservation for all rooms." do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
      moc_params = {cancel_option: "all"}
      @check_in.cancel!
      put "/api/v1/reservations/#{@check_in.id}/cancel_checkin.json?access_token=#{@token}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

  describe "PUT /v1/reservations/:id/cancel_checkin" do
    it "Cancel check_ins or Reservation for a single room." do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 2)
      moc_params = {cancel_option: "single", cancel_room_id: @check_in.allocated_rooms.first.id}
      @check_in.cancel_room!(@check_in.allocated_rooms.first.id)
      put "/api/v1/reservations/#{@check_in.id}/cancel_checkin.json?access_token=#{@token}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

  describe "PUT /v1/reservations/:id/cancel_checkout" do
    it "Cancel check_out with the check_in id provided." do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
      @check_in.cancel_checkout!
      put "/api/v1/reservations/#{@check_in.id}/cancel_checkout.json?access_token=#{@token}"
      response.body.should == {:success => true}.to_json
    end
  end

  describe "PUT /v1/reservations/:id/check_out" do
    it "Checkout a given checkin with the provided check_in id." do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
      @check_in.check_out!
      put "/api/v1/reservations/#{@check_in.id}/check_out.json?access_token=#{@token}&whodoneit=#{@user.id}"
      response.body.should == {:success => true}.to_json
    end
  end

  describe "PUT /v1/reservations/add_services" do
    it "Add Service charges to the Allocated room for corresponding check_in." do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
      @service = FactoryGirl.create(:service, hotel: @hotel)

      moc_params = {allocated_room: {service_charges_attributes:
                                         {"0" => {service_id: @service.id,
                                                  created_date: start_date,
                                                  amount: 500.0, note: "test note",
                                                  hotel_id: @check_in.hotel.id,
                                                  room_type_id: @check_in.allocated_rooms.first.room_type_id
                                         }
                                         }}, id: @check_in.allocated_rooms.first.id}


      put "/api/v1/reservations/add_services.json?access_token=#{@token}", moc_params
      response.body.should == {:success => true}.to_json
    end
  end

  describe "GET /v1/reservations/:room_type_id/check_available" do
    it "Available room in a given room type id." do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @check_in = FactoryGirl.create(:check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
      rooms = Room.where(:is_active => true, :room_type_id => @room.room_type_id).select { |room| room.available?(start_date, end_date, @check_in.id) }

      #moc_params = {room_type_id: @room.room_type_id, start_date: start_date, end_date: end_date, check_in_id: @check_in.id}
      @test_response = {
          :rooms => rooms.map { |r| {:id => r.id,
                                     :title => r.title,
                                     :price => r.price_with_date_range(nil, start_date, end_date)}
          }
      }
      get "/api/v1/reservations/#{@room.room_type_id}/check_available.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&room_type_id=#{@room.room_type_id}&start_date='#{start_date.to_date}'&end_date='#{end_date.to_date}'&check_in_id=#{@check_in.id}"
      response.body.should == @test_response.to_json
    end
  end


  describe "GET /v1/reservations/:id" do
    it "should search check in with given CheckIn ID" do
      start_date, end_date = Time.zone.now, 2.days.from_now
      @checkin1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)
      get "/api/v1/reservations/#{@checkin1.id}.json?whodoneit=#{@user.id}&access_token=#{@token}"
      check_in = CheckIn.find(@checkin1.id)
      @test_response = {:reservation => check_in.all_in_json}
      response.body.should == @test_response.to_json
      check_in.start_date.to_date.should == start_date.to_date
      check_in.end_date.to_date.should == end_date.to_date
      check_in.status.should == Constants::CHECK_IN_STATUSES[:checked_in]
    end
  end

  describe "PUT /v1/reservations/:id/room_transfer" do
    it "A checkin room transfer to another room" do
      start_date, end_date = Time.zone.now, 4.days.from_now
      @checkin1 = FactoryGirl.create(:checked_in_check_in, hotel: @hotel, start_date: start_date, end_date: end_date, number_of_rooms: 1)


      check_in_params = {check_in: {allocated_rooms_attributes: {"0" => {
          room_type_id: @room.room_type_id,
          room_id: @room.id,
          price: @room.price
      }
      }
      },
                         old_room: {price: @checkin1.allocated_rooms.first.price},
                         new_room: {price: @room.price},
                         old_room_id: @checkin1.allocated_rooms.first.id,
                         room_transfer_date: 2.days.from_now,
                         whodoneit: @user.id
      }


      put "/api/v1/reservations/#{@checkin1.id}/room_transfer.json?&access_token=#{@token}", check_in_params
      check_in = CheckIn.find(@checkin1.id)
      @test_response = {:status => true,
                        :new_room_id => @room.id,
                        :old_room_id => @checkin1.allocated_rooms.first.id
      }
      response.body.should == @test_response.to_json
      check_in.start_date.to_date.should == start_date.to_date
      check_in.end_date.to_date.should == end_date.to_date
      check_in.status.should == Constants::CHECK_IN_STATUSES[:checked_in]
      check_in.allocated_rooms.first.room_id == @room.id
      check_in.allocated_rooms.first.room_type_id == @room.room_type_id
      check_in.allocated_rooms.first.price == @room.price
    end
  end
end
