require 'spec_helper'
describe Moteel::API do

  before(:each) do
    initialize_api_data #initializes @user, @token, @hotel
    @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    @room = FactoryGirl.create(:room, hotel: @hotel, room_type: @room_type, price: 334.0, house_keeping_status: Constants::HOUSEKEEPING_STATUS_CODES[:dirty])
    @marker = FactoryGirl.create(:marker, hotel: @hotel)
    @department = FactoryGirl.create(:department, branch: @hotel)
  end


  describe "GET /v1/house_keeping/rooms/dashboard" do
    it "Returns all Rooms details under the hotel id provided. It also take input room type id as filter" do
      rooms = Room.where(hotel_id: @hotel.id)
      status_data = ReportItemDailyActivity.where(date: Time.now.to_date, second_shift: true).in(item_id: rooms.pluck(:id)).only(["status", "item_id"])
      @test_response = {:hotel_id => @hotel.id,
                        :title => @hotel.available_locales_hash(:title),
                        :rooms => rooms.collect { |room| {id: room.id, title: room.title, room_type: room.room_type.title, house_keeping_status: room.house_keeping_status,
                                                          front_desk_status: status_data.where(item_id: room.id).last.try(:status), task: room.tasks.last.try(:title), assigned_employee: room.tasks.assigned.map { |t| t.try(:assigned_user).try(:full_name) }.uniq.join(' ')} }}
      get "/api/v1/house_keeping/rooms/dashboard.json?&access_token=#{@token}&hotel_id=#{@hotel.id}"
      response.body.should == @test_response.to_json
    end
  end

  describe "GET /v1/house_keeping/rooms" do
    it "Returns all rooms and rooms status under the hotel id provided." do
      rooms = Room.where(hotel_id: @hotel.id)
      @test_response = {:hotel_id => @hotel.id,
                        :title => @hotel.available_locales_hash(:title),
                        :rooms => rooms.collect { |c| {id: c.id, :name => c.available_locales_hash(:title), :status => c.house_keeping_status} }}
      get "/api/v1/house_keeping/rooms.json?&access_token=#{@token}&hotel_id=#{@hotel.id}"
      response.body.should == @test_response.to_json
    end
  end

  describe "PUT /v1/house_keeping/rooms/update_housekeeping_status" do
    it "Update Rooms house keeping status under the hotel id and room id provided." do
      rooms = Room.where(hotel_id: @hotel.id)
      status = Constants::HOUSEKEEPING_STATUS_CODES[:dirty]

      rooms.update_all(house_keeping_status: Constants::HOUSEKEEPING_STATUS_CODES[status.downcase.to_sym])
      ReportItem.in(reference_id: rooms.pluck(:id)).update_all(house_keeping_status: Constants::HOUSEKEEPING_STATUS_CODES[status.to_sym])
      put "/api/v1/house_keeping/rooms/update_housekeeping_status.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&housekeeping_status=#{status}&set_status_selected_ids=#{rooms.first.id}"
      response.body.should == {:success => true}.to_json
    end
  end


  describe "PUT /v1/house_keeping/rooms/assign_employee" do
    it "Assign Employee under the hotel id and room id provided." do
      rooms = Room.where(hotel_id: @hotel.id)
      @task = FactoryGirl.create(:task, hotel: @hotel, room: @room, department: @department, assigned_user: @user, creator: @user)
      @department = FactoryGirl.create(:department, branch: @hotel, name: Constants::HOUSEKEEPING_DEPARTMENT_NAME)
      @department_assignment = FactoryGirl.create(:department_assignment, department: @department, user: @user)


      assigned_employee, current_user = @user, @user
      housekeeping_dept = @hotel.house_keeping_department


      if assigned_employee.present? && assigned_employee.departments.include?(housekeeping_dept)
        rooms.each { |r| r.assign_housekeeping_employee(assigned_employee, current_user, @task.title) }
        @test_response = {:success => true}
      else
        @test_response = {:success => false}
      end

      moc_params = {assign_employee_selected_ids: rooms.first.id, assigned_employee_id: @user.id, task_title: @task.title}

      put "/api/v1/house_keeping/rooms/assign_employee.json?&access_token=#{@token}&hotel_id=#{@hotel.id}&user_id=#{@user.id}", moc_params
      response.body.should == @test_response.to_json
    end
  end

end