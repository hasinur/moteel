require 'spec_helper'

describe Moteel::API do

  before(:each) do
    initialize_api_data #initializes @user, @token, @hotel
    @hotel = FactoryGirl.create(:hotel)
    @room = FactoryGirl.create(:room, hotel: @hotel)
    @guest = FactoryGirl.create(:guest)

    @user.assigned_hotels << @hotel

  end

  describe "POST /v1/online_reservations" do
    it "should create online_reservation with given params" do
      start_date, end_date = Time.zone.now, 1.days.from_now

      params = {online_reservation: {
          first_name: "first name",
          last_name: "last name",
          address: "test address",
          email: "hello@example.com",
          phone: "2342342",
          terms_condition: "1",
          start_date: start_date,
          end_date: end_date,
          details_attributes: {"0" => {"room_type_id" => @room.room_type.id, "rooms_count" => "1", "price" => "20.0"}},
      }}
      post "/api/v1/online_reservations.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", params
      online_reservation = OnlineReservation.last
      response.body.should == {:online_reservation_id => online_reservation.id, status: true}.to_json
      online_reservation.start_date.to_date.should == start_date.to_date
      online_reservation.end_date.to_date.should == end_date.to_date
    end
    it "should create online_reservation with multiple room type" do
      start_date, end_date = Time.zone.now, 1.days.from_now
      @room2 = FactoryGirl.create(:room, hotel: @hotel)
      params = {online_reservation: {
          first_name: "first name",
          last_name: "last name",
          address: "test address",
          email: "hello@example.com",
          phone: "2342342",
          terms_condition: "1",
          start_date: start_date,
          end_date: end_date,
          details_attributes: {"0" => {"room_type_id" => @room.room_type.id, "rooms_count" => "1", "price" => "20.0"},
                               "1" => {"room_type_id" => @room2.room_type.id, "rooms_count" => "2", "price" => "50.0"}},
      }}
      post "/api/v1/online_reservations.json?&access_token=#{@token}&hotel_id=#{@hotel.id}", params
      online_reservation = OnlineReservation.last
      expect(online_reservation.details.count).to eq 2
      response.body.should == {:online_reservation_id => online_reservation.id, status: true}.to_json
      online_reservation.start_date.to_date.should == start_date.to_date
      online_reservation.end_date.to_date.should == end_date.to_date
    end
  end

  describe "GET /v1/online_reservations/:id" do
    it "should get online_reservations with given ID" do
      @online_reservation = FactoryGirl.create(:online_reservation, hotel: @hotel)
      get "/api/v1/online_reservations/#{@online_reservation.id}.json?access_token=#{@token}"
      online_reservation = OnlineReservation.find(@online_reservation.id)
      test_response = {:online_reservation => online_reservation.as_json}
      response.body.should == test_response.to_json
    end
  end

end
