require 'spec_helper'

def login_with_hotel_owner
  login_with(create_user(:hotel_owner))
end

def login_with(user)
  @controller.stub!(:current_user).and_return(user)
  @controller.class.skip_before_filter :set_current_branch_and_time_zone
  @controller.class.skip_before_filter :switch_to_hotel_database_with_fallback
  @controller.class.skip_before_filter :filter_access_to #skipping the permission
  @controller.stub!(:current_reporting_branch_id).and_return(@hotel.id)
  @controller.stub!(:current_branch).and_return(@hotel)
  Time.zone = @hotel.time_zone
end

def create_user(role = :hotel_owner)
  @user = FactoryGirl.create(role, {password: '123456', password_confirmation: '123456'})

  @hotel = FactoryGirl.create(:hotel, user: @user)
  Hotel.current = @hotel
  @user.assigned_hotels << @hotel
  @user.hotels << @hotel
  @user.stub!(:hotels_count).and_return(1)
  @user
end