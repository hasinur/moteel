def initialize_api_data  #initializes @user, @token, @hotel
  @user = FactoryGirl.create(:user)
  @token = @user.access_grants.first.access_token
  @hotel = FactoryGirl.create(:hotel, time_zone: Time.zone.name)
  @user.assigned_hotels << @hotel
end