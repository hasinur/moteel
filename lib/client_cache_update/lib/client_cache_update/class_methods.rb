module ClientCacheUpdate
  module ClassMethods

    def enable_client_update(options = {})
      include ClientCacheUpdate::InstanceMethods
      class_attribute :client_cache_config

      after_save :send_update_cache_notification
      after_destroy :send_delete_cache_notification

      self.client_cache_config = ClientCacheUpdate::Configuration.new(self, options)
      handle_asynchronously :send_update_cache_notification
      handle_asynchronously :send_delete_cache_notification
    end

  end
end