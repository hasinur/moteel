require "net/http"
require "client_cache_update/class_methods"
require "client_cache_update/configuration"
require "client_cache_update/instance_methods"
require "locale_attribute_value_util"

module ClientCacheUpdate
  class Engine < ::Rails::Engine
    config.before_initialize do
      ActiveSupport.on_load :active_record do
        ActiveRecord::Base.send :extend, ClientCacheUpdate::ClassMethods
        ActiveRecord::Base.send :include, LocaleAttributeValueUtil
      end
    end
    initializer "client_cache_update" do

    end
  end
end
