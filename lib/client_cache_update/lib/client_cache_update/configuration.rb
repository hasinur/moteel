module ClientCacheUpdate
  class Configuration
    DEFAULTS = {
        :client_obj => 'hotel'
    }

    # The class that's using the configuration.
    attr_reader :configured_class
    attr_reader :client_object
    attr_reader :parent_references
    attr_reader :after_object

    def initialize(configured_class, options = nil)
      @configured_class = configured_class
      @client_object = DEFAULTS.merge(options || {})[:client_obj]
      @after_object = DEFAULTS.merge(options || {})[:after] #fired after this model being saved/updated/destroyed
      @parent_references = DEFAULTS.merge(options || {})[:parent_references]
    end
  end
end