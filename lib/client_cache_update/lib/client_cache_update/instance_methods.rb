require 'client_cache_update_job'

module ClientCacheUpdate
  module InstanceMethods
    #include HTTParty

    def send_insert_cache_notification
      logger.info "sending insert request"
      response_json = prepare_and_send_request("Insert")
      if response_json.present?
        logger.info response_json.inspect
      else
        logger.error "client data insert request failed for " + self.inspect
        #Need to update this code to handle any logging support for client data insert request failure
      end
    end

    def send_update_cache_notification
      return if Rails.env.test?
      if self.id_changed? #insert
        send_insert_cache_notification
      else #update
        logger.info "sending update request"
        response_json = prepare_and_send_request("Update")

        if response_json.present?
          logger.info response_json.inspect
        else
          logger.error "client data update request failed for " + self.inspect
          #Need to update this code to handle any logging support for client data update request failure
        end
      end
    end

    def send_delete_cache_notification
      return if Rails.env.test?
      logger.info "sending delete request"
      response_json = prepare_and_send_request("Remove")

      if response_json.present?
        logger.info response_json.inspect
      else
        logger.error "client data delete request failed for " + self.inspect
        #Need to update this code to handle any logging support for client data delete request failure
      end
    end

    def client_cache_generate_json(action_type)
      generated_json_msg = {}

      attributes_hash = client_cache_model_attributes
      object_id = attributes_hash['id'] if attributes_hash.has_key?('id')
      attributes_hash.delete_if { |key, val| key == 'id' }

      if !attributes_hash.empty?
        case action_type
          when "Insert"
            attributes_hash.merge!("reference_id" => object_id.to_s, "tag_ids" => self.respond_to?(:tag_ids) ? self.send(:tag_ids).collect { |tag_id| tag_id.to_s } : [])
            generated_json_msg = {"parent_references" => prepare_parent_references, 'Data' => self.respond_to?(:client_cache_update_data) ? attributes_hash.merge!(client_cache_update_data) : attributes_hash}

          when "Update"
            if object_id.present?
              attributes_hash.merge!("tag_ids" => self.respond_to?(:tag_ids) ? self.send(:tag_ids).collect { |tag_id| tag_id.to_s } : [])
              generated_json_msg = {'ID' => object_id, "parent_references" => prepare_parent_references, 'Data' => self.respond_to?(:client_cache_update_data) ? attributes_hash.merge!(client_cache_update_data) : attributes_hash}
            end

          when "Remove"
            generated_json_msg = {'ID' => object_id}
        end
      end
      generated_json_msg
    end


    def send_delayed_http_request(uri, json_body)
      #begin
        http = Net::HTTP.new(uri.host, uri.port)
        http.open_timeout = 10
        http.read_timeout = 10
        req = Net::HTTP::Post.new(uri.path)
        req.body = JSON.generate(json_body)
        req["Content-Type"] = "application/json"

        http.start { |htt| htt.request(req) }
      #rescue Timeout::Error
      #  logger.info "#{action_type} request failed for #{self.inspect}"
      #  logger.info "ERROR:: Timeout::Error occurred. Please check whether the domain is correct or not"
      #  nil
      #rescue => err
      #  logger.info "ERROR:: #{err.message}"
      #end
    end


    private

    def prepare_and_send_request(action_type)
      owner = find_owner_objects

      logger.info "Owner object :: " + owner.inspect
      logger.info "owner.domains.present? :: #{owner.domains.present?}"

      if owner.present? && !owner.is_a?(Array) && owner.domains.present?
        prepare_and_send_http_request(owner, action_type)
      elsif owner.present? && owner.is_a?(Array)
        owner.each do |owner_object|
          prepare_and_send_http_request(owner_object, action_type) if owner_object.domains.present?
        end
      else
        nil
      end
    end

    def find_owner_objects
      if self.class.name.downcase == 'hotel'
        self
      else
        self.respond_to?(:owner_objects) ? owner_objects : (eval "#{client_cache_config.client_object}" rescue nil)
      end
    end


    def prepare_and_send_http_request(owner, action_type)
      json_message_body = client_cache_prepare_json_message(action_type)

      logger.info "json message :: " + json_message_body.inspect
      logger.info "calling prepare_and_send_http_request for owner :: #{owner.inspect}"

      if json_message_body.present? && !json_message_body.empty?
        host_name = owner.domains.sub(/^http:\/\//, "")
        uri = URI.parse("http://#{host_name}/services/workspace/")

        begin
          Delayed::Job.enqueue ClientCacheUpdateJob.new(uri, json_message_body, self)
        rescue Timeout::Error
          logger.info "#{action_type} request failed for #{self.inspect}"
          logger.info "ERROR:: Timeout::Error occurred. Please check whether the domain is correct or not"
          nil
        rescue => err
          logger.info "#{action_type} request failed for #{self.inspect}"
          logger.info "ERROR:: #{err.message}"
          nil
        end
      end
    end

    def client_cache_prepare_json_message(action_type)
      message_body = {}
      document_names = {
          'hotels' => 'hotels',
          'rooms' => 'rooms',
          'room_types' => 'room_types'
      }

      document_to_update = document_names.has_key?(self.class.table_name) ? document_names[self.class.table_name] : self.class.table_name
      generated_json = client_cache_generate_json(action_type)

      if action_type.present? && !generated_json.empty?
        message_body[action_type] = generated_json

        if !message_body.empty?
          {
              "Message" => {
                  "Type" => "core2client",
                  "Command" => "data_update_notification",
                  "Document" => "#{document_to_update}",
                  "Body" => {"attributes" => message_body}
              }
          }
        else
          nil
        end
      else
        nil
      end
    end

    def prepare_parent_references
      parents_info = []

      client_cache_config.parent_references.each do |parent_info|
        belongs_to_hash = Hash.new

        if parent_info.present?
          if parent_info.is_a?(Hash)
            belongs_to_hash['document_name'] = parent_info.keys.first.to_s.pluralize if !parent_info.empty?
            belongs_to_hash['parent_reference_id'] = eval("#{parent_info[parent_info.keys.first]}").to_s
          else
            belongs_to_hash['document_name'] = "#{parent_info}".pluralize
            belongs_to_hash['parent_reference_id'] = eval("#{parent_info}_id").to_s
          end
        end
        parents_info << belongs_to_hash if !belongs_to_hash.empty?
      end if self.respond_to?(:client_cache_config) && client_cache_config.respond_to?(:parent_references) && client_cache_config.parent_references.present?
      parents_info
    end

    def client_cache_model_attributes
      attributes_hash = {}
      self.class.column_names.each do |column|
        attributes_hash[column] = self.send(column).to_s
      end

      attributes_hash.merge!(self.send(:locale_field_values)) if self.respond_to?(:locale_field_values)
      attributes_hash.merge!("custom_attributes" => custom_attributes_values)
    end

    def custom_attributes_values
      return_hash = {}
      if self.respond_to?(:dynamic_localized_attribute_values)
        dynamic_localized_attribute_values.each do |dla|
          tmp_hash = {}
          dla.translations.each do |gt|
            tmp_hash[gt.locale.to_s] = gt.value
          end
          return_hash[dla.dynamic_localized_attribute.name] = tmp_hash
        end
      end
      logger.info "custom_attributes_values :: #{return_hash.inspect}"
      return_hash
    end

  end
end