module LocaleAttributeValueUtil
  module InstanceMethods
    def locale_field_values
      if self.class.respond_to?(:translated_attribute_names)
        locale_fields_hash = Hash[*self.class.translated_attribute_names.collect { |v| [v.to_s, nil] }.flatten]

        self.translations.each do |translation|
          locale_fields_hash.each do |k, v|
            locale_fields_hash[k] ||= {}
            locale_fields_hash[k].merge!({"#{translation.locale}" => (translation.send(k.to_sym) rescue "")})
          end
        end

        locale_fields_hash
      else
        {}
      end
    end
  end
end