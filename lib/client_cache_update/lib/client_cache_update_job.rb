class ClientCacheUpdateJob
  attr_accessor :uri
  attr_accessor :json_data
  attr_accessor :caller_object

  def initialize(uri, json_data, caller_object)
    self.uri = uri
    self.json_data = json_data
    self.caller_object = caller_object
  end

  def perform
    @caller_object.send_delayed_http_request(@uri, @json_data)
  end
end