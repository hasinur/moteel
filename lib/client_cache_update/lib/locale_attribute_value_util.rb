require "locale_attribute_value_util/instance_methods"
module LocaleAttributeValueUtil
  def self.included(base)
    base.send :include, LocaleAttributeValueUtil::InstanceMethods
  end
end