module MoteelAuthorization

  protected
  #required for forum
  def admin?
    return false unless current_user
    current_user.admin?
  end

  # Checks if current user is hotel owner
  def hotel_agent?
    return false unless current_user
    current_user.hotel_owner?
  end

  # Checks if current user is agent user
  # Who is responsible for assigned hotels by hotel owner
  def agent_user?
    return false unless current_user
    current_user.hotel_owner? || admin?
  end

  def agent_user_id
    return nil unless current_user
    hotel_agent? || travel_agent? ? current_user.id : current_user.try(:hotel_agent).try(:id)
  end

  def travel_agent?
    return false unless current_user
    current_user.travel_agent?
  end

  def logged_in?
    user_signed_in?
  end

  def moderator_of?(forum)
    #TODO
  end

  #def current_user=(new_user)
  #  current_user.id = (new_user.nil? || new_user.is_a?(Symbol)) ? nil : new_user.id
  #  @current_user ||= new_user
  #end
  #
  #def current_user
  #  @current_user ||= login_from_session unless @current_user == false
  #end

  def last_active
    session[:last_active] ||= Time.now.utc
  end

  #end forum

  def check_admin_section
    if current_user.present?
      unless admin?
        flash[:error] = t('notifications.admin_section_access_error')
        redirect_to dashboard_home_path
        return
      end
    else
      redirect_to :new_user_session
      return
    end
  end

  def check_hotel_agent
    unless admin? || hotel_agent?
      flash[:error] = t('notifications.hotel_section_access')
      redirect_to :new_user_session
      return
    end
  end

  def check_travel_agent
    unless admin? || hotel_agent? || travel_agent? || agent_user?
      flash[:error] = t('notifications.hotel_section_access')
      redirect_to :new_user_session
      return
    end
  end

  def check_agent_user
    unless admin? || hotel_agent? || agent_user?
      flash[:error] = t('notifications.hotel_section_access')
      redirect_to :new_user_session
      return
    end
  end

  def login_required
    logger.info "in login required"
    #logger.info current_user.id
    unless logged_in?
      flash[:error] = 'login_required'
      redirect_to :new_user_session
      return
    end
  end

  def reset_wizard_sessions
    session[:dashboard_no_of_new_items] = nil
    session[:dashboard_new_item_ids] = nil
    session[:dashboard_tmp_new_room_ids] = nil
    session[:dashboard_current_item_id] = nil
    session[:dashboard_selected_branch_id] = nil
    session[:dashboard_selected_item_type_id] = nil
    session[:dashboard_entity_next_step] = nil
    session[:dashboard_entity_current_partial] = nil
    session[:dashboard_current_hotel_id] = nil
    session[:dashboard_from_wizard] = nil
    session[:dashboard_employee_next_step] = nil
    session[:dashboard_employee_id] = nil
  end

end