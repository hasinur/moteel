module DateFormatter
  def fdt(datetime, format = :default)
    I18n.l datetime, :format => format rescue nil
  end

  def parse_fdt(str)
    Time.zone.parse str rescue nil
  end
end

ActiveRecord::Base.send :extend, DateFormatter
