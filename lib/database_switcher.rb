module Moteel::DatabaseSwitcher
  def switch_to_hotel_database_with_fallback
    unless Rails.env == 'test'
      if !LOCAL_SERVER and current_branch and current_branch.has_local_database?
        begin
          if current_user.present?
            switch_to_hotel_database
          else
            switch_to_default_database
          end
        rescue
          switch_to_default_database
        end
      else
        switch_to_default_database
      end
    end
    true
  end

  def switch_to_hotel_database(h = current_branch)
    if h and h.has_local_database?
      begin
        sync_logger.debug "================================================================================================"
        sync_logger.debug "Switching database"
        config = Rails.configuration.database_configuration[Rails.env]
        config['username'] = h.database_username
        config['password'] = h.database_password
        config['database'] = h.database_name
        ActiveRecord::Base.establish_connection(config)
        sync_logger.debug "Current database :: #{ActiveRecord::Base.connection.instance_eval { @config[:database] }}"
        Mongoid.override_database(h.database_name)
        sync_logger.debug "================================================================================================"
      rescue
        sync_logger.error "Error connecting to #{h.database_name} :: #{$!}"
        sync_logger.debug "================================================================================================"
        raise $!
      end
    end
  end

  def switch_to_default_database
    sync_logger.info "Switching to default database *"
    config = Rails.configuration.database_configuration[Rails.env]
    ActiveRecord::Base.establish_connection(config)
    mongo_config = YAML.load_file(Rails.root.join('config', 'mongoid.yml'))[Rails.env]['sessions']['default']
    Mongoid.override_database(mongo_config['database'])
    sync_logger.info "* Switched to default database"
  end

  private
  def sync_logger
    @@sync_logger ||= Logger.new("#{Rails.root}/log/sync.log")
  end
end