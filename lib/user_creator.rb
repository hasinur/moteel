module Moteel::UserCreator
  def create_user(user_attributes)
    if LOCAL_SERVER
      url = URI.parse("#{APPLICATION_API_URL}/api/v1/users.json?api_key=#{APPLICATION_API_KEY}")
      response = Net::HTTP.post_form(url, user_attributes)
      user_attrs = JSON.parse(response.body)
      if user_attrs['alert']
        flash[:error] = user_attrs['alert']
        logger.debug "flash[:error] :: #{flash[:error]}"
        nil
      else
        user_attrs
      end
    else
      switch_to_default_database
      user = User.create user_attributes
      switch_to_hotel_database_with_fallback
      if user.persisted?
        flash[:error] = Common.validation_errors(user.errors)
        logger.debug "flash[:error] :: #{flash[:error]}"
        user.attributes
      else
        nil
      end
    end
  end
end