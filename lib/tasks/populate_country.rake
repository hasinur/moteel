desc "Populate Country"
task(:populate_country => :environment) do
  sql = ActiveRecord::Base.connection()
  Country.all.each do |country|
    puts "Inserting #{country.inspect}"
    sql.execute("insert into country_translations (locale, country_id, title) values('#{country.language}','#{country.group_id}','#{country.title.gsub(/\'/,'\'\'')}')")
  end
end

	