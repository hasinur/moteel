namespace :clear_data do
  desc "Clear all checkin related date"
  task(:all => :environment) do
    tables = [ "agent_discounts", "allocated_room_guests", "allocated_room_rates", "allocated_rooms",
               "billing_informations", "cash_closeouts",
              "cash_counter_access_logs",  "cash_counter_transactions",
              "check_in_add_ons", "check_in_credit_cards", "check_ins",
               "customer_rates",
              "damaged_room_notes", "damaged_rooms",
              "delayed_jobs",
              "folios", "forums", "guest_discounts",
              "hotel_service_payment_details", "hotel_service_payments",
               "invoices",
               "locked_room_notes", "locked_rooms", "messages",
              "messages_recipients","night_audits", "queued_tasks",
              "reservation_line_items",
              "room_assignments",
              "service_charge_details", "service_charges",
               "sync_entity_reports", "task_histories",
              "tasks", "transaction_details", "transactions", "wallet_transactions"]
    tables.each do |table|
      ActiveRecord::Base.connection.execute("TRUNCATE #{table}")
    end
    Room.all.each {|room| room.update_attribute(:guests_in_room, false)}
  end
end