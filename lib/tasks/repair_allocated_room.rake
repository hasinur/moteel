namespace :repair_resource do
  desc "Repair Allocated Rooms For Data Inconsistency"
  task(:checkin => :environment) do
    CheckIn.with_status(Constants::CHECK_IN_STATUSES[:checked_in]).each do |check_in|
      begin
        if check_in.billing_information.nil?
        check_in.destroy
        elsif check_in.billing_information && check_in.billing_information.billable.nil?
        puts "check_ins##{check_in.id} deleted"
        check_in.destroy
        end
      rescue => error
        puts error.message
      end
    end


    ReservationLineItem.where(serviceable_type: "HotelService").collect do |item|
      begin
        item.update_column(:serviceable_type, "Service")
      rescue => error
        puts error.message
      end
    end

    begin
      Folio.where(:department_id => nil).each(&:destroy)
    rescue => error
      puts error.message
    end

    items = ReservationLineItem.where(:folio_id => nil, :serviceable_type => "Service")
    count = 0
    items.each do |rsv|
      begin
        next if rsv.folio.present?
        unless rsv.serviceable.present?
          rsv.destroy
          next
        end
        unless rsv.check_in.billing_information.present?
          rsv.destroy
          next
        end
        check_in_id = rsv.check_in.id
        department = rsv.serviceable.department
        billable_id = rsv.check_in.billing_information.id rescue rsv.allocated_room.guests.first.id
        folio = Folio.create(department: department, check_in_id: check_in_id, billable_id: billable_id)
        rsv.folio = folio
        rsv.save!
        puts rsv.folio_id
      rescue => error
        puts error.message
      end
    end
    puts "#{count}"
  end


end