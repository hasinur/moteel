namespace :generate_json do
  #desc "Dump hotels table data into a rooms.json file"
  #task(:hotels => :environment) do
  #  msg = User.find(65).hotels.collect { |hotel| hotel.generate_json("Insert")['Data'] }
  #  File.open("/home/ashif/Desktop/DataExport/hotels.json", "w") { |f| f.write(msg.collect { |m| m.to_json }.join("\n")) }
  #end
  #
  #desc "Dump room_types table data into a room_types.json file"
  #task(:room_types => :environment) do
  #  msg = User.find(65).hotels.collect{|h| h.room_types}.flatten.collect{ |room| room.generate_json("Insert")['Data'] }
  #  File.open("/home/ashif/Desktop/DataExport/room_types.json", "w") { |f| f.write(msg.collect { |m| m.to_json }.join("\n")) }
  #end
  #
  #desc "Dump rooms table data into a cars.json file"
  #task(:rooms => :environment) do
  #  msg = User.find(65).rooms.collect { |room| room.generate_json("Insert")['Data'] }
  #  File.open("/home/ashif/Desktop/DataExport/rooms.json", "w") { |f| f.write(msg.collect { |m| m.to_json }.join("\n")) }
  #end
  #
  #desc "Dump tags table data into a cities.json file"
  #task(:cities => :environment) do
  #  msg = Country.find(96).cities.collect{|city| city.generate_json("Insert")['Data']} #Before running this make sure to add enable_client_cache_update call on City model
  #  File.open("/home/ashif/Desktop/DataExport/cities.json", "w") { |f| f.write(msg.collect { |m| m.to_json }.join("\n")) }
  #end

  desc "Dump tags table data into a cities.json file"
  task(:countries => :environment) do
    msg = Country.all.collect{|country| country.generate_json("Insert")['Data']} #Before running this make sure to add enable_client_cache_update call on County model
    File.open("/home/ashif/Desktop/DataExport/countries.json", "w") { |f| f.write(msg.collect { |m| m.to_json }.join("\n")) }
  end
end

