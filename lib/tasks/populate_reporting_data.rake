namespace :populate_reporting_data do
  desc "populate ReportItem document with all the data available on server"
  task(:all => :environment) do
    #Time.zone = "Dhaka"
    ReportItem.all.each {|item| item.destroy}
    ReportItemDailyActivity.all.each {|item| item.destroy}
    ReportEntityDailyActivity.all.each {|item| item.destroy}
    ReportTask.all.each {|item| item.destroy}

    Room.find_each(:batch_size => 10) {|item| item.save rescue ''}
    AllocatedRoom.find_each(:batch_size => 10) do |alloc_room|
      unless alloc_room.check_in
        alloc_room.destroy  rescue ""
        next
      end
      alloc_room.save rescue ''
      puts alloc_room.id
    end
    DamagedRoom.find_each(:batch_size => 10) {|dam_room| dam_room.save rescue ''}
    LockedRoom.find_each(:batch_size => 10) {|locked_room| locked_room.save rescue ''}
    Task.find_each(:batch_size => 10) {|task| task.save rescue ''}
  end

  #task(:generate_stay_over => :environment) do
  #  AllocatedRoom.find_each(:batch_size => 1000, :conditions => ["checked_out = ?", false]) { |alloc_room| alloc_room.save_stay_over_data}
  #end
end

