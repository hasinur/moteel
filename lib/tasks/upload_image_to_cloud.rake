namespace :upload do
  desc "Uploads all the images to could host"
  task 'image_to_cloud' => :environment do
    image_models = [
        {model: Group, property_name: :image},
        {model: Guest, property_name: :image},
        {model: GuestIdentityImage, property_name: :image},
        {model: Hotel, property_name: :logo},
        {model: HotelBanner, property_name: :image},
        {model: Profile, property_name: :avatar},
        {model: RoomImage, property_name: :image},
        {model: RoomTypeImage, property_name: :image}
    ]

    image_models.each do |image_klass_info|
      puts image_klass_info[:model]
      puts "=========================================="
      image_klass_info[:model].all.each do |image_model|
        file_path = image_model.send(image_klass_info[:property_name]).path

        if file_path.present?
          orig_file_path = File.join(Rails.root, 'public/system', file_path)
          #p "Checking for #{orig_file_path}"
          if File.exist?(orig_file_path)
            puts "Uploading for #{image_klass_info[:model]} :: #{image_model.id}"
            File.open(orig_file_path) { |photo_file| image_model.send("#{image_klass_info[:property_name].to_s}=", photo_file) }
            image_model.save!
            puts "Uploaded for #{image_klass_info[:model]} :: #{image_model.id}"
          else
            puts "Missing for #{image_klass_info[:model]} :: #{image_model.id}"
          end
        end
      end
    end
  end
end