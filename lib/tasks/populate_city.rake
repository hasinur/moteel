desc "Populate city"
task(:populate_city => :environment) do
  sql = ActiveRecord::Base.connection()
  City.all.each do |city|
    puts "Inserting #{city.id}"
    sql.execute("insert into city_translations (locale, city_id, title) values('#{city.language}','#{city.tg_id}','#{city.title.gsub(/\'/, '\'\'')}')")
  end
end

desc "Update city coordinates"
task(:update_city_coordinates => :environment) do
  @city_coordinates_logger = Logger.new(Rails.root.join('log', "city_coordinates_logger.log"), 10, 30*1024*1024)

  #City.find_each(:batch_size => 50) do |city|
  #  begin
  #    address = "#{city.read_attribute(:title, :locale => :en)},#{city.country.read_attribute(:title, :locale => :en)} "
  #    s= Geocoder.search(address)
  #    city.update_attributes(latitude: s[0].latitude, longitude: s[0].longitude)
  #    p "done for #{city.inspect}"
  #  rescue => e
  #    @city_coordinates_logger.info "#{e} :: failed for #{city.inspect}"
  #  end
  #end

  require 'csv'

  csv_text = File.read('/Users/muntasinahmed/Downloads/GeoLiteCity_20130702/GeoLiteCity-Location.csv').force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
  csv = CSV.parse(csv_text, :headers => false)
  csv.each do |row|
      city = City.find_by_title row[3]
    if city
      city.update_attributes(latitude: row[5], longitude: row[6])
      p city.latitude
    else
      p "failed for #{row[3]}"
    end
  end
end



