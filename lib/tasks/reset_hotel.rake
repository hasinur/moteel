namespace :reset_hotel do
	#desc "Update Exchange rates"
	#task(:reset_plan => :environment){ p Hotel.reset_plan  }

  desc "Reset roomtype price unit"
	task(:room_type_price_unit => :environment) do
    Hotel.all.each do |hotel|
      p Hotel.room_type_price_unit(hotel)
    end
  end
end

