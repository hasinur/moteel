namespace :default_data do
  #desc "Populate roles data"
  #task(:roles => :environment) do
  #  ActiveRecord::Base.connection.execute 'TRUNCATE TABLE roles'
  #  ActiveRecord::Base.connection.execute 'TRUNCATE TABLE role_translations'
  #  ActiveRecord::Base.connection.execute 'TRUNCATE TABLE assignments'
  #  p "Updating roles"
  #  Constants::GENERAL_ROLES.each do |role_name|
  #    Role.create(:name => role_name, :title => role_name.humanize, :role_type_id => Constants::GENERAL_ROLE_ID)
  #  end
  #
  #  Constants::AGENT_ROLES.each do |role_name|
  #    Role.create(:name => role_name, :title => role_name.humanize, :role_type_id => Constants::AGENT_PANEL_ROLE_ID)
  #  end
  #
  #end

  desc "Reset roomtype price unit"
  task(:elements => :environment) do
    p "removing old elements"
    Element.all.collect(&:destroy)
    p "creating elements"
    [["item_per_page_min", "10"], ["item_per_page_max", "500"], ["click_fee_rate_gold", "0.0"], ["click_fee_rate_platinum", "0.0"], ["monthly_fee_rate_gold", "0.0"], ["monthly_fee_rate_platinum", "0.0"], ["featured_item_price", "0.0"], ["featured_item_duration", "30"], ["max_featured_item", "100"], ["platinum_image_count", "15"], ["gold_image_count", "10"], ["hotel_list_page_video", "4xEAueTD7iA"], ["featured_deal_price", "0.0"], ["max_featured_deal", "100"], ["featured_deal_duration", "30"], ["click_fee_rate_platinum_deal", "0.0"], ["click_fee_rate_gold_deal", "0.0"], ["monthly_fee_rate_gold_deal", "0.0"], ["monthly_fee_rate_platinum_deal", "0.0"], ["image_count_for_deal", "4"], ["allowed_hotel_platinum", "50"], ["allowed_hotel_gold", "5"], ["allowed_hotel_silver", "3"], ["allowed_hotel_basic", "1"], ["allowed_hotel_free", "1"], ["hotel_rooms_platinum", "500"], ["hotel_rooms_gold", "75"], ["hotel_rooms_silver", "50"], ["hotel_rooms_basic", "10"], ["hotel_rooms_free", "1"], ["hotel_users_platinum", "50"], ["hotel_users_gold", "-1"], ["hotel_users_silver", "5"], ["hotel_users_basic", "3"], ["hotel_users_free", "1"], ["allowed_deals_platinum", "15"], ["allowed_deals_gold", "10"], ["allowed_deals_silver", "5"], ["allowed_deals_basic", "3"], ["allowed_deals_free", "1"], ["silver_image_count", "5"], ["basic_image_count", "3"], ["free_image_count", "1"], ["gold_plan_price", "1375"], ["silver_plan_price", "1050"], ["basic_plan_price", "450"], ["beginner_plan_price", "150"]].each do |title, value|
      Element.create(:title => title, :value => value)
    end
  end

  desc "Populate new roles data"
  task(:update_roles => :environment) do
    p "Syncing roles"
    ActiveRecord::Base.transaction do
      GENERAL_ROLES = (Constants::DEFAULTS_GROUPED_ROLES.values << Constants::GROUP_ROLES).flatten.sort
      p "Creating new roles roles"
      GENERAL_ROLES.each do |role_name|
        Role.create(:name => role_name.to_s, :title => role_name.to_s.humanize, :role_type_id => Constants::AGENT_PANEL_ROLE_ID)
      end

      #Here are all the roles that comes from user type i.e. hotel_owner
      [Constants::HOTEL_OWNER_ROLE_NAME].each do |role_name|
        Role.create(:name => role_name.to_s, :title => role_name.to_s.humanize, :role_type_id => Constants::GENERAL_ROLE_ID)
      end

      p "Reloading the role assignments for each group"
      Constants::DEFAULT_GROUP_PERMISSIONS_WITH_ROLES.each_pair do |group_name, permissions|
        permission_group = PermissionGroup.find_by_name(group_name) || PermissionGroup.create(:name => group_name)
        ActiveRecord::Base.connection.execute "delete from permission_groups_roles where permission_group_id = #{permission_group.id}"
        permission_group.roles << Role.where(name: permissions)
      end
    end
  end

  task :all => [:new_roles, :elements] do
    puts "All default data repopulated."
  end


end

