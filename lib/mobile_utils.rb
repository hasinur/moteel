module MobileUtils
  def mobile_device?
    return false
    # return true #only for hotelmobile
    mobile_user_agent = 'palm|palmos|palmsource|iphone|blackberry|nokia|phone|midp|mobi|pda|' +
      'wap|java|nokia|hand|symbian|chtml|wml|ericsson|lg|audiovox|motorola|' +
      'samsung|sanyo|sharp|telit|tsm|mobile|mini|windows ce|smartphone|' +
      '240x320|320x320|mobileexplorer|j2me|sgh|portable|sprint|vodafone|' +
      'docomo|kddi|softbank|pdxgw|j-phone|astel|minimo|plucker|netfront|' +
      'xiino|mot-v|mot-e|portalmmm|sagem|sie-s|sie-m|android|ipod'

    request.user_agent.to_s.downcase =~ Regexp.new(mobile_user_agent)
  end

  def prepare_for_mobile
    #session[:mobile_param] = params[:mobile] if params[:mobile]
    request.format = :mobile if mobile_device?
  end
end