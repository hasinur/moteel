module MongoIdExtension
  module ReportingDataUpdaterUtil

    def self.included(klass)
      klass.extend ClassMethods
    end

    module ClassMethods
      def insert(p_data)

        if self.respond_to?(:create_from_data_hash)
          if !p_data.empty? && p_data["Data"].first["reference_id"].present?
            self.create_from_data_hash(p_data["Data"])
          end
        else
          if !p_data.empty? && p_data["Data"].first["reference_id"].present?
            logger.info("p_data.size=#{p_data["Data"].size}")
            p_data["Data"].each do |single_doc|
              begin
                logger.info("****************creating #{single_doc}")
                logger.info("before create.....")
                logger.info(self.where(reference_id: single_doc["reference_id"].to_i, ar_model_name: single_doc["ar_model_name"]).count)

                self.create(single_doc)

                logger.info("after create .....")
                logger.info(self.where(reference_id: single_doc["reference_id"].to_i, ar_model_name: single_doc["ar_model_name"]).count)

              rescue
                logger.info("failed creating reporting data")
              end
            end

          end
        end
      end

      def update(p_data)
        logger.info("****************updating #{p_data}")
        self.update_from_hash(p_data) if self.respond_to?(:update_from_hash)
      end

      def remove(p_data)
        self.destroy_all(reference_id: p_data["reference_id"].to_i, ar_model_name: p_data["ar_model_name"])
      end
    end
  end
end
