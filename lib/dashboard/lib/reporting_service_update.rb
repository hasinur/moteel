require "reporting_service_update/class_methods"
require "reporting_service_update/configuration"
require "reporting_service_update/instance_methods"
module ReportingServiceUpdate
  def self.included(base)
    base.extend ReportingServiceUpdate::ClassMethods
  end
end