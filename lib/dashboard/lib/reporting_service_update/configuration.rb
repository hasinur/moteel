module ReportingServiceUpdate
  class Configuration
    attr_reader :document_to_update
    attr_reader :duplicable

    DEFAULTS = {
        :document_to_update => 'ReportItem',
        :duplicable => false
    }

    def initialize(configured_class, options = nil)
      @configured_class = configured_class
      @document_to_update = DEFAULTS.merge(options || {})[:document_to_update]
      @duplicable = DEFAULTS.merge(options || {})[:duplicable]
    end
  end
end