module ReportingServiceUpdate
  module InstanceMethods
    #include HTTParty

    def save_reporting_data
      if self.respond_to?(:parent_save_hook)
        self.parent_save_hook
      else
        self.sync_reporting_data
      end

    end

    def sync_reporting_data
      response_json = prepare_and_save_data("Update")
    end

    def destroy_reporting_data
      #logger.debug "sending delete request"
      response_json = prepare_and_save_data("Remove")

      #if response_json.present?
      #logger.debug response_json.inspect
      #else
      #logger.error "report data delete request failed for " + self.inspect
      #Need to update this code to handle any logging support for client data delete request failure
      #end
    end

    def reporting_service_generate_json(action_type)
      generated_json_msg = {}
      if action_type.to_s == "Remove"
        generated_json_msg = {'reference_id' => self.id.to_s, 'ar_model_name' => self.class.name}
      else
        generated_json_msg = {'Data' => self.respond_to?(:reporting_data_hash) ? self.reporting_data_hash : {}}
      end
      generated_json_msg
    end

    def update_reporting_data(json_body)
      #TODO enable exception handling
      #begin
      attrs = json_body["Message"]["attributes"]
      operation = attrs.keys.first
      data = attrs[operation]
      #logger.debug "json_body_data >>> #{json_body.inspect}"
      self.reporting_service_config.document_to_update.singularize.classify.constantize.send(operation.downcase.to_sym, data)
      #rescue => err
      #  logger.debug err.message.inspect + "+++++++++++++++"
      #  return {:success => false}
      #end

      #logger.debug "data >>> #{data.inspect}"
      begin
        hotel_id = data['Data'][0]['entity_id']
        Moteel::MongoSync.new(Hotel.find(hotel_id)).notify_server_to_update_mongo
      rescue
      end

      {:success => true}
    end

    private

    def prepare_and_save_data(action_type)
      json_message_body = reporting_service_prepare_json_message(action_type)

      #logger.debug "json message :: " + json_message_body.inspect
      #logger.debug "calling prepare_and_save_report_data"

      if json_message_body.present? && !json_message_body.empty?
        update_reporting_data(json_message_body)
      end
    end

    def reporting_service_prepare_json_message(action_type)
      message_body = {}
      document_to_update = self.reporting_service_config.document_to_update
      generated_json = reporting_service_generate_json(action_type)

      if action_type.present? && !generated_json.empty?
        message_body[action_type] = generated_json

        if !message_body.empty?
          {
              "Message" => {
                  "Document" => "#{document_to_update}",
                  "attributes" => message_body
              }
          }
        else
          nil
        end
      else
        nil
      end
    end

  end
end