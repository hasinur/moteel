module ReportingServiceUpdate
  module ClassMethods

    def enable_reporting_update(options = {})
      include ReportingServiceUpdate::InstanceMethods
      class_attribute :reporting_service_config

      after_save :save_reporting_data
      after_destroy :destroy_reporting_data
      attr_reader :how_important

      self.reporting_service_config = ReportingServiceUpdate::Configuration.new(self, options)
      #handle_asynchronously :save_reporting_data, :priority => Proc.new {|i| i.how_important}
      #handle_asynchronously :destroy_reporting_data
    end

  end
end