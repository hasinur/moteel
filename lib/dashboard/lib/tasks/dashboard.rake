namespace :dashboard do
  desc "Copy javascript and css files from plugin's public directory to Rails.root public directory"
  task :copy_asset_files => :environment do
    require "fileutils"
    include FileUtils::Verbose

    Rails.root = File.expand_path(File.join(File.dirname(__FILE__), "..", "..", "..", "..")) unless defined?(Rails.root)

    mkdir_p File.join(Rails.root, "public", "javascripts", "dashboard")
    mkdir_p File.join(Rails.root, "public", "stylesheets", "dashboard")
    mkdir_p File.join(Rails.root, "public", "images", "dashboard")

    cp_r "#{Rails.root}/vendor/plugins/dashboard/public/javascripts/dashboard/.", "#{Rails.root}/public/javascripts/dashboard"
    cp_r "#{Rails.root}/vendor/plugins/dashboard/public/stylesheets/dashboard/.", "#{Rails.root}/public/stylesheets/dashboard"
    cp_r "#{Rails.root}/vendor/plugins/dashboard/public/images/dashboard/.", "#{Rails.root}/public/images/dashboard"
  end

  desc "Generate sample data for dashboard"
  task :generate_sample_data => :environment do

    #first delete all current data
    all = ReportEntityDailyActivity.where(:entity_id => 7)
    all.each do |r|
      r.destroy
    end

    # add new data
    (0..7).each do |i|
      date = (Time.zone.today - i)
      #add new data
      r = ReportEntityDailyActivity.new
      r.date = date
      r.entity_id = 7
      r.todays_statistics = {
          'current_room_checked_in' => 1 + i,
          'rooms_due_out' => 1 + i,
          'stay_over' => 1 + i,
          'available' => 3 + ( i * 3 )
      }
      r.hotel_inventory = {
          'available' => 1 + i,
          'total_maintenance' => 1 + i,
          'total_available' => 2 + ( i * 2 )
      }
      r.total_guest = {
          'total_guest' => 3 + ( i * 3 ),
          'due_to_check_out' => 1 + i,
          'staying_over' => 1 + i,
          'expected_in_house' => 1 + i
      }
      r.todays_activity_count = {
          'checked_out' => 2 + ( i * 2 ),
          'arrived' => 1 + i,
          'due_out' => 1 + i
      }

      r.save
    end

    (1..7).each do |i|
      date = (Time.zone.today + i)
      #add new data
      r = ReportEntityDailyActivity.new
      r.date = date
      r.entity_id = 7
      r.todays_statistics = {
          'current_room_checked_in' => 1 + i,
          'rooms_due_out' => 1 + i,
          'stay_over' => 1 + i,
          'available' => 3 + ( i * 3 )
      }
      r.hotel_inventory = {
          'available' => 1 + i,
          'total_maintenance' => 1 + i,
          'total_available' => 2 + ( i * 2 )
      }
      r.total_guest = {
          'total_guest' => 3 + ( i * 3 ),
          'due_to_check_out' => 1 + i,
          'staying_over' => 1 + i,
          'expected_in_house' => 1 + i
      }
      r.todays_activity_count = {
          'checked_out' => 2 + ( i * 2 ),
          'arrived' => 1 + i,
          'due_out' => 1 + i
      }

      r.save
    end

    #first delete all current data
    all = ReportItemDailyActivity.where(:entity_id => 7)
    all.each do |r|
      r.destroy
    end

    # add new data
    (0..30).each do |i|
      date = Time.zone.today
      #add new data
      r = ReportItemDailyActivity.new
      r.date = date
      r.entity_id = 7
      r.markers = ["dirty", "clean"]
      r.report_item_daily_activity_translations = []
      r.allocated_item_id = 10
      r.reservation_id = 10
      r.start_date = date
      r.item_type_id = 10
      r.item_id = 10
      r.item_name = ""
      r.report_guests = []
      r.item_type_name = 'ABC'
      r.status = i % 2 == 1 ? "checked_in" : "reserved"
      r.end_date = date
      r.save
    end

  end

  desc "Copy javascript and css files from Rails.root public directory to plugin's' public directory"
  task :overwrite_asset_files => :environment do
    require "fileutils"
    include FileUtils::Verbose

    Rails.root = File.expand_path(File.join(File.dirname(__FILE__), "..", "..", "..", "..")) unless defined?(Rails.root)

    cp_r "#{Rails.root}/public/javascripts/dashboard/.", "#{Rails.root}/vendor/plugins/dashboard/public/javascripts/dashboard"
    cp_r "#{Rails.root}/public/stylesheets/dashboard/.", "#{Rails.root}/vendor/plugins/dashboard/public/stylesheets/dashboard"
    cp_r "#{Rails.root}/public/images/dashboard/.", "#{Rails.root}/vendor/plugins/dashboard/public/images/dashboard"
  end

end