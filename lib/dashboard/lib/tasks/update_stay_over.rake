namespace :reporting do

  desc "update stay over data for reporting"
  task(:update_stay_over => :environment) do
    stayed_over_data = ReportItemDailyActivity.where(:ar_model_name => 'AllocatedRoom', :second_shift => false, :status => /checked_in/, :start_date.gt => 7.days.ago, :end_date.lt => Time.zone.today)
    stayed_over_data.each do |sod|
      AllocatedRoom.find(sod.reference_id.to_i).save rescue ''
    end
  end
end

