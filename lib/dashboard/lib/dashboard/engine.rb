require "reporting_service_update"

module Dashboard
  class Engine < Rails::Engine
    config.before_initialize do
      ActiveSupport.on_load :active_record do
        ActiveRecord::Base.send(:include, ReportingServiceUpdate)
      end
    end
    initializer "dashboard" do
      require "daily_activity_calendar"
      require "mongo_id_extension/reporting_data_updater_util"
      ActiveRecord::Base.send :include, ReportingServiceUpdate
    end

    initializer "static assets" do |app|
      app.middleware.use ::ActionDispatch::Static, "#{root}/app/assets"
    end

  end
end