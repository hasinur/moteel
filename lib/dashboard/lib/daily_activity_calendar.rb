module DailyActivityCalendar

  def self.included(base)
    base.send :extend, ClassMethods
  end

  module ClassMethods

    TOTAL_GRID_WIDTH = {7 => 98.5, 14=> 98.0, 21 =>97.4, 28 => 96.0}

    def has_daily_activity_calendar(options={})
      send :include, InstanceMethods
    end

    # For the given date and range, find the start and end dates
    # Find all the events within this range, and create event strips for them
    def daily_activities_hash(shown_date, day_count = 7, filter_options = {}, find_options = {})
      shown_date ||= Time.zone.now
      shown_date = Time.zone.parse(shown_date).to_time if shown_date.class == String

      unless TOTAL_GRID_WIDTH.has_key?(day_count)
        day_count = 7
      else
        day_count = day_count.to_i
      end

      strip_start, strip_end = get_start_and_end_dates(shown_date, day_count)
     # raise "#{strip_start}  -- #{strip_end}"
      items = items_for_date_range(strip_start, strip_end, find_options)
      item_hash = generate_hash_by_item_id(items)
      items_by_type_date = create_item_hash_by_type_date(strip_start, strip_end, item_hash, filter_options)
      grid_info = calculate_grid_info(day_count)
      items_by_type_date << grid_info
    end

    def get_start_and_end_dates(shown_date, days_count)
      #some logic might be needed next
      strip_start = shown_date
      strip_end = shown_date + (days_count-1).days
      [strip_start, strip_end]
    end

    # Get the items overlapping the given start and end dates
    def items_for_date_range(start_d, end_d, find_options = {})
      self.where(find_options).in(status: Constants::ITEM_VIEW_STATUSES).where(:date.gte => start_d, :date.lte => end_d).asc(:date)
    end

    # Create the various strips that show items.
    def create_item_hash_by_type_date(strip_start, strip_end, items, filter_options={})
      # create an inital item strip, with a nil entry for every day of the displayed days
      date_range = []

      strip_start.to_date.upto(strip_end.to_date) do |day|
        date_range << day
      end

      report_items = ReportItem.where(filter_options).asc(:reference_id)

      items_by_item_types = {}
      report_items.each do |report_item|
        available_rooms = {}
        items_by_item_types[report_item.item_type_id] ||= {}
        tmp_hash = {}
        tmp_hash[:item_id] = report_item.reference_id
        tmp_hash[:item_name] = report_item.name
        tmp_hash[:markers] = report_item.markers if report_item.markers.present?
        tmp_hash[:activities] = []

        item_activities = items[report_item.reference_id.to_i]
        date_range.each do |date|
          tmp_hash[:activities] << (item_activities[date.to_s] rescue nil)
        end

        unless items_by_item_types[report_item.item_type_id]['info'].present?
          items_by_item_types[report_item.item_type_id]['info'] = {'name' => report_item.item_type_name, :entity_id => report_item.entity_id}
        end

        items_by_item_types[report_item.item_type_id]['data'] ||= []
        items_by_item_types[report_item.item_type_id]['available_rooms'] ||= []
        items_by_item_types[report_item.item_type_id]['data'] << tmp_hash
        items_by_item_types[report_item.item_type_id]['available_rooms'] << available_rooms
      end

      [items_by_item_types, date_range]
    end

    def calculate_grid_info(day_count)
      TOTAL_GRID_WIDTH[day_count]/day_count
    end

    def generate_hash_by_item_id(items)
      hash = {}
      items.each do |item|
        hash[item.item_id] ||= {}
        date_key = item.date.to_s
        hash[item.item_id][date_key] ||= {}
        if item.second_shift?
          hash[item.item_id][date_key][:b] = item.stay_view_data
        else
          hash[item.item_id][date_key][:a] = item.stay_view_data
        end
      end
      hash
    end

  end


  # Instance Methods
  module InstanceMethods
    def stay_view_data
      h = {:status => self.respond_to?(:raw_status) ? self.raw_status : status,
           :second_shift => second_shift,
           :main_guest => self.displayable_billing_guest,
           :event_length => event_length,
           :day_passed => day_passed,
           :ar_model_name => self.ar_model_name,
           :reference_id => reference_id
      }
      if event_start?
        h.merge!({:event_start => true})
      end
      h.merge!({:event_end => true}) if event_end?
      h
    end

    def event_start?
      return true if second_shift? && start_date && date.to_date == start_date.to_date
      false
    end

    def event_end?
      return true if !second_shift? && end_date && date.to_date == end_date.to_date
      false
    end

    def event_length
      (end_date.to_date - start_date.to_date).to_i rescue 0
    end

    def main_guest
      self.report_guests.first.full_name rescue note
    end


    def day_passed
      (date.to_date - start_date.to_date).to_i rescue 0
    end
  end
end

