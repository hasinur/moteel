class ReportItemDailyActivity
  include Mongoid::Document
  include MongoIdExtension::ReportingDataUpdaterUtil
  include DailyActivityCalendar

  field :date, type: Date, :default => Time.zone.today
  field :status, type: String
  field :allocated_item_id, type: Integer
  field :reservation_id, type: Integer
  field :start_date, type: Date
  field :end_date, type: Date

  field :item_type_name, type: String, localize: true
  field :item_type_id, type: Integer
  field :entity_id, type: Integer
  field :item_id, type: Integer
  field :item_name, type: String
  field :markers, type: Array
  field :second_shift, type: Boolean
  field :reference_id, type: Integer
  field :ar_model_name, type: String
  field :check_in_id, type: Integer
  field :reserved_by, type: String
  field :transferred_at, type: Date
  field :house_keeping_status, type: String
  field :associated_item_ids, type: Array # for tracking multiple room checkin, it will store other room ids

  embeds_many :report_guests
                                          #after_save :update_entity_activities
                                          #after_destroy :update_entity_activities, :update_default_entity_activities
  accepts_nested_attributes_for :report_guests

  has_daily_activity_calendar

  scope :with_branch, lambda { |branch_id| where(entity_id: branch_id) }
  scope :available, not_in(:status => "checked_in", :second_shift => true)
  scope :occupied, where(status: "checked_in")
  scope :checked_in, where(status: "checked_in")
  scope :today, lambda { where(date: Time.zone.today) }
  scope :with_date, ->(date) { where(date: date) }
  scope :occupied, where(status: 'checked_in')
  scope :with_house_keeping_status, lambda { |status| where(:house_keeping_status => status) }
  scope :last_check_ins, lambda { |item_ids, date| where(:item_id => item_ids, :end_date.lte => date.to_date).in(:status => Constants::REPORT_CHECKED_IN_STATUS).order_by(:end_date => :desc).first }

  def total_guests
    report_guests.collect { |rg| rg.dependants.count + 1 }.sum
  end

  def customized_status
    if Constants::REPORT_CHECKED_OUT_STATUS.include?(self.status)
      return 'available'
    end
    status
  end

  def raw_status
    status == Constants::REPORT_CONFIRMED_STATUS ? Constants::REPORT_RESERVED_STATUS.join(' ') : status
  end

  def displayable_billing_guest
    main_guest.length > 0 ? main_guest : reserved_by
  end

  #def date=(date)
  #  if date.class == String
  #    write_attribute :date, Time.zone.parse(date).to_time.beginning_of_day
  #  else
  #    write_attribute :date, date
  #  end
  #end

  class << self

    def update_associated_items(check_in)
      room_ids = check_in.allocated_rooms.collect(&:room_id)
      check_in.allocated_rooms.each do |ar|
        t_room_ids = room_ids - [ar.room_id]
        ReportItemDailyActivity.where(:check_in_id => ar.check_in_id, :item_id => ar.room_id).set(:associated_item_ids, t_room_ids)
      end
    end

    def generate_availability(entity_id, item_type_id=nil, start_date = Time.zone.today, end_date=Time.zone.today, extra = {})
      return [] unless entity_id.present?
      return_array = []
      item_filters = extra[:item_filters]
      items = ReportItem.where(:entity_id => entity_id.to_i).asc(:reference_id)
      items = items.where(:item_type_id => item_type_id.to_i) if item_type_id.present?
      items = item_filters.call(items) if item_filters.present?
      daily_activities = self.where(:entity_id => entity_id.to_i,
                                     :second_shift => true,
                                     :start_date.lte => start_date.to_date,
                                     :end_date.gte => end_date.to_date).in(:item_id => items.map(&:reference_id),
                                                                           :status => Constants::CHECK_IN_STATUS_CODES)

      daily_activities = Hash[*daily_activities.collect { |v|
        [v.item_id, v]
      }.flatten]

      first_shift_activities = self.where(:entity_id => entity_id.to_i,
                                          :second_shift => true,
                                          :date.gte => start_date.to_date,
                                          :date.lte => end_date.to_date).in(:status => Constants::CHECK_IN_STATUS_CODES)

      first_shift_activities = Hash[*first_shift_activities.collect { |v|
        [v.item_id, v]
      }.flatten]

      items.each do |item|
        if daily_activities[item.reference_id.to_i].present? && first_shift_activities[item.reference_id.to_i].present?
          return_array << item_view_hash(item, daily_activities[item.reference_id.to_i], nil, {:start_date => start_date, :end_date => end_date})
        elsif daily_activities[item.reference_id.to_i].blank? && first_shift_activities[item.reference_id.to_i].present?
          next
        else
          return_array << item_view_hash(item, nil, nil, {:start_date => start_date, :end_date => end_date})
        end
      end
      return_array
    end

    def generate_availability_item(item_id, date = Time.zone.today)
      return [] unless item_id.present?
      daily_activity = self.where(:item_id => item_id.to_i, :second_shift => true, :date => date).first
      first_shift_activity = self.where(:item_id => item_id.to_i, :second_shift => false, :date => date).first
      item = ReportItem.where(:reference_id => item_id.to_i).first
      item_view_hash(item, daily_activity, first_shift_activity)
    end

    def item_view_hash_from_activity(item_with_status, existing_hash = {})

      return existing_hash if item_with_status.nil?

      if item_with_status.status == Constants::REPORT_CONFIRMED_STATUS
        existing_hash[:status] = item_with_status.raw_status
        existing_hash[:markers] << item_with_status.status
      else
        existing_hash[:status] = item_with_status.customized_status
      end

      if Constants::MONGODB_RESERVATION_STATUS.include?(item_with_status.status)
        guest_info = item_with_status.displayable_billing_guest
        existing_hash[:days_left] = (item_with_status.end_date - Time.zone.today).to_i
      end

      existing_hash[:guest_note] = guest_info || item_with_status.note
      existing_hash[:markers] += item_with_status.markers if item_with_status.markers.present?
      #existing_hash[:due_out] = 1 if item_with_status.markers.include?('due_out') if item_with_status.markers.present?
      existing_hash[:reference_id] = item_with_status.reference_id
      existing_hash[:check_in_id] = item_with_status.check_in_id
      existing_hash[:associated_item_ids] = item_with_status.associated_item_ids
      existing_hash[:guests_count] = item_with_status.total_guests
      existing_hash[:transferred_at] = item_with_status.transferred_at

      existing_hash[:dirty] = true if item_with_status.house_keeping_status.to_s == Constants::HOUSEKEEPING_STATUS_CODES[:dirty]
      existing_hash[:clean] = true unless item_with_status.house_keeping_status.to_s == Constants::HOUSEKEEPING_STATUS_CODES[:dirty]

      existing_hash[:partial_room_checkout] = true if item_with_status.associated_item_ids.try(:size).to_i > 0
      existing_hash[:partial_guest_checkout] = true if item_with_status.total_guests.to_i > 1

      existing_hash
    end

    def item_view_hash(item, item_with_status, first_shift_activity, extra = {})
      tmp_hash = {}
      tmp_hash[:name] = item.name
      tmp_hash[:item_id] = item.reference_id.to_i

      tmp_hash[:item_type_id] = item.item_type_id
      tmp_hash[:current_balance] = item.checkin_total_charge - item.checkin_total_paid rescue 0
      tmp_hash[:markers] = []

      tmp_hash[:item_type_name] = item.item_type_name
      if item_with_status.present?
        tmp_hash = item_view_hash_from_activity(item_with_status, tmp_hash)
      elsif item.guests_in_room?
        start_date = extra[:start_date] || Time.zone.today
        end_date = extra[:end_date] || Time.zone.today

        last_check_in = ReportItemDailyActivity.where(:item_id => item.reference_id, :end_date.lte => start_date.to_date).in(:status => Constants::REPORT_CHECKED_IN_STATUS).order_by(:end_date => :desc).first
        if last_check_in.present?
          tmp_hash = item_view_hash_from_activity(last_check_in, tmp_hash)
          tmp_hash[:status] = 'due_out'
        else
          tmp_hash[:status] = 'available'
        end
      else
        tmp_hash[:status] = 'available'
      end
      tmp_hash[:markers] += item.markers
      tmp_hash[:markers] += first_shift_activity.markers if first_shift_activity.present? && first_shift_activity.markers.present?
      tmp_hash[:markers] += item.house_keeping_status.present? ? [item.house_keeping_status.downcase] : [Constants::HOUSEKEEPING_STATUS_CODES[:clean].downcase]

      tmp_hash[:cleaning] = item.house_keeping_status.to_s == Constants::HOUSEKEEPING_STATUS_CODES[:dirty] ? 1 : 0
      tmp_hash[:dirty] = item.house_keeping_status.to_s == Constants::HOUSEKEEPING_STATUS_CODES[:dirty]
      tmp_hash[:clean] = item.house_keeping_status.to_s != Constants::HOUSEKEEPING_STATUS_CODES[:dirty]
      tmp_hash[:confirmed] = true if tmp_hash[:markers].include?(Constants::ROOM_STATUS_CODES[:confirmed])

      tmp_hash[:multi_select_room] = tmp_hash[:status] == Constants::ITEM_VIEW_AVAILABLE_STATUS

      if tmp_hash[:status] == Constants::ROOM_STATUS_CODES[:locked]
        tmp_hash[:locked_room_task_id] = LockedRoom.find(item_with_status.reference_id).try(:task_id)
      end

      if tmp_hash[:status] == Constants::ROOM_STATUS_CODES[:maintenance]
        tmp_hash[:damaged_room_task_id] = DamagedRoom.find(item_with_status.reference_id).try(:task_id)
      end

      tmp_hash
    end


    def available?(daily_activities)
      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.second_shift == true
            return false
          end
        end
      else
        return true
      end
      true
    end

    def checked_in?(daily_activities)

      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.status == 'checked_in' && daily_activity.second_shift == true
            return true
          end
        end
      else
        return false
      end
      false
    end

    def reserved?(daily_activities)
      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.status == 'reserved' && daily_activity.second_shift == true
            return true
          end
        end
      else
        return false
      end
      false
    end

    def due_out?(daily_activities, process_date=Time.zone.today)
      daily_activities.where(:status => 'checked_in', :end_date => process_date, :second_shift => false).all.count > 0
    end

    def stay_over?(daily_activities)
      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.status == 'stay_over' && daily_activity.second_shift == true
            return true
          end
        end
      else
        return false
      end
      false
    end

    def checked_out?(daily_activities)
      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.status == 'checked_out' && daily_activity.second_shift == true
            return true
          end
        end
      else
        return false
      end
      false
    end

    def maintenance?(daily_activities)
      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.status == 'maintenance' && daily_activity.second_shift == true
            return true
          end
        end
      else
        return false
      end
      false
    end

  end


  def item_type_activity_count
    ReportItem.where(:entity_id => entity_id.to_i)
  end


  def update_default_entity_activities(entity_id = nil)
    entity_id = entity_id.nil? ? self.entity_id : entity_id
    checked_in = 0
    rooms_due_out = 0
    stay_over = 0
    available = 0
    total_maintenance = 0
    total_guest = 0
    due_to_check_out = 0
    expected_in_house = 0
    checked_out = 0
    item_type_availability_count = {}
    reserved = 0
    item_type_availability = {}
    item_type_checked_in = {}
    item_type_checked_out = {}
    item_type_reserved = {}
    item_type_maintenance = {}
    item_type_due_out = {}

    items = ReportItem.where(:entity_id => entity_id.to_i)
    items.each do |item|

      available += 1
      item_type_availability_count[item.item_type_id.to_s] ||= 0
      item_type_availability_count[item.item_type_id.to_s] = item_type_availability_count[item.item_type_id.to_s].to_i + 1

      item_type_availability[item.item_type_id.to_s] ||= 0
      item_type_availability[item.item_type_id.to_s] = item_type_availability[item.item_type_id.to_s].to_i + 1

      item_type_checked_in[item.item_type_id.to_s] ||= 0

      item_type_reserved[item.item_type_id.to_s] ||= 0

      item_type_due_out[item.item_type_id.to_s] ||= 0

      item_type_checked_out[item.item_type_id.to_s] ||= 0

      item_type_maintenance[item.item_type_id.to_s] ||= 0

    end
    reporting_data = {
        'entity_id' => entity_id,
        'todays_statistics' => {
            'current_room_checked_in' => checked_in,
            'rooms_due_out' => rooms_due_out,
            'stay_over' => stay_over,
            'available' => available,
            'reserved' => reserved
        },

        'hotel_inventory' => {
            'available' => available,
            'total_maintenance' => total_maintenance,
            'total_available' => available - total_maintenance
        },

        'total_guest' => {
            'total_guest' => total_guest,
            'due_to_check_out' => due_to_check_out,
            'staying_over' => stay_over,
            'expected_in_house' => expected_in_house
        },

        'todays_activity_count' => {
            'checked_out' => checked_out,
            'arrived' => checked_in,
            'due_out' => rooms_due_out
        },

        'item_type_activity_count' => item_type_availability_count,
        'item_type_availability' => item_type_availability,
        'item_type_checked_in' => item_type_checked_in,
        'item_type_checked_out' => item_type_checked_out,
        'item_type_reserved' => item_type_reserved,
        'item_type_due_out' => item_type_due_out,
        'item_type_maintenance' => item_type_maintenance,

    }

    report_entity_daily_activity = DefaultEntityDailyActivity.where(
        :entity_id => entity_id)

    if !report_entity_daily_activity.empty?
      report_entity_daily_activity.first.update_attributes(reporting_data)
    else
      DefaultEntityDailyActivity.create(reporting_data)
    end


  end

  def self.update_from_hash(p_data)
    begin
      if p_data.present?
        logger.info("inserting.....")
        logger.info("p_data.size=#{p_data["Data"].size}")
        logger.info(p_data.inspect)
        self.delete_all(reference_id: p_data["Data"].first["reference_id"].to_i, ar_model_name: p_data["Data"].first["ar_model_name"])
        unless p_data["Data"].size == 1 and p_data["Data"].first["second_shift"].to_s == 'false'
          self.insert(p_data)
        end
      end
    rescue
      logger.error "Exception from update_from_hash :: #{p_data.inspect} :: Exception :: #{$!.inspect}"
    end
  end

  #all item ids including self.item_id
  def all_associated_item_ids
    (associated_item_ids << self.item_id).uniq
  end


  #all item ids excluding self.item_id
  def prepared_associated_item_ids(others = [])
    others - [self.item_id]
  end

end
