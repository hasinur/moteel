class ReportGuest
  include Mongoid::Document

  field :full_name, type: String
  field :id_type, type: String
  field :id_number, type: String
  field :dependants, type: Array

  #embeds_many :report_guest_recommendations
  embedded_in :report_item_daily_activity
end