class HotelConfig
  include Mongoid::Document

  field :hotel_id, type: Integer
  field :shomoos_config, type: Array

  validates :hotel_id, uniqueness: true
end
