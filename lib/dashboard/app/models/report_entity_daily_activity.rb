class ReportEntityDailyActivity
  include Mongoid::Document

  field :date, type: Date
  field :entity_id, type: Integer
  field :todays_statistics, type: Hash
  field :hotel_inventory, type: Hash
  field :total_guest, type: Hash
  field :todays_activity_count, type: Hash
  field :item_type_activity_count, type: Hash
  field :item_type_availability, type: Hash
  field :item_type_checked_in, type: Hash
  field :item_type_checked_out, type: Hash
  field :item_type_reserved, type: Hash
  field :item_type_maintenance, type: Hash
  field :item_type_due_out, type: Hash
  field :last_synced_at, type: Time

  embeds_one :report_entity_night_audit
  accepts_nested_attributes_for :report_entity_night_audit

  #def date=(date)
  #  if date.class == String
  #    write_attribute :date, Time.zone.parse(date).to_time.utc.beginning_of_day
  #  else
  #    write_attribute :date, date
  #  end
  #end

  class << self
    def sync_activity(entity_id, p_start_date, p_end_date)
      p_start_date.to_date.upto(p_end_date.to_date) do |day|
        update_daily_activities(entity_id, day)
      end
    end

    def update_daily_activities(entity_id, process_date)
      sync_logger = Logger.new("#{Rails.root}/log/sync_logger.log")
      p
      reporting_data = prepare_entity_hash_data(entity_id, process_date)
      report_entity_daily_activity = ReportEntityDailyActivity.where(
          :entity_id => entity_id,
          :date => process_date).first

      if report_entity_daily_activity.present?
        report_entity_daily_activity.update_attributes!(reporting_data)
      else
        ReportEntityDailyActivity.create!(reporting_data)
      end
    end

    def day_statistics(entity_id, process_date)
      active_items = ReportItem.where(entity_id: entity_id, active: true)
      data = ReportItemDailyActivity.where(date: process_date, :entity_id => entity_id.to_i, second_shift: true).in(item_id: active_items.collect(&:reference_id),
                                                                                                                    status: Constants::CHECK_IN_STATUS_CODES)
      data_for_departure = ReportItemDailyActivity.where(date: process_date, :entity_id => entity_id.to_i).in(item_id: active_items.collect(&:reference_id),
                                                                                                              status: Constants::CHECK_IN_STATUS_CODES)
      #data.each {|d| ap d}
      #ref_for_check_in = ReportItem.where(:entity_id => entity_id, active: true, guests_in_room: true).collect(&:reference_id)
      return_hash ={}
      data.each do |d|
        return_hash[:todays_expected_guests] ||= 0
        return_hash[:todays_expected_guests] += 1 if Constants::REPORT_EXPECTED_GUESTS_STATUS.include?(d.status) && (d.start_date == process_date)
        return_hash[:no_show_reservation] ||= 0
        return_hash[:no_show_reservation] += 1 if Constants::EXPECTED_RESERVATION_STATUSES.include?(d.status) && (d.start_date == process_date) #TO DO later
        return_hash[:todays_reservation] ||= 0
        return_hash[:todays_reservation] += 1 if Constants::REPORT_RESERVED_STATUS.include?(d.status) && (d.start_date == process_date)
        return_hash[:todays_confirmed_reservation] ||= 0
        return_hash[:todays_confirmed_reservation] += 1 if Constants::REPORT_CONFIRMED_STATUS == d.status && (d.start_date == process_date)
      end

      data_for_departure.each do |d|
        return_hash[:todays_departure_guest] ||= 0
        return_hash[:todays_departure_guest] += 1 if Constants::REPORT_CHECKED_IN_STATUS.include?(d.status) && d.end_date == process_date
      end
      return_hash
    end


    def prepare_entity_hash_data(entity_id, process_date)

      checked_in = 0
      checked_out = CheckIn.with_branch(entity_id).to_days_check_out.count rescue 0 #Check out count by force.
      rooms_due_out = 0
      stay_over = 0
      todays_checkin_rooms = ReportItemDailyActivity.with_branch(entity_id).with_date(process_date).checked_in.count

      p "todays_checkin_rooms >> #{todays_checkin_rooms}"
      #ReportItemDailyActivity.with_branch(entity_id).today.collect{|i| p i }
      #p ">>>>>>>"

      todays_checkout_rooms = AllocatedRoom.with_branch(entity_id).not_transferred.check_out_at(process_date).count
      available = 0
      todays_departure_guest = 0
      total_maintenance = 0
      total_guest = 0
      due_to_check_out = 0
      expected_in_house = 0
      reservation = 0
      confirmed_reservation = 0
      item_type_availability_count = {}
      reserved = 0
      item_type_availability = {}
      item_type_checked_in = {}
      item_type_checked_out = {}
      item_type_reserved = {}
      item_type_maintenance = {}
      item_type_due_out = {}
      todays_stats = {}
      no_show_reservation = 0
      #availability checking
      active_items = ReportItem.where(entity_id: entity_id.to_i, active: true)
      unavailable_room_activities = ReportItemDailyActivity.where(:entity_id => entity_id.to_i,
                                                                  :date => process_date,
                                                                  :second_shift => true).in(item_id: active_items.collect(&:reference_id),
                                                                                            status: Constants::CHECK_IN_STATUS_CODES)


      occupied_rooms = unavailable_room_activities.in(status: Constants::OCCUPIED_STATUS_CODES).collect(&:item_id)
      housekeeping_data = {}
      active_items.each do |item|
        status_code = Constants::HOUSEKEEPING_STATUS_CODES.invert[item.house_keeping_status] || :clean
        housekeeping_data[status_code] ||= {}
        housekeeping_data[status_code][:occupied] ||= 0
        housekeeping_data[status_code][:vacant] ||= 0
        if occupied_rooms.include?(item.reference_id)
          housekeeping_data[status_code][:occupied] += 1
        else
          housekeeping_data[status_code][:vacant] += 1
        end
      end

      due_out_activities = ReportItemDailyActivity.where(:entity_id => entity_id.to_i,
                                                         :date => process_date,
                                                         :end_date => process_date,
                                                         :second_shift => false).in(item_id: active_items.collect(&:reference_id),
                                                                                    status: Constants::ROOM_STATUS_CODES[:checked_in])

      #available room activity count
      available_rooms = unavailable_room_activities.present? ? active_items.not_in(reference_id: unavailable_room_activities.collect(&:item_id)) : active_items
      available = available_rooms.size
      available_rooms.each do |item|
        item_type_availability_count[item.item_type_id.to_s] ||= 0
        item_type_availability_count[item.item_type_id.to_s] = item_type_availability_count[item.item_type_id.to_s].to_i + 1

        item_type_availability[item.item_type_id.to_s] ||= 0
        item_type_availability[item.item_type_id.to_s] = item_type_availability[item.item_type_id.to_s].to_i + 1
      end

      if unavailable_room_activities.present?
        #checked in room activity count
        checked_in_rooms = active_items.in(reference_id: unavailable_room_activities.where(status: Constants::ROOM_STATUS_CODES[:checked_in]).collect(&:item_id))
        checked_in_rooms.each do |item|
          item_type_checked_in[item.item_type_id.to_s] ||= 0
          item_type_checked_in[item.item_type_id.to_s] = item_type_checked_in[item.item_type_id.to_s].to_i + 1
        end

        #checked out room activity count
        checked_out = CheckIn.with_branch(entity_id).to_days_check_out.count rescue 0 #Fetch data from mysql, because we erase checkout data from mongodb.
        checked_in = CheckIn.with_branch(entity_id).to_days_check_in.count rescue 0 #Fetch data from mysql, because we erase checkout data from mongodb.
        #maintenance room activity count
        maintenance_rooms = active_items.in(reference_id: unavailable_room_activities.where(status: Constants::ROOM_STATUS_CODES[:maintenance]).collect(&:item_id))
        total_maintenance = maintenance_rooms.size
        maintenance_rooms.each do |item|
          item_type_maintenance[item.item_type_id.to_s] ||= 0
          item_type_maintenance[item.item_type_id.to_s] = item_type_maintenance[item.item_type_id.to_s].to_i + 1
        end

        #reserved and confirmed room activity count
        reserved_rooms = active_items.in(reference_id: unavailable_room_activities.in(status: Constants::ROOM_STATUS_CODES[:reserved]).collect(&:item_id))
        reserved = reserved_rooms.size
        reserved_rooms.each do |item|
          item_type_reserved[item.item_type_id.to_s] ||= 0
          item_type_reserved[item.item_type_id.to_s] = item_type_reserved[item.item_type_id.to_s].to_i + 1
        end

        #due out room activity count
        due_out_rooms = active_items.in(reference_id: due_out_activities.collect(&:item_id))
        rooms_due_out = due_out_rooms.size
        due_out_rooms.each do |item|
          item_type_due_out[item.item_type_id.to_s] ||= 0
          item_type_due_out[item.item_type_id.to_s] = item_type_due_out[item.item_type_id.to_s].to_i + 1
        end

        #stay over room activity count
        total_maintenance = active_items.in(reference_id: unavailable_room_activities.where(status: Constants::ROOM_STATUS_CODES[:stay_over].downcase).collect(&:item_id)).size

        #total guest count

        total_guest += find_total_guest(unavailable_room_activities)
        todays_stats = day_statistics(entity_id, process_date)
        expected_in_house = todays_stats[:todays_expected_guests].nil? ? 0 : todays_stats[:todays_expected_guests]
        todays_departure_guest = todays_stats[:todays_departure_guest].nil? ? 0 : todays_stats[:todays_departure_guest]
        no_show_reservation = todays_stats[:no_show_reservation].nil? ? 0 : todays_stats[:no_show_reservation]
        reservation = todays_stats[:todays_reservation].nil? ? 0 : todays_stats[:todays_reservation]
        confirmed_reservation = todays_stats[:todays_confirmed_reservation].nil? ? 0 : todays_stats[:todays_confirmed_reservation]
      end


      {
          'date' => process_date,
          'entity_id' => entity_id,
          'todays_statistics' => {
              'current_room_checked_in' => todays_checkin_rooms,
              'rooms_due_out' => rooms_due_out,
              'stay_over' => stay_over,
              'available' => available,
              'reserved' => reserved,
              'housekeeping_data' => housekeeping_data
          },

          'hotel_inventory' => {
              'available' => available,
              'total_maintenance' => total_maintenance,
              'total_available' => available - total_maintenance
          },

          'total_guest' => {
              'total_guest' => total_guest,
              'due_to_check_out' => due_to_check_out,
              'staying_over' => stay_over,
              'expected_in_house' => expected_in_house
          },

          'todays_activity_count' => {
              'checked_out' => checked_out,
              'todays_checkin' => checked_in,
              'todays_checkin_rooms' => todays_checkin_rooms,
              'todays_checkout_rooms' => todays_checkout_rooms,
              'todays_reservation' => reservation,
              'todays_confirmed_reservation' => confirmed_reservation,
              'todays_departures_guests' => todays_departure_guest,
              'todays_departures_groups' => 0,
              'todays_expected_guests' => -1,
              'todays_expected_groups' => -1,
              'todays_no_show_guests' => no_show_reservation,
              'todays_no_show_groups' => -1,
              'arrived' => expected_in_house,
              'due_out' => rooms_due_out
          },


          'item_type_activity_count' => item_type_availability_count,
          'item_type_availability' => item_type_availability,
          'item_type_checked_in' => item_type_checked_in,
          'item_type_checked_out' => item_type_checked_out,
          'item_type_reserved' => item_type_reserved,
          'item_type_due_out' => item_type_due_out,
          'item_type_maintenance' => item_type_maintenance
      }
    end


    def find_total_guest(daily_activities)
      total_guest_count = 0
      if daily_activities.present?
        daily_activities.each do |daily_activity|
          if daily_activity.status != Constants::ROOM_STATUS_CODES[:checked_out]
            total_guest_count += (daily_activity.report_guests.first[:dependants].size + 1 rescue 0)
          end
        end
      end
      total_guest_count
    end

    def range_activity(p_entity_id, days_count, start_date = Time.zone.today)
      return_hash = {}
      days_count.to_i.times do |i|
        start_date = start_date + 1
        data = date_activity_or_default(p_entity_id, start_date)
        return_hash[start_date.to_s] = data
      end
      return_hash
    end

    def date_activity_or_default(p_entity_id, p_date)
      where(:date => p_date, :entity_id => p_entity_id).first ||
          DefaultEntityDailyActivity.where(:entity_id => p_entity_id).first
    end
  end
end