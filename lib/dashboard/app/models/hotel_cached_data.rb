class HotelCachedData
  include Mongoid::Document
  include MongoIdExtension::ReportingDataUpdaterUtil

  field :hotel_id, type: Integer
  field :total_not_viewed_online_reservation, type: Integer, default: 0
  field :total_online_reservation, type: Integer, default: 0

  def self.find_by_hotel(hotel)
    HotelCachedData.where(hotel_id: hotel.id).last || HotelCachedData.create(hotel_id: hotel.id)
  end

  def self.current
    RequestStore.store[:hotel_cached_data] ||= HotelCachedData.find_by_hotel(Hotel.current)
  end

  def hotel
    @hotel ||= Hotel.find(self.hotel_id)
  end

  def expire(column, value)
    self.update_attribute(column, value)
  end

end