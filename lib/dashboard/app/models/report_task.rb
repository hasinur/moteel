class ReportTask
  include Mongoid::Document
  include MongoIdExtension::ReportingDataUpdaterUtil

  field :title, type: String
  field :department_id, type: Integer
  field :entity_id, type: Integer
  field :item_type_id, type: Integer
  field :item_id, type: Integer
  field :reference_id, type: Integer
  field :status, type: String
  field :assigned_user_id, type: Integer
  field :creator_id, type: Integer
  field :due_date, type: DateTime

  scope :for_room, lambda { |room_id| where(item_id: room_id) }
  scope :assigned, where(status: Constants::TASK_STATUSES[:assigned])
  scope :not_assigned, where(status: Constants::TASK_STATUSES[:not_assigned])
  scope :completed, where(status: Constants::TASK_STATUSES[:completed])
  scope :owned_by, lambda { |user_id| where(creator_id: user_id) }
  scope :assigned_to, lambda { |user_id| where(assigned_user_id: user_id) }
  scope :over_due, where(:due_date.lt => Time.zone.today.to_date.beginning_of_day)
  scope :with_user, lambda { |user_id| any_of({:assigned_user_id => user_id}, {:creator_id => user_id}) }
  scope :with_branch, lambda { |branch_ids| where(:entity_id => branch_ids)}
  scope :due_between, lambda { |start_date, end_date|
    where(:due_date.gte => start_date.beginning_of_day,
          :due_date.lte => end_date.end_of_day).in(:status => [Constants::TASK_STATUSES[:assigned],
                                                    Constants::TASK_STATUSES[:not_assigned]]) }

  def self.create_from_data_hash(data_array)
    data_array.each do |data|
      new_item = self.new(data)
      new_item.save
    end
  end

  def self.update_from_hash(p_data)
    data_hash = p_data["Data"].first

    if !p_data.empty?
      existing_record = self.where(:reference_id => data_hash["reference_id"].to_i, :ar_model_name => data_hash["ar_model_name"]).first
      if existing_record
        existing_record.update_attributes(data_hash)
      else
        self.insert(p_data)
      end
    end
  end

  def self.tasks_by_room_within_dates(entity_id, user_id=0, start_date=nil, end_date=nil)
    start_date ||= Time.zone.today
    end_date = start_date if end_date.nil? || end_date.to_date <= start_date.to_date
    scoped_data = self.with_branch(entity_id)
    scoped_data = scoped_data.with_user(user_id).due_between(start_date, end_date) if user_id > 0
    return_hash = {}

    scoped_data.all.collect do |d|
      return_hash[d.item_id.to_s] ||= 0
      return_hash[d.item_id.to_s] +=1
    end
    return_hash
  end

end
