class ReportEntityNightAudit
  include Mongoid::Document

  embedded_in :report_entity_daily_activity
end