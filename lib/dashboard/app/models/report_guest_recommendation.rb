class ReportGuestRecommendation
  include Mongoid::Document

  field :recommendation_status, type: String
  field :remarks, type: String

  embedded_in :report_guest
end