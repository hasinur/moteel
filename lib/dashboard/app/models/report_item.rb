class ReportItem
  include Mongoid::Document
  include MongoIdExtension::ReportingDataUpdaterUtil

  field :name, type: String
  field :item_type_name, type: String, localize: true
  field :item_type_id, type: Integer
  field :entity_id, type: Integer
  field :reference_id, type: Integer
  field :markers, type: Array
  field :active, type: Boolean
  field :checkin_total_charge, type: Float
  field :checkin_total_paid, type: Float
  field :guests_in_room, type: Boolean, default: false

  after_destroy :remove_item_daily_activities
  after_save :update_entity_activities_data
  scope :available, where(:active => true, :guests_in_room => false)
  scope :for_item_type, lambda { |item_type_id| where(:item_type_id => item_type_id) }
  default_scope where(:active => true)
  REPORTING_UPDATE_IGNORED_ATTRS = [:guests_in_room]

  def self.create_from_data_hash(data_array)
    data_array.each do |data|
      if data.has_key?("translations")
        item_translations = data.delete("translations")

        new_item = self.new(data)

        item_translations.each do |translation|
          eval("new_item.#{translation.keys.first}_translations = translation['#{translation.keys.first}']")
        end
        new_item.save
      else
        new_item = self.new(data)
        new_item.save
      end
    end
  end

  def self.update_from_hash(p_data)
    data_hash = p_data["Data"].first

    if !p_data.empty?
      existing_record = self.where(:reference_id => data_hash["reference_id"].to_i, :ar_model_name => data_hash["ar_model_name"]).first
      if existing_record
        if data_hash.has_key?("translations")
          item_translations = data_hash.delete("translations")
          item_translations.each do |translation|
            eval("existing_record.#{translation.keys.first}_translations = translation['#{translation.keys.first}']")
          end
        end

        logger.info "********************* >>> update"

        existing_record.update_attributes(data_hash)
      else
        logger.info "********************* >>> insert"

        self.insert(p_data)
      end

    end
  end

  private
  def remove_item_daily_activities
    ReportItemDailyActivity.destroy_all(item_id: self.reference_id)
  end

  def update_entity_activities_data
    process_date = Time.zone.today
    if self.changes.except(REPORTING_UPDATE_IGNORED_ATTRS).any?
      ReportEntityDailyActivity.update_daily_activities(self.entity_id, process_date)
    end
  end

end
