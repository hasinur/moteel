class DefaultEntityDailyActivity
  include Mongoid::Document

  field :entity_id, type: Integer
  field :todays_statistics, type: Hash
  field :hotel_inventory, type: Hash
  field :total_guest, type: Hash
  field :todays_activity_count, type: Hash
  field :item_type_activity_count, type: Hash

  def date=(date)
    if date.class == String
      write_attribute :date, Time.zone.parse(date).to_time.utc.beginning_of_day
    else
      write_attribute :date, date
    end
  end
end