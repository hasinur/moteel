class DashboardReport
  class << self
    def get_check_in_report_data(entity_id, days_count = 0)
      get_report_data_by_key_index('todays_statistics', 'current_room_checked_in', entity_id, days_count, 'past')
    end

    def get_available_report_data(entity_id, days_count = 0)
      get_report_data_by_key_index('todays_statistics', 'available', entity_id, days_count)
    end

    def get_maintenance_report_data(entity_id, days_count = 0)
      get_report_data_by_key_index('hotel_inventory', 'total_maintenance', entity_id, days_count)
    end

    def get_pending_checkout_report_data(entity_id, days_count = 0)
      get_report_data_by_key_index('todays_statistics', 'stay_over', entity_id, days_count)
    end

    def get_report_data_by_key_index(hash_key, hash_index, entity_id, days_count = 0, tense='future')
      current_total = change_total = 0
      current_data, change_data = get_report_data(entity_id, days_count, tense)

      current_data.each do |r|
        current_total += r.send(hash_key)[hash_index]
      end

      change_data.each do |r|
        change_total += r.send(hash_key)[hash_index]
      end

      change = get_change_text(current_total, change_total, tense)

      [current_total, change_total, change]
    end

    def get_report_data(entity_id, days_count = 0, tense='future')
      if days_count == 0
        current_data = ReportEntityDailyActivity.where(:entity_id => entity_id, :date => Time.zone.today).all
        tense == 'future' ? end_date = Time.zone.today + 1 : end_date = Time.zone.today - 1
        change_data = ReportEntityDailyActivity.where(:entity_id => entity_id, :date => end_date).all
      else
        if tense == 'future'
          start_date = Time.zone.today
          end_date = start_date + days_count
        else
          end_date = Time.zone.today
          start_date = end_date - days_count
        end
        current_data = ReportEntityDailyActivity.where(:entity_id => entity_id, :date.gt => start_date, :date.lte => end_date).all

        if tense == 'future'
          start_date = end_date
          end_date = start_date + days_count
        else
          end_date = start_date
          start_date = end_date - days_count
        end
        change_data = ReportEntityDailyActivity.where(:entity_id => entity_id, :date.gt => start_date, :date.lte => end_date).all
      end

      # If the current data is empty get the default data
      if current_data.empty?
        current_data = []
        current_data[0] = get_default_data(entity_id)
      end

      [current_data, change_data]
    end

    def get_change_text(current_total, change_total, tense = 'future')
      if current_total == change_total
        change = 'equal'
      elsif current_total < change_total && tense == 'past'
        change = 'down'
      elsif current_total < change_total && tense == 'future'
        change = 'up'
      elsif current_total > change_total && tense == 'past'
        change = 'up'
      elsif current_total > change_total && tense == 'future'
        change = 'down'
      else
        change = 'up'
      end
      change
    end

    def get_default_data(entity_id)
      reportItem = ReportItemDailyActivity.new
      reportItem.update_default_entity_activities(entity_id)
      data = DefaultEntityDailyActivity.where(:entity_id => entity_id).first
    end

  end
end