module ReportingService::ItemsHelper
  def view_options_selected_tab(expected, classes = '')
    @view_options_selected_tab.to_s == expected ? 'h_tab_select '+ classes : classes
  end

  def merged_params_for_vew_options(p_hash = {})
    params.delete_if { |key, _| ['controller', 'action'].include? key }.merge(p_hash)
  end

  def merged_params_for_stay_view_date(type='next')
    days_count = params[:days_count].present? ? params[:days_count].to_i : 7
    filter_date = params[:stay_view_filter_date].present? ? params[:stay_view_filter_date].to_s : Time.zone.today.to_s
    filter_date = Time.zone.parse(filter_date)
    filter_date = type == 'next' ? (filter_date + days_count).to_s : (filter_date - days_count).to_s

    params.delete_if { |key, _| ['controller', 'action'].include? key }.merge(:stay_view_filter_date => filter_date)
  end
end