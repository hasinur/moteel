module ReportingService::ContextMenuHelper


  COMMON_WRAPPERS = {
      :outer_div_class => "context-menu-item",
      :inner_div_class => "context-menu-item-inner"
  }


  def link_to_check_in_from_reservation(item)
    if permitted_to?(:edit, :check_ins)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to 'javascript:void(0)', :url => check_in_reservation_path(item[:check_in_id], :target_dom_id => "fdt_checkin_#{item[:check_in_id]}_content", :open_tab => '0'),
                :rel => "fdt_checkin_#{item[:check_in_id]}", :class => 'for_tab_view',
                :loading_container_id => "fdt_checkin_#{item[:check_in_id]}_content",
                :title => check_in_tab_title(item[:status], item[:check_in_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_payment_#{item[:item_id]}" do
            t('views.labels.check_in')
          end
        end
      end
    end
  end

  # for available room context menu

  def link_to_check_in_context_menu(item)
    if  permitted_to?(:from_room_view, :check_ins)
      url = from_room_view_check_ins_path(:item_id => item[:item_id], :target_dom_id => 'fdr_checkin_form_content')
      construct_link(item, url, 'check_in', 'fdr_checkin_form', 'check_in', 'check_in')
    end
  end


  def link_to_reservation_context_menu(item)
    if  permitted_to?(:from_room_view, :reservations)
      url = from_room_view_reservations_path(:item_id => item[:item_id], :target_dom_id => 'fdr_reservation_form_content')
      construct_link(item, url, 'check_in', 'fdr_reservation_form', 'reservation', 'reservation')
    end
  end


  def link_to_mark_as_clean_or_dirty_context_menu(item)


    content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do

      if item[:cleaning].present?
        if item[:cleaning] == 1
          link_to mark_as_clean_rooms_path(:room_id => item[:item_id]), :remote => true, :method => :post do
            content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_cleaning_#{item[:item_id]}" do
              t('views.labels.mark_as_clean')
            end
          end
        else
          link_to mark_as_dirty_rooms_path(:room_id => item[:item_id]), :remote => true, :method => :post do
            content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_cleaning_#{item[:item_id]}" do
              t('views.labels.mark_as_dirty')
            end
          end
        end
      end

    end

  end


  def link_to_assign_markers_menu(item)
    if permitted_to?(:edit_markers, :rooms)

      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to edit_markers_rooms_path(:id => item[:item_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
            t('views.labels.assign_markers')
          end
        end
      end
    end
  end


  def link_to_mark_as_maintenance_context_menu(item)

    content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
      link_to 'javascript:void(0)',
              :class => "add_edit_task icon_ad", :remote => true,
              :'data-url' => new_lb_tasks_path(:supplied_type => 'DamagedRoomTask', :'task[room_id]' => item[:item_id], 'hide_due_date' => 'true') do
        content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
          t('views.labels.mark_as_maintenance')
        end
      end
    end
  end


  def link_to_mark_as_locked_context_menu(item)

    content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
      link_to 'javascript:void(0)',
              :class => "add_edit_task icon_ad", :remote => true,
              :'data-url' => new_lb_tasks_path(:supplied_type => 'LockedRoomTask', :'task[room_id]' => item[:item_id], 'hide_due_date' => 'true') do
        content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
          t('views.labels.mark_as_locked')
        end
      end
    end
  end

  # for checked room context menu

  def link_to_add_charge_context_menu(item)
    if permitted_to?(:add_charge_from_room_view, :allocated_rooms)

      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to add_charge_from_room_view_allocated_rooms_path(:reference_id => item[:reference_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_charge_#{item[:item_id]}" do
            t('views.labels.add_charge')
          end
        end
      end
    end
  end

  def link_to_add_payment_context_menu(item)
    if permitted_to?(:add_payment_from_room_view, :check_ins)

      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to add_payment_from_room_view_check_ins_path(:reference_id => item[:reference_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_payment_#{item[:item_id]}" do
            t('views.labels.add_payment')
          end
        end
      end
    end
  end

  def link_to_partial_checkout_context_menu(item)
    if permitted_to?(:add_payment_from_room_view, :check_ins)
      if item[:associated_item_ids].size > 0 #room check out able
        content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
          link_to new_partial_checkout_path(:context => 'room', :check_in_id => item[:check_in_id], :allocated_room_id => item[:reference_id]), 'data-remote' => true do
            content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_payment_#{item[:item_id]}" do
              t('views.labels.partial_checkout')
            end
          end
        end
      end
    end
  end

  def link_to_partial_guest_checkout_context_menu(item)
    if permitted_to?(:add_payment_from_room_view, :check_ins)
      if item[:guests_count].to_i > 1 #guest check out able
        content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
          link_to new_partial_checkout_path(:context => 'guest', :check_in_id => item[:check_in_id], :allocated_room_id => item[:reference_id]), 'data-remote' => true do
            content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_payment_#{item[:item_id]}" do
              t('views.labels.partial_guest_checkout')
            end
          end
        end
      end
    end
  end

  def link_to_assign_employee_context_menu(item)
    if permitted_to?(:assign_employee, :rooms)

      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to assign_employee_rooms_path(:room_id => item[:item_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
            t('views.labels.assign_employee')
          end
        end
      end
    end
  end

  def link_to_assign_task_context_menu(item)
    if permitted_to?(:new_lb, :tasks)

      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to 'javascript:void(0)', :class => "add_edit_task icon_ad", :remote => true,
                :'data-url' => new_lb_tasks_path(:'task[room_id]' => item[:item_id]) do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
            t('views.labels.assign_task')
          end
        end
      end
    end
  end

  def link_to_room_transfer_context_menu(item)
    if permitted_to?(:edit, :check_ins)

      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to change_room_from_room_view_check_ins_path(:reference_id => item[:reference_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "room_change__#{item[:item_id]}" do
            t('views.labels.room_transfer')
          end
        end
      end
    end
  end


  def link_to_check_out_context_menu(item)
    if permitted_to?(:edit, :check_ins)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to edit_check_in_path(item[:check_in_id], :target_dom_id => "fdt_checkin_#{item[:check_in_id]}_content", :open_tab => '2'),
                :rel => "fdt_checkin_#{item[:check_in_id]}", :class => 'for_tab_view',
                :title => check_in_tab_title(item[:status], item[:check_in_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_check_out_#{item[:item_id]}" do
            t('views.labels.check_out')
          end
        end
      end
    end
  end

  def link_to_edit_context_menu(item)
    if permitted_to?(:edit, :check_ins)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to 'javascript:void(0)', :url => edit_check_in_path(item[:check_in_id], :target_dom_id => "fdt_checkin_#{item[:check_in_id]}_content", :open_tab => '0'),
                :rel => "fdt_checkin_#{item[:check_in_id]}", :class => 'for_tab_view',
                :loading_container_id => "fdt_checkin_#{item[:check_in_id]}_content",
                :title => check_in_tab_title(item[:status], item[:check_in_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "manage_payment_#{item[:item_id]}" do
            t('views.labels.edit')
          end
        end
      end
    end
  end

  def link_to_uncheck_in_context_menu(item)
    if permitted_to?(:edit, :check_ins)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to cancel_checkin_from_room_view_check_ins_path(:reference_id => item[:reference_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "room_change_#{item[:item_id]}" do
            t('views.labels.uncheck_in')
          end
        end
      end
    end
  end

  def link_to_uncheck_out_context_menu(item)
    if permitted_to?(:edit, :check_ins)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to cancel_checkout_from_room_view_check_ins_path(:check_in_id => item[:check_in_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "room_change_#{item[:item_id]}" do
            t('views.labels.uncheck_out')
          end
        end
      end
    end
  end

  def link_to_assign_markers_context_menu(item)
    if permitted_to?(:edit_markers, :rooms)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to edit_markers_rooms_path(:id => item[:item_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
            t('views.labels.assign_markers')
          end
        end
      end
    end
  end

  # mark as clean and dirty will be called from available context menu option

  # for reservation context menu
  def link_to_cancel_context_menu(item)
    if permitted_to?(:check_in, :reservations)
      content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
        link_to cancel_checkin_from_room_view_check_ins_path(:reference_id => item[:reference_id]), 'data-remote' => true do
          content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "room_change_#{item[:item_id]}" do
            t('views.labels.cancel')
          end
        end
      end
    end
  end

  def link_to_unlock_context_menu(item)
    locked_room_task_id = LockedRoom.find(item[:reference_id]).try(:task_id)
    content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
      link_to 'javascript:void(0)',
              :class => "add_edit_task icon_ad", :remote => true,
              :'data-url' => edit_lb_task_path(locked_room_task_id, :supplied_type => 'LockedRoomTask', 'hide_due_date' => 'true') do
        content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "room_change_#{item[:item_id]}" do
          t('views.labels.unlocked')
        end
      end
    end
    end

  def link_to_removed_from_maintainance_context_menu(item)
    damaged_room_task_id = DamagedRoom.find(item[:reference_id]).try(:task_id)
    content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
      link_to 'javascript:void(0)',
              :class => "add_edit_task icon_ad",
              :remote => true,
              :'data-url' => edit_lb_task_path(damaged_room_task_id, :supplied_type => 'DamagedRoomTask', 'hide_due_date' => 'true') do

        content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class], :id => "room_change_#{item[:item_id]}" do
          t('views.labels.remove_from_maintenance')
        end
      end
    end
  end

# link constructor for big link

  def construct_link(item, url, class_name, rel, title, label)

    content_tag :div, :class => COMMON_WRAPPERS[:outer_div_class] do
      link_to 'javascript:void(0)',
              :url => url,
              :class => class_name, :rel => rel, :title => t("views.labels.#{title}") do
        content_tag :div, :class => COMMON_WRAPPERS[:inner_div_class] do
          t("views.labels.#{label}")
        end
      end
    end
  end


end
