module HotelAdminNavHelper

  def hotel_admin_ability
    @ha_ability ||= ::HotelAdminAbility.new(current_user)
  end

  def hotel_admin_can?(*args)
    hotel_admin_ability.can?(*args)
  end

# Start helper methods for Hotel and room management menu
  def render_branch_management_link(tag=:li)
    if hotel_admin_can? :read, Hotel
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.branch_management'), [:hotel_admin, :hotels])
      end
    end
  end

  def render_room_type_management_link(tag=:li, options ={})
    if hotel_admin_can? :read, RoomType
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.room_type_management'), [:hotel_admin, :room_types], options)
      end
    end
  end

  def render_room_management_link(tag=:li)
    if hotel_admin_can? :index, Room
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.room_management'), [:hotel_admin, :rooms])
      end
    end
  end

  def render_department_management_link(tag=:li)
    if hotel_admin_can? :index, Department
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.department_management'), [:hotel_admin, :departments])
      end
    end
  end

  def render_marker_management_link(tag=:li)
    if hotel_admin_can? :index, Marker
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.marker_management'), [:hotel_admin, :markers])
      end
    end
  end

  def render_amenities_management_link(tag=:li)
    content_tag tag do
      link_to(t('template.dashboard.hotel_admin_nav.amenities_management'), '')
    end
  end

  def render_rate_setup_link(tag=:li)
    if hotel_admin_can? :read, Rate
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.rate_management'), [:hotel_admin, :rates])
      end
    end
  end

  def render_manage_asset_category_link(tag=:li)
    if hotel_admin_can? :index, AssetCategory
      content_tag tag do
        link_to(t('template.dashboard.manage_asset_category'), [:hotel_admin, :asset_categories])
      end
    end
  end

  def render_manage_inventory_item_link(tag=:li)
    if hotel_admin_can? :index, InventoryItem
      content_tag tag do
        link_to(t('template.dashboard.manage_inventory_item'), [:hotel_admin, :inventory_items])
      end
    end
  end

  def render_inventory_management_link(tag=:li)
    if hotel_admin_can? :index, ItemTransaction
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.inventory_management'), [:hotel_admin, :item_transactions])
      end
    end
  end

  def render_asset_management_link(tag=:li)
    if hotel_admin_can? :index, FixedAsset
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.asset_management'), [:hotel_admin, :fixed_assets])
      end
    end
  end


  def render_branch_service_management_link(tag=:li)
    if hotel_admin_can? :index, Service
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.branch_service_management'), [:hotel_admin, :services])
      end
    end
  end


  def render_night_audit_link(tag=:li)
    if hotel_admin_can? :start_up, NightAuditWizard
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.night_audit'), [:hotel_admin, :night_audit_wizards])
      end
    end
  end

  # End helper methods for Hotel and room management menu

  # Start helper methods for User management menu

  def render_task_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, Task
      content_tag tag do
        link_to(t('views.labels.task_management'), [:hotel_admin, :tasks], options)
      end
    end
  end

  def render_emp_management_link(tag=:li, options ={})
    if permitted_to? :index, :user_panel_agent_users
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.emp_management'), [:hotel_admin, :employees], options)
      end
    end
  end

  def render_permission_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, PermissionGroup
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.permission_group_management'), [:hotel_admin, :permission_groups], options)
      end
    end
  end

  def render_extend_link(tag=:li, options ={})
    render_extend_link
    if hotel_admin_can? :extend, PermissionGroup
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.assign_branch'), :extend_permission_groups, options)
      end
    end
  end


  def render_assign_branch_link(tag=:li, options ={})
    if hotel_admin_can? :assign_branch, Employee
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.assign_branch'), :assign_branch_employees, options)
      end
    end
  end

  # End helper methods for User management menu

  # Start helper methods for CRM management menu

  def render_travel_agent_management_link(tag=:li, options ={})
    if permitted_to? :index, :user_panel_agents
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.travel_agent_management'), :user_panel_agents, options)
      end
    end
  end

  def render_guest_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, Guest
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.guest_management'), [:hotel_admin, :guests], options)
      end
    end
  end

  def render_group_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, Group
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.group_management'), [:hotel_admin, :groups], options)
      end
    end
  end

  def render_wallet_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, Wallet
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.wallet_management'), [:hotel_admin, :wallets], options)
      end
    end
  end


  # End helper methods for CRM management menu
  # Start helper methods for Accounting menu

  def render_account_head_management_link(tag=:li, options ={})
    # raise (permitted_to? :index, :account_panel_account_heads).inspect
    if permitted_to? :index, :account_panel_account_heads
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.account_head_management'), :account_panel_account_heads, options)
      end
    end
  end

  def render_bank_management_link(tag=:li, options ={})
    if permitted_to? :index, :account_panel_banks
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.bank_management'), :account_panel_banks, options)
      end
    end
  end

  def render_bank_branch_management_link(tag=:li, options ={})
    if permitted_to? :index, :account_panel_bank_branches
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.bank_branch_management'), :account_panel_bank_branches, options)
      end
    end
  end

  def render_bank_account_management_link(tag=:li, options ={})
    if permitted_to? :index, :account_panel_bank_branch_accounts
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.bank_account_management'), :account_panel_bank_branch_accounts, options)
      end
    end
  end

  def render_cash_counter_management_link(tag=:li, options ={})
    if permitted_to? :index, :cash_counters
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.cash_counter_management'), [:hotel_admin, :cash_counters], options)
      end
    end
  end

  def render_discount_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, HotelDiscount
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.discount_management'), [:hotel_admin, :hotel_discounts], options)
      end
    end
  end

  def render_journal_vouchers_link(tag=:li, options ={})
    if permitted_to? :index, :account_panel_transactions
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.journal_vouchers'), :account_panel_transactions, options)
      end
    end
  end

  def render_bank_voucher_link(tag=:li, options ={})
    if permitted_to? :index, :account_panel_receivable_transactions
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.bank_voucher'), :account_panel_receivable_transactions, options)
      end
    end
  end

  def render_payment_type_management_link(tag=:li, options ={})
    if hotel_admin_can? :index, HotelPaymentType
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.payment_type_management'), :hotel_payment_types, options)
      end
    end
  end

  def render_trial_balance_link(tag=:li, options ={})
    if permitted_to? :trial_balance, :account_reports
      content_tag tag do
        link_to(t('template.dashboard.trial_balance'), :trial_balance_account_reports, options)
      end
    end
  end

  def render_ledger_link(tag=:li, options ={})
    if permitted_to? :ledger, :account_reports
      content_tag tag do
        link_to(t('template.dashboard.ledger'), :ledger_account_reports, options)
      end
    end
  end

  # End helper methods for Accounting menu

  # Start helper methods for Online management menu

  def render_customize_template_link(tag=:li, options ={})
    if permitted_to? :index, :module_integration
      content_tag tag do
        link_to(t('template.dashboard.customize_template'), :module_integration, options)
      end
    end
  end

  def render_assign_template_link(tag=:li, options ={})
    if hotel_admin_can? :assign_template, Hotel
      content_tag tag do
        link_to(t('template.dashboard.assign_template'), [:hotel_admin, :assign_template_wizards], options)
      end
    end
  end

  def render_customize_theme_link(tag=:li, options ={})
    if hotel_admin_can? :index, LayoutTheme
      content_tag tag do
        link_to(t('template.dashboard.customize_theme'), :layout_themes, options)
      end
    end
  end

  def render_upload_layout_link(tag=:li, options ={})
    if hotel_admin_can? :upload_layout, Layout
      content_tag tag do
        link_to(t('template.dashboard.upload_layout'), :upload_layout_layouts, options)
      end
    end
  end

  def render_shomoos_config_link(tag=:li, options ={})
    if permitted_to? :index, :shomoos_config
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.shomoos_config'), :shomoos_config, options)
      end
    end
  end

  def render_audit_log_link(tag=:li, options ={})
    if hotel_admin_can? :read, AuditTrail
      content_tag tag do
        link_to(t('template.dashboard.hotel_admin_nav.audit_log'), [:hotel_admin, :activity_logs], options)
      end
    end
  end
# End helper methods for Online management menu
end
