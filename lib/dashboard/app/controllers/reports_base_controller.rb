class ReportsBaseController < DashboardController
  filter_access_to :home, :room_occupancy_report, :sales_by_services_report
  layout 'reports_base'

  def home
    @selected_menu = 'reports_base'
    @view_options_selected_tab = 'reports_base_dashboard'

    @todays_activity_count = entity_daily_activity[:todays_activity_count] rescue {}
    @todays_statistics = entity_daily_activity[:todays_statistics] rescue {}
    respond_to do |format|
      format.js
      format.html
    end
  end

  def room_occupancy_report
    current_hotel = current_reporting_branch_id
    @room_occupancies = AllocatedRoom.room_wise_occupancy_revenue(current_hotel, params)
    respond_to do |format|
      format.js  {render :action => 'room_occupancy_report/index'}
    end
  end

  def sales_by_services_report
    @sales_by_services = []
    respond_to do |format|
      format.js {render :action => 'sales_by_services_report/index'}
    end
  end

end