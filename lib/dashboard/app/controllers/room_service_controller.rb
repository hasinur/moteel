class RoomServiceController < DashboardController
  before_filter :ensure_current_branch_exists
  layout 'room_service'

  def home
    @selected_menu = 'room_service'
    initialize_room_services
    @task = CheckInAddOnTask.new
    respond_to do |format|
      format.js
      format.html
    end
  end

  def assign_employee
    begin
      success = CheckInAddOnTask.bulk_assign(params[:assign_employee_selected_ids], params[:assigned_employee_id], params[:task_title], params[:room_service_status])
      unless success.empty?
        flash.now[:notice] = t('notifications.operation_successful')
      else
        flash.now[:error] = t('notifications.no_service_selected') if params[:assign_employee_selected_ids].empty?
        flash.now[:error] = t('notifications.no_emp_selected') if params[:assigned_employee_id].empty?
        #flash.now[:error] = t('notifications.no_service_selected')  if params[:assign_employee_selected_ids].empty?
      end
      @success = true
    rescue Exception => e
      flash.now[:error] = t('notifications.no_service_selected') if params[:assign_employee_selected_ids].empty?
      flash.now[:error] = t('notifications.no_emp_selected') if params[:assigned_employee_id].empty?
      logger.error "Error occurred Assigning Employee :: #{e}"
      @success = false
    end
    initialize_room_services
    respond_to do |format|
      format.js
    end
  end

  def update_status
    status = params[:room_service_status].present? && Constants::TASK_STATUSES.has_key?(params[:room_service_status].downcase.to_sym) ? params[:room_service_status] : ""
    @tasks = CheckInAddOnTask.where(id: params[:set_status_selected_ids].split(','))
    if status.present? && params[:set_status_selected_ids].present?
      @tasks.update_all(status: Constants::TASK_STATUSES[status.downcase.to_sym])
      flash.now[:notice] = t('notifications.operation_successful')
    else
      flash.now[:error] = t('notifications.no_service_selected') if params[:set_status_selected_ids].empty?
    end
    respond_to do |format|
      format.js
    end
  end

  private
  def initialize_room_services
    @room_services,@room_services_status_count_hash  = CheckInAddOnTask.list_for_room_service_home(current_branch, params)
    @assigned_users = current_branch.room_service_department.users
  end
end