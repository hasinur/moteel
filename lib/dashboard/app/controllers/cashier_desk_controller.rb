class CashierDeskController < DashboardController
  before_filter :ensure_current_branch_exists
  layout 'cashier_desk'

  def home
    @selected_menu = 'cashier_desk'

    @todays_activity_count = entity_daily_activity[:todays_activity_count] rescue {}
    @todays_statistics = entity_daily_activity[:todays_statistics] rescue {}
    respond_to do |format|
      format.js
      format.html
    end
  end

end