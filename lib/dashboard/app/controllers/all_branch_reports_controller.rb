class AllBranchReportsController < ReportsBaseController

  def home
    @selected_menu = 'all_branch_reports'
    respond_to do |format|
      format.js
      format.html
    end
  end

end