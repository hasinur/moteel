require 'mongo_sync_after_shard_sync'

class ReportingService::ItemsController < FrontDeskController
  include Moteel::MongoSyncAfterShardSync
  filter_access_to [:availability_list, :quick_view, :stay_view]

  before_filter :load_entities_for_front_desk, :only => [:availability_list, :quick_view, :stay_view]
  before_filter :mongo_sync_after_shard_sync, :only => [:availability_list, :quick_view, :stay_view]

  def index

  end

  def new
    @item = ReportItem.new
  end

  def create
    @item = ReportItem.new(params[:report_item])

    if @item.save
      flash[:notice] = @item.item_type_name
      redirect_to dashboard_home_path
    else
      flash[:error] = Common.validation_errors(@item.errors)
      render :new
    end
  end

  def availability_list
    reporting_date = params[:filter_start_date] || Time.zone.today
    @selected_menu = 'front_desk'
    @todays_activity_count = ReportEntityDailyActivity.where(:date => reporting_date, :entity_id => current_reporting_branch_id).first[:todays_activity_count] rescue {}
    @todays_activity_count ||= {}
    @filter_start_date = params[:filter_start_date].present? ? Time.zone.parse(params[:filter_start_date]) : Time.zone.today
    @filter_end_date = params[:filter_end_date].present? ? Time.zone.parse(params[:filter_end_date]) : Time.zone.today

    @items = ReportItemDailyActivity.generate_availability(current_reporting_branch_id, params[:item_type_id], @filter_start_date, @filter_end_date)
    @item_types = RoomType.where('room_types.hotel_id' => current_reporting_branch_id).includes(:translations)
    @item_with_task_count = {}
    @item_with_task_count = ReportTask.tasks_by_room_within_dates(current_reporting_branch_id, current_user.id, @filter_start_date, @filter_end_date)

    @items = fill_in_additional_data_for_template @items

    @custom_markers = Marker.by_entity(current_reporting_branch_id).includes(:translations)
    @compulsory_markers = Marker.compulsory.includes(:translations)
    @view_options_selected_tab = 'front_desk'
    respond_to do |format|
      format.html
      format.js
    end
  end


  def stay_view
    @view_options_selected_tab = 'stay_view'
    @item_types = RoomType.all(:include => :translations,
                               :conditions =>
                                   {'room_types.hotel_id' => current_reporting_branch_id})
    find_options, filter_options = prepare_finder_options
    unless current_reporting_branch_id.present?
      @no_data = true
      return false
    end
    @stay_view_data, @date_range, @grid_width = ReportItemDailyActivity.daily_activities_hash(
        params[:stay_view_filter_date], params[:days_count].to_i, find_options, {})
    @custom_markers = Marker.by_entity(current_reporting_branch_id)
    @compulsory_markers = Marker.compulsory
    respond_to do |format|
      format.html
      format.js
    end
  end

  def quick_view
    @view_options_selected_tab = 'quick_view'
    unless current_reporting_branch_id.present?
      @no_data = true
      return false
    end
    data = ReportEntityDailyActivity.where(:date => Time.zone.today, :entity_id => current_reporting_branch_id).first
    if data.nil?
      ReportEntityDailyActivity.update_daily_activities(current_reporting_branch_id, Time.zone.today)
      data = ReportEntityDailyActivity.where(:date => Time.zone.today, :entity_id => current_reporting_branch_id).first
    end

    @quick_view_data = data
    @data_after_today = ReportEntityDailyActivity.range_activity(current_reporting_branch_id, 12)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def quick_view_future_inventory
    if params[:check_in].present?
      start_date = Time.zone.parse(params[:check_in]).to_date
    else
      start_date = Time.zone.today
    end

    @data_after_today = ReportEntityDailyActivity.range_activity(current_reporting_branch_id, 3)

    respond_to do |format|
      format.js
    end
  end

  def quick_view_future_inventory_more
    if params[:start_date].present?
      start_date = Time.zone.parse(params[:start_date]).to_date
    else
      start_date = Time.zone.now.to_date
    end

    @data_after_today = ReportEntityDailyActivity.range_activity(current_reporting_branch_id, 12)

    respond_to do |format|
      format.js
    end
  end

  protected


  def prepare_finder_options
    #populate the find_option hash
    find_options = {}
    find_options[:entity_id] = current_reporting_branch_id.to_i if current_reporting_branch_id.present?
    find_options[:item_type_id] = params[:item_type_id].to_i if params[:item_type_id].present?

    #populate the find_option hash
    filter_options = {}

    filter_options[:entity_id] = current_reporting_branch_id.to_i if current_reporting_branch_id.present?
    filter_options[:item_type_id] = params[:item_type_id].to_i if params[:item_type_id].present?

    [find_options, filter_options]
  end

  def load_entities_for_front_desk
    @entities = Hotel.where('hotels.id' => manageable_hotel_ids).includes(:translations)
  end

  def fill_in_additional_data_for_template(items)
    items.each do |item|
      item[:multi_select_room] = item[:status] == Constants::ITEM_VIEW_AVAILABLE_STATUS
      item[:task_count] = @item_with_task_count[item[:item_id].to_s]
    end
  end
end