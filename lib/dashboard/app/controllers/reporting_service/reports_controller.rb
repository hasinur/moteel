class ReportingService::ReportsController < ApplicationController

  def check_in
    unless params[:current_reporting_branch_id].present?
      @current_total = ''
      @change_total = ''
      @change = ''
      return false
    end
    entity_id = current_reporting_branch_id
    days_count = params[:days_count].present? ? params[:days_count].to_i : 0

    @current_total, @change_total, @change = DashboardReport.get_check_in_report_data(entity_id, days_count)

    respond_to do |format|
      format.js
    end
  end

  def available
    unless params[:current_reporting_branch_id].present?
      @current_total = ''
      @change_total = ''
      @change = ''
      return false
    end
    entity_id = current_reporting_branch_id
    days_count = params[:days_count].present? ? params[:days_count].to_i : 0

    @current_total, @change_total, @change = DashboardReport.get_available_report_data(entity_id, days_count)

    respond_to do |format|
      format.js
    end
  end

  def maintenance
    unless params[:current_reporting_branch_id].present?
      @current_total = ''
      @change_total = ''
      @change = ''
      return false
    end
    entity_id = current_reporting_branch_id
    days_count = params[:days_count].present? ? params[:days_count].to_i : 0

    @current_total, @change_total, @change = DashboardReport.get_maintenance_report_data(entity_id, days_count)

    respond_to do |format|
      format.js
    end
  end

  def pending_checkout
    unless params[:current_reporting_branch_id].present?
      @current_total = ''
      @change_total = ''
      @change = ''
      return false
    end
    entity_id = current_reporting_branch_id
    days_count = params[:days_count].present? ? params[:days_count].to_i : 0

    @current_total, @change_total, @change = DashboardReport.get_pending_checkout_report_data(entity_id, days_count)

    respond_to do |format|
      format.js
    end
  end
end