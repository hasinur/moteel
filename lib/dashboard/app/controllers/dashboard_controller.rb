class DashboardController < ApplicationController
  before_filter :login_required

  layout 'dashboard_full', :only => ['home']
  before_filter :run_initial_setup

  def index
    redirect_to_dashboard
  end

  def checkout_tab
    @check_in = CheckIn.find params[:check_in_id]
  end

  def full_page
    render :layout => 'hotel_admin'
  end

  def home
    redirect_to_dashboard
  end

  def run_initial_setup
    @selected_menu = 'dashboard'
    if hotel_agent? && !admin?
      if current_user.hotels_count > 0
        return
      else
        redirect_to new_hotel_admin_hotel_path
        #redirect_to new_hotel_path(:from => 'item')
        return
      end

      #if RoomType.by_branches(hotel_ids).count == 0
      #  redirect_to new_room_path(:from => 'item_type')
      #  return
      #end
      #
      #if  Room.with_hotel_id(hotel_ids).count == 0
      #  redirect_to new_room_path(:from => 'item')
      #end
    end
  end

  private
  def redirect_to_dashboard
    flash.keep
    if current_user.admin?
      redirect_to rails_admin_path
    elsif has_default_dashboard_access?
      redirect_to default_dashboard.to_sym
      return
    else
      @selected_menu = 'dashboard'
    end
  end

end