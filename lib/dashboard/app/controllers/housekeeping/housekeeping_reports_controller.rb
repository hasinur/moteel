class Housekeeping::HousekeepingReportsController < HousekeepingController
  filter_access_to :inventory_transaction_list, :fixed_asset_allocation_history, :service_wise_revenue,
                   :payment_type_wise_transactions, :deposit_listing, :financial_statement, :billing_information,
                   :cashier_statement, :revenue_by_counter
  layout 'hotel_admin'

  def payment_type_wise_transactions
    set_month_ago_date_parameter
    query = HotelServicePayment.by_hotel(current_reporting_branch_id)

    reporter(query, 'ajax_report') do
      filter :payment_date, type: :date
      filter :payment_method, type: :payment_method, comp: {eq: I18n.t('views.reports.payment_type')}
      column :payment_date do |hsp|
        fdt(hsp.payment_date)
      end
      column :payment_method
      column :check_in_id
      column :note
      column :amount
    end

  end

  def inventory_transaction_list
    set_date_parameter
    @inventory_items = InventoryItem.all(:include => :translations, :conditions => {:branch_id => current_reporting_branch_id});
    query = ItemTransaction.by_hotel(current_reporting_branch_id)
    reporter(query, 'ajax_report') do
      filter :transaction_date, type: :date
      filter :inventory_item_id, type: :inventory_item, comp: {eq: I18n.t('views.labels.inventory_items')}
      column :transaction_date do |transaction|
        fdt(transaction.transaction_date)
      end
      column :transaction_type do |transaction|
        Constants::INVENTORY_ITEM_TRANSACTION_TYPE.invert[transaction.transaction_type]
      end
      column :quantity
      column :balance
    end
  end

  def fixed_asset_allocation_history

    set_date_parameter
    @fixed_assets = FixedAsset.all(:conditions => {:branch_id => current_reporting_branch_id})
    query = FixedAssetAllocation.includes(:room)
    reporter(query, 'ajax_report') do
      filter :date, type: :date do |query, from, to|
        query.in_between(parse_fdt(from), parse_fdt(to))
      end
      filter :fixed_asset_detail_id, type: :fixed_asset_detail, comp: {eq: I18n.t('views.labels.show_fixed_asset_detail')}
      column :start_date do |transaction|
        fdt(transaction.start_date)
      end
      column :end_date do |transaction|
        fdt(transaction.end_date)
      end
      column :room_no do |transaction|
        transaction.room.prefix + ' - ' + transaction.room.room_number
      end
    end

  end

  def service_wise_revenue
    @services = Service.all(:include => :translations, :conditions => {:branch_id => current_reporting_branch_id});
    set_month_ago_date_parameter
    @results = ReservationLineItem.service_wise_revenue(current_reporting_branch_id, params[:start_date], params[:end_date], params[:service_id])
    @service_revenue = ReservationLineItem.sum_of_service_wise_revenue(current_reporting_branch_id, params[:start_date], params[:end_date])
    @transaction_list = generate_service_wise_rev_hash(@results)
    respond_to do |format|
      format.html
      format.js
      format.csv {
        file_name = "Service Wise Revenue(#{Time.zone.now.to_s(:short)})"
        send_data to_csv(@transaction_list, nil), :filename => "#{file_name}.csv"
      }
      format.pdf {
        file_name = "Service Wise Revenue(#{Time.zone.now.to_s(:short)})"
        service_name = Service.find(params[:service_id]).name rescue ''
        pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).service_wise_revenue_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], service: service_name}).render
        send_data pdf_data_obj, :filename => "#{file_name}.pdf"
      }
    end
  end

  def email_service_wise_revenue_pdf
    set_month_ago_date_parameter
    results = ReservationLineItem.service_wise_revenue(current_reporting_branch_id, params[:start_date], params[:end_date], params[:service_id])
    @service_revenue = ReservationLineItem.sum_of_service_wise_revenue(current_reporting_branch_id, params[:start_date], params[:end_date])
    @transaction_list = generate_service_wise_rev_hash(results)
    file_name = "Service Wise Revenue(#{Time.zone.now.to_s(:short)})"
    pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).service_wise_revenue_pdf({start_date: params[:start_date], end_date: params[:end_date], current_branch: current_branch}).render
    generate_pdf_email(file_name, pdf_data_obj, 'Service wise Reveneue Report of ', 'Please check the attach pdf file. ')
  end

  def deposit_listing
    set_month_ago_date_parameter
    @check_status = params[:checkstatus]
    @reportType = params[:checkstatus]
    @transaction_list = CheckIn.get_deposit_info(current_reporting_branch_id, @reportType, params[:start_date], params[:end_date])
    respond_to do |format|
      format.html
      format.js
      format.csv {
        file_name = "Deposit List(#{Time.zone.now.to_s(:short)})"
        send_data to_csv(@transaction_list, nil), :filename => "#{file_name}.csv"
      }
      format.pdf {
        file_name = "Deposit List(#{Time.zone.now.to_s(:short)})"
        pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).deposit_listing_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date]}).render
        send_data pdf_data_obj, :filename => "#{file_name}.pdf"
      }
    end
  end


  def email_deposit_listing_pdf
    set_month_ago_date_parameter
    @check_status = params[:checkstatus]
    @reportType = params[:checkstatus]
    @transaction_list = CheckIn.get_deposit_info(current_reporting_branch_id, @reportType, params[:start_date], params[:end_date])
    file_name = "Deposit List(#{Time.zone.now.to_s(:short)})"
    pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).deposit_listing_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date]}).render
    generate_pdf_email(file_name, pdf_data_obj, 'Deposit Listing Report of ', 'Please check the attach pdf file. ')
  end

  def financial_statement
    set_week_ago_date_parameter
    sel_option = ''
    if params[:days_count].present?
      sel_option = params[:days_count]
    end
    @duration = set_duration(sel_option, params[:start_date], params[:end_date])
    @transaction_list = CheckIn.get_financial_information(current_reporting_branch_id, params[:start_date], params[:end_date])
    transaction_sum = CheckIn.get_financial_information_sum_in_range(current_reporting_branch_id, @duration[0], @duration[1])
    room_charge, discount_amount, service_charge, paid_amount = [0, 0, 0, 0]
    l_room_charge = transaction_sum[0]['discounted_price'].to_f
    l_discount_amount = transaction_sum[0]['total_room_price'].to_f - transaction_sum[0]['discounted_price'].to_f
    l_service_charge = transaction_sum[0]['total_charge'].to_f - transaction_sum[0]['discounted_price'].to_f
    l_paid_amount = transaction_sum[0]['total_paid'].to_f
    @transaction_list.each do |itm|
      room_charge += itm['Room Charge'].to_f
      discount_amount += itm['Discount on Room Charge'].to_f
      service_charge += itm['Service Charges'].to_f
      paid_amount += itm['Total paid'].to_f
    end
    #-------------------------------------
    @room_charge_pie_chart = generate_pie_chart(room_charge, l_room_charge, @duration[2], @duration[3], I18n.t('views.labels.room_charge'))
    @discount_pie_chart = generate_pie_chart(discount_amount, l_discount_amount, @duration[2], @duration[3], I18n.t('views.labels.discount_on_room_charge'))
    @service_charge_pie_chart = generate_pie_chart(service_charge, l_service_charge, @duration[2], @duration[3], I18n.t('views.labels.service_charges'))
    @total_paid_pie_chart = generate_pie_chart(paid_amount, l_paid_amount, @duration[2], @duration[3], I18n.t('views.labels.total_paid'))
    file_name = "Financial Statement(#{Time.zone.now.to_s(:short)})"
    pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).financial_statement_pdf({start_date: params[:start_date], end_date: params[:end_date], current_branch: current_branch}).render
    if params['report_type'] == 'email'
      generate_pdf_email(file_name, pdf_data_obj, 'Financial Statement of ', 'Please check the attach pdf file. ')
    end
    respond_to do |format|
      format.html
      format.js
      format.csv {
        send_data to_csv(@transaction_list, nil), :filename => "#{file_name}.csv"
      }
      format.pdf {
        send_data pdf_data_obj, :filename => "#{file_name}.pdf"
      }
    end
  end

  def billing_information
    set_month_ago_date_parameter
    @group_type = params[:group_type]
    @billing_info = CheckIn.find_billing_information(current_reporting_branch_id, params[:start_date], params[:end_date], @group_type)
    respond_to do |format|
      format.html
      format.js
      format.csv {
        file_name = "Billing Information(#{Time.zone.now.to_s(:short)})"
        send_data to_csv(@billing_info, nil), :filename => "#{file_name}.csv"
      }
      format.pdf {
        file_name = "Billing Information(#{Time.zone.now.to_s(:short)})"
        pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@billing_info).billing_information_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], group_type: params[:group_type]}).render
        send_data pdf_data_obj, :filename => "#{file_name}.pdf"
      }
    end
  end

  def email_billing_information_pdf
    set_month_ago_date_parameter
    @group_type = params[:group_type]
    @billing_info = CheckIn.find_billing_information(current_reporting_branch_id, params[:start_date], params[:end_date], @group_type)
    file_name = "Billing Information(#{Time.zone.now.to_s(:short)})"
    pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@billing_info).billing_information_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], group_type: params[:group_type]}).render
    generate_pdf_email(file_name, pdf_data_obj, 'Billing Information Report of ', 'Please check the attach pdf file. ')
  end

  def cashier_statement
    @cash_counters = CashCounter.all(:include => :translations, :conditions => {:branch_id => current_reporting_branch_id});
    set_month_ago_date_parameter
    @cash_counter_id = params[:cash_counter_id]
    @transaction_list = CashCounterTransaction.cashier_transaction_info(current_reporting_branch_id, params[:start_date], params[:end_date], @cash_counter_id)
    respond_to do |format|
      format.html
      format.js
      format.csv {
        file_name = "Cashier Statement(#{Time.zone.now.to_s(:short)})"
        send_data to_csv(@transaction_list, nil), :filename => "#{file_name}.csv"
      }
      format.pdf {
        cash_counter = CashCounter.find(params[:cash_counter_id]).name rescue ""
        file_name = "Cashier Statement(#{Time.zone.now.to_s(:short)})"
        pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).cashier_statement_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], cash_couner: cash_counter}).render
        send_data pdf_data_obj, :filename => "#{file_name}.pdf"
      }
    end
  end

  def email_cashier_statement_pdf
    set_month_ago_date_parameter
    @cash_counter_id = params[:cash_counter_id]
    @transaction_list = CashCounterTransaction.cashier_transaction_info(current_reporting_branch_id, params[:start_date], params[:end_date], @cash_counter_id)
    cash_counter = CashCounter.find(params[:cash_counter_id]).name rescue ""
    file_name = "Cashier Statement(#{Time.zone.now.to_s(:short)})"
    pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).cashier_statement_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], cash_counter: cash_counter}).render
    generate_pdf_email(file_name, pdf_data_obj, 'Cashier Statement Report of ', 'Please check the attach pdf file. ')

  end

  def revenue_by_counter
    @cash_counters = CashCounter.all(:include => :translations, :conditions => {:branch_id => current_reporting_branch_id})
    set_month_ago_date_parameter
    results = ReservationLineItem.counter_wise_revenue(current_reporting_branch_id, params[:start_date], params[:end_date], params[:cash_counter_id])
    @transaction_list = generate_revenue_by_counter_hash(results)
    respond_to do |format|
      format.html
      format.js
      format.csv {
        file_name = "Revenue by Counter(#{Time.zone.now.to_s(:short)})"
        send_data to_csv(@transaction_list, nil), :filename => "#{file_name}.csv"
      }
      format.pdf {
        cash_counter = CashCounter.find(params[:cash_counter_id]).name rescue ""
        file_name = "Revenue by Counter(#{Time.zone.now.to_s(:short)})"
        pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).revenue_by_counter_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], cash_counter: cash_counter}).render
        send_data pdf_data_obj, :filename => "#{file_name}.pdf"
      }
    end
  end

  def email_revenue_by_counter_pdf
    set_month_ago_date_parameter
    results = ReservationLineItem.counter_wise_revenue(current_reporting_branch_id, params[:start_date], params[:end_date], params[:cash_counter_id])
    @transaction_list = generate_revenue_by_counter_hash(results)
    cash_counter = CashCounter.find(params[:cash_counter_id]).name rescue ""
    file_name = "Revenue by Counter(#{Time.zone.now.to_s(:short)})"
    pdf_data_obj = AccountingPaymentFinancialReportPdf.new(@transaction_list).revenue_by_counter_pdf({current_branch: current_branch, start_date: params[:start_date], end_date: params[:end_date], cash_counter: cash_counter}).render
    generate_pdf_email(file_name, pdf_data_obj, 'Revenue Report by Counter of ', 'Please check the attach pdf file. ')
  end

  private

  def set_date_parameter

    if params.has_key?('start_date')
      params[:start_date] = Time.zone.parse(params[:start_date])
    else
      params[:start_date] = Time.zone.now
    end

    if params.has_key?('end_date')
      params[:end_date] = Time.zone.parse(params[:end_date])
    else
      params[:end_date] = Time.zone.now
    end
    params[:start_date] = params[:start_date].beginning_of_day
    params[:end_date] = params[:end_date].end_of_day


  end

  def set_month_ago_date_parameter
    if params.has_key?('start_date')
      params[:start_date] = Time.zone.parse(params[:start_date])
    else
      params[:start_date] = 1.months.ago.to_date
    end

    if params.has_key?('end_date')
      params[:end_date] = Time.zone.parse(params[:end_date])
    else
      params[:end_date] = Time.zone.today
    end
    params[:start_date] = params[:start_date].beginning_of_day
    params[:end_date] = params[:end_date].end_of_day
  end

  def set_week_ago_date_parameter
    if params.has_key?('start_date')
      params[:start_date] = Time.zone.parse(params[:start_date])
    else
      params[:start_date] = 1.week.ago.to_date
    end

    if params.has_key?('end_date')
      params[:end_date] = Time.zone.parse(params[:end_date])
    else
      params[:end_date] = Time.zone.today
    end
    params[:start_date] = params[:start_date].beginning_of_day
    params[:end_date] = params[:end_date].end_of_day
  end

  def generate_payment_hash(results)
    transaction_list = results.map do |transaction|
      {I18n.t('views.labels.payment') + ' ' + I18n.t('views.labels.date') => fdt(transaction.created_at),
       I18n.t('views.labels.payment_type') => transaction.serviceable.title,
       I18n.t('views.labels.check_in') => transaction.check_in_id,
       I18n.t('views.labels.notes') => transaction.note,
       I18n.t('views.labels.paid_amount') => transaction.paid_amount
      }
    end
  end

  #def generate_inv_transaction_hash(result)
  #  transaction_list = result.map do |transaction|
  #    {I18n.t('views.labels.transaction_date') => fdt(transaction.transaction_date),
  #     I18n.t('views.labels.transaction_type') => Constants::INVENTORY_ITEM_TRANSACTION_TYPE.invert[transaction.transaction_type],
  #     I18n.t('views.labels.quantity') => transaction.quantity,
  #     I18n.t('views.labels.balance') => transaction.balance
  #    }
  #  end
  #end

  #def generate_fixed_asset_alloc_hash(result)
  #  transaction_list = result.map do |transaction|
  #    {I18n.t('views.labels.start_date') => fdt(transaction.start_date),
  #     I18n.t('views.labels.end_date') => transaction.end_date ? transaction.end_date.calendarize : "Currently Allocated",
  #     I18n.t('views.labels.room_no') => transaction.room.prefix + ' - ' + transaction.room.room_number
  #    }
  #  end
  #end

  def generate_service_wise_rev_hash(results)
    transaction_list = results.map do |transaction|
      {I18n.t('views.labels.service') + ' ' +I18n.t('views.labels.name') => transaction['name'],
       I18n.t('views.labels.amount') => transaction['amount']
      }
    end

  end

  def generate_revenue_by_counter_hash(results)
    transaction_list = results.map do |transaction|
      {I18n.t('views.labels.cash_counters') => transaction['counter'],
       I18n.t('views.labels.service') + ' ' + I18n.t('views.labels.name') => transaction['name'],
       I18n.t('views.labels.amount') => transaction['amount']
      }
    end
  end

  def generate_pdf_email(file_name, pdf_data_obj, str_subject, txt_message)
    @user = current_user
    if params.has_key?('email_report')
      if params[:report_type] == 'email'
        if params[:email_report][:subject].present?
          subject = params[:email_report][:subject]
        else
          subject = str_subject + "#{params[:start_date]} to #{params[:end_date]}"
        end
        if params[:email_report][:message].present?
          message = params[:email_report][:message]
        else
          message = txt_message
        end
        if params[:email_report][:email_to].present?
          to = params[:email_report][:email_to].split(',')
          attachment =pdf_data_obj
          UserMailer.report_mailer(@user, to, subject, message, file_name, attachment).deliver
        end
      end
    end
  end

  def generate_pie_chart(room_charge, l_room_charge, duration1, duration2, title)
    room_charge_data_table = GoogleVisualr::DataTable.new
    room_charge_data_table.new_column('string', 'key')
    room_charge_data_table.new_column('number', 'value')
    room_charge_data_table.add_rows(2)
    room_charge_data_table.set_cell(0, 0, duration1)
    room_charge_data_table.set_cell(0, 1, room_charge)
    room_charge_data_table.set_cell(1, 0, duration2)
    room_charge_data_table.set_cell(1, 1, l_room_charge)

    opts = {:width => 280, :height => 200, :title => title, :is3D => true, :legend => 'bottom'}
    pie_chart = GoogleVisualr::Interactive::PieChart.new(room_charge_data_table, opts)
    return pie_chart
  end

  def set_duration(duration_option, start_date, end_date)
    duration1 = ''
    duration2 = ''
    g_start_date = ''
    g_end_date = ''
    #start_date = Time.zone.parse(start_date)
    #end_date = Time.zone.parse(end_date)

    if duration_option == "1"
      g_end_date = start_date - 1.days
      g_start_date = g_end_date - 1.weeks
      duration1 = t('template.dashboard.days_count_options.this_week')
      duration2 = t('template.dashboard.days_count_options.last_week')

    elsif duration_option == "2"
      g_end_date = start_date - 1.days
      g_start_date = g_end_date. - 1.months

      duration1 = t("template.dashboard.days_count_options.this_month")
      duration2 = t("template.dashboard.days_count_options.last_month")

    elsif duration_option == "3"
      g_end_date = start_date - 1.days
      g_start_date = g_end_date - 1.years
      duration1 = t('template.dashboard.days_count_options.this_year')
      duration2 = t('template.dashboard.days_count_options.last_year')
    else
      no_of_days = (end_date - start_date).to_i
      g_end_date = start_date - 1.days
      g_start_date = g_end_date - no_of_days.to_i.days
      duration1 = t("template.dashboard.days_count_options.this_no_of_days", :no_of_days => no_of_days)
      duration2 = t("template.dashboard.days_count_options.last_no_of_days", :no_of_days => no_of_days)
    end
    return duration = [g_start_date, g_end_date, duration1, duration2]
  end
end