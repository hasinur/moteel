class HousekeepingController < DashboardController
  before_filter :ensure_current_branch_exists
  layout 'housekeeping'

  def home
    load_home_data
    respond_to do |format|
      format.js
      format.html
    end
  end

  def home_print
    load_home_data
    respond_to do |format|
      format.html { render :layout => 'blank' }
    end

  end

  def update_housekeeping_status
    status = params[:housekeeping_status].present? && Constants::HOUSEKEEPING_STATUS_CODES.has_key?(params[:housekeeping_status].downcase.to_sym) ? params[:housekeeping_status] : ""
    @rooms = Room.where(id: params[:set_status_selected_ids].split(','), hotel_id: current_reporting_branch_id)

    if status.present? && params[:set_status_selected_ids].present?
      @rooms.update_all(house_keeping_status: Constants::HOUSEKEEPING_STATUS_CODES[status.downcase.to_sym])
      ReportItem.in(reference_id: @rooms.pluck(:id)).update_all(house_keeping_status: Constants::HOUSEKEEPING_STATUS_CODES[status.to_sym])
      flash.now[:notice] = t('notifications.room_status_updated')
    else
      flash.now[:error] = t('notifications.room_status_update_failed')
    end

    respond_to do |format|
      format.js
    end
  end

  def assign_employee
    @rooms = Room.where(id: params[:assign_employee_selected_ids].split(','), hotel_id: current_reporting_branch_id)
    assigned_employee = User.find(params[:assigned_employee_id]) rescue nil
    housekeeping_dept = current_branch.house_keeping_department
    if assigned_employee.present? && assigned_employee.departments.include?(housekeeping_dept)
      begin
        @rooms.each { |r| r.assign_housekeeping_employee(assigned_employee, current_user, params[:task_title]) }
        flash.now[:notice] = t('notifications.operation_successful')
      rescue
        flash.now[:error] = t('notifications.operation_failed')
      end

    else
      flash.now[:error] = t('notifications.operation_failed')
    end

    respond_to do |format|
      format.js
    end
  end

  def remove_employee
    task = Task.find(params[:task_id])
    if task.destroy
      flash.now[:notice] = t('notifications.operation_successful')
    else
      flash.now[:error] = t('notifications.operation_failed')
    end
    respond_to do |format|
      format.js
    end
  end

  private

  def load_home_data
    @selected_menu = 'house_keeping'
    @selected_room_type = ""

    if params[:room_type_id].present?
      @rooms = Room.where(room_type_id: params[:room_type_id].to_i, hotel_id: current_reporting_branch_id).includes(:room_type => :translations)
      @selected_room_type = params[:room_type_id]
    else
      @rooms = Room.where(hotel_id: current_reporting_branch_id).includes(:room_type => :translations)
    end

    @room_types = RoomType.where(hotel_id: current_reporting_branch_id).includes(:translations)
    status_data
    @assigned_users = current_branch.house_keeping_department.users.includes(:profile => :translations)
    entity_daily_activity = ReportEntityDailyActivity.where(date: Time.zone.today, entity_id: current_reporting_branch_id).first rescue {}
    @todays_activity_count = entity_daily_activity[:todays_activity_count] rescue {}
    @todays_statistics = entity_daily_activity[:todays_statistics] rescue {}
  end

  def status_data
    @status_data = {}
    room_ids = @rooms.pluck(:id)
    ReportItemDailyActivity.where(date: Time.now.to_date, second_shift: true).in(item_id: room_ids).only(["status", "item_id"]).each do |row|
      @status_data[row.item_id] = row.status
    end

    @remarks_and_assignments = {}
    Task.with_room(room_ids).due_between.incomplete.includes(:assigned_user).each do |task|
      @remarks_and_assignments[task.room_id] ||= {}
      @remarks_and_assignments[task.room_id][:assigned_user_task] ||= []
      @remarks_and_assignments[task.room_id][:assigned_user_task] << task if task.assigned_user.present?
      @remarks_and_assignments[task.room_id][:remarks] = task.title
    end
  end
end