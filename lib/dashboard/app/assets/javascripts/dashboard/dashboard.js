$j = jQuery;

(function ($) {
    $.fn.callRemote = function () {
        $.rails.handleRemote($(this));
    };

    var _old_unique = $.unique;

    $.unique = function (arr) {

        // do the default behavior only if we got an array of elements
        if (!!arr[0].nodeType) {
            return _old_unique.apply(this, arguments);
        } else {
            // reduce the array to contain no dupes via grep/inArray
            return $.grep(arr, function (v, k) {
                return $.inArray(v, arr) === k;
            });
        }
    };
})(jQuery);

jQuery(function ($) {
    $(document).ready(function () {


        $j('.rm_view_notification a').live('click', function (e) {
            var roomId = $j(this).data('room-id');
            DASHBOARD.shadeWithLoading($('body'));
            $j.ajax({
                url: "/tasks/incomplete",
                data: {room_id: roomId}
            }).done(function () {
                    DASHBOARD.removeShadeWithLoading();
                });
            e.preventDefault();
        });

        $('.multiple_check_in_link').live('click', function (e) {
            var roomIds = [];
            var startDate = $('#filter_start_date').val();
            var endDate = $('#filter_end_date').val();

            $('#multi_select_room:checked').each(function () {
                roomIds.push($(this).val());
            });

            $.ajax({
                type: "GET",
                url: "/check_ins/new",
                data: { target_dom_id: 'fdr_multiple_checkin_form_content', item_ids: roomIds, start_date: startDate, end_date: endDate }
            });
            e.preventDefault();
        });

        $('.multiple_reservation_link').live('click', function (e) {
            var roomIds = [];
            var startDate = $('#filter_start_date').val();
            var endDate = $('#filter_end_date').val();

            $('#multi_select_room:checked').each(function () {
                roomIds.push($(this).val());
            });

            $.ajax({
                type: "GET",
                url: "/check_ins/new",
                data: { target_dom_id: 'fdr_multiple_reservation_form_content', item_ids: roomIds, start_date: startDate, end_date: endDate, status: 'RESERVED' }
            });
            DASHBOARD.showInnerLoading('fdr_multiple_reservation_form_content');
            e.preventDefault();
        });

        $('#multi_select_room').live('change', function () {
            if ($(this).is(':checked')) {
                if ($('#multi_select_room:checked').size() > 1) {
                    $('#multi_select_room:checked').each(function () {
                        $(this).parents('.room_view_box').attr('context-menu-id', 'room_view_multi_context_' + $(this).val());
                    });
                } else {
                    $(this).parents('.room_view_box').attr('context-menu-id', 'room_view_context_' + $(this).val());
                }

            } else {
                if ($('#multi_select_room:checked').size() > 1) {
                    $('#multi_select_room:checked').each(function () {
                        $(this).parents('.room_view_box').attr('context-menu-id', 'room_view_multi_context_' + $(this).val());
                    });
                } else {
                    $('#multi_select_room:checked').each(function () {
                        $(this).parents('.room_view_box').attr('context-menu-id', 'room_view_context_' + $(this).val());
                    });
                }
                $(this).parents('.room_view_box').attr('context-menu-id', 'room_view_context_' + $(this).val());
            }
            DASHBOARD.resetItemsOnContextMenu();
            DASHBOARD.bindContextMenu();
        });

        $('.select_ajax_submit, .stay_view_item_type_container select').live('change', function (e) {
            $(this).closest('form').submit();
        });

        $('.checkbox_ajax_submit').live('click', function (e) {
            $(this).closest('form').submit();
        });

        // Submit the future inventory form in quick view
        $('#submit_future_inventory_lb').live('click', function (e) {
            $(this).closest('form').submit();
            e.preventDefault();
        });

        if ($('a#submit_future_inventory').hasClass('submit_future_inventory')) {
            $.delay(0.0001, function () {
                $('a.submit_future_inventory').closest('form').submit();
            });
        }

        // submit the submit onload forms
        if ($('form').hasClass('submit_on_load')) {
            $.delay(0.0001, function () {
                $('form.submit_on_load').submit();
            });
        }

        // submit the report forms when the branch is selected
        $('.report_branch_selector').change(function () {
            var currentReportingBranchId = $(this).val();
            if (currentReportingBranchId == undefined) {
                currentReportingBranchId = 0;
            }

            $(this).parents('#report_wrapper').find('form')
                .find('.current_reporting_branch_id')
                .val(currentReportingBranchId)
                .submit();

        });

        /*Show-hide loading panel during  ajax submit*/
        $("form[data-remote], a[data-remote], a[loading_container_id]").live('ajax:before', function () {
            if ($(this).is('form') && $(this).attr('loading-effect')) {
                //do nothing ; it will be handled by the loading effect handler
            }
            else if ($(this).hasClass('show_inner_loading')) {
                DASHBOARD.showInnerLoading($(this).closest('.infoboxes').attr('id'));
            } else if ($(this).attr('loading_container_id')) {
                DASHBOARD.showInnerLoading($(this).attr('loading_container_id'));
            } else {
                DASHBOARD.shadeWithLoading($('body'));
            }
        });

        $("form[data-remote], a[data-remote], a[loading-effect]").ajaxComplete(function () {
            if (!$(this).hasClass('show_inner_loading')) {
                DASHBOARD.removeShadeWithLoading();
            }
        });


        /*Show-hide loading instead of  submit panels*/
        $("form.reporting-remote, form.loading-effect, form[loading-effect]").live('ajax:before', function () {
            $(this).find('.button_panels, .loading_panels').toggle();
        });

        $("form.reporting-remote, form.loading-effect, form[loading-effect]").live('ajax:complete', function () {
            $(this).find('.button_panels, .loading_panels').toggle();

        });


//        $j(".reverse_strip, .strip, .room_view_box").each(function () {
//            $j(this).contextMenu('#' + $j(this).attr('context-menu-id'), {theme:'gloss,gloss-semitransparent'});
//        });
//
        $j(".room_view_box").each(function () {
            $j(this).contextMenu('#' + $j(this).attr('context-menu-id'), {theme: 'gloss,gloss-semitransparent'});
        });


        $j('a.mark_cleaning').live('click', function () {
            $j('#marker_item_id').val($j(this).attr('item_id'));
            $j('#cleaning_marker_id').attr('checked', 'checked');
            $j('#marker_item_id').closest('form').submit();
        });

        $j('a.remove_cleaning').live('click', function () {
            $j('#marker_item_id').val($j(this).attr('item_id'));
            $j('#cleaning_marker_id').removeAttr('checked');
            $j('#marker_item_id').closest('form').submit();
        });

        $j('a.mark_clean').live('click', function () {
            var itemId = $j(this).attr('item_id');
            $.ajax({
                url: "/rooms/mark_as_clean",
                type: "POST",
                data: {room_id: itemId}
            });
        });

        $j('a.mark_dirty').live('click', function () {
            var itemId = $j(this).attr('item_id');
            $.ajax({
                url: "/rooms/mark_as_dirty",
                type: "POST",
                data: {room_id: itemId}
            });
        });

        $j('a.mark_maintenance').live('click', function () {
            $j('#damaged_room_room_id').val($j(this).attr('item_id'));
            $j('#damaged_lb #from_room_view').val(1);
            initialize();
            $j("#damaged_room_entry_date").attr("value", "");
            $j("#damaged_room_release_date").attr("value", "");
            $j("#damaged_room_notes_attributes_0_content").attr("value", "");
            gLightBoxes['damaged_lb'].activate();
        });

        $j('a.remove_maintenance').live('click', function () {
            editDamagedRoom($j(this).attr('reference_id'))
            initialize();
            gLightBoxes['edit_damaged_lb'].activate();
        });
        //Check in from room view
        $j('a.room_view_check_in').live('click', function () {
            initialize();
            gLightBoxes['check_in_lb'].activate();
        });


        $j('#report_view form.reporting-remote').live('submit', function (e) {
            $(this).callRemote();
            e.preventDefault();
        });

        DASHBOARD.bindItemViewFilter();

        $j('a.mark_locked').live('click', function () {
//            $j('#locked_room_room_id').val($j(this).attr('item_id'));
//            $j('#locked_lb #from_room_view').val(1);
            DASHBOARD.Room.newLockedRoom($j(this).attr('item_id'));
            initialize();
            gLightBoxes['locked_lb'].activate();
        });

        $j('a.remove_locked').live('click', function () {
            //editLockedRoom($j(this).attr('reference_id'))
            DASHBOARD.Room.editLockedRoom($j(this).attr('reference_id'));
            initialize();
            gLightBoxes['edit_locked_lb'].activate();
        });

        DASHBOARD.filterItemView();

        $j('a[enabled_lb]').click(function () {
            initialize();
            gLightBoxes[$j(this).attr('enabled_lb')].activate();
        });

        $j('.load_another_lightbox').live('click', function () {
            gLightBoxes[$j(this).attr('new_lb')].activate({'callerLB': $j(this).attr('old_lb')});
        });

//        $j('.add_guest').live('click', function () {
//            if ($j(this).nextAll('.guest_form_container:first').find('.guest_form').length > 0)
//                $j(this).nextAll('.guest_form_container:first').show('slow');
//            else
//                $j(this).nextAll('.guest_form_container:first').html($j('#guest_form_html').html()).slideDown('slow');
//            $j(this).hide();
//            $j(this).nextAll('.hide_guest_form:first').show();
//        });

//        $j('.hide_guest_form').live('click', function () {
//            $j(this).nextAll('.guest_form_container:first').hide('slow');
//            $j(this).hide();
//            $j(this).prevAll('.add_guest:first').show();
//        });

//        $j('a.show-check-in-room').live('click', function () {
//            if ($j(this).parent().parent().parent().find('.room-wrapper-part').length > 0) {
//                $j(this).parent().parent().parent().find('.room-wrapper-part').show('slow')
//            } else {
//                $j(this).parent().parent().parent().find('.room-wrapper-part').html($j("#js_check_in_form_html").html()).slideDown('slow');
//            }
//            $j(this).hide();
//            $j(this).nextAll('.hide-check-in-room:first').show();
//
//
//        });

//        $j('a.hide-check-in-room').live('click', function () {
//            $j(this).parent().parent().parent().find('.room-wrapper-part').hide('slow')
//            $j(this).hide();
//            $j(this).prevAll('.show-check-in-room:first').show();
//        });


        $j('.notification a.close').live('click', function () {
            $j(this).closest('.notification').hide("slow");
        });

//        $j('.allocated_room_row  select.room_type').live('change', function () {
//            if ($j(this).val().length > 0) {
//                var start_date = $j(this).parents('.check_in_form').find('.check_in_start_date').val();
//                var end_date = $j(this).parents('.check_in_form').find('.check_in_end_date').val();
//                var $this = $j(this);
//                DASHBOARD.Checkin.updatePrice(DASHBOARD.Checkin.referenceDomId(this));
//
//                $.ajax({
//                    url:'/check_ins/filter_rooms',
//                    data:{room_type_id:$j(this).val(), start_date:start_date, end_date:end_date},
//                    dataType:'html',
//                    type:'GET',
//                    success:function (data) {
//                        var roomField = $this.closest('.room_and_price_block').find('select.room');
//                        roomField.empty().find('option').remove().end().append(data);
//                        var seleted_room_ids = DASHBOARD.Checkin.selectedRooms();
//                        roomField.find('option').each(function (i, el) {
//                            var value = parseInt($j(el).val());
//                            if (value > 0 && $j.inArray(value, seleted_room_ids) < 0) {
//                                roomField.val(value).trigger('change');
//                                return false;
//                            }
//                        });
//                        DASHBOARD.Checkin.updateRoomContainerTitle(roomField);
//                    }
//                });
//            }
//            else {
//
//            }
//
//            DASHBOARD.updateRoomTypeCapacity(this)
//        });

        //Calculate room price
//        $j('.allocated_room_row select.room').live('change', function () {
//            //console.log("allocated_room_row select.room");
//            var $this = $(this);
//            var selected_rooms = DASHBOARD.Checkin.selectedRooms($this)
//            if ($j.inArray($this.val(), selected_rooms) >= 0) {
//                $this.val("");
//            }
//            else {
//                DASHBOARD.Checkin.updatePrice(DASHBOARD.Checkin.referenceDomId(this));
//                DASHBOARD.Checkin.updateRoomContainerTitle(this);
//
//            }
//        });

//        $j('#check_in_hotel_discount_id').live('change', function () {
//            DASHBOARD.Checkin.updatePrice(DASHBOARD.Checkin.referenceDomId(this));
//        });

        //handle night_count update on date change
//        $j('input.check_in_start_date').live('change', function () {
//            $j(this).parents('.check_in_form').find('.check_in_nights_count').trigger('change');
//        });

//        $j('input.check_in_end_date').live('change', function () {
//            var start_date_field = $j(this).parents('.check_in_form').find('.check_in_start_date');
//            var end_date_field = $j(this).parents('.check_in_form').find('.check_in_end_date');
//            var start_date = start_date_field.val();
//            var end_date = end_date_field.val();
//            var last_date = $j(this).data('last_date');
//            var cur_date = $j(this).val();
//            var nights_count = $j(this).parents('.check_in_form').find('#check_in_nights_count').val();
//            if (last_date != cur_date) {
//                $j(this).data('last_date', cur_date);
//                DASHBOARD.Checkin.checkRoomAvailabilityAndPrice(DASHBOARD.Checkin.referenceDomId(this), start_date, end_date);
//            }
//
//            $j(this).parents('.check_in_form').find('#check_in_nights_count').val(DASHBOARD.Checkin.nightCount(start_date_field, end_date_field));
//        });


//        $j('.hide_guest_lb_form').live('click', function () {
//            $j(this).closest('.guest_form').hide('fast');
//        });

        //set main guest to all dependents of a room
//        $j('.allocated_room_row input.main_guest_id_field').live('change', function () {
//            $j(this).parents('.guests_container').find('.allocated_room_dependent_row input.dependent_main_guest').val($j(this).val());
//        });

//        $j('form.guest_form_checkin').live('ajax:success', function (xhr, data, status) {
//            var $this = $j(xhr.target);
//            var response = eval(data);
//            if (response.success == "1") {
//                if (response.reference_field_id.length > 1) {
//                    var reference_field = $j('#' + response.reference_field_id);
//                    reference_field.val(response.guest_name);
//                    reference_field.parents('.rs_row').find('.form_field_to_set').val(response.guest_id);
//                    if (reference_field.attr('main_guest') == '1') {
//                        reference_field.parent().find('.dependent_main_guest').val(response.guest_id);
//                    }
//                } else {
//                    $this.parents('.guest_form_container:first').prevAll('input.allocated_room_field_guest_id').val(response.guest_id);
//                    $this.parents('.guest_form_container:first').prevAll('input.allocated_room_field_guest_name').val(response.guest_name);
//                    $this.parents('.guest_form_container:first').prevAll('.hide_guest_form').trigger('click');
//                }
//                $('#add_edit_guest_lb').dialog('close');
////                $j("form.guest_form_checkin").each(function (i, o) {
////                    $j("form.guest_form_checkin")[i].reset();
////                });
//            }
//            else {
//                $this.find('.messages').html(response.error);
//                $this.find('.notification').addClass('error').show();
//                $j(".guest_form_container .button_panels").show();
//            }
//
//        });

//        populate main guest id for each dependent
//        $j('.dependent_guest_id_field').live('change', function () {
//            var main_guest_id = $j(this).closest(".allocated_room_row").find('.main_guest_id_field').val();
//            $j(this).parent().find('.dependent_main_guest').val(main_guest_id);
//        })
//
//        $j('img.allocated_room_guests').live('click', function () {
//            DASHBOARD.activeAllocatedRoom = $j(this).parents('.allocated_room_row:first');
//            DASHBOARD.Guests.clearGuestsLb();
//            DASHBOARD.Guests.populateGuestsLb();
//
//        });

//        $j('input.guests_portion_done').live('click', function () {
//            if (DASHBOARD.activeAllocatedRoom) {
//                var guest_ids = []
//                var guest_names = []
//                var guest_relations = []
//                $j('.guests_portion_done').parents('.check_in_guests_container').find('.allocated_room_field_guest_name').each(function () {
//                    guest_names.push($j(this).val());
//                });
//
//                $j('.guests_portion_done').parents('.check_in_guests_container').find('.allocated_room_field_guest_id').each(function () {
//                    if ($j.trim($j(this).val()).length > 0)
//                        guest_ids.push($j(this).val());
//                });
//
//                $j('.guests_portion_done').parents('.check_in_guests_container').find('.allocated_room_field_guest_relation').each(function () {
//                    guest_relations.push($j(this).val());
//                });
//
//                var guest_id_relation_array = []
//                $.each(guest_ids, function (i, v) {
//                    guest_id_relation_array.push(v + '|' + guest_relations[i])
//                });
//
//                //set prime guest if not selected
//                if ($j("input.billable_id").val().length == 0) {
//                    $j("input.billable_id").val(guest_ids[0]);
//                    $j("input.billable_type").val('Guest');
//                }
//
//
//                DASHBOARD.activeAllocatedRoom.find('.guest_id_relation_array').val(guest_id_relation_array);
//                if (guest_ids.length > 0) {
//                    var guestsHtml = DASHBOARD.renderCheckinGuest(guest_ids, guest_names, guest_relations);
//                    DASHBOARD.activeAllocatedRoom.find('.guest_information .guests_container').html(guestsHtml);
//
//                }
//
//            }
//        });

//        $j('.check_in_rooms_count').live('change', function () {
//            var wrapper = $j(DASHBOARD.Checkin.referenceDomId($j(this)));
//            var existing_rows = wrapper.find('#allocated_rooms_container .allocated_room_row').length;
//            //console.log("existing_rows :: " + existing_rows);
//            var counts = parseInt($j(this).val()) || 0;
//            if (counts > 50) {
//                alert('Maximum 50 rooms allowed under one checkin!');
//                return false;
//            }
//
//            if (existing_rows > counts) {
//                if (confirm('You are going to remove ' + (existing_rows - counts) + ' room information. You will have ' + counts + ' left. Are you sure?')) {
//                    for (var j = 0; j < existing_rows - counts; j++) {
//                        $j(this).parents('.check_in_form').find('#allocated_rooms_container .allocated_room_row:last').remove();
//                    }
//                }
//                else {
//                    $j(this).parents('.check_in_form').find('input.rooms_count').val(existing_rows);
//                }
//
//            } else {
//                for (var i = 0; i < counts - existing_rows; i++) {
//                    wrapper.find('a.adMore').trigger('click');
//                }
//            }
//
//            DASHBOARD.Checkin.updateTotalPrice(wrapper);
//            DASHBOARD.adjustTotalAllocatedRoom(wrapper);
//        });

//        $j('.check_in_nights_count').live('change', function () {
//            var start_date_input = $j(this).parents('.check_in_form').find('.check_in_start_date');
//            var end_date_input = $j(this).parents('.check_in_form').find('.check_in_end_date');
//            var night_counts = parseInt($j(this).val());
//            if (DASHBOARD.Checkin.nightCount(start_date_input, end_date_input) != night_counts) {
//
//                var start_date_val = start_date_input.val().split(' ');
//                var end_date_val = end_date_input.val().split(' ');
//                var start_date_val_splits = start_date_val[0].split('-');
//
//                var t_date = new Date(start_date_val_splits[1] + "/" + start_date_val_splits[2] + "/" + start_date_val_splits[0]);
//                // new Date(start_date_val_splits[0], start_date_val_splits[1], start_date_val_splits[2]);
//                t_date.setDate(t_date.getDate() + night_counts);
//
//                var dd = t_date.getDate();
//                var mm = t_date.getMonth() + 1;
//                if (mm < 10) mm = "0" + mm;
//                var y = t_date.getFullYear();
//                var end_date = y + '-' + mm + '-' + dd + ' ' + end_date_val[1] + ' ' + end_date_val[2];
//                end_date_input.val(end_date).trigger('change');
//            }
//        });

        $j('.room_view_nights_count').live('change', function () {
            var start_date_input = $j(this).parents('.v_tab_content_item').find('#item_view_filter_start_date');
            var end_date_input = $j(this).parents('.v_tab_content_item').find('#filter_end_date');
            var night_counts = parseInt($j(this).val());
            if (DASHBOARD.Checkin.nightCount(start_date_input, end_date_input) != night_counts) {

                var start_date_val = start_date_input.val().split(' ');
                var end_date_val = end_date_input.val().split(' ');
                var start_date_val_splits = start_date_val[0].split('-');

                var t_date = new Date(start_date_val_splits[1] + "/" + start_date_val_splits[2] + "/" + start_date_val_splits[0]);
                // new Date(start_date_val_splits[0], start_date_val_splits[1], start_date_val_splits[2]);
                t_date.setDate(t_date.getDate() + night_counts);

                var dd = t_date.getDate();
                var mm = t_date.getMonth() + 1;
                if (mm < 10) mm = "0" + mm;
                var y = t_date.getFullYear();
                var end_date = y + '-' + mm + '-' + dd; // + ' ' + end_date_val[1] + ' ' + end_date_val[2];
                end_date_input.val(end_date);
                end_date_input.trigger('change');
            }
        });

//        $j('.rs_room_row .price input').live('blur', function () {
//            DASHBOARD.Checkin.updateTotalPrice(DASHBOARD.Checkin.referenceDomId($j(this)));
//        });

        $j('a.chargeroom').live('click', function () {
            $j('#search_allocated_room').val("");
            $j('#search_allocated_room_auto_complete').hide();
            $j('#service_charge_form_container').html("");
            $("#charge_room_lb").dialog({
                width: 650,
                modal: true
            })
        });

        $j('a.manage_payment').live('click', function () {
            $j('#room_service_check_in_search').val("Search CheckIn");
            $j('#add_payment_container').html("");
            $("#manage_payment_lb").dialog({
                width: 800,
                modal: true,
                position: ['middle', 20]
            })
        });

        $j('a.check_out').live('click', function () {
            $j('#search_check_out').val("");
            $j('#search_check_in_for_check_out_auto_complete').hide();
            $j('#check_out_container').html("");
            $("#check_out_lb").dialog({
                width: 650,
                modal: true
            })
        });


        $j("a.for_tab_view").live('click', function (e) {
            DASHBOARD.TabView.addTab($j(this));
            e.preventDefault();
        });


        $j('.tab_view #v_tabs a.tab').live('click', function (e) {
            // Get the tab name
            var contentname = $j(this).attr("id") + "_content";
            // hide all other tabs
            $j("#v_tab_content .v_tab_content_item").hide();
            $j(".tab_view #v_tabs li").removeClass("current");
            $j(".tab_view #v_tabs li a").removeClass("h_tab_select");
            // show current tab
            $j("#" + contentname).show();
            $j(this).parent().addClass("current");
            $j(this).addClass("h_tab_select");
            if ($j(this).hasClass('existing_tab') && $j(this).attr('load_url') && !$j(this).attr('loaded')) {
                DASHBOARD.TabView.open_existing_tab(this, contentname);
            }
            else if ($j(this).attr('url') && !$j(this).attr('loaded')) {
                var content_item_id = $j(this).id + "_content"
                DASHBOARD.TabView.load_content(this, content_item_id);
            }

            if ($j(this).hasClass('existing_tab')) {
                DASHBOARD.TabView.populate_selected_filters(this);
            }

            e.preventDefault();

        });

        //double click to refresh
        $j('.tab_view #v_tabs a.tab').live('dblclick', function (e) {
            DASHBOARD.TabView.refresh_tab($j(this).attr('id'));
            e.preventDefault();

        });


        $j('.tab_view #v_tabs span.remove').live('click', function (e) {
            // Get the tab name
            var tabid = $j(this).parent().attr("id");
            // remove tab and related content
            var contentname = tabid + "_content";
            $j("#" + contentname).remove();
            $j(this).closest('li').remove();

            // if there is no current tab and if there are still tabs left, show the first one
            if ($j(".tab_view #v_tabs li.current").length == 0 && $j(".tab_view #v_tabs li").length > 0) {
                var firsttab = $j(".tab_view #v_tabs li:last");
                firsttab.find('a').trigger("click");
                e.preventDefault();
            }

            e.stopPropagation();
        });

        $j('.tab_refresh').live('click', function (e) {
            DASHBOARD.shadeWithLoading($('body'));
            DASHBOARD.TabView.refresh_tab($j(".tab_view #v_tabs a.h_tab_select").attr('id'));

        });

        setTimeout(function () {
            // Slide
            $j('#menu1 > li > a.expanded + ul').slideToggle('medium');
            $j('#menu1 > li > a').live("click", function () {
                $j(this).toggleClass('expanded').toggleClass('collapsed').parent().find('> ul').slideToggle('medium');
            });
            $j('.treeview .expand_all').live("click", function () {
                $j('#menu1 > li > a.collapsed').addClass('expanded').removeClass('collapsed').parent().find('> ul').slideDown('medium');
            });
            $j('.treeview .collapse_all').live("click", function () {
                $j('#menu1 > li > a.expanded').addClass('collapsed').removeClass('expanded').parent().find('> ul').slideUp('medium');
            });

        }, 250);

        $j('#manage_payment_lb #print_invoice').live('click', function (e) {
            e.stopPropagation();
            var payment_ids = "";
            $j(".invoice_chk_bx").each(function () {
                var payment_id = $j(this).attr("id").split("payment_id_")[1];
                if ($j(this).is(':checked'))
                    payment_ids += ("," + payment_id);
            });
            payment_ids = payment_ids.substring(1);
            if (payment_ids.length > 0)
                window.open("/hotel_services_payments/invoice?payment_ids=" + payment_ids);
            else {
                var alert_text = $j(this).attr('nothing_to_print');
                alert(alert_text);
            }
        });


        $j('[service-charge-root] .service_charge_paid_chk').live('click', function (e) {
            e.stopPropagation();
            if ($j(this).is(':checked')) {
                $j(this).siblings(".hotel_payment_type_id").removeClass("hidden");
            } else {
                $j(this).siblings(".hotel_payment_type_id").addClass("hidden");
                $j(this).siblings(".hotel_payment_type_id").val("");
                $j(this).siblings(".pay_service_charge").addClass("hidden");
                var url = "/hotel_services_payments/cancel_service_charge_payment";
                if (confirm("Are you sure you want to refund?")) {
                    var service_charge_id = $j(this).data("service-charge-id");
                    $.post(url, {service_charge_id: service_charge_id});
                    $j(this).siblings("input[name=payment_ids]").val("");
                } else {
                    $j(this).attr('checked', true);
                }
            }
        });

        $j('[service-charge-root] .hotel_payment_type_id').live('change', function (e) {
            if ($j(this).val() != '') {
                $j(this).siblings(".pay_service_charge").removeClass("hidden");
            } else {
                $j(this).siblings(".pay_service_charge").addClass("hidden");
            }
        });

        $j('[service-charge-root] .pay_service_charge').live('click', function (e) {
            e.stopPropagation();
            var service_charge_id = $j(this).data("service-charge-id");
            var hotel_payment_type_id = $j(this).siblings(".hotel_payment_type_id").val();
            var amount = $j(this).parents(".row").find(".service_charge_amount:first").val();

            var url = "/hotel_services_payments/pay_service_charge";
            var link = $j(this);
            var check_in_id = $j(this).data("check-in-id");
            link.html("Paying...");
            $.post(url,
                {service_charge_id: service_charge_id,
                    payment_type: hotel_payment_type_id,
                    amount: amount,
                    note: "Paid for services"}, function (data) {
                    if (data.errors) {
                        link.html("");
                        Message.showError(data.errors, $j("#charge_room_lb .qv_row_full_content"));
                    }
                    else {
                        link.html("Pay").addClass("hidden");
                        link.siblings(".hotel_payment_type_id").addClass("hidden");
                        link.siblings(".hotel_payment_type_id").val("");
                        link.siblings("input[name=payment_ids]").val(data.id);
                        $j("body").trigger('checkin_updated', [check_in_id, 2]);
                    }

                }
            );
        });

//        DASHBOARD.addMoreGuest();
//        DASHBOARD.addMoreDependent();
//        DASHBOARD.Checkin.enableGuestAutocomplete();

        $j('.add_edit_guest').live('click', function () {
//            gLightBoxes['add_edit_guest_lb'].activate();
            $j("#add_edit_guest_lb").dialog({
                width: 750,
                modal: true
            })
            var url = '';
            var data = {lan: Teks.ChooseLanguage.current_lan, target_dom_id: 'add_edit_guest_container', reference_field_id: $j(this).attr('reference_field_id')};
            var selected_guest_id = $j(this).parents('.rs_row').find('.form_field_to_set').val();
            if (parseInt(selected_guest_id) > 0) {
                url = "/guests/" + selected_guest_id + "/edit"
            }
            else {
                url = "/guests/new"
            }
            DASHBOARD.shadeWithLoading($j('#add_edit_guest_lb'));

            $.ajax({
                url: url,
                data: data,
                dataType: 'script',
                type: 'GET'
            }).done(function () {
                    DASHBOARD.removeShadeWithLoading();
                });
        });

        $j('.add_edit_task').live('click', function (e) {
            e.stopPropagation();
            var url = $j(this).data('url');
            $j('#add_new_task_lb').dialog({
                width: 750,
                modal: true,
                position: ['middle', 20]
            });

            $j('#add_new_task_lb').find('.task_flash_messages').html('');
            DASHBOARD.shadeWithLoading($j('#add_new_task_lb_container'));
            $.ajax({
                url: url,
                dataType: 'script',
                type: 'GET'
            });
            return false;
        });

        $j('.add_edit_group').live('click', function () {
            var field_value = $j('input.auto_group_name').val().length
            DASHBOARD.showInnerLoading('add_edit_group_lb');
            $j("#add_edit_group_lb").dialog({
                width: 675,
                modal: true
            })
//            gLightBoxes['add_edit_group_lb'].activate();
            var url = '';
            var data = {lan: Teks.ChooseLanguage.current_lan, target_dom_id: 'add_edit_group_container',
                unique_tab_id: $j(this).attr('unique_tab_id')};

            var billable_id = $j('#' + $j(this).attr('unique_tab_id')).find('#check_in_billable_id').val();
            var billable_type = $j('#' + $j(this).attr('unique_tab_id')).find('#check_in_billable_type').val();
            var selected_group_id = billable_type == 'Group' ? billable_id : '';
            if (parseInt(selected_group_id) > 0 && field_value > 0) {
                url = "/groups/" + selected_group_id + "/edit"
            }
            else {
                url = "/groups/new"
            }

            $.ajax({
                url: url,
                data: data,
                dataType: 'script',
                type: 'GET'
            }).done(function () {
                    DASHBOARD.removeShadeWithLoading();
                });
        });


        $j('.check_in_search_results li.check_in_search_item').live('click', function () {
            var url = '';
            var target_dom_id = $j(this).parents('.v_tab_content_item').attr('id');
            var data = {lan: Teks.ChooseLanguage.current_lan, target_dom_id: target_dom_id};
            var selected_check_in_id = $j(this).attr('check_in_id');
            if (parseInt(selected_check_in_id) > 0) {
                url = "/check_ins/" + selected_check_in_id + "/edit";
                $.ajax({
                    url: url,
                    data: data,
                    dataType: 'script',
                    type: 'GET'
                });
                $j(this).parents('.check_in_search_results').hide();
                DASHBOARD.shadeWithLoading($j(this).closest('.reservation_wrapper'));
            }
            else {

            }

        });

//        $j('.billing_information_billable_type').live('change', function () {
//            DASHBOARD.Checkin.handleBillableTypeOptions($j(this));
//        });

//        $j('#billable_type_str').live('change', function () {
//            var wrapper = $j(DASHBOARD.Checkin.referenceDomId($j(this)))
//            wrapper.find("#check_in_group_id").val('');
//            wrapper.find(".auto_group_name").val('');
//        });

        $j('body').on('checkin_updated', function (e, check_in_id, tab_id) {
            DASHBOARD.Checkin.reloadToolTipFor(check_in_id);
            DASHBOARD.Checkin.reloadCheckInTab(check_in_id, tab_id);
        });

        //close context menu upon click on menu item
        //$j('.context-menu-item a[data-remote]').live('click', function () {
        $j('.context-menu-item a').live('click', function () {
            $j(this).closest('.context-menu').hide().next('.context-menu-shadow').hide();
        });

        $j('.context-menu-item a.check_in').live('click', function (e) {
            var startDate = $j('#item_view_filter_start_date').val();
            var endDate = $j('#filter_end_date').val();
            var clonedLink = $j(this).clone();
            var url = clonedLink.attr('url') + '&start_date=' + startDate + '&end_date=' + endDate;
            clonedLink.attr('url', url);
            DASHBOARD.TabView.addTab(clonedLink, true);
            e.preventDefault();
        });

        //seome helper method to support check and uncheck all
        $j('a.check_all[data-checkbox-css]').live('click', function () {
            var css_class = $j(this).data('checkbox-css');
            $j('.' + css_class).attr('checked', true);
        });

        $j('a.uncheck_all[data-checkbox-css]').live('click', function () {
            var css_class = $j(this).data('checkbox-css');
            $j('.' + css_class).attr('checked', false);
        });

        $j('input[data-checkbox-toggle-all]').live('click', function () {
            var target_attr = $j(this).data('checkbox-toggle-all');
            $j('[toggle_check_attr="' + target_attr + '"]').attr('checked', $j(this).attr('checked') || false);
        });

        //adding the clear auto complete result binding
        $j('a[clear-input-values]').live('click', function (e) {
            var $this = $j(this);
            var wrapper = $this.parents($this.attr('parent-wrapper'));
            wrapper.find($this.attr('clear-selectors')).each(function () {
                $j(this).val('');
            });
        });

        $('#payment_dashboard_check_in_search').live('item_selected', function (e, check_in) {
            if (check_in.id) {
                $j.ajax({
                    type: "GET",
                    url: "/folios",
                    data: { check_in_id: check_in.id }
                });
            }
        });

        $('#room_service_check_in_search').live('item_selected', function (e, check_in) {
            if (check_in.id) {
                $j.ajax({
                    type: "POST",
                    url: "hotel_services_payments/add_payment",
                    data: { check_in_id: check_in.id, previous_page: 'room_service_dashboard' }
                });
            }
        });

    });

}
    (jQuery)
)
;


var DASHBOARD = {
    StayView: {
        updateRoomStats: function () {
            $j('.stay_view_content_wrapper span[item_type_daily_stat]').each(function () {
                var available_room_count = $j('.' + $j(this).attr('item_type_daily_stat') + '.' + 'ds_available').size();
                $j(this).text(available_room_count);
            })
        }
    },
    TabView: {
        setTabTitle: function (tab_id, title) {
            $j('#' + tab_id).html(title).append('<span class="remove">x</span>')
        },

        addTab: function (link, timestamp) {
            var tab_id = '';
            var loading_contain_id = '';
            if (typeof timestamp == "undefined") {
                tab_id = $j(link).attr("rel");
                loading_contain_id = $j(link).attr("loading_container_id");
            }
            else {
                tab_id = $j(link).attr("rel") + (new Date().getTime());
                var param_to_modify = 'target_dom_id=' + tab_id + "_content";
                $j(link).attr('url', $j(link).attr('url').replace(/target_dom_id=(\d|_|\w)*/, param_to_modify));
                loading_contain_id = tab_id + "_content"
            }

            var content_item_id = tab_id + "_content"
            // If tab already exist in the list, return
            if ($j("#" + tab_id).length != 0) {
                $j(".tab_view #v_tabs li").removeClass("current");
                $j(".tab_view #v_tabs li a").removeClass("h_tab_select");
                $j("#v_tab_content .v_tab_content_item").hide();
                DASHBOARD.TabView.showTab(tab_id);
                $j("#" + tab_id + "_content").show();
                if (!$j(link).attr('loaded')) {
                    DASHBOARD.TabView.load_content(link, content_item_id);
                }

                return;
            }
            // hide other tabs
            $j(".tab_view #v_tabs li").removeClass("current");
            $j(".tab_view #v_tabs li a").removeClass("h_tab_select");
            $j("#v_tab_content .v_tab_content_item").hide();

            // add new tab and related content
            $j(".tab_view #v_tabs").append("<li class='current'><a class='tab h_tab_select' id='" +
                tab_id + "' href='javascript:void(0)' loaded='true' loading_container_id='" + loading_contain_id + "' title='" + $j(link).attr('title') + "' url='" + $j(link).attr('url') + "' >" + $j(link).attr('title') +
                "<span class='remove'>x</span></a></li>");


            $j("#v_tab_content").append("<div class='v_tab_content_item' id='" + content_item_id + "'></div>");
            DASHBOARD.TabView.load_content(link, content_item_id);
            // set the newly added tab as current
            $j("#" + tab_id + "_content").show();
        },

        showTab: function (tab_id) {
            var tab_obj = $j('#v_tabs #' + tab_id);
            var contentname = tab_obj.attr("id") + "_content";
            tab_obj.parent().addClass("current");
            tab_obj.addClass("h_tab_select");

        },

        load_content: function (link, target_id) {
            link = $j(link);
            DASHBOARD.showInnerLoading(target_id);
            if (link.attr('url')) {
                link.attr('href', link.attr('url'));

                var remove = link.attr('manual-remove-loading');
                if (remove == undefined || !remove == 'true') {
                    link.off('ajax:complete');
                    link.on('ajax:complete', function () {
                        DASHBOARD.removeShadeWithLoading();
                    });
                }

                link.callRemote();
                link.attr('loaded', true);
            }

        },

        refresh_tab: function (tab_id) {
            var target = $j('a#' + tab_id);
            target.removeAttr('loaded').trigger('click');
        },

        open_existing_tab: function (o, contentname) {
            if (!contentname)
                contentname = $j(o).attr("id") + "_content";
            $j(o).attr('href', $j(o).attr('load_url'));
            $j(o).attr('loading-effect', true);
            $j(o).attr('loading_container_id', contentname);
            $j(o).attr('loaded', true);
            $j(o).callRemote();
        },
        populate_selected_filters: function (o) {
            var $this = $j(o);
            var stay_view_start_date = $j('#stay_view_filter_date');
            var item_view_start_date = $j('#item_view_filter_start_date');
            if ($this.attr('id') == 'stay_view') {
                try {
                    $j('.stay_view_marker_filters input:checked').prop("checked", false).trigger('change');
                }
                catch (err) {
                }

                $j.each(DASHBOARD.itemViewClasses, function (index, css_class) {
                    if (css_class.indexOf("custom_marker") == 0) {
                        try {
                            $j('.stay_view_marker_filters input[filter=' + css_class + ']').prop("checked", true).trigger('change');
                        }
                        catch (err) {
                            // console.log(css_class);
                        }
                    }
                    else if (css_class.indexOf("item_type_filter_") == 0) {
                        $j('#stay_view_content select#item_type_id').val(css_class.split('item_type_filter_')[1]).trigger('change');
                    }
                });
                if (stay_view_start_date.val() != item_view_start_date.val())
                    stay_view_start_date.val(item_view_start_date.val()).trigger('change');
            }
            else if ($this.attr('id') == 'item_view') {
                try {
                    $j('.item_view_marker_filters input:checked').prop("checked", false).trigger('change');
                }
                catch (err) {
                }

                $j.each(DASHBOARD.stayViewClasses, function (index, css_class) {
                    if (css_class.indexOf("custom_marker") == 0) {
                        try {
                            $j('.item_view_marker_filters input[filter=' + css_class + ']').prop("checked", true).trigger('change');
                        }
                        catch (err) {
                            console.log(css_class);
                        }
                    }
                    else if (css_class.indexOf("item_type_filter_") == 0) {
                        $j('#item_view_content select#item_type_id').val(css_class.split('item_type_filter_')[1]).trigger('change');
                    }

                });

//                if (stay_view_start_date.val() != item_view_start_date.val()) {
//                    item_view_start_date.val(stay_view_start_date.val()).trigger('change');
//                    //$j('.room_view_nights_count').trigger('change');
//                }
            }
        }

    },

    hideAutocompleteResults: function (e, u) {
        var search_field = $j(e);
        if (search_field.val().length > 0) {
            $j(u).hide();
        }
    },

    Dhtml: {


    },

    Room: {
        editLockedRoom: function (lockedID) {
            var date = $j("#item_view_filter_start_date").val();
            $j.ajax({
                method: 'get',
                dataType: 'script',
                url: '/locked_rooms/edit?id=' + lockedID + '&filter_date=' + date + '&from_room_view=1'
            });
        },
        newLockedRoom: function (roomId) {
            var date = $j("#item_view_filter_start_date").val();
            var data = {lan: Teks.ChooseLanguage.current_lan, filter_date: date, from_room_view: 1, room_id: roomId}
            $j.ajax({
                method: 'get',
                dataType: 'script',
                url: '/locked_rooms/new',
                data: data
            });
        }
    },

    Checkin: {
        reloadRoomViewBlocks: function (check_in_id) {
            $j.ajax({
                method: 'get',
                dataType: 'script',
                url: '/front_desk/load_room_blocks',
                data: {check_in_id: check_in_id}
            });
            $j('a#stay_view').removeAttr('loaded');
        },

//        selectedRooms:function (current_obj) {
//            var otherRooms = jQuery.grep($j('.allocated_room_row select.room '),
//                function (value) {
//                    if (current_obj)
//                        return $j(value).get(0) !== current_obj.get(0);
//                    else
//                        return true;
//
//                });
//            var selected_rooms = $j.map(otherRooms, function (e) {
//                return parseInt($j(e).val()) || '';
//            });
//            return selected_rooms;
//        },

        reloadToolTipFor: function (check_in_id) {
            $j('div.item_view_block div.checked_in[data-check-in-id="' + check_in_id + '"]').each(function () {
                var api = $j(this).qtip();
                api.set('content.ajax.url', api.options.content.ajax.url);
            });
        },

        reloadCheckInTab: function (check_in_id, tab_id) {
            if ($j("#fdt_checkin_" + check_in_id).length > 0) {
                var url = "/check_ins/" + check_in_id + "/edit?lan=en&target_dom_id=fdt_checkin_" + check_in_id + "_content";
                $j.ajax({
                    url: url,
                    dataType: 'script',
                    type: 'GET'
                }).done(function () {
                        if (tab_id) $j('#check_in_tab_' + check_in_id).tabs('select', tab_id);
                    });
            }
        },

        nightCount: function (start_date, end_date) {
            var start_date = start_date ? start_date : $j('input.check_in_start_date');
            var end_date = end_date ? end_date : $j('input.check_in_end_date');
            try {
                var start_date = Date.parse(start_date.val().split(' ')[0].replaceAll('-', '/'));
                var end_date = Date.parse(end_date.val().split(' ')[0].replaceAll('-', '/'));
                var ONE_DAY = 1000 * 60 * 60 * 24;
                return Math.round((end_date - start_date) / ONE_DAY)
            }
            catch (err) {
                return 0;
            }
        },

        checkInAddOnsAddChosen: function (wrapper) {
            wrapper = $j(wrapper);
            wrapper.find('.checkbox_for_textarea input[type=checkbox]').live('click', function () {
                $j(this).parent().next('span.hidden').toggle();
            });
            wrapper.find('.check_in_add_ons textarea').on('focus', function () {
                $j(this).addClass('large');
            });
            wrapper.find('.check_in_add_ons textarea').on('focusout', function () {
                $j(this).removeClass('large');
            });

            wrapper.find(".multiselect").chosen();
            wrapper.find(".multiselect").chosen().change(function () {
                var $this = $j(this);
                var seletedLabels = $j.map($j(this).next().find('ul.chzn-choices li span'), function (el) {
                    return $j(el).html();
                })
                $j(this).parent().find('input.check_in_service_value').val(seletedLabels.join(','));
                var selected_service_ids = $j(this).val() ? $j(this).val() : [];
                $j(this).parent().find('input.check_in_service_option_ids').val(selected_service_ids.join(','));
                if ($j(this).hasClass('price_from_option')) {
                    var prices = $j.map(selected_service_ids, function (ssid) {
                        return $this.find('option[value="' + ssid + '"]').attr('price');
                    });

                    var sum = 0;
                    $j.each(prices, function () {
                        sum += parseFloat(this) || 0;
                    });
                    $this.parent().find('.price_block input.price').val(sum);
                }

            });
            wrapper.find(".allocated_room_more_fields_chosen").chosen();
            wrapper.find(".allocated_room_more_fields_chosen").chosen().change(function () {
                var selected_service_ids = $j(this).val() ? $j(this).val() : [];
                var viewable_classes = $j.map(selected_service_ids, function (val, i) {
                    return ".repeatable_service_" + val;
                });
                $j(this).closest('.more_options_scroll').find('.repeatable_service').hide().removeClass('alt_2');
                $j(this).closest('.more_options_scroll').find(viewable_classes.join(', ')).show();
                $j.each($j(this).closest('.more_options_scroll').find(viewable_classes.join(', ')), function (i, div) {
                    if (i % 2 == 0)
                        $j(div).addClass('alt_2');
                });
                // console.log(viewable_classes);
            });


            wrapper.find('.check_in_add_ons input[type=checkbox]').live('change', function () {
                var price = 0;
                if ($j(this).is(':checked'))
                    price = parseFloat($j(this).attr('unit_price'));
                $j(this).next('.price_block').find('input.price').val(price);
            });
            wrapper.find('.check_in_add_ons select.price_from_option').live('change', function () {
                $j(this).next('.price_block').find('input.price').val(parseFloat($j(this).find('option:selected').attr('price')));
            });

            wrapper.find('.check_in_add_ons input.price_multipliable, .check_in_add_ons select.price_multipliable').live('change', function () {
                var unit_price = parseFloat($j(this).attr('unit_price'));
                var field_value = parseFloat($j(this).is('input:text') ? $j(this).val() : $j(this).find('option:selected').text());
                if (unit_price > 0 && field_value >= 0) {
                    $j(this).next('.price_block').find('input.price').val(unit_price * field_value);
                }
            });

            //copy options to all
            wrapper.find('input.for_populating_all_days').live('click', function () {
                if ($j(this).is(':checked')) {
                    $j(this).parents('.repeatable_service').find('.repeatable').each(function () {
                        if ($j(this).is(':checkbox') && $j(this).is(':checked')) {
                            $j("input[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").not(".repeatable").attr('checked', true)
                        }

                        else if ($j(this).is('input') && $j(this).val().length > 0) {
                            $j("input[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").not(".repeatable").val($j(this).val());
                        }

                        else if ($j(this).is('textarea') && $j(this).val().length > 0) {
                            $j("textarea[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").not(".repeatable").val($j(this).val());
                        }
                        else if ($j(this).is('select[multiple]')) {
                            var val_to_replace = $j(this).val();
                            $j("select[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").not(".repeatable").val(val_to_replace).trigger("liszt:updated");
                        }
                        else if ($j(this).is('select') && $j(this).val().length > 0) {
                            var val_to_replace = $j(this).val() + "";
                            $j("select[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").not(".repeatable").val(val_to_replace);
                        }

                    })
                }
            });

            //clear options for all
            wrapper.find('input.for_clearing_all_days').live('click', function () {
                if ($j(this).is(':checked')) {
                    $j('.' + $j(this).attr('parent_selector') + ' .repeatable').each(function () {
                        if ($j(this).is(':checkbox')) {
                            $j("input[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").attr('checked', false)
                        }
                        else {
                            $j("[repeatable_field_attr=" + $j(this).attr('repeatable_field_attr') + "]").val('');
                        }
                    })
                }
            });
        }

    },
    Tasks: {
        initializeChozenSelectors: function (elementSelector) {
            var wrapper = $j(elementSelector);
            wrapper.find('.chzn-select.select_department_user').chosen({no_results_text: "No results matched", allow_single_deselect: true});
            wrapper.find('.chzn-select.select_room').chosen({no_results_text: "No results matched", allow_single_deselect: true}).change(function (e) {
                var roomId = $j(this).val();
                $j(this).trigger('room_changed', roomId);
            });
            wrapper.find('.chzn-select.select_department').chosen({no_results_text: "No results matched", allow_single_deselect: true}).change(function (e) {
                var departmentId = $j(this).val();

                if (departmentId != null) {
                    $j.ajax({
                        url: '/branch_panel/departments/department_users',
                        data: {department_id: departmentId}
                    });
                } else {
                    wrapper.find('#task_assigned_user_id').html('');
                }
                wrapper.find('.chzn-select.select_department_user').trigger("liszt:updated");
                e.preventDefault();
            });
        },
        reloadTask: function (task_id, context, existing_task) {
            $j.ajax({
                url: "/tasks/" + task_id + "/reload",
                data: {context: context, existing_task: existing_task}
            }).done(function () {
                    DASHBOARD.removeShadeWithLoading();
                });
        },
        reloadFullTask: function (context) {
            $j.ajax({
                url: "/room_service",
                data: {context: context}
            }).done(function () {
                    DASHBOARD.removeShadeWithLoading();
                });
        },
        reloadAllTask: function (context) {
            $j.ajax({
                url: "/tasks/created",
                data: {context: context}
            }).done(function () {
                    DASHBOARD.removeShadeWithLoading();
                });
        }
    },
    Guests: {
        clearGuestsLb: function () {
            $j('#checkin_guests_lb #main_guest_name').val('');
            $j('#checkin_guests_lb #selected_main_guest_id').val('');
            $j('#checkin_guests_lb #mark_as_prime').attr('checked', false);
            $j('#checkin_guests_lb #dependent_guests').html('');
        },

        populateGuestsLb: function () {
            var guest_ids = [], guest_names = [], guest_relations = [];
            var main_guest_id = '', main_guest_name = ''

            DASHBOARD.activeAllocatedRoom.find('.guests_container span').each(function (index, o) {
                if (main_guest_id == '' && ($j(o).attr('relation').length == 0 || $j(o).attr('relation') == 'nil')) {
                    main_guest_id = $j(o).attr('guest_id');
                    main_guest_name = $j(o).attr('name');
                }
                else {
                    guest_ids.push($j(o).attr('guest_id'));
                    guest_names.push($j(o).attr('name'));
                    guest_relations.push($j(o).attr('relation'));
                }
            });

            if (main_guest_id.length > 0) {
                $j('#checkin_guests_lb #main_guest_name').val(main_guest_name);
                $j('#checkin_guests_lb #selected_main_guest_id').val(main_guest_id);

                if ($j("input.billable_type").val() == 'Guest' && $j("input.billable_id").val() == main_guest_id)
                    $j('#checkin_guests_lb #mark_as_prime').attr('checked', true);
            }

            if (guest_ids.length > 0) {
                for (var i = 0; i < guest_ids.length; i++)
                    $j('#checkin_guests_lb .addMoreDependent').trigger('click');
            }

            $j('#dependent_guests .dependent_guest').each(function (index, o) {
                $j(o).find('.allocated_room_field_guest_name').val(guest_names[index]);
                $j(o).find('.allocated_room_field_guest_relation').val(guest_relations[index]);
                $j(o).find('.allocated_room_field_guest_id').val(guest_ids[index]);
            });


        }
    },
//    renderCheckinGuest:function (guest_ids, guest_names, guest_relations) {
//        var dhtml = "";
//        $j.each(guest_ids, function (i, guest_id) {
//            var guest_name = guest_relations[i] == 'nil' ? '<b>' + guest_names[i] + '</b>' : guest_names[i];
//
//            dhtml += '<span guest_id="' + guest_id + '" relation="' + guest_relations[i] + '" name="' + guest_names[i] + '">' + guest_name + '</span>';
//        });
//        return dhtml;
//    },

//    updateRoomTypeCapacity:function (obj) {
//        var capacityObj = $j('.room_type_capacity_map #capacity_of_' + $j(obj).val());
//        $j(obj).parent().nextAll('.adults_children').find('input.adults').val(capacityObj.attr('adults'));
//        $j(obj).parent().nextAll('.adults_children').find('input.children').val(capacityObj.attr('children'));
//    },

    showInnerLoading: function (dom_id) {
        var obj = $j('#' + dom_id);
        obj.css({position: 'relative'});
        $j('<div class="inner_loading_div">&nbsp;</div>')
            .appendTo(obj)
            .css(
            {
                top: 0,
                left: 0,
                width: obj.width(),
                height: obj.height(),
                position: 'absolute',
                opacity: 0.2
            }
        );
    },

    shadeWithLoading: function (element) {
        $j('<div class="loading_div">&nbsp;</div>').appendTo(element).css('top', 0).css('left', 0);
    },

    removeShadeWithLoading: function () {
        $j('div.loading_div').remove();
        $j('div.inner_loading_div').remove();
    },

    bindContextMenu: function (room_id) {
        var root = typeof room_id !== 'undefined' ? ('#room_view_' + room_id) : 'body';
        $j(root).find(".room_view_box, .have_context_menu, div.strip, div.reverse_strip, .folio_item_box").each(function () {
            $j(this).contextMenu('#' + $j(this).attr('context-menu-id'), {theme: 'gloss,gloss-semitransparent'});
        });
    },

    itemViewClasses: [],
    stayViewClasses: [],
    itemViewStatuses: [],
    activeAllocatedRoom: {
    },

    bindItemViewFilter: function () {
        $j("#item_view #item_type_container select").live('change', function () {
            var cleanItemViewClasses = jQuery.grep(DASHBOARD.itemViewClasses, function (value) {
                return value.indexOf('item_type_filter_') == -1;
            });
            if ($j(this).val().length > 0) {
                cleanItemViewClasses.push('item_type_filter_' + $j(this).val());
            }
            DASHBOARD.itemViewClasses = cleanItemViewClasses;
            DASHBOARD.filterItemView();
        });

        $j(".m_room_type select").live('change', function () {
            var cleanItemViewClasses = [];
            var $this = $j(this);
            $j(this).removeClass();
            var cleanItemViewClasses = jQuery.grep(DASHBOARD.itemViewClasses, function (value) {
                return $j.inArray(value, DASHBOARD.itemViewStatuses) == -1;
            });
            DASHBOARD.itemViewClasses = cleanItemViewClasses;

            if ($j(this).val().length > 0) {
                DASHBOARD.itemViewClasses.push($j(this).val());
                $j(this).addClass(" with_status " + $j(this).val());
            }

            DASHBOARD.filterItemView();
        });


        $j("#item_view .select_rm_type input[type=checkbox]").live('change', function () {
            var cleanItemViewClasses = [];
            var $this = $j(this);
            if ($j(this).is(':checked')) {
                DASHBOARD.itemViewClasses.push($j(this).attr('filter'));
            }
            else {
                var cleanItemViewClasses = jQuery.grep(DASHBOARD.itemViewClasses, function (value) {
                    return value != $this.attr('filter');
                });
                DASHBOARD.itemViewClasses = cleanItemViewClasses;
            }

            DASHBOARD.clearMultipleSelection();
            DASHBOARD.filterItemView();
        });

        $j("#stay_view_content input[type=checkbox]").live('change', function () {
            var cleanStayViewClasses = [];
            var $this = $j(this);
            if ($j(this).is(':checked')) {
                DASHBOARD.stayViewClasses.push($j(this).attr('filter'));
            }
            else {
                cleanStayViewClasses = jQuery.grep(DASHBOARD.stayViewClasses, function (value) {
                    return value != $this.attr('filter');
                });
                DASHBOARD.stayViewClasses = cleanStayViewClasses;
            }

            DASHBOARD.filterStayView();
        });

        $j("#stay_view_content #item_type_container select").live('change', function () {
            var cleanStayViewClasses = jQuery.grep(DASHBOARD.stayViewClasses, function (value) {
                return value.indexOf('item_type_filter_') == -1;
            });
            if ($j(this).val().length > 0) {
                cleanStayViewClasses.push('item_type_filter_' + $j(this).val());
            }
            DASHBOARD.stayViewClasses = cleanStayViewClasses;
            DASHBOARD.filterStayView();
        });
    },

    filterItemView: function () {
        if (DASHBOARD.itemViewClasses.length > 0) {

            DASHBOARD.itemViewClasses = $j.unique(DASHBOARD.itemViewClasses);
            $j('.item_view_block').hide();
            var display_classes = '.' + DASHBOARD.itemViewClasses.join('.');
            $j(display_classes).show();
            //DASHBOARD.updateStatusCount('.item_view_block' + display_classes)
        }
        else {
            $j('.item_view_block').show();
            //DASHBOARD.updateStatusCount('.item_view_block')
        }
    },


    filterStayView: function () {

        if (DASHBOARD.stayViewClasses.length > 0) {
            DASHBOARD.stayViewClasses = $j.unique(DASHBOARD.stayViewClasses);

            $j('.room_strip_row').hide();
            var display_classes = '.' + DASHBOARD.stayViewClasses.join('.');
            $j(display_classes).show();
            DASHBOARD.updateItemCount('.room_strip_row' + display_classes)
        }
        else {
            $j('.room_strip_row').show();
            DASHBOARD.updateItemCount('.room_strip_row')
        }
    },

    initToolTipAtItemView: function (tool_tip_title, room_id) {
        var root = typeof room_id !== 'undefined' ? ('#room_view_' + room_id) : 'div.item_view_block';
        $j(root + " div.checked_in").each(function () {
            $j(this).qtip({
                content: {
                    text: 'Loading...',
                    title: {
                        text: tool_tip_title,
                        button: true
                    },
                    ajax: {
                        url: '/check_ins/get_info_by_room_id/' + $j(this).parent().attr("id") + '?check_in_id=' + $j(this).data("check-in-id"),
                        type: 'GET',
                        data: {},
                        success: function (data, status) {
                            this.set('content.text', data);
                        }
                    }
                },
                show: {
                    event: 'mouseenter',
                    solo: true
                },
                position: {
                    at: 'top center',
                    my: 'bottom center'
                },

                style: {
                    classes: 'ui-tooltip-shadow ui-tooltip-light'
                },

                hide: 'mouseleave'
            });
        });

    },

    updateStatusCount: function (finder_string) {
        $j.each(DASHBOARD.itemViewStatuses, function (index, status) {
            if (status == 'confirmed')
                $j('.identy_col_row_other #' + status + '_count').html($j(finder_string + '.custom_marker_confirmed').length);
            else
                $j('.identy_col_row_other #' + status + '_count').html($j(finder_string + '.' + status).length);
        });

    },

    updateItemCount: function (finder_string) {
//        $j.each(DASHBOARD.itemViewStatuses, function (index, status) {
//            $j('.identy_col_row_other #' + status + '_count').html($j(finder_string + '.' + status).length);
//        });

    },

//    addMoreAllocatedRoom:function (tab_id, clickableSelector, source, placeHolder, removeSelector, itemClass) {
//
//        $j(clickableSelector).click(function () {
//
////            if (!parseInt($j('#check_in_items_count').val()) || parseInt($j('#check_in_items_count').val()) <= $j('#allocated_rooms_container .allocated_room_row').length) {
////                alert('Number of room field is not filled up properly');
////                return false;
////            }
//            var new_object_id = new Date().getTime();
//            var wrapper = $j(this).parents('.check_in_tab_wrapper');
//            var html = $j(wrapper.find(source).html().replace(/index_to_replace_with_js/g, new_object_id));
//            html.appendTo(wrapper.find(placeHolder)).slideDown("slow");
//            DASHBOARD.adjustTotalAllocatedRoom(DASHBOARD.Checkin.referenceDomId(this));
//            //DASHBOARD.removeDHTMLItem(removeSelector, itemClass);
//
//        });
//
//        DASHBOARD.removeDHTMLItem(removeSelector, itemClass, function () {
//            DASHBOARD.adjustTotalAllocatedRoom(tab_id);
//        });
//        $j(clickableSelector).removeClass('addMoreAllocatedRoom');
//    },
//

    addMoreItem: function (clickableSelector, source, placeHolder, removeSelector, itemClass, callbacks) {
        $j(clickableSelector).live('click', function () {
            var new_object_id = new Date().getTime();
            var html = $j($j(source).html().replace(/index_to_replace_with_js/g, new_object_id));

            html.appendTo($j(placeHolder)).slideDown("slow");

            if (callbacks)
                eval(callbacks);
            DASHBOARD.removeDHTMLItem(removeSelector, itemClass);

        });

        DASHBOARD.removeDHTMLItem(removeSelector, itemClass, callbacks);
    },

    addMorePhoto: function (container, allowed_images, callbacks) {
        var allowed_images = parseInt(allowed_images);
        $j('a.addMoreImage').click(function () {
            if (allowed_images && ($j('#' + container + ' span.removeImageField').length + $j('a.del_photo').length) >= allowed_images) {
                alert('Maximum allowed ' + allowed_images + "  Featured package can have more photos");
                return false;
            }
            else {
                var new_object_id = new Date().getTime();
                var html = $j($j('#new_photo_html').html().replace(/index_to_replace_with_js/g, new_object_id)).hide();
                html.appendTo($j('#' + container)).slideDown('slow');
            }
            if (callbacks)
                eval(callbacks);

            DASHBOARD.removeDHTMLItem('span.removeImageField', 'photo');
        });
    },

//    addMoreGuest:function () {
//        var new_object_id;
//        $j('a.addMoreGuest').live('click', function () {
//            new_object_id = new Date().getTime();
//            var html = $j($j(this).closest(".allocated_room_row").find('.new_allocated_room_guest_html').html().replace(/index_to_replace_with_js/g, new_object_id));
//
//            html.appendTo($j(this).closest(".allocatdased_room_row").find('.guests_container')).slideDown("slow");
//
////            if (callbacks)
////                eval(callbacks);
////            DASHBOARD.removeDHTMLItem(removeSelector, itemClass);
////
////            if (enable_auto_fill) {
////                DASHBOARD.enableGuestDependentAutoFill(new_object_id);
////
////            }
//
//        });
//
//        DASHBOARD.removeDHTMLItem('span.removeAllocatedRoomGuest', 'allocated_room_guest_row');
//    },
//    addMoreDependent:function () {
//        var new_object_id;
//        $j('a.addMoreDependent').live('click', function (e) {
//            new_object_id = new Date().getTime();
//            var html = $j($j(this).closest(".allocated_room_row").find('.new_allocated_room_dependent_html:first').html().replace(/index_to_replace_with_js/g, new_object_id));
//            html.appendTo($j(this).closest('.guests_container')).slideDown("slow");
//        });
//
//        DASHBOARD.removeDHTMLItem('span.removeAllocatedRoomDependent', 'allocated_room_dependent_row');
//    },

    removeDHTMLItem: function (selector, itemClass, callbacks) {
        $j(selector).live('click', function () {
            $j(this).parents('.' + itemClass).remove().slideUp('slow');
            if (callbacks)
                callbacks();
        });
    },

//    adjustAllocatedRoomSerial:function () {
//        $j('#allocated_rooms_container .allocated_room_row').each(function (index, o) {
//            $j(o).find('span.sl').html(index + 1);
//        });
//    },

//    adjustTotalAllocatedRoom:function (reference) {
//        var total_room = $j(reference).find('#allocated_rooms_container .allocated_room_row').size();
//        $j(reference).find('.total_check_in_room').html(total_room);
//        $j(reference).find('#check_in_rooms_count').val(total_room);
////        DASHBOARD.Checkin.updateTotalPrice(reference);
//    },


//    enableGuestDependentAutoFill:function (newId) {
//        var form_token = $j("#" + newId + "_form_token").val();
//        var current_lan = $j("#" + newId + "_current_lan").val();
//        var main_guest_id = $j("#selected_main_guest_id").val();
//        $j(".check_in_main_guest").val(newId);
//        new Ajax.Autocompleter(newId + '_first_name',
//            newId + '_auto_div',
//            '/guests/auto_complete_for_guest?authenticity_token=' +
//                encodeURIComponent(form_token) +
//                '&lan=' + current_lan + '&cur_element_id=' + newId + '&main_guest_id=' + main_guest_id,
//            {indicator:newId + '_where_spin', afterUpdateElement:DASHBOARD.setDependentGuestData});
//    },
//
//    setDependentGuestData:function (text, li) {
//        var ids = li.id.split('_');
//        $j("#" + ids[2] + "_selected_dependent_id").val(ids[0]);
//    },

    resetItemsOnContextMenu: function () {
        var isOccupied = false;
        $j('#multi_select_room:checked').each(function () {
            if ($j(this).parents('.room_view_box').hasClass('checked_in') == true || $j(this).parents('.room_view_box').hasClass('reserved') == true) {
                isOccupied = true;
            }
        });

        if (isOccupied) {
            $j('#multi_select_room:checked').each(function () {
                $j('#room_view_multi_context_' + $j(this).val() + ' .check_in_option').hide();
                $j('#room_view_multi_context_' + $j(this).val() + ' .reservation_option').hide();
            });
        } else {
            $j('#multi_select_room:checked').each(function () {
                $j('#room_view_multi_context_' + $j(this).val() + ' .check_in_option').show();
                $j('#room_view_multi_context_' + $j(this).val() + ' .reservation_option').show();
            });
        }
    },

    clearMultipleSelection: function () {
        $j('#multi_select_room:checked').each(function () {
            $j(this).parents('.room_view_box').attr('context-menu-id', 'room_view_context_' + $j(this).val());
            $j(this).attr('checked', false);
        });

        DASHBOARD.bindContextMenu();
    },

    updateLeftPanelWithDelay: function () {
        setTimeout('DASHBOARD.updateLeftPanel()', 3000);
    },

    updateLeftPanel: function () {
        $j('#front_desk_left_column').load('front_desk/left_panel', function () {
            $j('#menu1 > li > a.expanded + ul').slideToggle('medium');
            DASHBOARD.updateStatusCount('.item_view_block');
        });
    }
}

function reloadTaskCounts() {
    $j('.rm_view_notification').hide();
    $j('.rm_view_notification a').each(function () {
        if (parseInt($j(this).html()) > 0) {
            $j(this).parent().fadeIn(2000);
        }
    })
}

$j('.create_new_folio').live('click', function (e) {
    e.stopPropagation();
    var url = $j(this).data('url');
    $j('#create_new_folio_lb').dialog({
        width: 750,
        modal: true
    });
    $j('#create_new_folio_lb').find('.folio_flash_messages').html('');
    DASHBOARD.shadeWithLoading($j('#create_new_folio_lb_container'));
    $.ajax({
        url: url,
        dataType: 'script',
        type: 'GET'
    });
    return false;
});

$j('.move_folio_context').live('click', function (e) {
    e.stopPropagation();
    var url = $j(this).data('url');
    $j('#move_folio_confirm_lb').dialog({
        width: 565,
        modal: true
    });
    $j('#move_folio_confirm_lb').find('.folio_flash_messages').html('');
    DASHBOARD.shadeWithLoading($j('#move_folio_confirm_lb_container'));
    $.ajax({
        url: url,
        dataType: 'script',
        type: 'GET'
    });
    return false;
});

$j('.add_payment').live('click', function (e) {
    e.stopPropagation();
    var url = $j(this).data('url');
    $j('#add_payment_lb').dialog({
        width: 750,
        modal: true,
        position: ['middle', 20]
    });

    DASHBOARD.shadeWithLoading($j('#add_payment_lb_container'));
    $.ajax({
        url: url,
        dataType: 'script',
        type: 'POST'
    });
    return false;
});

$j('#v_tabs li a:not(#payment_view)').live('click', function (e) {
    $j("#report_dashboard").slideDown('slow');
    $j("#invoice_dashboard").slideUp('slow');
});

$j('#v_tabs li a#payment_view').live('click', function (e) {
    $j("#report_dashboard").slideUp('slow');
    $j("#invoice_dashboard").slideDown('slow');
});

$j('#submit_future_inventory').live('click', function (e) {
    $j('#quick_view_future_lb').dialog({
        width: 750,
        modal: true
    });
    return false;
});

function showEmailModalbox(email_body_id) {
    $("#" + email_body_id).dialog({
        width: 650,
        modal: true
    });
}

