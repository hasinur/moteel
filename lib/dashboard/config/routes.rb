Moteel::Application.routes.draw do

  namespace :service do |serve|
    resources :hotels, :as => 'entity' do
      collection do
        get 'available_rooms'
        get 'managers'
        get 'users'

      end
    end
    resources :reservations do
      collection do
        get 'create'
        get 'show'
        get 'delete'
      end
    end
    resources :checkins, :controller => 'checkin' do
      member do
        post 'check_out'
        get 'add_service_charge'
        get 'add_service_payment'
      end
    end
  end
  resources :hotel_reviews, :as => 'entity_reviews'
  resources :hotel_modules, :as => 'entity_modules'

  resources :hotel_payment_types, :as => 'entity_payment_types' do
    member do
      put 'change_status'
    end
  end

  resources :hotel_discounts, :path => 'entity_discounts' do
    collection do
      post 'delete_multiple'
    end
  end
  resources :hotel_reviews, :as => 'entity_reviews'
  resources :hotels, :as => 'entity' do
    member do
      put 'change_status'
      put 'change_featured_status'
      delete 'delete_featured'
      get 'location_map'
      get 'amenities'
      get 'rooms'
      get 'policies'
      get 'description'
      get 'photos'
      get 'videos'
      get 'inquiry'
      get 'reviews'
      get 'contact'
      get 'invalidate_cache'
      get 'custom_action'
    end
    collection do
      get 'search'
      get 'country_changed'
      post 'send_feedback'
      post 'delete_image'
      get 'redirect_provider'
      get 'api_hotel_rooms'
      post 'delete_logo'
      get 'show_api'
      get 'assign_template'
      get 'assign_theme'
      post 'assign_theme'
      get 'render_google_map'
      get 'list'
      get 'featured_requests'
      get 'make_featured'
      post 'make_featured'
      get 'featured'
      post 'do_featured'
      post 'auto_complete_for_hotel'
    end
  end

  resources :rooms, :as => 'items' do
    collection do
      get 'category_changed'
      get 'info_for_reserve'
      get 'booking_local'
      get 'amenities'
      get 'available_particular_room'
      post 'delete_image'
      post 'add_service'
      post 'delete_multiple'
      post 'switch_branch'
      post 'auto_complete_results'
    end
    member do
      put 'change_status'
    end
  end
  resources :room_types, :as => 'item_types' do
    collection do
      get 'category_changed'
      get 'detail_info'
      get 'info_for_reserve'
      post 'delete_image'
      get 'amenities'
    end
    member do
      put 'change_status'
    end
  end
  namespace :user_panel do |user_panel|
    resources :hotels, :as => 'entity', :only => [:index] do
      collection do
        post 'make_featured'
        get 'check_in'
        get 'check_out'
        get 'clicked_bill'
      end
    end
    resources :hotel_assignments, :as => 'entity_assignments', :only => [:create]
    resources :recharge_requests, :only => [:index]
    resources :room_types, :as => 'item_types', :only => [:index]
    resources :rooms, :as => 'items', :only => [:index]
    resources :finances, :only => [:index] do
      collection do
        get 'view_balance'
        get 'billing_statement'
      end
    end

    resources :feedbacks, :only => [:index] do
      collection do
        get 'featured_request'
        get 'reply_feedback'
        get 'delete_feedback'
      end
    end
  end
  #resources :users
  get 'my/entity_reports' => 'users#moteel_reports_users', :as => 'moteel_reports_users'
  get 'assign_entity' => 'users#assign_hotel_users', :as => 'assign_hotel_users'

  get 'my/entity_list' => 'users#hotel_list_users', :as => 'hotel_list_users'
  get 'my/item_type_list' => 'users#room_type_list_users', :as => 'room_type_list_users'
  get 'my/item_list' => 'users#room_list_users', :as => 'room_list_users'

  get 'entity_visit_detail/:id' => 'users#hotel_visit_detail', :as => 'hotel_visit_detail'

  get 'dashboard' => 'dashboard#home', :as => 'dashboard_home'

  get 'housekeeping' => 'housekeeping#home', :as => 'housekeeping'
  get 'housekeeping_print' => 'housekeeping#home_print', :as => 'housekeeping_print'
  post 'update_housekeeping_status' => 'housekeeping#update_housekeeping_status'
  post 'assign_employee_housekeeping' => 'housekeeping#assign_employee'
  post 'remove_employee_housekeeping' => 'housekeeping#remove_employee'

  get 'room_service' => 'room_service#home', :as => 'room_service'
  post 'assign_employee_room_service' => 'room_service#assign_employee'
  post 'update_room_service_status' => 'room_service#update_status'

  get 'reports_base' => 'reports_base#home', :as => 'reports_base'
  get 'all_branch_reports' => 'all_branch_reports#home', :as => 'all_branch_reports'
  get 'room_occupancy_report' => 'reports_base#room_occupancy_report', :as => 'room_occupancy_report'
  get 'sales_by_services_report' => 'reports_base#sales_by_services_report', :as => 'sales_by_services_report'

  get 'cashier_desk' => 'cashier_desk#home'
  #For Report
  get 'inventory_transaction_list_report' => 'housekeeping/housekeeping_reports#inventory_transaction_list', :as => 'inventory_transaction_list_report'
  post 'inventory_transaction_list_report' => 'housekeeping/housekeeping_reports#inventory_transaction_list', :as => 'inventory_transaction_list_report'
  get 'fixed_asset_transactions_report' => 'housekeeping/housekeeping_reports#fixed_asset_allocation_history', :as => 'fixed_asset_transactions_report'
  post 'email_fixed_asset_allocation_pdf' => 'housekeeping/housekeeping_reports#email_fixed_asset_allocation_pdf', :as => 'email_fixed_asset_allocation_pdf'
  get 'service_wise_revenue_report' => 'housekeeping/housekeeping_reports#service_wise_revenue', :as => 'service_wise_revenue_report'
  post 'email_service_wise_revenue_pdf' => 'housekeeping/housekeeping_reports#email_service_wise_revenue_pdf', :as => 'email_service_wise_revenue_pdf'
  get 'payment_type_wise_transactions_report' => 'housekeeping/housekeeping_reports#payment_type_wise_transactions', :as => 'payment_type_wise_transactions_report'
  post 'payment_type_wise_transactions_report' => 'housekeeping/housekeeping_reports#payment_type_wise_transactions', :as => 'payment_type_wise_transactions_report'
  get 'deposit_listing_report' => 'housekeeping/housekeeping_reports#deposit_listing', :as => 'deposit_listing_report'
  post 'email_deposit_listing_pdf' => 'housekeeping/housekeeping_reports#email_deposit_listing_pdf', :as => 'email_deposit_listing_pdf'
  get 'financial_statement_report' => 'housekeeping/housekeeping_reports#financial_statement', :as => 'financial_statement_report'
  post 'financial_statement_report' => 'housekeeping/housekeeping_reports#financial_statement', :as => 'financial_statement_report'
  get 'billing_information_report' => 'housekeeping/housekeeping_reports#billing_information', :as => 'billing_information_report'
  post 'email_billing_information_pdf' => 'housekeeping/housekeeping_reports#email_billing_information_pdf', :as => 'email_billing_information_pdf'
  get 'cashier_statement_report' => 'housekeeping/housekeeping_reports#cashier_statement', :as => 'cashier_statement_report'
  post 'email_cashier_statement_pdf' => 'housekeeping/housekeeping_reports#email_cashier_statement_pdf', :as => 'email_cashier_statement_pdf'
  get 'revenue_by_counter_report' => 'housekeeping/housekeeping_reports#revenue_by_counter', :as => 'revenue_by_counter_report'
  post 'email_revenue_by_counter_pdf' => 'housekeeping/housekeeping_reports#email_revenue_by_counter_pdf', :as => 'email_revenue_by_counter_pdf'

  #End Report
  namespace :reporting_service do |service|
    resources :items do
      member do
        post 'create'
        get 'new'
      end
    end
  end
  get 'front_desk' => 'reporting_service/items#availability_list', :as => 'front_desk'
  get 'stay_view' => 'reporting_service/items#stay_view', :as => 'stay_view'
  get 'quick_view' => 'reporting_service/items#quick_view', :as => 'quick_view'
  get 'quick_view_future_inventory' => 'reporting_service/items#quick_view_future_inventory', :as => 'quick_view_future_inventory'
  get 'quick_view_future_inventory_more' => 'reporting_service/items#quick_view_future_inventory_more', :as => 'quick_view_future_inventory_more'
  #reporting
  get 'report_checkin' => 'reporting_service/reports#report_checkin', :as => 'report_checkin'
  get 'report_available' => 'reporting_service/reports#report_available', :as => 'report_available'
  get 'report_maintenance' => 'reporting_service/reports#report_maintenance', :as => 'report_maintenance'
  get 'report_pending_checkout' => 'reporting_service/reports#report_pending_checkout', :as => 'report_pending_checkout'

end