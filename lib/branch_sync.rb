require 'database_switcher'

class Moteel::BranchSync
  include Moteel::DatabaseSwitcher
  PROTECTED_ATTRS = [:id, :type]

  def initialize(hotel, current_user)
    @hotel = hotel
    @current_user = current_user
  end

  def sync
    unless LOCAL_SERVER
      users = @hotel.assigned_users.all
      users << @current_user
      room_types = @hotel.room_types.with_translations.all
      rooms = @hotel.rooms.all

      switch_to_hotel_database(@hotel)

      ActiveRecord::Base.transaction do
        sync_hotel
        sync_users(users)
        sync_entities(room_types)
        sync_entities(rooms)
      end
    end
  end

  def sync_users(users)
    users.each do |user|
      sync_entity(user)
      sync_entity(user.profile)
    end
  end

  def sync_hotel
    sync_entity(@hotel)
  end

  private
  def sync_entities(entity)
    entities = entity.kind_of?(Array) ? entity : [entity]
    entities.each do |entity|
      sync_entity(entity)
    end
  end

  def sync_entity(entity)
    klass = entity.class
    e = klass.where(id: entity.id).count > 0 ? klass.find(entity.id) : klass.new
    protected_attrs_with_val = entity.attributes.select { |attr, val| PROTECTED_ATTRS.include? attr.to_sym }

    locale = I18n.locale
    [:en, :ar].each do |l|
      I18n.locale = l
      e.attributes = entity.attributes.select { |attr, value| !PROTECTED_ATTRS.include?(attr.to_sym) }
    end
    I18n.locale = locale

    protected_attrs_with_val.each do |meth, val|
      e.send "#{meth}=".to_sym, val
    end
    e.save!(:validate => false)
  end
end