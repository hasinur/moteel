module MoteelUtils
  include MobileUtils


  def assigned_hotel_id_arr
    return [0] unless current_user
    current_user.hotel_assignments.collect { |c| c.hotel_id }
  end

  def manageable_hotel_ids
    return [0] unless current_user
      assigned_hotel_id_arr + current_user.hotels.collect(&:id)
  end

  def accessible_hotel_ids(role_name=nil)
    return [0] unless current_user
    if hotel_agent?
      hotel_ids = manageable_hotel_ids
      hotel_ids.empty? ? hotel_ids << 0 : hotel_ids
    else
      hotels_with_access = []
      if current_user.present?
        role_to_check = Role.find_by_name(role_name.to_s)

        if role_to_check.present?
          hotels_with_access = current_user.assignments.find(:all, :conditions => {:role_id => role_to_check.id}).collect { |assignment| assignment.hotel_id }.uniq
        end
      end

      assigned_hotels_with_access = current_user.assigned_hotels.map(&:id)
      hotels_with_access.delete_if { |hotel_id| !assigned_hotels_with_access.include?(hotel_id) }
      hotels_with_access.empty? ? hotels_with_access << 0 : hotels_with_access
    end
  end

  def assigned_hotels_filter
    condition = " true "
    condition += " AND hotels.user_id = #{agent_user_id}" if hotel_agent?

    if agent_user?
      assigned_hotels = current_user.hotel_assignments.collect(&:hotel_id)
      assigned_hotels << 0
      condition += " AND hotels.id IN(#{assigned_hotels.join(',')})"
    end
    condition
  end

  def plan_type_id_from_title(title='')
    title = 'free' unless title.present?
    plan_type_hash[title]
  end

  def plan_type_hash
    {'free' => 0, 'basic' => 1, 'silver' => 2, 'gold' => 3, 'platinum' => 4}
  end

  def user_plan_type
    return '' unless hotel_agent? || agent_user?
    hotel_agent? ? (current_user.plan_type ? current_user.plan_type.title : 'platinum') : (current_user.hotel_agent.plan_type ? current_user.hotel_agent.plan_type.title : 'platinum')
  end

  def paid_hotel?(hotel)
    hotel.payment_type > 0 #(hotel.payment_type == 1 && (User.free_plan_duration(hotel.user_id) || Balance.user_balance(hotel.user_id) )) || (hotel.payment_type == 2 && hotel.valid_till >= Time.now)  for cron job
  end

  def permission_denied
    #flash[:error] = t('notifications.lack_of_permission')
    #redirect_to root_url
  end

  def plan_type_by_user_id(user_id)
    unless user_id.blank?
      user = User.find(user_id) rescue nil
      user.plan_type.title if user and user.plan_type
    end
  end
end