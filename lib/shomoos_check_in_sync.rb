module ShomoosCheckInSync
  module ClassMethods
    def sync_with_shomoos_check_in(sync_wrapper_model)
      self.send :attr_accessor, :just_created
      self.before_create do
        self.just_created = true
      end

      self.after_save do
        self.delay.sync_with_shomoos(sync_wrapper_model) if self.hotel.shomoos_enabled?
        #self.sync_with_shomoos(sync_wrapper_model)
      end
    end
  end

  def sync_with_shomoos(sync_wrapper_model)
    #logger = Delayed::Worker.logger
    begin
      check_in_wrapper = self.send(sync_wrapper_model)
      unless self.hotel.shomoos_enabled? #only do when shomoos is enabled
        logger.debug "Shomoos is not enabled"
        return
      end

      unless check_in_wrapper.respond_to? :to_hash
        logger.error "Shomoos checkin sync :: #{check_in_wrapper.class} does not have method to_hash"
        return
      end
      check_in = check_in_wrapper.check_in
      if (check_in.checked_in? || check_in.checked_out?) && check_in.changed?
        check_in_hash = check_in_wrapper.to_hash
        logger.debug "check_in_hash :: #{check_in_hash.inspect}"

        logger.debug "ShomoosBase.base_url :: #{ShomoosBase.config.inspect}"

        client = Savon.client("#{ShomoosBase.base_url}/shomoosSDKserver/AccomodationService.svc?wsdl")
        #logger.debug "client.wsdl.soap_actions :: #{client.wsdl.soap_actions}"

        response = nil
        if check_in.checked_in?
          logger.info "Sending shomoos checkIn request :: #{check_in.inspect}"
          shomoos_action = (self.id_changed? || check_in.status_changed?) ? :check_in : :update_check_in
          if shomoos_action == :check_in # only support check in for now
            response = client.request(shomoos_action) do
              soap.namespaces["xmlns:tem"] = "http://tempuri.org/"
              soap.namespaces["xmlns:shom"] = "http://schemas.datacontract.org/2004/07/ShomoosSDK.Common"
              soap.namespaces["xmlns:shom1"] = "http://schemas.datacontract.org/2004/07/ShomoosSDK.Accomodation"
              soap.namespaces["xmlns:arr"] = "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
              soap.endpoint = "#{ShomoosBase.base_url}/shomoosSDKserver/AccomodationService.svc/inout?languageID=1"
              soap.body = {'tem:record' => check_in_hash, 'tem:language' => 1}
            end

            begin
              record_id = response.to_hash[:check_in_response][:check_in_result][:record][:record_id]
              logger.info "record_id :: #{record_id}"
              check_in.update_column(:shomoos_inout_id, record_id)
            rescue
            end
          end
        elsif check_in.checked_out? and check_in.shomoos_inout_id.present?
          logger.info "Sending shomoos checkIn update request :: #{check_in.inspect}"
          response = client.request(:check_out) do
            soap.namespaces["xmlns:tem"] = "http://tempuri.org/"
            soap.endpoint = "#{ShomoosBase.base_url}/shomoosSDKserver/AccomodationService.svc/inout?languageID=1"
            soap.body = {'tem:username' => check_in_wrapper.user_name,
                         'tem:password' => check_in_wrapper.password,
                         'tem:SPSCID' => check_in_wrapper.spsc_id,
                         'tem:InOutID' => check_in.shomoos_inout_id,
                         'tem:paidAmount' => check_in.total_paid,
                         'tem:language' => 1
            }
          end
        end

        ap response.to_hash
        logger.debug "response :: #{response.to_hash.inspect}"
      end
    rescue
      logger.error "#{$!.inspect}"
    end

  end

  def self.included(receiver)
    receiver.extend ClassMethods
  end
end

ActiveRecord::Base.send :include, ShomoosCheckInSync