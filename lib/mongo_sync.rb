class Moteel::MongoSync
  def initialize(hotel)
    @hotel = hotel
  end

  def has_changes?
    if @hotel.present? and @hotel.has_local_database? and
        (!LOCAL_SERVER and !@hotel.synced_mongodb_in_server?) or (LOCAL_SERVER and !@hotel.synced_mongodb_in_local?)
      return true
    end
    false
  end

  def sync
    if self.has_changes?
      Rails.logger.debug "Syncing to mongo *"
      sync_models([CheckIn, AllocatedRoom, AllocatedRoomGuest, DamagedRoom, LockedRoom, Room, Task, GuestRecommendation])
      #TODO: need AllocatedRoomGuest, GuestRecommendation to hotel association
      Rails.logger.debug "* Mongo synced"
    end
  end

  def notify_server_to_update_mongo
    sync_attr = LOCAL_SERVER ? :synced_mongodb_in_server : :synced_mongodb_in_local # to let the server know that some thing has been changed that is needed to sync in server
    @hotel.update_column(sync_attr, false)
  end

  private
  def sync_models(model_classes)
    reda = ReportEntityDailyActivity.where(:entity_id => @hotel.id, :date => Date.today).first || ReportEntityDailyActivity.new(:entity_id => @hotel.id, :date => Date.today)
    sync_from = reda.last_synced_at

    model_classes.each do |model_class|
      sync_model(model_class, sync_from)
    end

    reda.last_synced_at = Time.now
    reda.save

    sync_attr = LOCAL_SERVER ? :synced_mongodb_in_local : :synced_mongodb_in_server
    @hotel.update_column(sync_attr, true)
  end

  def sync_model(model_class, sync_from)
    records = model_class.scoped
    records = records.where('? <= updated_at', sync_from) if sync_from
    obj = model_class.new

    hotel_attribute = nil
    hotel_attribute = :hotel_id if obj.has_attribute? :hotel_id
    hotel_attribute = :branch_id if obj.has_attribute? :branch_id

    records = records.where(hotel_attribute => @hotel.id) if hotel_attribute

    records.each do |record|
      begin
        record.save!
      rescue
        logger.error "Error syncing #{model_class.name.to_s} #{$!}"
      end
    end
  end

  def logger
    @@sync_logger ||= Logger.new("#{Rails.root}/log/sync.log")
  end
end