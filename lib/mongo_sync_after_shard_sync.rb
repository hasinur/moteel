require 'mongo_sync'

module Moteel::MongoSyncAfterShardSync
  def mongo_sync_after_shard_sync
    Moteel::MongoSync.new(current_branch).sync
    true
  end
end