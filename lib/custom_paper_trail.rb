module CustomPaperTrail
  module InstanceMethods
    def reference_changes(o_changes = {})
      return {} if o_changes.empty?
      r_changes = o_changes.select { |attr, change| /.+_id$/.match(attr).present? }

      r_changes.each do |attr, change|
        begin
          reference_name = (/(.+)_id$/.match attr)[1] #dont support polymorphic now, will work on that later
          reference_klass = self.association(reference_name.to_sym).klass
          old_ref = reference_klass.find(change[0]) rescue nil
          new_ref = reference_klass.find(change[1]) rescue nil
          r_changes[attr] = [self.class.readable_name_for_paper_trail(old_ref), self.class.readable_name_for_paper_trail(new_ref)]
        rescue
        end
      end
      o_changes.merge!(r_changes)
    end

    def version!(changes)
      version = AuditTrail.new(changes)
      version.item = self
      version.uid = RequestStore.store[:version_uid]
      begin
        version.object_description = self.audit_trail_object_description if self.respond_to?(:audit_trail_object_description)
      rescue Exception => ex
        logger.error ex.message
      end
      version.save
    end
  end

  module ActivityInstanceMethods
    def activity_log!(attrs={})
      unless RequestStore.store[:activity_log].present?
        activity_log = ActivityLog.new(attrs)
        activity_log.owner = RequestStore.store[:activist]
        activity_log.trackable = self
        activity_log.object_description = self.audit_trail_object_description if self.respond_to?(:audit_trail_object_description)
        activity_log.save
        RequestStore.store[:activity_log] = activity_log if activity_log.persisted?
      end
    end

    def process_activity_attrs(options)
      meta = options[:meta] || []
      name = options[:name]
      meta_attrs = Hash[meta.collect { |k, v| [k, v.is_a?(Symbol) ? self.send(v) : v.call(self)] }]
      {name: name}.merge(meta_attrs)
    end

    def store_activity_event_before_save
      RequestStore.store[:actual_event] ||= {}
      if RequestStore.store[:actual_event].keys.empty? || RequestStore.store[:actual_event].keys.include?(object_signature)
        RequestStore.store[:actual_event][object_signature] = created_or_updated
      end
    end

    def object_signature
      "#{self.class.name}##{object_id}"
    end

    def created_or_updated
      self.persisted? ? 'updated' : 'created'
    end
  end

  module ClassMethods
    def readable_name_for_paper_trail(obj)
      return nil if obj.nil?
      obj.respond_to?(:name) ? obj.name : obj.try(:title) rescue obj.id
    end

    def has_paper_trail(options = {})
      attr_accessor :audit_trail_custom_changes
      include CustomPaperTrail::InstanceMethods
      activity_track_options = options.delete(:activity_track)
      activity_track activity_track_options if activity_track_options.present?

      custom_papetrail_default_block = ->(obj, paper_trail_sync_event, &block) do
        meta = options[:meta] || []
        ignore = options[:ignore] || []
        ignore = (ignore + [:created_at, :updated_at, :id]).uniq
        fields_for_custom_values = options[:custom_values] || {}

        sanitized_changes = obj.changes.clone.merge(obj.audit_trail_custom_changes || {})
        sanitized_changes = sanitized_changes.delete_if { |attr, change| change[0] == change[1] || ignore.include?(attr.to_sym) }
        custom_values = {}
        fields_for_custom_values.each do |attr, changer|
          change = sanitized_changes[attr.to_sym]
          if change.is_a? Array
            changed_values =
                if changer.lambda?
                  [(changer.call(change[0]) if change[0].present?), (changer.call(change[1]) if  change[1].present?)]
                else
                  [(change[0].send(changer.to_sym) if change[0].present?), (change[1].send(changer.to_sym) if change[1].present?)]
                end
            custom_values[attr] = changed_values
          end
        end

        meta_attrs = Hash[meta.collect { |k, v| [k, v.is_a?(Symbol) ? obj.send(v) : v.call(obj)] }]

        sanitized_changes.merge! obj.reference_changes(sanitized_changes.clone)
        sanitized_changes.merge! custom_values
        if !sanitized_changes.empty? || paper_trail_sync_event.to_s == 'destroyed'
          attributes = block.call(sanitized_changes)
          obj.version!(attributes.merge(meta_attrs))
        end
      end


      self.after_save do
        paper_trail_sync_event = self.id_changed? ? 'created' : 'updated'
        custom_papetrail_default_block.call(self, paper_trail_sync_event) do |sanitized_changes|
          {event: paper_trail_sync_event, owner: RequestStore.store[:activist], object: self.attributes, object_changes: sanitized_changes.to_hash}
        end
      end

      self.before_destroy do
        paper_trail_sync_event = 'destroyed'
        custom_papetrail_default_block.call(self, paper_trail_sync_event) do
          {event: paper_trail_sync_event, owner: RequestStore.store[:activist], object: self.attributes}
        end
        true
      end

    end

    def paper_trail_patch(target, options = {})
      only_opts = options.delete(:only)
      meta = options[:target_meta] || []
      include CustomPaperTrail::InstanceMethods


      after_save do
        target = self.send(target.to_sym) rescue nil
        if target.present?
          sanitized_changes = self.changes.clone
          sanitized_changes = sanitized_changes.delete_if { |attr, change| change[0] == change[1] || !only_opts.include?(attr.to_sym) }

          audit_trail_custom_changes = self.reference_changes(sanitized_changes)
          paper_trail_sync_event = target.id_changed? ? 'created' : 'updated'
          meta_attrs = Hash[meta.collect { |k, v| [k, v.is_a?(Symbol) ? target.send(v) : v.call(target)] }]

          default_fields = {event: paper_trail_sync_event, owner: RequestStore.store[:activist], object: target.attributes, object_changes: audit_trail_custom_changes.to_hash}
          target.version!(default_fields.merge(meta_attrs))
        end
      end
    end

    def activity_track(options = {})

      include CustomPaperTrail::ActivityInstanceMethods
      attr_accessor :activity_event

      before_save do
        store_activity_event_before_save
      end

      after_save do
        if  RequestStore.store[:actual_event][object_signature].present?
          attrs = {event: RequestStore.store[:actual_event][object_signature]}.merge(process_activity_attrs(options))
          activity_log!(attrs)
        end
      end

      before_destroy do
        attrs = {event: 'destroyed'}.merge(process_activity_attrs(options))
        activity_log!(attrs)
      end
    end

  end

  def self.included(receiver)
    receiver.extend ClassMethods
  end

end

ActiveSupport.on_load(:active_record) do
  include CustomPaperTrail
end