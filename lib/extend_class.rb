module CustomUtils
  module DateTimeExtension
    def calendarize
      self.strftime("%Y-%m-%d %I:%M %p")
    end

    def calendarize_with_day
      self.strftime("%Y-%m-%d %A %I:%M %p")
    end

    def checked_in_between?(from_date, to_date)
      self.to_date >= from_date.to_date and self.to_date < to_date.to_date
    end

    def overlaps?(other, extra = {})
      start_date_field = extra[:start_date_field] || :start_date
      end_date_field = extra[:end_date_field] || :end_date
      (self.send(start_date_field) - other.send(end_date_field)) * (other.send(start_date_field) - self.send(end_date_field)) >= 0
    end

    def start_of(hotel)
      self.change(:hour => hotel.starting_hour, :min => hotel.starting_minute, :sec => 0)
    end

    def end_of(hotel)
      self.change(:hour => hotel.ending_hour, :min => hotel.ending_minute, :sec => 0)
    end

  end
end

class DateTime
  include CustomUtils::DateTimeExtension
end

class Time
  include CustomUtils::DateTimeExtension
end

class Date
  def calendarize
    self.strftime("%Y-%m-%d")
  end

  def checked_in_between?(from_date, to_date)
    self >= from_date.to_date and self < to_date.to_date
  end

  def overlaps?(other, extra = {})
    start_date_field = extra[:start_date_field] || :start_date
    end_date_field = extra[:end_date_field] || :end_date
    (self.send(start_date_field) - other.send(end_date_field)) * (other.send(start_date_field) - self.send(end_date_field)) >= 0
  end
end

class NilClass
  def calendarize
    ""
  end
end

class String
  def calendarize
    self
  end

  def zerofy
    self.to_i < 10 ? "0#{self}" : self
  end

  def dehumanize
    ActiveSupport::Inflector.dehumanize(self)
  end

  def underscorize
    gsub(/\W/, '').underscore
  end
end

class Number
  def zerofy
    self < 10 ? "0#{self}" : self
  end
end

class Array
  def swap!(a, b)
    self[a], self[b] = self[b], self[a]
    self
  end

  def add_condition! (condition, conjunction = 'AND')
    if String === condition
      add_condition!([condition])
    elsif Hash === condition
      add_condition!([condition.keys.map { |attr| "#{attr}=?" }.join(' AND ')] + condition.values)
    elsif Array === condition
      self[0] = "(#{self[0]}) #{conjunction} (#{condition.shift})" unless empty?
      (self << condition).flatten!

    else
      raise "don't know how to handle this condition type"
    end
    self
  end
end

module ActiveSupport::Inflector
  # does the opposite of humanize ... mostly.
  # Basically does a space-substituting .underscore
  def dehumanize(the_string)
    result = the_string.to_s.dup
    result.downcase.gsub(/ +/, '_')
  end
end

module Globalize3Extension
  def available_locales_hash(field)
    if self.respond_to?(:translated_locales) && self.translated_attribute_names.include?(field.to_sym)
      self.translated_locales.collect { |locale| {locale => self.read_attribute(field.to_sym, :locale => locale.to_sym)} }.reduce(Hash.new, :merge)
    else
      {field.to_sym => self.send(field.to_sym)}
    end
  end
end

ActiveRecord::Base.send :include, Globalize3Extension

#Following block for resolve error::wrong argument type JSON::Pure::Generator::State (expected Data)
class Fixnum
  def to_json(options = nil)
    to_s
  end
end