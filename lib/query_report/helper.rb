require 'query_report/report'

module QueryReport
  module Helper
    def reporter(query, render_template, options={}, &block)
      @report ||= QueryReport::Report.new(params, view_context, options)
      @report.query = query
      @report.instance_eval &block

      if (params[:send_as_email].to_i > 0)
        send_pdf_email(params[:email_to], params[:subject], params[:message], action_name, pdf_for_report)
      end

      respond_to do |format|
        format.js { render "query_report/#{render_template}" }
        format.html { render 'query_report/list' }
        format.json { render json: @report.records }
        format.csv { send_data generate_csv_for_report(@report.all_records), :disposition => "attachment;" }
        format.pdf {
          send_data pdf_for_report
        }
      end
    end

    def generate_csv_for_report(records)
      if records.size > 0
        columns = records.first.keys
        CSV.generate do |csv|
          csv << columns
          records.each do |record|
            csv << record.values.collect { |val| val.kind_of?(String) ? view_context.strip_links(val) : val }
          end
        end
      else
        nil
      end
    end

    def pdf_for_report
      report_name = I18n.t("views.reports.#{action_name}")
      @pdf_data_obj ||= BasicReportPdf.new(@report, report_name).standard(current_branch: current_branch).render
    end

    def send_pdf_email(email, subject, message, file_name, attachment)
      @user = current_user
      to = email.split(',')
      UserMailer.report_mailer(@user, to, subject, message, file_name, attachment).deliver
    end
  end
end