require 'test_helper'
require 'rails/performance_test_help'

class FrontDeskTest < ActionDispatch::PerformanceTest
  PASSWORD = '123456'
  # Refer to the documentation for all available options
  #self.profile_options = { :runs => 5, :metrics => [:wall_time, :memory]
  #                          :output => 'tmp/performance', :formats => [:flat] }
  self.profile_options = {:runs => 2}

  def test_front_desk
    user = create_user
    post 'users/sign_in', :user => { :email => user.email, password: PASSWORD }
    get '/front_desk'
    #get '/'
  end

  private
  def login_with(user)

  end

  def create_user
    @user = FactoryGirl.create(:hotel_owner, {password: PASSWORD, password_confirmation: PASSWORD})
    #db_number = Random.rand(5) + 1
    #@hotel = FactoryGirl.create(:hotel, user: @user, database_name: "moteel_production#{db_number}", database_username: 'root')
    @hotel = FactoryGirl.create(:hotel, user: @user, database_name: "moteel_production", database_username: 'root')

    pg = FactoryGirl.create(:permission_group, hotel: @hotel)
    @user.assignments.create(permission_group: pg, hotel_id: @hotel.id)
    pg.roles << Role.where(name: permission.underscorize).first
    @user.hotel_assignments.create(hotel: @hotel)
    @user.current_branch_id = @hotel.id
    @user.update_assigned_roles
    @user
  end
end
